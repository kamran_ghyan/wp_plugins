
<style>
    .slick-prev:before, .slick-next:before {
        color: #fff !important;
        font-size: 30px !important;
    }
</style>
<?php
wp_enqueue_style('property-detail2', i5PBIntegration__PLUGIN_URL . 'css/property-detail2.css', array(), '1.1', 'all');
wp_enqueue_style('property-search-header', i5PBIntegration__PLUGIN_URL . 'css/search-header2.css', array(), '1.1', 'all');
wp_enqueue_style('slick-styles', i5PBIntegration__PLUGIN_URL . 'css/slick.css', array(), '1.1', 'all');
wp_enqueue_style('slick-theme', i5PBIntegration__PLUGIN_URL . 'css/slick-theme.css', array(), '1.1', 'all');
wp_enqueue_script('slick', i5PBIntegration__PLUGIN_URL . 'js/slick.min.js', array('jquery'), '1.1', true);
wp_enqueue_script('PrintElement', i5PBIntegration__PLUGIN_URL . 'js/PrintElement.js', array('jquery'), '1.1', true);
/*
Template Name: Property Detail 2h
 */
$showSave = false;
get_header();
include i5PBIntegration__PLUGIN_DIR . 'Includes/Prospect-Functions.php';
include i5PBIntegration__PLUGIN_DIR . 'Includes/PropertyDetail-Functions.php';
include i5PBIntegration__PLUGIN_DIR . 'Includes/PropertySearch-Functions.php';
include i5PBIntegration__PLUGIN_DIR . "Includes/HeaderSearch.php";
include i5PBIntegration__PLUGIN_DIR . 'Includes/Global-Functions.php';
?>
<div class="printContainer">
    <div class="detailsHeader">
        <div class="container">
            <div class="buttonBack printHide">
                <a href="/map">
                    <i class="fa fa-angle-left"></i> Return to Full Map
                </a>
            </div>
            <div class="detailsAddress div2-3">
                <h1 itemprop="address" itemtype="http://schema.org/PostalAddress">
                    <?php echo $property->address . ($property->unitNumber == "" ? "" : " " . $property->unitNumber) ?>
                </h1>
                <div class="address2">
                    <?php echo $property->city ?>, <?php echo $property->state ?> <?php echo $property->zipCode ?>
                </div>
                <div class="addressDetails">
                    <div class="addressListingStatus">
                        <div class="new">
                            <?php echo $property->status ?> - <?php echo $property->custom4 ?>
                        </div>
                    </div>
                    <div class="addressPrice">
                        PRICE:
                        <span class="success">
                            <?php echo "$" . number_format($property->price, 0) . ($result->propertytype == "Rental" || $result->recordType == "Rent" ? " /Month" : "") ?>
                        </span>
                    </div>
                    <div class="addressStatus">
                        STATUS:
                        <b>
                            <?php echo $property->status ?>
                        </b>
                    </div>
                    <div class="addressUpdated">
                        UPDATED:
                        <b>
                            <?php
$settings = get_option('WebListing_Settings');
$lastRun = strtotime(date("Y-m-d\Th:i:s"));
$currTime = $lastRun;

if (isset($settings["LastRun"])) {
	$lastRun = strtotime($settings["LastRun"]);
}

echo round(abs($currTime - $lastRun) / 60, 0) . " MIN AGO";
?>
                        </b>
                    </div>
                    <div class="addressID">
                        ID#:
                        <b>
                            <?php echo $property->mlsid ?>
                        </b>
                    </div>

                </div>
            </div>
            <!-- end detailsAddress -->
            <div class="div1-3 printHide">
                <!--<div id="fav<?php echo $property->mlsid ?>" class="<?php echo ($favs == '' || !in_array($property->mlsid, $favs) ? 'detailsFavorite' : 'detailsFavoriteSelected') ?>">
                    <a href="javascript:toggleFav('<?php echo $property->mlsid ?>');setFavorite('<?php echo $property->mlsid ?>',<?php echo ($favs == '' || !in_array($property->mlsid, $favs) ? '1' : '0') ?>);">
                        <i class="fa fa-heart"></i>
                    </a>
                </div>-->
                <!--<div class="detailsShowing">
                    <a class="button" href="#showingForm">Request Showing</a>
                </div>-->
                <div class="detailsButtons">
                    <!--<div class="btnShowMap">
                        <a href="#map" id="btnMap"><i class="fa fa-map-marker"></i> View on Map</a>
                    </div>

                    <div onclick="printListing();" class="btnPrint">Print</div>
                        -->
                    <div onclick="showSharing()" class="btnShare dropbtn"><i class="fa fa-user"></i> My Account</div>

                    <div id="ddShare" class="ddShare dropdownContent">
                        <div class="padding10">
                            <ul>
															<li><a href="/favorites"><i class="fa fa-heart"></i> My Favorites</a></li>
                              <li><a href="<?php echo wp_logout_url(get_permalink()); ?>"><i class="fa fa-sign-out"></i> Logout</a></li>
														</ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end contain -->
    </div>
    <!-- end detailsHeader -->
    <div class="detailsBody">
        <div class="container">
            <div class="div2-3">
                <div class="propertyImages">
                    <?php
$fpUrl = '';
if (sizeof($images) == 0) {
	echo " <div><img src='" . i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png' /></div>";
} else {
	$index = 0;

	foreach ($images as $image) {
		if (strrpos($image->url, ".pdf") === false) {
			if ($index == 0) {
				echo " <div><img onerror='fixImage(this);' src='" . $image->url . "' /></div>";
			} else {
				echo " <div><img style='display:none;' onerror='fixImage(this);' src='" . $image->url . "' /></div>";
			}

			$index += 1;
		} else {
			$fpUrl = $image->url;
		}

	}
}
?>
                </div>
            </div>
            <!-- end div 2-3 -->
            <div class="div1-3">
                <div class="detailsBox" id="detailsBox">
                    <div class="detailsPrice" style="font-size:20px;">
                      <?php echo (isset($property->status) && $property->status != "" ? $property->status : "&nbsp;") ?>
		                  <?php echo (isset($property->custom4) && $property->custom4 != "" ? " - " . $property->custom4 : "") ?>
                    </div>
                    <div class="detailsPrice" style="font-size:20px;">
                      <?php echo (isset($property->address) && $property->address != "" ? $property->address : "&nbsp;") ?> - Lot <?php echo (isset($property->custom3) && $property->custom3 != "" ? $property->custom3 : "&nbsp;") ?>
                    </div>
                    <div class="detailsPrice">
                      	<?php if ($property->price == 0): ?>
                      	<?php echo $property->agentFirstName . " " . $property->agentLastName . " " . $property->agentOfficePhone; ?>
                      	<?php else: ?>
                        <?php echo "$" . number_format($property->price, 0) . ($property->propertytype == "Rental" ? " /Month" : "") ?>
                         <?php endif;?>
                    </div>
                    <div class="detailsPrice" style="font-size:20px;">
                      <?php echo (isset($property->custom2) && $property->custom2 != "" ? $property->custom2 : "&nbsp;") ?>
                    </div>
                    <?php if (isset($property->custom5) && $property->custom5 != ""): ?>
                        <div class="detailsPrice" style="font-size:20px;">
                          <?php echo 'Creation Date: ' . $property->custom5; ?>
                        </div>
                    <?php endif;?>
                    <div class="detailsBeds">
                        <div class="detailsValue">
                            <?php echo number_format($property->bedrooms, 0) ?>
                        </div>
                        <div class="detailsLabel">
                            Beds
                        </div>
                    </div>
                    <div class="detailsAcres">
                        <div class="detailsValue">

                          	<?php echo number_format($property->fullBaths, 0) ?>
                        </div>
                        <div class="detailsLabel">

                            Baths
                        </div>
                    </div>
                    <div class="detailsSqft">
                        <div class="detailsValue">
                            <?php echo number_format($property->lotsize, 2) ?>
                        </div>
                        <div class="detailsLabel">
                            Home SQFT
                        </div>
                    </div>
                    <div class="details1-2Baths">
                        <div class="detailsValue">
                            <?php echo number_format($property->size, 0) ?>
                        </div>
                        <div class="detailsLabel">
                            Lot SQFT
                        </div>
                    </div>


                    <!--<div class="detailsSpecs">
                        <?php if (isset($property->subdivision) && $property->subdivision != ""): ?>
                        <div class="specsLabel">
                            Neighborhood:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->subdivision) && $property->subdivision != "" ? $property->subdivision : "&nbsp;") ?>
                        </div>
                        <?php endif;?>
                        <div class="specsLabel">
                            Type:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->propertytype) && $property->propertytype != "" ? $property->propertytype : "&nbsp;") ?>
                        </div>
                        <div class="specsLabel">
                            Built:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->yearbuilt) && $property->yearbuilt != "" ? $property->yearbuilt : "&nbsp;") ?>
                        </div>
                        <div class="specsLabel">
                            Area:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->area) && $property->area != "" ? $property->area : "&nbsp;") ?>
                        </div>
                    </div>-->
                </div>
                <div class="detailsBoxFooter">
                  <div id="fav<?php echo $property->mlsid ?>" class="<?php echo ($favs == '' || !in_array($property->mlsid, $favs) ? 'detailsFavorite' : 'btnSaveToFavoritesSelected') ?>">
                    <a href="javascript:toggleFav('<?php echo $property->mlsid ?>');setFavorite('<?php echo $property->mlsid ?>',<?php echo ($favs == '' || !in_array($property->mlsid, $favs) ? '1' : '0') ?>);">
                        <button class="btnSaveToFavorites"><i class="fa fa-heart"></i> Save to Favorites</button>
                    </a>
                    <a href="#showingForm"><button class="btnSaveToFavorites">Request Information</button></a>
                 </div>
                  <?php if ($fpUrl != ''): ?>
                 	 <button class="btnViewFloorPlan" onclick='openWin();'>View Site/Floor Plan</button>
                  	<script>
                      function openWin() {
    window.open("<?php echo $fpUrl ?>");
}
                      </script>
                  <?php endif;?>
                </div>
                <div class="detailsShareButtons">
                  <?php echo do_shortcode('[wp_social_sharing social_options="facebook,twitter,googleplus,linkedin"  show_icons="1"]
'); ?>
                </div>
                <!-- end detailsBox -->
                <!--<div class="detailsBox">
                    <?php if ((isset($property->highSchool) && $property->highSchool != "") or (isset($property->elementarySchool) && $property->elementarySchool != "") or (isset($property->middleSchool) && $property->middleSchool != "")): ?>
                    <div class="headerSchoolInfo">
                        School Information
                    </div>
                    <div class="detailsSpecs">
                    <?php endif;?>
                        <?php if (isset($property->elementarySchool) && $property->elementarySchool != ""): ?>
                        <div class="specsLabel">
                            Elementary School:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->elementarySchool) && $property->elementarySchool != "" ? $property->elementarySchool : "&nbsp;") ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->middleSchool) && $property->middleSchool != ""): ?>
                        <div class="specsLabel">
                            Middle School:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->middleSchool) && $property->middleSchool != "" ? $property->middleSchool : "&nbsp;") ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->highSchool) && $property->highSchool != ""): ?>
                        <div class="specsLabel">
                            High School:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->highSchool) && $property->highSchool != "" ? $property->highSchool : "&nbsp;") ?>
                        </div>
                        <?php endif;?>
                        <?php if ((isset($property->highSchool) && $property->highSchool != "") or (isset($property->elementarySchool) && $property->elementarySchool != "") or (isset($property->middleSchool) && $property->middleSchool != "")): ?>
                        <div class="specsLabel"></div>
                        <?php endif;?>
                    </div>
                </div>-->
                <!-- end detailsBox -->
            </div>
            <!-- end div1-3 -->
            <div class="detailsDescriptionHolder">
                <div class="div2-3">
                    <div class="detailsDescriptionHeader">
                        Description
                    </div>
                    <p>
                        <?php echo $property->description ?>
                    </p>
                </div>
                <!-- end div2-3 -->
                <div class="div1-3">
                    <div class="agentTile">
                        <?php
$agent;
if (post_type_exists("i5agents")) {
	global $wpdb;
	$sql = "select p.id from {$wpdb->prefix}postmeta pm join {$wpdb->prefix}posts p on p.id=pm.post_id where meta_key='domain' and p.post_status='publish' and p.post_type='i5agents' and pm.meta_value='" . $_SERVER['HTTP_HOST'] . "'";
	$agent = $wpdb->get_row($sql);
}

if ($agent != null) {
	echo do_shortcode("[i5AgentData show_social_links='false' office_phone_label='O: ' mobile_phone_label='M: ']");
} else {?>
                        <div class="div1-3">
                            <div class="padding10">

                            </div>
                        </div>
                        <!-- end div1-3 -->
                        <div class="div2-3">
                            <div class="agentTitle">
                                <?php echo esc_attr(get_post_meta(get_the_ID(), 'companyname', true)); ?>
                            </div>
                            <div class="agentPhone">
                                <a href="tel:<?php echo esc_attr(get_post_meta(get_the_ID(), 'companyphone', true)); ?>">
                                    <span class="red">O:</span>
                                    <?php echo esc_attr(get_post_meta(get_the_ID(), 'companyphone', true)); ?>
                                </a>
                            </div>
                        </div>
                        <!-- end div2-3 -->
                        <?php }?>
                    </div>
                    <!-- end agentTile -->
                </div>
                <!-- end div1-3 -->
            </div>
            <div class="featuresHolder">
                <div class="div1-3 marginRight">
                    <div class="featuresHeader">
                        Exterior Features
                    </div>
                    <div class="featuresBox">
                        <?php if (isset($property->exteriorFeatures) && $property->exteriorFeatures != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Exterior Features
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->exteriorFeatures ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->exteriorSiding) && $property->exteriorSiding != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Exterior Siding
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->exteriorSiding ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->fenceDescription) && $property->fenceDescription != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Fence Description
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->fenceDescription ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->hasFence) && $property->hasFence != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Fence
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->hasFence ? "Yes" : "No" ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->foundationType) && $property->foundationType != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Foundation Type
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->foundationType ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->parkingDescription) && $property->parkingDescription != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Parking
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->parkingDescription ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->pool) && $property->pool != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Pool
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->pool ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->roof) && $property->roof != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Roof
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->roof ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->style) && $property->style != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Style
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->style ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->waterfrontDescription) && $property->waterfrontDescription != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Waterfront Description
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->waterfrontDescription ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->waterfront) && $property->waterfront != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Waterfront
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php
if ($property->waterfront == "1") {
	echo "Yes";
} else if ($property->waterfront == "0") {
	echo "No";
} else {
	echo $property->waterfront
	;
}
?>
                        </div>
                        <?php endif;?>
                    </div>
                    <!-- end featuresBox -->
                </div>
                <!-- end div1-3 -->
                <div class="div1-3 marginRight">
                    <div class="featuresHeader">
                        Interior Features
                    </div>
                    <div class="featuresBox">
                        <?php if (isset($property->appliances) && $property->appliances != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Appliances
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo str_replace(",", "<br/>", $property->appliances) ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->cooling) && $property->cooling != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Cooling
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->cooling ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->equipment) && $property->equipment != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Equipment
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->equipment ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->numberOfFireplaces) && $property->numberOfFireplaces != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Fireplaces Number
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->numberOfFireplaces ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->flooring) && $property->flooring != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Flooring
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->flooring ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->heating) && $property->heating != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Heating
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->heating ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->interiorFeatures) && $property->interiorFeatures != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Interior Features
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->interiorFeatures ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->numberOfRooms) && $property->numberOfRooms != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Number Of Rooms
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->numberOfRooms ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->otherRooms) && $property->otherRooms != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Other Rooms
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo str_replace(",", "<br/>", $property->otherRooms) ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->waterHeaterType) && $property->waterHeaterType != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Water Heater Type
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->waterHeaterType ?>
                        </div>
                        <?php endif;?>
                    </div>
                    <!-- end featuresBox -->
                </div>
                <!-- end div1-3 -->
                <div class="div1-3">
                    <div class="featuresHeader">
                        Property Features
                    </div>
                    <div class="featuresBox">
                        <?php if (isset($property->custom1) && $property->custom1 != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Aicuz Crash
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->custom1 ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->custom2) && $property->custom2 != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Aicuz Noise
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->custom2 ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->disclosure) && $property->disclosure != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Disclosure
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->disclosure ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->custom3) && $property->custom3 != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Fees POA Monthly
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->custom3 ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->custom4) && $property->custom4 != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Master Model
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->custom4 ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->subdivision) && $property->subdivision != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Neighborhood Name
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->subdivision ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->newConstruction) && $property->newConstruction != ""): ?>
                        <div class="detailsFeaturesLabel">
                            New Construction
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->newConstruction ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->ownershipType) && $property->ownershipType != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Ownership Type
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->ownershipType ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->custom5) && $property->custom5 != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Residential Type
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->custom5 ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->sewerType) && $property->sewerType != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Sewer Type
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->sewerType ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->taxes) && $property->taxes != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Tax Amount Approx
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->taxes ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->waterType) && $property->waterType != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Water Type
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->waterType ?>
                        </div>
                        <?php endif;?>
                        <?php if (isset($property->zoning) && $property->zoning != ""): ?>
                        <div class="detailsFeaturesLabel">
                            Zoning
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->zoning ?>
                        </div>
                        <?php endif;?>
                    </div>
                    <!-- end featuresBox -->
                </div>
                <!-- end div1-3 -->
            </div>
            <?php if (isset($property->listingOffice) && $property->listingOffice != ""): ?>
            <div style="width:100%">
                Listing provided courtesy of <?php echo $property->listingOffice; ?>
            </div>
            <?php endif;?>
            <!-- end featuresHolder -->
            <div class="col1-2">
              <div class="detailsMap">
                <a id="map"></a>
                <p style="text-align:center; margin:0;">
                    <?php
do_shortcode('[i5ListingMap maxzoom="100" minzoom="1" style="display:block;width: 100%; height: 478px" showdetails="false" showpolygon="false" zoom="15" lat="' . $property->latitude . '" long="' . $property->longitude . '" ]');
echo "<script>markAddress(" . json_encode($property) . ");</script>";
?>
                </p>
            	</div>
          	</div>
      	<div class="col1-2">
            <div class="detailsBox" id="detailsBox">
                <div class="padding10">
                    <a id="showingForm"></a>
                    <!--<div class="col1-4">
                        <div class="agentTitle">
                            <?php echo esc_attr(get_post_meta(get_the_ID(), 'companyname', true)); ?>
                        </div>
                        <div class="agentPhone">
                            <a href="tel:<?php echo esc_attr(get_post_meta(get_the_ID(), 'companyphone', true)); ?>">
                                <span class="red">O:</span>
                                <?php echo esc_attr(get_post_meta(get_the_ID(), 'companyphone', true)); ?>
                            </a>
                        </div>
                    </div>-->
                    <div class="col3-4">
                        <div class="padding10">
                            <?php
echo do_shortcode('[contact-form-7 id="6202" title="Schedule a Showing"]');
?>
                        </div>
                    </div>
                    <div class="col1-4">
                        <?php echo htmlspecialchars_decode(get_post_meta(get_the_ID(), 'companylogo', true)); ?>
                    </div>
                </div>
                <!-- end padding10 -->
            </div>
            <!-- end detailsBox -->
          </div>
            <?php include i5PBIntegration__PLUGIN_DIR . 'Includes/RelatedProperties.php';?>
            <div class="Disclaimer">
                <?php echo htmlspecialchars_decode(get_post_meta(get_the_ID(), 'disclaimer', true)); ?>
            </div>
        </div>
        <!-- end container -->
    </div>
</div>
<!-- end detailsBody -->


<script>
    jQuery(function () {
        jQuery(".propertyImages").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true
        });

        jQuery("img").show();
    });
    function toggleFav(id)
    {
        if (jQuery("#fav" + id).hasClass("detailsFavoriteSelected"))
        {
            jQuery("#fav" + id).removeClass("detailsFavoriteSelected");
            jQuery("#fav" + id).addClass("detailsFavorite");
            jQuery(jQuery("#fav" + id).children()[0]).prop("href","javascript:toggleFav('" + id + "');setFavorite('" + id + "',1);");
        }
        else {
            jQuery("#fav" + id).addClass("detailsFavoriteSelected");
            jQuery("#fav" + id).removeClass("detailsFavorite");
            jQuery(jQuery("#fav" + id).children()[0]).prop("href", "javascript:toggleFav('" + id + "');setFavorite('" + id + "',0);");
        }

    }
    function printListing()
    {
        jQuery(".printHide").hide();
        jQuery(".printContainer").printElement();
        jQuery(".printHide").show();
    }
  function showSharing() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddShare").classList.toggle("show");
  }

  // Close the dropdown if the user clicks outside of it
  window.onclick = function(event) {
  if (!event.target.matches('.dropBtn')) {

  var dropdowns = document.getElementsByClassName("dropdownContent");
  var i;
  for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

</script>

<?php
get_footer();
include i5PBIntegration__PLUGIN_DIR . 'Includes/Footer-Disclaimer.php';

?>
