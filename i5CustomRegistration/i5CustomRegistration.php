<?php
/*
Plugin Name:i5CustomRegistration
Plugin URI: http://www.i5fusion.com
Description: Displays listings from Propertybase and Setups Webtoprospect for Propertybase
Author: Jarrett Fisher
Version: 2.0
Author URI:http://www.i5fusion.com
 */

add_filter('simplemodal_registration_form', 'mytheme_registration_form');
add_filter('simplemodal_login_form', 'mytheme_login_form');

function mytheme_login_form($form) {
	$users_can_register = get_option('users_can_register') ? true : false;
	$options = get_option('simplemodal_login_options');

	$output = sprintf('
<form name="loginform" id="loginform" action="%s" method="post">
	<div style="display:none;" class="title">%s</div>
	<div class="simplemodal-login-fields">
	<p>
		<label>%s<br />
		<input type="text" name="log" class="user_login input" value="" size="20" tabindex="10" /></label>
	</p>
	<p>
		<label>%s<br />
		<input type="password" name="pwd" class="user_pass input" value="" size="20" tabindex="20" /></label>
	</p>',
		site_url('wp-login.php', 'login_post'),
		__('Login', 'simplemodal-login'),
		__('E-Mail', 'simplemodal-login'),
		__('Phone Number', 'simplemodal-login')
	);

	ob_start();
	do_action('login_form');
	$output .= ob_get_clean();

    $output .= sprintf('
    <p class="submit">
        <input type="submit" name="wp-submit" value="%s" tabindex="100" />
        <input type="button" class="simplemodal-close" value="%s" tabindex="101" />
        <input type="hidden" name="testcookie" value="1" />
    </p>
    <p class="nav">',
        __('Log In', 'simplemodal-login'),
        __('Cancel', 'simplemodal-login')
    );

	if ($users_can_register && $options['registration']) {
		$output .= sprintf('<a class="simplemodal-register" href="%s">%s</a>',
			site_url('wp-login.php?action=register', 'login'),
			__('Register', 'simplemodal-login')
		);
	}

	if (($users_can_register && $options['registration']) && $options['reset']) {
		$output .= ' | ';
	}

	if ($options['reset']) {
		$output .= sprintf('<a class="simplemodal-forgotpw" href="%s" title="%s">%s</a>',
			site_url('wp-login.php?action=lostpassword', 'login'),
			__('Password Lost and Found', 'simplemodal-login'),
			__('Lost your password?', 'simplemodal-login')
		);
	}

	$output .= '
	</p>
	</div>
	<div class="simplemodal-login-activity" style="display:none;"></div>
</form>';

	return $output;
}
function mytheme_registration_form($form) {
	$options = get_option('simplemodal_login_options');

	$output = sprintf('
<form name="registerform" id="registerform" action="%s" method="post">
    <input id="cimy_uef_wp_PASSWORD" type="hidden" name="cimy_uef_wp_PASSWORD" class="input" value="" size="20"/>
    <div class="title">%s</div>
    <div class="simplemodal-login-fields">
         <p>
            <label>
                %s
                <br />
                <input type="text" name="first_name" class="first_name input" value="" size="20" tabindex="10" />
            </label>
        </p>
        <p>
            <label>
                %s
                <br />
                <input type="text" name="last_name" class="last_name input" value="" size="20" tabindex="20" />
            </label>
        </p>
        <p>
            <label>
                %s
                <br />
                <input id="email" type="text" name="user_email" class="user_email input" value="" size="20" tabindex="30" />
                <input id="login" type="text" name="user_login" class="user_login input" value="" size="20" />
            </label>
        </p>
        <p>
            <label>
                %s
                <br />
                <input id="phone" type="text" placeholder="Phone will be used as a password" name="pwd" class="user_password input" value="" size="25" tabindex="40" />
                <script>
                    //jQuery("#email").keyup(function(){jQuery("#login").val(jQuery("#email").val());});
                    jQuery("#phone").keyup(function(){
											jQuery("#cimy_uef_wp_PASSWORD").val(jQuery("#phone").val());
										});
                    jQuery(window).ready(function(){
                        jQuery("p[class^=\"tml-user-\"]").hide();
                    });
                </script>
            </label>
        </p>
        ',
		site_url('wp-login.php?action=register', 'login_post'),
		__('Please create your FREE account to enjoy all of our features!', 'simplemodal-login'),
        __('First Name', 'simplemodal-login'),
        __('Last Name', 'simplemodal-login'),
		__('E-Mail', 'simplemodal-login'),
		__('Phone Number', 'simplemodal-login')
	);

	ob_start();
	do_action('register_form');
	$output .= ob_get_clean();

	$output .= sprintf('
        <p class="reg_passmail">%s</p>
        <p class="submit">
            <input type="submit" name="wp-submit" value="%s" tabindex="100" />
            <input type="button" class="simplemodal-close" value="%s" tabindex="101" />
        </p>
        <p class="nav">
            <a class="simplemodal-login" href="%s">%s</a>
            ',
		__('', 'simplemodal-login'),
		__('Join for FREE', 'simplemodal-login'),
		__('Cancel', 'simplemodal-login'),
		site_url('wp-login.php', 'login'),
		__('Already Registered? Sign in', 'simplemodal-login')
	);

    //if ($options['reset']) {
    //    $output .= sprintf(' |
    //        <a class="simplemodal-forgotpw" href="%s" title="%s">%s</a>
    //        ',
    //        site_url('wp-login.php?action=lostpassword', 'login'),
    //        __('Password Lost and Found', 'simplemodal-login'),
    //        __('Lost your password?', 'simplemodal-login')
    //    );
    //}

	$output .= '
        </p>
    </div>
    <div class="simplemodal-login-activity" style="display:none;"></div>
</form>
';

	return $output;
}
?>