<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.i5fusion.com
 * @since      1.0.0
 *
 * @package    I5propertybase
 * @subpackage I5propertybase/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    I5propertybase
 * @subpackage I5propertybase/includes
 * @author     Jarret Fisher <jarret@i5fusion.com>
 */
class I5propertybase_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
