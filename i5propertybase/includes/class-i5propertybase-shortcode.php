<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.i5fusion.com
 * @since      1.0.0
 *
 * @package    I5propertybase
 * @subpackage I5propertybase/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    I5propertybase
 * @subpackage I5propertybase/includes
 * @author     Jarret Fisher <jarret@i5fusion.com>
 */
class I5propertybase_Shortcode {
	/**
	 * Retrieve the shortcode for search.
	 *
	 * @since     1.0.0
	 */
	public static function search_shortcode($atts) {

		$vars = shortcode_atts(array(
		  'bgcolor' => '',
          'lat' => 38.980731,
          'listingtemplate'=>'',
          'long' => -107.7936206,
          'zoom' => 10,
          'maxzoom'=>12,
          'minzoom'=>8,
          'showdetails'=>true,
          'showpolygon'=>true
          ),$atts);
	?>
	<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 form-group">
			<div class="col-xs-12 col-sm-4 col-md-3">
				<label>Location</label>
				<input type="text" class="form-control" id="zip_code" name="zip_code" placeholder="Location">
			</div>
			<div class="col-xs-12 col-sm-4 col-md-2">
				<label>Property Type</label>
				<input type="text" class="form-control" id="zip_code" name="zip_code" placeholder="Property Type">
			</div>
			<div class="col-xs-12 col-sm-4 col-md-2">
				<label>Min. Price</label>
				<input type="text" class="form-control" id="zip_code" name="zip_code" placeholder="Min. Price">
			</div>
			<div class="col-xs-12 col-sm-4 col-md-2">
				<label>Max. Price</label>
				<input type="text" class="form-control" id="zip_code" name="zip_code" placeholder="Max. Price">
			</div>
			<div class="col-xs-12 col-sm-4 col-md-1">
				<label>Beds</label>
				<input type="text" class="form-control" id="zip_code" name="zip_code" placeholder="Beds">
			</div>
			<div class="col-xs-12 col-sm-4 col-md-1">
				<label>Baths</label>
				<input type="text" class="form-control" id="zip_code" name="zip_code" placeholder="Baths">
			</div>
			<div class="col-xs-12 col-sm-2 col-md-1">
				<button type="submit" class="btn btn-default glyphicon glyphicon-search"></button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	Placeholdem( document.querySelectorAll( '[placeholder]' ) );
</script>
	<?php
	}

}
