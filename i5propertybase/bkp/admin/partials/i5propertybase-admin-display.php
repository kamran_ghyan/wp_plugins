<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.i5fusion.com
 * @since      1.0.0
 *
 * @package    I5propertybase
 * @subpackage I5propertybase/admin/partials
 */
 
class I5propertybase_Admin_Display{

    /**
     * Hook in methods
     * @since    1.0.0
     * @access   static
     */
    public static function init() {
        add_action('admin_menu', array(__CLASS__, 'add_settings_menu'));
    }

    /**
     * add_settings_menu helper function used for add menu for pluging setting
     * @since    1.0.0
     * @access   public
     */
    public static function add_settings_menu() {
        add_options_page('I5 Property Base', 'I5PB Settings', 'manage_options', 'i5propertybase-option', array(__CLASS__, 'i5propertybase_options'));
    }
	
	 /**
     * i5_property_base_options helper will trigger hook and handle all the settings section
     * @since    1.0.0
     * @access   public
     */
    public static function i5propertybase_options() {
        
		//i5PBIntegration_Functions::mlsSync();

		
         $doOAuth = false;
		 
         $settings = get_option('WebListing_Settings');
		 
         if(!isset($settings))
             $settings = array('Prospect' => false,'EndPoint' => '','Token' => '','Debug' => false);

         if(isset($_REQUEST['code']) && !isset($settings["OAuth"]["OAuthToken"]))
         {

             //Get the token
             $url="https://login.salesforce.com/services/oauth2/token";

             if($settings["Debug"]=="Yes")
                 $url="https://test.salesforce.com/services/oauth2/token";

             $body="grant_type=authorization_code&client_id=" . $settings["OAuth"]["OAuthClientId"] . "&client_secret=" . $settings["OAuth"]["OAuthSecret"] . "&redirect_uri=" . urlencode('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']) . "&code=" . $_REQUEST['code'];

             $ch=curl_init($url . "?" . $body);

             curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
             curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

             $token = curl_exec($ch);

             if(FALSE == $token)
             {
                 $err = curl_error($ch);
             }
             else
             {
                 $var = json_decode($token);

                 if(isset($var->access_token))
                 {
                     $settings["OAuth"]["OAuthToken"] = $var->access_token;
                     $settings["OAuth"]["RefreshToken"] = $var->refresh_token;
                     $settings["OAuth"]["InstanceUrl"] = $var->instance_url;
                     update_option("WebListing_Settings", $settings);
                 }
             }

             curl_close($ch);
         }
		 else if(isset($_POST['submit']))
		 {
             if(isset($_POST["ClientId"])) //Check to see if this is oauth
             {
                 $doOAuth=true;
                 $settings["OAuth"]["OAuthClientId"]=$_POST["ClientId"];
                 $settings["OAuth"]["OAuthSecret"]=$_POST["Secret"];
                 $settings["Debug"]=isset($_POST["WebListingDebug"])?"Yes":"No";

                 update_option("WebListing_Settings",$settings);
             }
             else if(isset($_POST["ResetOAuth"]))
             {
                 $settings["OAuth"]["OAuthToken"]=null;
                 update_option("WebListing_Settings",$settings);
             }
             else
             {
                 $ok = true;
                 $message = '<p>Please Correct the Following Errors</p>';
                
                 if($ok)
                 {
                     $settings["Prospect"] = isset($_POST["EnableWebProspect"])?true:false;
                     $settings["Debug"] = isset($_POST["WebListingDebug"])?"Yes":"No";
                     $settings["EnableSync"] = isset($_POST["EnableSync"])?"Yes":"No";
                     $settings["GoogleAPIKey"] = $_POST["GoogleAPIKey"];
                     $settings["ExcludeMapsAPI"] = isset($_POST["ExcludeMapsAPI"])?"Yes":"No";

                     $settings["ProspectMappings"] = array();
                     if(isset($_POST["MapFirstName"]) && $_POST["MapFirstName"] != "")
                         $settings["ProspectMappings"]["FirstName"] = $_POST["MapFirstName"];
                     if(isset($_POST["MapLastName"]) && $_POST["MapLastName"]!="")
                         $settings["ProspectMappings"]["LastName"] = $_POST["MapLastName"];
                     if(isset($_POST["MapEmail"]) && $_POST["MapEmail"] != "")
                         $settings["ProspectMappings"]["Email"] = $_POST["MapEmail"];
                     if(isset($_POST["MapSaveSearchId"]) && $_POST["MapSaveSearchId"] != "")
                         $settings["ProspectMappings"]["MapSaveSearchId"] = $_POST["MapSaveSearchId"];
                     if(isset($_POST["MapPhone"]) && $_POST["MapPhone"] != "")
                         $settings["ProspectMappings"]["Phone"] = $_POST["MapPhone"];
                     if(isset($_POST["MapCust1PB"]) && isset($_POST["MapCust1WP"]) && $_POST["MapCust1PB"] != "" && $_POST["MapCust1WP"] != "")
                         $settings["ProspectMappings"]["MapCust1"] = array($_POST["MapCust1PB"], $_POST["MapCust1WP"]);
                     if(isset($_POST["MapCust2PB"]) && isset($_POST["MapCust2WP"]) && $_POST["MapCust2PB"] != "" && $_POST["MapCust2WP"]!="")
                         $settings["ProspectMappings"]["MapCust2"] = array($_POST["MapCust2PB"], $_POST["MapCust2WP"]);
                     if(isset($_POST["MapCust3PB"]) && isset($_POST["MapCust3WP"]) && $_POST["MapCust3PB"] != "" && $_POST["MapCust3WP"]!="")
                         $settings["ProspectMappings"]["MapCust3"] = array($_POST["MapCust3PB"], $_POST["MapCust3WP"]);
                     if(isset($_POST["MapCust4PB"]) && isset($_POST["MapCust4WP"]) && $_POST["MapCust4PB"]!="" && $_POST["MapCust4WP"]!="")
                         $settings["ProspectMappings"]["MapCust4"] = array($_POST["MapCust4PB"], $_POST["MapCust4WP"]);
                     if(isset($_POST["MapCust5PB"]) && isset($_POST["MapCust5WP"]) && $_POST["MapCust5PB"] != "" && $_POST["MapCust5WP"]!="")
                         $settings["ProspectMappings"]["MapCust5"] = array($_POST["MapCust5PB"], $_POST["MapCust5WP"]);
                     $settings["SelectFields"] = array();
                     //setup pb mappings
                     foreach($_POST as $key => $value)
                     {
                         if(substr($key,0,3) == "PBS" && isset($value) && $value!="")
                             $settings["SelectFields"][$key] = $value;
                     }

                     //Setup where clauses
                     if(isset($_POST["WHField1"]) && $_POST["WHField1"] != "" && isset($_POST["WHValue1"]) && $_POST["WHValue1"] != "")
                     {
                         $settings["WHERE"]["Condition1"]["Field"] = $_POST["WHField1"];
                         $settings["WHERE"]["Condition1"]["Value"] = $_POST["WHValue1"];
                         $settings["WHERE"]["Condition1"]["Type"] = $_POST["WHType1"];
                     }
                     else
                     {
                         $settings["WHERE"]["Condition1"] = null;
                         $settings["WHERE"]["Condition2"] = null;
                         $settings["WHERE"]["Condition3"] = null;
                     }

                     if(isset($_POST["WHField2"]) && $_POST["WHField2"] != "" && isset($_POST["WHValue2"]) && $_POST["WHValue2"] != "")
                     {
                         $settings["WHERE"]["Condition2"]["Field"] = $_POST["WHField2"];
                         $settings["WHERE"]["Condition2"]["Value"] = $_POST["WHValue2"];
                         $settings["WHERE"]["Condition2"]["Type"] = $_POST["WHType2"];
                     }
                     else
                     {
                         $settings["WHERE"]["Condition2"] = null;
                         $settings["WHERE"]["Condition3"] = null;
                     }

                     if(isset($_POST["WHField3"]) && $_POST["WHField3"] != "" && isset($_POST["WHValue3"]) && $_POST["WHValue3"] != "")
                     {
                         $settings["WHERE"]["Condition3"]["Field"] = $_POST["WHField3"];
                         $settings["WHERE"]["Condition3"]["Value"] = $_POST["WHValue3"];
                         $settings["WHERE"]["Condition3"]["Type"] = $_POST["WHType3"];
                     }
                     else
                     {
                         $settings["WHERE"]["Condition3"] = null;
                     }

                     //Setup delete clauses
                     if(isset($_POST["DELField1"]) && $_POST["DELField1"] != "" && isset($_POST["DELValue1"]) && $_POST["DELValue1"] != "")
                     {
                         $settings["DELETE"]["Condition1"]["Field"] = $_POST["DELField1"];
                         $settings["DELETE"]["Condition1"]["Value"] = $_POST["DELValue1"];
                         $settings["DELETE"]["Condition1"]["Type"] = $_POST["DELType1"];
                     }
                     else
                     {
                         $settings["DELETE"]["Condition1"] = null;
                         $settings["DELETE"]["Condition2"] = null;
                         $settings["DELETE"]["Condition3"] = null;
                     }

                     if(isset($_POST["DELField2"]) && $_POST["DELField2"] != "" && isset($_POST["DELValue2"]) && $_POST["DELValue2"] != "")
                     {
                         $settings["DELETE"]["Condition2"]["Field"] = $_POST["DELField2"];
                         $settings["DELETE"]["Condition2"]["Value"] = $_POST["DELValue2"];
                         $settings["DELETE"]["Condition2"]["Type"] = $_POST["DELType2"];
                     }
                     else
                     {
                         $settings["DELETE"]["Condition2"] = null;
                         $settings["DELETE"]["Condition3"] = null;
                     }

                     if(isset($_POST["DELField3"]) && $_POST["DELField3"]!="" && isset($_POST["DELValue3"]) && $_POST["DELValue3"]!="")
                     {
                         $settings["DELETE"]["Condition3"]["Field"] = $_POST["DELField3"];
                         $settings["DELETE"]["Condition3"]["Value"] = $_POST["DELValue3"];
                         $settings["DELETE"]["Condition3"]["Type"] = $_POST["DELType3"];
                     }
                     else
                     {
                         $settings["DELETE"]["Condition3"] = null;
                     }

                     $result = update_option("WebListing_Settings",$settings);
                     echo "<p>Settings Updated</p>";
                 }
                 else
                 {
                     echo $message;
                 }
             }
		 }
		 
		
?>
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
        $( function() {
        $( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
        $( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
        } );
        </script>
		<style>
		#tabs{ width:100%; }
		.ui-tabs-nav{ width:20% !important; }
		.ui-tabs-vertical .ui-tabs-panel{ float:left !important; width:75% !important ; }
		ui-tabs-panel h1{ margin:0; padding:0; }
        .ui-tabs-vertical { width: 55em; }
        .ui-tabs-vertical .ui-tabs-nav { padding: .2em .1em .2em .2em; float: left; width: 12em; }
        .ui-tabs-vertical .ui-tabs-nav li { clear: left; width: 100%; border-bottom-width: 1px !important; border-right-width: 0 !important; margin: 0 -1px .2em 0; }
        .ui-tabs-vertical .ui-tabs-nav li a { display:block; }
        .ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active { padding-bottom: 0; padding-right: .1em; border-right-width: 1px; }
        .ui-tabs-vertical .ui-tabs-panel { padding: 1em; float: right; width: 40em;}
		.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active, a.ui-button:active, .ui-button:active, .ui-button.ui-state-active:hover{background:#58748c !important; border: 1px solid #58748c !important;}
		.form-table th, .form-wrap label { color:#333 !important; }
		.ui-tabs .ui-tabs-nav .ui-tabs-anchor { padding: 1em; }
		.wrap h2.nav-tab-wrapper, h1.nav-tab-wrapper, h3.nav-tab-wrapper{ margin-bottom:10px; }
		.form-table th{ font-size:13px; }
		.wp-person a:focus .gravatar, a:focus, a:focus .media-icon img{ box-shadow:0 0 0 1px #58748c, 0 0 2px 1px rgba(30, 140, 190, 0.0) !important; }
		.ui-corner-all, .ui-corner-bottom, .ui-corner-left, .ui-corner-bl{border-bottom-left-radius:0;}
		.ui-corner-all, .ui-corner-top, .ui-corner-left, .ui-corner-tl{border-top-left-radius:0;}
		.ui-tabs-vertical .ui-tabs-nav li a{ width:90%; }
		#setting-error-dreamvilla-multiple-property{ display:none !important; }
        </style>
<div class="wrap">
    <form method="post" action="" novalidate>
    
    
    
    
    <?php
        if(isset($settings["OAuth"]["OAuthToken"]))
        {
            $items = self::getPBFields($settings);
           
		    asort($items);
    ?>
    <h1><?php _e( 'Web Listings Settings', 'i5pb-options' ); ?></h1>
 
 <?php 
 		if(empty($_GET['tab'])){
			$current = 'general';
		} else{
 			$current = $_GET['tab'];
		}
 		$tabs = array( 'general' => 'General Settings', 'mapping' => 'Fields Mapping', 'prospect' => 'Prospect Mapping', 'contact' => 'Contact Form ID' );
		echo '<div id="icon-themes" class="icon32"><br></div>';
		echo '<h2 class="nav-tab-wrapper">';
		foreach( $tabs as $tab => $name ){
		$class = ( $tab == $current ) ? ' nav-tab-active' : '';
		echo "<a class='nav-tab$class' href='?page=i5propertybase-option&tab=$tab'>$name</a>";
		
		}
		echo '</h2>';
		
		if ( isset ( $_GET['tab'] ) ) $tab = $_GET['tab'];
   		else $tab = 'general';
		
   		switch ( $tab ){
			case 'general' :
		?>
        <div class="ui-widget-content">
        <table class="form-table">
    <tr>
                    <th scope="row"><?php _e( 'Status:', 'i5pb-options' ); ?></th>
                    <td>
                        <?php echo isset($settings["EnableSync"])?($settings["EnableSync"]=="No"?"Disabled":(isset($settings["Status"])?$settings["Status"]:"Never Run")):"Disabled"?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e( 'Last Sync:', 'i5pb-options' ); ?></th>
                    <td>
                        <?php echo isset($settings["LastRun"])?$settings["LastRun"]:'Never'?>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e( 'Next Run:', 'i5pb-options' ); ?></th>
                    <td>
                        <?php echo date("Y-m-d\Th:i:s",wp_next_scheduled("SyncListings"))?></td>
                </tr>
                <tr>
                    <th scope="row"><?php _e( 'Enable Listing Sync:', 'i5pb-options' ); ?></th>
                    <td>
                        <input name="EnableSync" type="checkbox" <?php echo $settings["EnableSync"]=='Yes'? 'checked="true"':'' ?> />
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e( 'Reset OAuth:', 'i5pb-options' ); ?></th>
                    <td>
                        <input name="ResetOAuth" type="checkbox" />
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e( 'Sandbox:', 'i5pb-options' ); ?></th>
                    <td>
                        <input name="WebListingDebug" type="checkbox" <?php echo $settings["Debug"]=='Yes'? 'checked="true"':'' ?> />
                    </td>
                </tr>
                <tr>
                    <th colspan="2" scope="row"><?php _e( 'Google Maps API Settings:', 'i5pb-options' ); ?></th>
                </tr>
                <tr>
                    <th scope="row"><?php _e( 'API Key:', 'i5pb-options' ); ?></th>
                    <td>
                        <input name="GoogleAPIKey" type="text" value='<?php echo isset($settings["GoogleAPIKey"])?$settings["GoogleAPIKey"]:"" ?>' />
                    </td>
                </tr>
                <tr>
                    <th scope="row"><?php _e( 'Don\'t Include Google Maps API Code:', 'i5pb-options' ); ?></th>
                    <td>
                        <input name="ExcludeMapsAPI" type="checkbox" <?php echo $settings["ExcludeMapsAPI"]=='Yes'? 'checked="true"':'' ?> />
                    </td>
                </tr>
                </table>
                </div>
        <?php
			break;
			case 'mapping' :
		?>
        

 
<div id="tabs">
  <ul>
    <li><a href="#tabs-1">Basic Details</a></li>
    <li><a href="#tabs-2">Amenities</a></li>
    <li><a href="#tabs-3">Interior</a></li>
    <li><a href="#tabs-4">Exterior</a></li>
    <li><a href="#tabs-5">Diemension</a></li>
    <li><a href="#tabs-6">Address</a></li>
    <li><a href="#tabs-7">Agent</a></li>
    <li><a href="#tabs-8">Others</a></li>
    <li><a href="#tabs-9">Where Clause</a></li>
    <li><a href="#tabs-10">Delete Rule</a></li>
  </ul>
  <div id="tabs-1">
    <table class="form-table">
    	<tr>
        	<th><?php _e( 'Listing Type', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSListType">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSListType"]) && $settings["SelectFields"]["PBSListType"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
        	<th><?php _e( 'Property Type', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSPropertyType">
                            <?php
                            foreach($items as $key => $value)
                            {
                                if(isset($settings["SelectFields"]["PBSPropertyType"]) && $settings["SelectFields"]["PBSPropertyType"]==$key)
                                    echo "<option value='$key' selected='true'>" . $value . "</option>";
                                else
                                    echo "<option value='$key'>" . $value . "</option>";
                            }
                            ?>
                        </select>
                    </td>
        </tr>
    	<tr>
            <th><?php _e( 'MLS Id', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSMLSID">
                    <?php
                        
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSMLSID"]) && $settings["SelectFields"]["PBSMLSID"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            <th><?php _e( 'Size', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSSize">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSSize"]) && $settings["SelectFields"]["PBSSize"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
                	<th><?php _e( 'Bedrooms', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSBeds">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSBeds"]) && $settings["SelectFields"]["PBSBeds"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Other rooms', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSOtherBeds">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSOtherBeds"]) && $settings["SelectFields"]["PBSOtherBeds"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
        <tr>
                    <th><?php _e( 'Full Baths', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSFullBaths">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSFullBaths"]) && $settings["SelectFields"]["PBSFullBaths"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Half Baths', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSHalfBaths">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSHalfBaths"]) && $settings["SelectFields"]["PBSHalfBaths"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                	<th><?php _e( 'Lot Size', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSLotSize">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSLotSize"]) && $settings["SelectFields"]["PBSLotSize"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Description', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSDescription">
                            <?php
                            foreach($items as $key => $value)
                            {
                                if(isset($settings["SelectFields"]["PBSDescription"]) && $settings["SelectFields"]["PBSDescription"]==$key)
                                    echo "<option value='$key' selected='true'>" . $value . "</option>";
                                else
                                    echo "<option value='$key'>" . $value . "</option>";
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                	<th><?php _e( 'Year Built', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSYearBuilt">
                            <?php
                            foreach($items as $key => $value)
                            {
                                if(isset($settings["SelectFields"]["PBSYearBuilt"]) && $settings["SelectFields"]["PBSYearBuilt"]==$key)
                                    echo "<option value='$key' selected='true'>" . $value . "</option>";
                                else
                                    echo "<option value='$key'>" . $value . "</option>";
                            }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Style', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSStyle">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSStyle"]) && $settings["SelectFields"]["PBSStyle"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                	<th><?php _e( 'Flooring', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSFlooring">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSFlooring"]) && $settings["SelectFields"]["PBSFlooring"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Appliances', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSAppliances">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSAppliances"]) && $settings["SelectFields"]["PBSAppliances"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                	<th><?php _e( 'Status', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSStatus">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSStatus"]) && $settings["SelectFields"]["PBSStatus"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Unit Number', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSUnitNumber">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSUnitNumber"]) && $settings["SelectFields"]["PBSUnitNumber"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    
                    <th><?php _e( 'Price', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSPrice">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSPrice"]) && $settings["SelectFields"]["PBSPrice"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                     <th><?php _e( 'Stories', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSStories">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSStories"]) && $settings["SelectFields"]["PBSStories"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    
                    <th><?php _e( 'Garage', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSGarage">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSGarage"]) && $settings["SelectFields"]["PBSGarage"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'No. Of Garage', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSNoOfGarage">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSNoOfGarage"]) && $settings["SelectFields"]["PBSNoOfGarage"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                
        
    </table>
  </div>
  <div id="tabs-2">
    <table class="form-table">
    	<tr>
        	<th><?php _e( 'Parking', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSParkingDesc">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSParkingDesc"]) && $settings["SelectFields"]["PBSParkingDesc"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            <th><?php _e( 'Parking Spaces', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSParkingSpaces">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSParkingSpaces"]) && $settings["SelectFields"]["PBSParkingSpaces"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            
        </tr>
        <tr>
        	<th><?php _e( 'Fireplaces', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSFirePlaceNum">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSFirePlaceNum"]) && $settings["SelectFields"]["PBSFirePlaceNum"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            <th><?php _e( 'Pool', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSPool">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSPool"]) && $settings["SelectFields"]["PBSPool"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
        	<th><?php _e( 'Basketball court', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSBaseballCourt">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSBaseballCourt"]) && $settings["SelectFields"]["PBSBaseballCourt"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            <th><?php _e( 'Doorman', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSDoorman">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSDoorman"]) && $settings["SelectFields"]["PBSDoorman"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
        	<th><?php _e( 'Gym', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSGym">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSGym"]) && $settings["SelectFields"]["PBSGym"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            <th><?php _e( 'Laundry', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSLaundry">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSLaundry"]) && $settings["SelectFields"]["PBSLaundry"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
        	<th><?php _e( 'Private space', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSPrivateSpace">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSPrivateSpace"]) && $settings["SelectFields"]["PBSPrivateSpace"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            <th><?php _e( 'Wifi', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSWifi">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSWifi"]) && $settings["SelectFields"]["PBSWifi"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
        </tr>
    </table>
  </div>
  <div id="tabs-3">
    <table class="form-table">
    	<tr>
        	<th><?php _e( 'Basement', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSBasement">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSBasement"]) && $settings["SelectFields"]["PBSBasement"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            <th><?php _e( 'Basement Type', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSBasementType">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSBasementType"]) && $settings["SelectFields"]["PBSBasementType"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
        </tr>
    	<tr>
        	<th><?php _e( 'Heating', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSHeating">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSHeating"]) && $settings["SelectFields"]["PBSHeating"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            <th><?php _e( 'New Construction', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSNewConstruction">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSNewConstruction"]) && $settings["SelectFields"]["PBSNewConstruction"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
        </tr>
    </table>
  </div>
  <div id="tabs-4">
    <table class="form-table">
    	<tr>
        	<th><?php _e( 'Roof', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSRoof">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSRoof"]) && $settings["SelectFields"]["PBSRoof"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            <th><?php _e( 'Foundation Type', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSFoundationType">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSFoundationType"]) && $settings["SelectFields"]["PBSFoundationType"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
        	<th><?php _e( 'Exterior Features', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSExteriorFeatures">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSExteriorFeatures"]) && $settings["SelectFields"]["PBSExteriorFeatures"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            <th><?php _e( 'Exterior Siding', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSExteriorSiding">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSExteriorSiding"]) && $settings["SelectFields"]["PBSExteriorSiding"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
        	<th><?php _e( 'Frontage Meters', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSFrontageMeters">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSFrontageMeters"]) && $settings["SelectFields"]["PBSFrontageMeters"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            <th><?php _e( 'Frunt Exposure', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSFruntExposure">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSFruntExposure"]) && $settings["SelectFields"]["PBSFruntExposure"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
        </tr>
    </table>
  </div>
  <div id="tabs-5">
    <table class="form-table">
    	<tr>
        	<th><?php _e( 'Room ', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSRoomDiemension">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSRoomDiemension"]) && $settings["SelectFields"]["PBSRoomDiemension"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            <th><?php _e( 'Bathroom', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSBathroomDiemension">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSBathroomDiemension"]) && $settings["SelectFields"]["PBSBathroomDiemension"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
        	<th><?php _e( 'Kitchen ', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSKitchenDiemension">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSKitchenDiemension"]) && $settings["SelectFields"]["PBSKitchenDiemension"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            <th><?php _e( 'Swimming Pool', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSSwimmingPoolDiemension">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSSwimmingPoolDiemension"]) && $settings["SelectFields"]["PBSSwimmingPoolDiemension"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
        	<th><?php _e( 'Gym ', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSGymDiemension">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSGymDiemension"]) && $settings["SelectFields"]["PBSGymDiemension"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            <th>&nbsp;</th>
            <td>&nbsp;</td>
        </tr>
    </table>
  </div>
  <div id="tabs-6">
    <table class="form-table">
    	<tr>
        	<th><?php _e( 'Address', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSAddress">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSAddress"]) && $settings["SelectFields"]["PBSAddress"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            <th><?php _e( 'Zipcode', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSZip">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSZip"]) && $settings["SelectFields"]["PBSZip"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
        </tr>
        <tr>
        	<th><?php _e( 'State', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSState">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSState"]) && $settings["SelectFields"]["PBSState"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            <th><?php _e( 'City', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSCity">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSCity"]) && $settings["SelectFields"]["PBSCity"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
          </tr>
          <tr>
                    <th><?php _e( 'Latitude', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSLatitude">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSLatitude"]) && $settings["SelectFields"]["PBSLatitude"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Longitude', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSLongitude">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSLongitude"]) && $settings["SelectFields"]["PBSLongitude"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
    </table>
  </div>
  <div id="tabs-7">
    <table class="form-table">
    	<tr>
                    <th><?php _e( 'Agent First Name', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSAgentFirstName">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSAgentFirstName"]) && $settings["SelectFields"]["PBSAgentFirstName"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Agent Last Name', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSAgentLastName">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSAgentLastName"]) && $settings["SelectFields"]["PBSAgentLastName"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                
        <tr>
            <th><?php _e( 'Agent Mobile', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSAgentMobile">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSAgentMobile"]) && $settings["SelectFields"]["PBSAgentMobile"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            <th><?php _e( 'Agent Office Phone', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSAgentOfficePhone">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSAgentOfficePhone"]) && $settings["SelectFields"]["PBSAgentOfficePhone"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
        </tr>
        
        <tr>
            <th><?php _e( 'Agent Email', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSAgentEmail">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSAgentEmail"]) && $settings["SelectFields"]["PBSAgentEmail"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            <th><?php _e( 'Agent MLS ID', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSAgentMLSID">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSAgentMLSID"]) && $settings["SelectFields"]["PBSAgentMLSID"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
        </tr>
        
        <tr>
            <th><?php _e( 'Listing Office Name', 'i5pb-options' ); ?></th>
            <td>
                <select name="PBSListingOffice">
                    <?php
                        foreach($items as $key => $value)
                        {
                            if(isset($settings["SelectFields"]["PBSListingOffice"]) && $settings["SelectFields"]["PBSListingOffice"]==$key)
                                echo "<option value='$key' selected='true'>" . $value . "</option>";
                            else
                                echo "<option value='$key'>" . $value . "</option>";
                        }
                    ?>
                </select>
            </td>
            
        </tr>
    </table>
  </div>
  <div id="tabs-8">
    <table class="form-table">
            <tbody>
                <tr>
                    
                    <th><?php _e( 'Subdivision', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSSubdivision">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSSubdivision"]) && $settings["SelectFields"]["PBSSubdivision"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Record Type', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSRecordType">
                            <?php
                            foreach($items as $key => $value)
                            {
                                if(isset($settings["SelectFields"]["PBSRecordType"]) && $settings["SelectFields"]["PBSRecordType"]==$key)
                                    echo "<option value='$key' selected='true'>" . $value . "</option>";
                                else
                                    echo "<option value='$key'>" . $value . "</option>";
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    
                     <th><?php _e( 'School District', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSSchoolDistrict">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSSchoolDistrict"]) && $settings["SelectFields"]["PBSSchoolDistrict"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Fence Description', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSFenceDesc">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSFenceDesc"]) && $settings["SelectFields"]["PBSFenceDesc"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                   
                </tr>
                 <tr>
                    <th><?php _e( 'Elementary School', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSElementarySchool">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSElementarySchool"]) && $settings["SelectFields"]["PBSElementarySchool"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Middle School', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSMiddleSchool">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSMiddleSchool"]) && $settings["SelectFields"]["PBSMiddleSchool"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
               
                <tr>
                    <th><?php _e( 'High School', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSHighSchool">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSHighSchool"]) && $settings["SelectFields"]["PBSHighSchool"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'View', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSView">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSView"]) && $settings["SelectFields"]["PBSView"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    
                </tr>
                <tr>
                    <th><?php _e( 'Area', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSArea">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSArea"]) && $settings["SelectFields"]["PBSArea"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Detail URL Field', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSURL">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSURL"]) && $settings["SelectFields"]["PBSURL"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
               
                
				 
				<tr>
                    
                    
                </tr>
				<tr>
                    <th><?php _e( 'Has Fence', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSFence">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSFence"]) && $settings["SelectFields"]["PBSFence"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Cooling', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSCooling">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCooling"]) && $settings["SelectFields"]["PBSCooling"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>

                </tr>
				<tr>
                    
                    
                </tr>
				<tr>
                    
                    
                </tr>
				<tr>
                    <th><?php _e( 'Waterfront Description', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSWaterfrontDesc">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSWaterfrontDesc"]) && $settings["SelectFields"]["PBSWaterfrontDesc"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Waterfront', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSWaterfront">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSWaterfront"]) && $settings["SelectFields"]["PBSWaterfront"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    
                    
                </tr>
				<tr>
                    <th><?php _e( 'Equipment', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSEquipment">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSEquipment"]) && $settings["SelectFields"]["PBSEquipment"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Disclosure', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSDisclosure">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSDisclosure"]) && $settings["SelectFields"]["PBSDisclosure"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    
                    
                </tr>
				<tr>
                    <th><?php _e( 'Interior Features', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSInteriorFeat">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSInteriorFeat"]) && $settings["SelectFields"]["PBSInteriorFeat"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Number of Rooms', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSRoomNum">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSRoomNum"]) && $settings["SelectFields"]["PBSRoomNum"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <th><?php _e( 'Other Rooms', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSOtherRooms">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSOtherRooms"]) && $settings["SelectFields"]["PBSOtherRooms"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Water Heater Type', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSWaterHeater">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSWaterHeater"]) && $settings["SelectFields"]["PBSWaterHeater"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    
                    
                </tr>
				<tr>
                    <th><?php _e( 'Ownership Type', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSOwnershipType">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSOwnershipType"]) && $settings["SelectFields"]["PBSOwnershipType"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Sewer Type', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSSewer">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSSewer"]) && $settings["SelectFields"]["PBSSewer"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				
				<tr>
                    <th><?php _e( 'Taxes', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSTaxes">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSTaxes"]) && $settings["SelectFields"]["PBSTaxes"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Water Type', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSWater">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSWater"]) && $settings["SelectFields"]["PBSWater"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <th><?php _e( 'Zoning', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSZoning">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSZoning"]) && $settings["SelectFields"]["PBSZoning"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Custom 1', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSCustom1">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom1"]) && $settings["SelectFields"]["PBSCustom1"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>

                <tr>
                    <th><?php _e( 'Custom 2', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSCustom2">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom2"]) && $settings["SelectFields"]["PBSCustom2"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Custom 3', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSCustom3">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom3"]) && $settings["SelectFields"]["PBSCustom3"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><?php _e( 'Custom 4', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSCustom4">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom4"]) && $settings["SelectFields"]["PBSCustom4"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Custom 5', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSCustom5">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom5"]) && $settings["SelectFields"]["PBSCustom5"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    
                </tr>
                <tr>
                    <th><?php _e( 'Custom 6', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSCustom6">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom6"]) && $settings["SelectFields"]["PBSCustom6"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Custom 7', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSCustom7">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom7"]) && $settings["SelectFields"]["PBSCustom7"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th><?php _e( 'Custom 8', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSCustom8">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom8"]) && $settings["SelectFields"]["PBSCustom8"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else

                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                    <th><?php _e( 'Custom 9', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSCustom9">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom9"]) && $settings["SelectFields"]["PBSCustom9"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    
                </tr>
                <tr>
                    <th><?php _e( 'Custom 10', 'i5pb-options' ); ?></th>
                    <td>
                        <select name="PBSCustom10">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom10"]) && $settings["SelectFields"]["PBSCustom10"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                
                
                
                
                
                </table>
  </div>
  <div id="tabs-9">
    <table>
                <tr>
                    <th colspan="4"><?php _e( 'Where Clauses:', 'i5pb-options' ); ?></th>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <select name="WHField1">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["WHERE"]["Condition1"]["Field"]) && $settings["WHERE"]["Condition1"]["Field"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                        </td>
                    <td>
                        <select name="WHType1">
                            <option value="=" <?php echo (isset($settings["WHERE"]["Condition1"]["Type"]) && $settings["WHERE"]["Condition1"]["Type"]=="=")?"selected='true'":"" ?>>Equals</option>
                            <option value="!=" <?php echo (isset($settings["WHERE"]["Condition1"]["Type"]) && $settings["WHERE"]["Condition1"]["Type"]=="!=")?"selected='true'":"" ?>>Not Equal To</option>
                        </select>
                    </td>
                    <td>
                        <input name="WHValue1" type="text" value="<?php echo isset($settings["WHERE"]["Condition1"]["Value"])?$settings["WHERE"]["Condition1"]["Value"]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                       <?php _e( 'And', 'i5pb-options' ); ?>
                        </td>
                    <td>
                        <select name="WHField2">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["WHERE"]["Condition2"]["Field"]) && $settings["WHERE"]["Condition2"]["Field"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                        </td>
                    <td>
                        <select name="WHType2">
                            <option value="=" <?php echo (isset($settings["WHERE"]["Condition2"]["Type"]) && $settings["WHERE"]["Condition2"]["Type"]=="=")?"selected='true'":"" ?>><?php _e( 'Equals', 'i5pb-options' ); ?></option>
                            <option value="!=" <?php echo (isset($settings["WHERE"]["Condition2"]["Type"]) && $settings["WHERE"]["Condition2"]["Type"]=="!=")?"selected='true'":"" ?>><?php _e( 'Not Equal To', 'i5pb-options' ); ?></option>
                        </select>
                    </td>
                    <td>
                        <input name="WHValue2" type="text" value="<?php echo isset($settings["WHERE"]["Condition2"]["Value"])?$settings["WHERE"]["Condition2"]["Value"]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php _e( 'And', 'i5pb-options' ); ?>
                        </td>
                    <td>
                        <select name="WHField3">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["WHERE"]["Condition3"]["Field"]) && $settings["WHERE"]["Condition3"]["Field"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                        </td>
                    <td>
                        <select name="WHType3">
                            <option value="=" <?php echo (isset($settings["WHERE"]["Condition3"]["Type"]) && $settings["WHERE"]["Condition3"]["Type"]=="=")?"selected='true'":"" ?>><?php _e( 'Equals', 'i5pb-options' ); ?></option>
                            <option value="!=" <?php echo (isset($settings["WHERE"]["Condition3"]["Type"]) && $settings["WHERE"]["Condition3"]["Type"]=="!=")?"selected='true'":"" ?>><?php _e( 'Not Equal To', 'i5pb-options' ); ?></option>
                        </select>
                    </td>
                    <td>
                        <input name="WHValue3" type="text" value="<?php echo isset($settings["WHERE"]["Condition3"]["Value"])?$settings["WHERE"]["Condition3"]["Value"]:""?>" />
                    </td>
                </tr>
            </table>
  </div>
  <div id="tabs-10">
    <table>
                <tr>
                    <th colspan="4"><?php _e( 'Delete Rules', 'i5pb-options' ); ?><br/><?php _e( 'Listings Matching The Following Conditions will Be Deleted', 'i5pb-options' ); ?></th>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <select name="DELField1">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["DELETE"]["Condition1"]["Field"]) && $settings["DELETE"]["Condition1"]["Field"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                        </td>
                    <td>
                        <select name="DELType1">
                            <option value="=" <?php echo (isset($settings["DELETE"]["Condition1"]["Type"]) && $settings["DELETE"]["Condition1"]["Type"]=="=")?"selected='true'":"" ?>><?php _e( 'Equals', 'i5pb-options' ); ?></option>
                            <option value="!=" <?php echo (isset($settings["DELETE"]["Condition1"]["Type"]) && $settings["DELETE"]["Condition1"]["Type"]=="!=")?"selected='true'":"" ?>><?php _e( 'Not Equal To', 'i5pb-options' ); ?></option>
                        </select>
                    </td>
                    <td>
                        <input name="DELValue1" type="text" value="<?php echo isset($settings["DELETE"]["Condition1"]["Value"])?$settings["DELETE"]["Condition1"]["Value"]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                       <?php _e( 'Or', 'i5pb-options' ); ?>
                        </td>
                    <td>
                        <select name="DELField2">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["DELETE"]["Condition2"]["Field"]) && $settings["DELETE"]["Condition2"]["Field"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                        </td>
                    <td>
                        <select name="DELType2">
                            <option value="=" <?php echo (isset($settings["DELETE"]["Condition2"]["Type"]) && $settings["DELETE"]["Condition2"]["Type"]=="=")?"selected='true'":"" ?>><?php _e( 'Equals', 'i5pb-options' ); ?></option>
                            <option value="!=" <?php echo (isset($settings["DELETE"]["Condition2"]["Type"]) && $settings["DELETE"]["Condition2"]["Type"]=="!=")?"selected='true'":"" ?>><?php _e( 'Not Equal To', 'i5pb-options' ); ?></option>
                        </select>
                    </td>
                    <td>
                        <input name="DELValue2" type="text" value="<?php echo isset($settings["DELETE"]["Condition2"]["Value"])?$settings["DELETE"]["Condition2"]["Value"]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php _e( 'Or', 'i5pb-options' ); ?>
                        </td>
                    <td>
                        <select name="DELField3">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["DELETE"]["Condition3"]["Field"]) && $settings["DELETE"]["Condition3"]["Field"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                        </td>
                    <td>
                        <select name="DELType3">
                            <option value="=" <?php echo (isset($settings["DELETE"]["Condition3"]["Type"]) && $settings["DELETE"]["Condition3"]["Type"]=="=")?"selected='true'":"" ?>>Equals</option>
                            <option value="!=" <?php echo (isset($settings["DELETE"]["Condition3"]["Type"]) && $settings["DELETE"]["Condition3"]["Type"]=="!=")?"selected='true'":"" ?>>Not Equal To</option>
                        </select>
                    </td>
                    <td>
                        <input name="DELValue3" type="text" value="<?php echo isset($settings["DELETE"]["Condition3"]["Value"])?$settings["DELETE"]["Condition3"]["Value"]:""?>" />
                    </td>
                </tr>
            </table>
  </div>
</div>
        
        
        
       
        
        <?php
			break;
			case 'prospect' :
		?>
        <div class="ui-widget-content">
        <table>
        		<tr>
                    <th colspan="2" scope="row"><h3><?php _e( 'Prospect Mappings', 'i5pb-options' ); ?></h3></th>
                </tr>
                <tr>
                    <th scope="row"><?php _e( 'Save Contacts on Registration:', 'i5pb-options' ); ?></th>
                    <td>
                        <input name="EnableWebProspect" type="checkbox" <?php echo (isset($settings["Prospect"]) && $settings["Prospect"])?'checked="true"':'' ?> />
                    </td>
                </tr>
                
                <tr>
                    <th><?php _e( 'PropertyBase Field', 'i5pb-options' ); ?></th>
                    <th><?php _e( 'WordPress Field', 'i5pb-options' ); ?></th>
                </tr>
                <tr>
                    <th>
                        <?php _e( 'FirstName', 'i5pb-options' ); ?>
                    </th>
                    <td>
                        <input name="MapFirstName" type="text" value="<?php echo isset($settings["ProspectMappings"]["FirstName"])?$settings["ProspectMappings"]["FirstName"]:""?>" />
                    </td>
                </tr>
                <tr>
                    <th>
                        <?php _e( 'LastName', 'i5pb-options' ); ?>
                    </th>
                    <td>
                        <input name="MapLastName" type="text" value="<?php echo isset($settings["ProspectMappings"]["LastName"])?$settings["ProspectMappings"]["LastName"]:""?>" />
                    </td>
                </tr>
                <tr>
                    <th>
                        <?php _e( 'Email', 'i5pb-options' ); ?>
                    </th>
                    <td>
                        <input name="MapEmail" type="text" value="<?php echo isset($settings["ProspectMappings"]["Email"])?$settings["ProspectMappings"]["Email"]:""?>" />
                    </td>
                </tr>
                <tr>
                    <th>
                        <?php _e( 'Phone', 'i5pb-options' ); ?>
                    </th>
                    <td>
                        <input name="MapPhone" type="text" value="<?php echo isset($settings["ProspectMappings"]["Phone"])?$settings["ProspectMappings"]["Phone"]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input name="MapCust1PB" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust1"][0])?$settings["ProspectMappings"]["MapCust1"][0]:""?>" />
                    </td>
                    <td>
                        <input name="MapCust1WP" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust1"][1])?$settings["ProspectMappings"]["MapCust1"][1]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input name="MapCust2PB" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust2"][0])?$settings["ProspectMappings"]["MapCust2"][0]:""?>" />
                    </td>
                    <td>
                        <input name="MapCust2WP" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust2"][1])?$settings["ProspectMappings"]["MapCust2"][1]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input name="MapCust3PB" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust3"][0])?$settings["ProspectMappings"]["MapCust3"][0]:""?>" />
                    </td>
                    <td>
                        <input name="MapCust3WP" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust3"][1])?$settings["ProspectMappings"]["MapCust3"][1]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input name="MapCust4PB" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust4"][0])?$settings["ProspectMappings"]["MapCust4"][0]:""?>" />
                    </td>
                    <td>
                        <input name="MapCust4WP" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust4"][1])?$settings["ProspectMappings"]["MapCust4"][1]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input name="MapCust5PB" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust5"][0])?$settings["ProspectMappings"]["MapCust5"][0]:""?>" />
                    </td>
                    <td>
                        <input name="MapCust5WP" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust5"][1])?$settings["ProspectMappings"]["MapCust5"][1]:""?>" />
                    </td>
                </tr>
                <tr>
                    <th colspan="2">
                        <?php _e( 'Save Search Settings', 'i5pb-options' ); ?>
                    </th>
                </tr>
                
            </tbody>
        </table>
        </div>
        <?php
			break;
			case 'contact' :
		?>
        <div class="ui-widget-content">
        <table>
        	<tr>
                    <th><?php _e( 'Contact Form Id', 'i5pb-options' ); ?></th>
                    <td>
                        <input name="MapSaveSearchId" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapSaveSearchId"])?$settings["ProspectMappings"]["MapSaveSearchId"]:""?>" />
                    </td>
                </tr>
        </table>
        </div>
		<?php
			break;
		}
 ?>
   		
   
        

		
        <?php submit_button(); 
        }
        else
        {
            if($doOAuth)
            {
                if($settings["Debug"]=="Yes")
                    echo "<script>window.location='https://test.salesforce.com/services/oauth2/authorize?response_type=code&client_id=" . $_POST["ClientId"] . "&redirect_uri=" . urlencode('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']) . "';</script>";
                else
                    echo "<script>window.location='https://login.salesforce.com/services/oauth2/authorize?response_type=code&client_id=" . $_POST["ClientId"] . "&redirect_uri=" . urlencode('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']) . "';</script>";
            }
        ?>
<h1><?php _e( 'OAuth Configuration', 'i5pb-options' ); ?></h1>
<h3><?php _e( 'You must authorize Wordpress to be able to access your salesforce site.', 'i5pb-options' ); ?></h3>
<table class="form-table">
    <tbody>
        <tr>
            <th scope="row"><?php _e( 'Sandbox', 'i5pb-options' ); ?></th>
            <td>
                        <input name="WebListingDebug" type="checkbox" <?php echo $settings["Debug"]=='Yes'? 'checked="true"':'' ?> />
            </td>
        </tr>
        <tr>
            <th scope="row"><?php _e( 'Client ID', 'i5pb-options' ); ?>
            </th>
            <td>
                        <input name="ClientId" type="text" value="<?php echo isset($settings["OAuth"]['OAuthClientId'])?$settings["OAuth"]['OAuthClientId']:'' ?>" />
            </td>
        </tr>
        <tr>
            <th scope="row"><?php _e( 'Secret', 'i5pb-options' ); ?></th>
            <td>
                        <input name="Secret" type="text" value="<?php echo isset($settings["OAuth"]['OAuthSecret'])?$settings["OAuth"]['OAuthSecret']:'' ?>" />
            </td>
        </tr>
        </tbody>
    </table>
        <?php submit_button(); ?>
        
        <?php
        }
        ?>
    </form>
</div>
<?php
	 
    }
	
	public static function fieldsHtmlSelect($field){

		 $settings = get_option('WebListing_Settings');
		
		 $items = self::getPBFields($settings);
           
		 asort($items);
		
		foreach($items as $key => $value)
		{
			if(isset($settings["SelectFields"][$field]) && $settings["SelectFields"][$field]==$key)
				echo "<option value='$key' selected='true'>" . $value . "</option>";
			else
				echo "<option value='$key'>" . $value . "</option>";
		}

	}
	
	private static function getPBFields($settings){
		

        $vals = i5PBIntegration_Functions::GetApi($settings["OAuth"],"/services/data/v20.0/sobjects/pba__listing__c/describe");
		
		//print_r($vals);
		
        $items=array();

        $items[""]="";

        foreach($vals->fields as $field)
        {
            $items[$field->name]=$field->label;// . "(" . $field->name . ")";
        }
        
        return $items;
    } 
 }

add_action( 'init', array( 'I5propertybase_Admin_Display', 'init' ) );

add_action('SyncListings', array('i5PBIntegration_Functions', 'mlsSync'));




