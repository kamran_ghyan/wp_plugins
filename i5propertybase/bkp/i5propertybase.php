<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.i5fusion.com
 * @since             1.0.0
 * @package           I5propertybase
 *
 * @wordpress-plugin
 * Plugin Name:       i5propertybase
 * Plugin URI:        http://www.i5fusion.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Jarret Fisher
 * Author URI:        http://www.i5fusion.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       i5propertybase
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-i5propertybase-activator.php
 */
function activate_i5propertybase() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-i5propertybase-activator.php';
	I5propertybase_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-i5propertybase-deactivator.php
 */
function deactivate_i5propertybase() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-i5propertybase-deactivator.php';
	I5propertybase_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_i5propertybase' );
register_deactivation_hook( __FILE__, 'deactivate_i5propertybase' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-i5propertybase.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_i5propertybase() {

	$plugin = new I5propertybase();
	$plugin->run();

}

run_i5propertybase();

