<?php
class i5PBIntegration_Functions
{
	private static $initiated=false;
    private static $isRunning=false;
	
    public static function GetVersions($oAuth)
    {
        $ch = curl_init($oAuth["InstanceUrl"] . "/services/data/");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array("Authorization: OAuth " . $oAuth["OAuthToken"]));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($ch);

        echo $resp;
    }
	
	
    public static function GetAPI($oAuth, $methodUrl)
    {
        $ch = curl_init($oAuth["InstanceUrl"] . $methodUrl);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array("Authorization: OAuth " . $oAuth["OAuthToken"]));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($ch);

        if(FALSE == $resp)
        {
            $err = curl_error($ch);
        }

        curl_close($ch);

        $vals = json_decode($resp);

        if(is_array($vals) && isset($vals[0]->errorCode) && $vals[0]->errorCode == "INVALID_SESSION_ID")
        {
            $refresh = i5PBIntegration_Functions::RefreshToken($oAuth);

            if(isset($refresh->access_token))
            {
                $settings=get_option('WebListing_Settings');

                $settings["OAuth"]["OAuthToken"]=$refresh->access_token;
                $settings["OAuth"]["InstanceUrl"]=$refresh->instance_url;
                update_option("WebListing_Settings",$settings);

                return self::GetAPI($settings["OAuth"],$methodUrl);
            }
        }
        else
            return $vals;
    }
	
	
    public static function PostAPI($oAuth,$methodUrl,$postData,$method="POST")
    {
        $ch=curl_init($oAuth["InstanceUrl"] . $methodUrl);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        if(isset($postData))
        {
            $jData=json_encode($postData);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jData);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array("Authorization: OAuth " . $oAuth["OAuthToken"],'Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $resp=curl_exec($ch);

        if(FALSE==$resp)
        {
            $err=curl_error($ch);
        }

        curl_close($ch);

        $vals = json_decode($resp);

        if(is_array($vals) && isset($vals[0]->errorCode) && $vals[0]->errorCode=="INVALID_SESSION_ID")
        {
            $refresh = i5PBIntegration_Functions::RefreshToken($oAuth);

            if(isset($refresh->access_token))
            {
                $settings=get_option('WebListing_Settings');

                $settings["OAuth"]["OAuthToken"]=$refresh->access_token;
                $settings["OAuth"]["InstanceUrl"]=$refresh->instance_url;
                update_option("WebListing_Settings",$settings);

                return self::PostAPI($settings["OAuth"],$methodUrl,$postData);
            }
        }
        else
            return $vals;
    }
	
	
    public static function DeleteAPI($oAuth,$methodUrl)
    {
        $ch=curl_init($oAuth["InstanceUrl"] . $methodUrl);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array("Authorization: OAuth " . $oAuth["OAuthToken"],'Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $resp=curl_exec($ch);

        if(FALSE==$resp)
        {
            $err=curl_error($ch);
        }

        curl_close($ch);

        $vals = json_decode($resp);

        if(is_array($vals) && isset($vals[0]->errorCode) && $vals[0]->errorCode=="INVALID_SESSION_ID")
        {
            $refresh = i5PBIntegration_Functions::RefreshToken($oAuth);

            if(isset($refresh->access_token))
            {
                $settings=get_option('WebListing_Settings');

                $settings["OAuth"]["OAuthToken"]=$refresh->access_token;
                $settings["OAuth"]["InstanceUrl"]=$refresh->instance_url;
                update_option("WebListing_Settings",$settings);

                return self::PostAPI($settings["OAuth"],$methodUrl,$postData);
            }
        }
        else
            return $vals;
    }
	
	
    public static function RefreshToken($oAuth)
    {
        $body="grant_type=refresh_token&client_id=" . $oAuth["OAuthClientId"] . "&client_secret=" . $oAuth["OAuthSecret"] . "&refresh_token=" . $oAuth["RefreshToken"];
        $ch=curl_init($oAuth["InstanceUrl"] . "/services/oauth2/token");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $body);

        $ref=curl_exec($ch);

        if(FALSE==$ref)
        {
            $err=curl_error($ch);
        }

        return json_decode($ref);
    }
	
	
	public static function mlsSync()
    {
        $settings  = get_option('WebListing_Settings');
		
        if(!self::$isRunning && isset($settings) && isset($settings["EnableSync"]) && $settings["EnableSync"]=="Yes" && isset($settings["OAuth"]) && isset($settings["OAuth"]["OAuthToken"]) && (!isset($settings["Status"]) || $settings["Status"]=="Idle"))
        {
            try
            {
				
			
            self::$isRunning = true;
			
            $settings["Status"] = "Running";

            //self::removeExpired();

            global $wpdb;

            if(sizeof($settings["SelectFields"])>0)
            {
                $currRun = date("Y-m-d\Th:i:s");
                $lastRun = date("Y-m-d\Th:i:s",strtotime("January 1 1900"));

                //if(isset($settings["LastRun"]))
                  //  $lastRun = $settings["LastRun"];

                
                
				$query = "SELECT Id,(Select pba__ExternalLink__c,pba__url__c From pba__PropertyMedia__r),(select Id,Name,CreatedDate,LastModifiedDate,";
                $where = " where LastModifiedDate>" . $lastRun . "Z";

                foreach($settings["SelectFields"] as $key => $field)
                {
                    if($field!="Id")
                        $query.= $field . ",";
                }

                if(isset($settings["WHERE"]))
                {
                    if(isset($settings["WHERE"]["Condition1"]))
                    {
                        $values = explode(";",$settings["WHERE"]["Condition1"]["Value"]);

                        if(sizeof($values)>1)
                        {
                            $where .= " and " . $settings["WHERE"]["Condition1"]["Field"];

                            if($settings["WHERE"]["Condition1"]["Type"] == "!=")
                                $where .= " not";

                            $where .= " in (";

                            foreach($values as $value)
                                $where .= ((strtolower($value) == "true"||strtolower($value) == "false")?"":"'") . $value . ((strtolower($value) == "true"||strtolower($value) == "false")?"":"'") . ",";

                            $where = rtrim($where,",") . ")";
                        }
                        else
                            $where .= " and " . $settings["WHERE"]["Condition1"]["Field"] . $settings["WHERE"]["Condition1"]["Type"] . ((strtolower($values[0]) == "true"||strtolower($values[0]) == "false")?"":"'") . $values[0] . ((strtolower($values[0]) == "true"||strtolower($values[0]) == "false")?"":"'");
                    }

                    if(isset($settings["WHERE"]["Condition2"]))
                    {
                        $values = explode(";",$settings["WHERE"]["Condition2"]["Value"]);

                        if(sizeof($values)>1)
                        {
                            $where .= " and " . $settings["WHERE"]["Condition2"]["Field"];

                            if($settings["WHERE"]["Condition2"]["Type"]=="!=")
                                $where .= " not";

                            $where .= " in (";

                            foreach($values as $value)
                                $where .= ((strtolower($value) == "true"||strtolower($value) == "false")?"":"'") . $value . ((strtolower($value) == "true"||strtolower($value) == "false")?"":"'") . ",";

                            $where = rtrim($where,",") . ")";
                        }
                        else
                            $where .= " and " . $settings["WHERE"]["Condition2"]["Field"] . $settings["WHERE"]["Condition2"]["Type"] . ((strtolower($values[0]) == "true"||strtolower($values[0]) == "false")?"":"'") . $values[0] . ((strtolower($values[0]) == "true"||strtolower($values[0]) == "false")?"":"'");
                    }

                    if(isset($settings["WHERE"]["Condition3"]))
                    {
                        $values = explode(";",$settings["WHERE"]["Condition3"]["Value"]);

                        if(sizeof($values)>1)
                        {
                            $where .= " or " . $settings["WHERE"]["Condition3"]["Field"];

                            if($settings["WHERE"]["Condition3"]["Type"] == "!=")
                                $where .= " not";

                            $where .= " in (";

                            foreach($values as $value)
                                $where .= ((strtolower($value) == "true"||strtolower($value) == "false")?"":"'") . $value . ((strtolower($value) == "true"||strtolower($value) == "false")?"":"'") . ",";

                            $where = rtrim($where,",") . ")";
                        }
                        else
                            $where .= " and " . $settings["WHERE"]["Condition3"]["Field"] . $settings["WHERE"]["Condition3"]["Type"] . ((strtolower($values[0]) == "true"||strtolower($values[0]) == "false")?"":"'") . $values[0] . ((strtolower($values[0]) == "true"||strtolower($values[0]) == "false")?"":"'");
                    }
                }

                $query = rtrim($query,",");

                $query .= " from pba__listings__r" . $where . ") from pba__property__c where id in (select pba__property__c from pba__listing__c" . $where . ")" ;

                $done = false;
				
                $methodUrl = "/services/data/v20.0/query/?q=" . urlencode($query);
                
				$fields = $settings["SelectFields"];
                
				$current = 0;

                while(!$done)
                {
                    $results = self::GetAPI($settings["OAuth"], $methodUrl);

                    if(isset($results->totalSize) && $results->totalSize>0)
                    {
                        $done = $results->done;

                        if(!$done)
                            $methodUrl = $results->nextRecordsUrl;

                        foreach($results->records as $property)
                        {
                            try
                            {
                            $current+=1;
							
                            $settings["Status"]="Syncing Property: " . $current . " of " .$results->totalSize;
							
                            update_option("WebListing_Settings",$settings);

                            if(isset($property->pba__Listings__r))
                            {
                                foreach($property->pba__Listings__r->records as $record)
                                {
			
			// post fields
			
			$author_id = 1;
			$post_type = 'property';			
			$url = strtolower(str_replace(" ", "-", str_replace(",", "", $record->Name)));
			$post_date = current_time( 'mysql' );
			
			
			// Prepare query for custom post type
			$wpdb->query( $wpdb->prepare( 
				"
					INSERT INTO $wpdb->posts
					( post_author, post_date, post_date_gmt post_title, post_name, post_type )
					VALUES ( %d, %s, %s, %s, %s, %s )
				", 
				array(
					$author_id, 
					$post_date,
					$post_date,
					$record->Name, 
					$url,
					$post_type
				) 
			) );
			
			// Get last post ID
			$post_id = $wpdb->insert_id;
				
			// General fields
			$plisttype 							= isset($fields["PBSListType"])?$record->$fields["PBSListType"]:null;
			$ptype 								= isset($fields["PBSPropertyType"])?$record->$fields["PBSPropertyType"]:null;
			$pmls 								= $record->$fields["PBSMLSID"];
			$psbuilduparea 						= isset($fields["PBSSize"])?$record->$fields["PBSSize"]:null;
			$propertyroom 						= isset($fields["PBSBeds"])?$record->$fields["PBSBeds"]:null;
			$propertyotherroom 					= isset($fields["PBSOtherBeds"])?$record->$fields["PBSOtherBeds"]:null;
			$pfullbath 							= isset($fields["PBSFullBaths"])?$record->$fields["PBSFullBaths"]:null;
			$phalfbath 							= isset($fields["PBSHalfBaths"])?$record->$fields["PBSHalfBaths"]:nulll;
			$plostsoft 							= isset($fields["PBSLotSize"])?$record->$fields["PBSLotSize"]:null;
			$pdescription  						= isset($fields["PBSDescription"])?$record->$fields["PBSDescription"]:null;
			$pbuiltupyear 						= isset($fields["PBSYearBuilt"])?$record->$fields["PBSYearBuilt"]:null;
			$pstyle 							= isset($fields["PBSStyle"])?$record->$fields["PBSStyle"]:null;
			$pflooring 							= isset($fields["PBSFlooring"])?$record->$fields["PBSFlooring"]:null;
			$pnooffloor 						= isset($fields["PBSStories"])?$record->$fields["PBSStories"]:null;
			$pgoodsincluded 					= isset($fields["PBSAppliances"])?$record->$fields["PBSAppliances"]:null;
			$pcurrentstatus 					= isset($fields["PBSStatus"])?$record->$fields["PBSStatus"]:null;
			$punitnumber 						= isset($fields["PBSUnitNumber"])?$record->$fields["PBSUnitNumber"]:null;
			$price 	 							= isset($fields["PBSPrice"])?$record->$fields["PBSPrice"]:null;
			$pprice 							= isset($fields["PBSPrice"])?$record->$fields["PBSPrice"]:null;
			$pgarage 							= isset($fields["PBSGarage"])?$record->$fields["PBSGarage"]:null;
			$pnoofgarage 						= isset($fields["PBSNoOfGarage"])?$record->$fields["PBSNoOfGarage"]:null;
				
			// Amenities fields
			$pnoofparking 						= isset($fields["PBSParkingSpaces"])?$record->$fields["PBSParkingSpaces"]:null;
			$pparking 							= isset($fields["PBSParkingDesc"])?$record->$fields["PBSParkingDesc"]:null;
			$pfireexting 						= isset($fields["PBSrecordType"])?$record->$fields["PBSrecordType"]:null;
			$propertyswimmingpool  				= isset($fields["PBSPool"])?$record->$fields["PBSPool"]:null;
			$pbasekballcourt  					= isset($fields["PBSPool"])?$record->$fields["PBSPool"]:null;
			$pdoorman			  				= isset($fields["PBSPool"])?$record->$fields["PBSPool"]:null;
			$pgym				  				= isset($fields["PBSPool"])?$record->$fields["PBSPool"]:null;
			$plaundry			  				= isset($fields["PBSPool"])?$record->$fields["PBSPool"]:null;
			$pprivatespaces		  				= isset($fields["PBSPool"])?$record->$fields["PBSPool"]:null;
			$pwifi				  				= isset($fields["PBSPool"])?$record->$fields["PBSPool"]:null;
				
			// Interiors fields
			$pbasement 							= isset($fields["PBSBasement"])?$record->$fields["PBSBasement"]:null;			
			$pbasementtype 						= isset($fields["PBSBasementType"])?$record->$fields["PBSBasementType"]:null;
			$pheating 							= isset($fields["PBSHeating"])?$record->$fields["PBSHeating"]:null;
			$pconstruction 						= isset($fields["PBSNewConstruction"])?$record->$fields["PBSNewConstruction"]:null;
			
			// Exteriors fields
			$proof 								= isset($fields["PBSRoof"])?$record->$fields["PBSRoof"]:null;
			$pfoundation 						= isset($fields["PBSFoundationType"])?$record->$fields["PBSFoundationType"]:null;
			$pexteriorfeature 					= isset($fields["PBSExteriorFeatures"])?$record->$fields["PBSExteriorFeatures"]:null;
			$pexteriorsliding 					= isset($fields["PBSExteriorSiding"])?$record->$fields["PBSExteriorSiding"]:null;
			$pfrontagemeter 					= isset($fields["PBSFrontageMeters"])?$record->$fields["PBSFrontageMeters"]:null;
			$pfruntexposure 					= isset($fields["PBSFruntExposure"])?$record->$fields["PBSFruntExposure"]:null;
			
			// Diemension fields
			$proomdiemension 				= isset($fields["PBSRoomDiemension"])?$record->$fields["PBSRoomDiemension"]:null;
			$pbathdiemension 				= isset($fields["PBSBathroomDiemension"])?$record->$fields["PBSBathroomDiemension"]:null;
			$pkitchendiemension 			= isset($fields["PBSKitchenDiemension"])?$record->$fields["PBSKitchenDiemension"]:null;
			$ppooldiemension 		= isset($fields["PBSSwimmingPoolDiemension"])?$record->$fields["PBSSwimmingPoolDiemension"]:null;
			$proomdiemension 				= isset($fields["PBSGymDiemension"])?$record->$fields["PBSGymDiemension"]:null;
			
			// Address fields
			$paddress 							= isset($fields["PBSAddress"])?$record->$fields["PBSAddress"]:null;
			$pstate 							= isset($fields["PBSState"])?$record->$fields["PBSState"]:null;
			$pcity 								= isset($fields["PBSCity"])?$record->$fields["PBSCity"]:null;
			$platitude 							= isset($fields["PBSLatitude"])?$record->$fields["PBSLatitude"]:null;
            $plognitude 						= isset($fields["PBSLongitude"])?$record->$fields["PBSLongitude"]:null;
			$pzipcode 							= isset($fields["PBSrecordType"])?$record->$fields["PBSrecordType"]:null;
			
			// Agent fields
			$pagentfname						= isset($fields["PBSAgentFirstName"])?$record->$fields["PBSAgentFirstName"]:null;
			$pagentlname						= isset($fields["PBSAgentLastName"])?$record->$fields["PBSAgentLastName"]:null;
			$pagentmobilr 						= isset($fields["PBSAgentMobile"])?$record->$fields["PBSAgentMobile"]:null;
			$pagentoffice 						= isset($fields["PBSAgentOfficePhone"])?$record->$fields["PBSAgentOfficePhone"]:null;
			$pagentemail 						= isset($fields["PBSAgentEmail"])?$record->$fields["PBSAgentEmail"]:null;
			$pagentmlsid 						= isset($fields["PBSAgentMLSID"])?$record->$fields["PBSAgentMLSID"]:null;
			$pagentlistingoffice				= isset($fields["PBSListingOffice"])?$record->$fields["PBSListingOffice"]:null;
			
			// Other fields
			$pfencing 							= isset($fields["PBSFenceDesc"])?$record->$fields["PBSFenceDesc"]:null;	
			$pwaterdesc							= isset($fields["PBSWaterfrontDesc"])?$record->$fields["PBSWaterfrontDesc"]:null;	
			$pnoofaircondition					= isset($fields["PBSCooling"])?$record->$fields["PBSCooling"]:null;	
				
				
			// General metapost for property post type					
			add_post_meta( $post_id, 'plisttype', $plisttype );
			add_post_meta( $post_id, 'ptype', $ptype );
			add_post_meta( $post_id, 'pmls', $pmls );
			add_post_meta( $post_id, 'psbuilduparea', $psbuilduparea );
			add_post_meta( $post_id, 'propertyroom', $propertyroom );
			add_post_meta( $post_id, 'propertyotherroom', $propertyotherroom );
			add_post_meta( $post_id, 'pfullbath', $pfullbath );
			add_post_meta( $post_id, 'phalfbath', $phalfbath );
			add_post_meta( $post_id, 'plostsoft', $plostsoft );
			add_post_meta( $post_id, 'pdescription', $pdescription );
			add_post_meta( $post_id, 'pbuiltupyear', $pbuiltupyear );
			add_post_meta( $post_id, 'pstyle', $pstyle );
			add_post_meta( $post_id, 'pflooring', $pflooring );
			add_post_meta( $post_id, 'pnooffloor', $pnooffloor );
			add_post_meta( $post_id, 'pgoodsincluded', $pgoodsincluded );
			add_post_meta( $post_id, 'pcurrentstatus', $pcurrentstatus );
			add_post_meta( $post_id, 'punitnumber', $punitnumber );
			add_post_meta( $post_id, 'price', $price );
			add_post_meta( $post_id, 'pprice', $pprice );
			add_post_meta( $post_id, 'pgarage', $pgarage );
			add_post_meta( $post_id, 'pnoofgarage', $pnoofgarage );
			
			// Amenities metapost for property post type
			add_post_meta( $post_id, 'pnoofparking', $pnoofparking );
			add_post_meta( $post_id, 'pparking', $pparking );
			add_post_meta( $post_id, 'pfireexting', $pfireexting );
			add_post_meta( $post_id, 'propertyswimmingpool', $propertyswimmingpool );
			add_post_meta( $post_id, 'pbasekballcourt', $pbasekballcourt );
			add_post_meta( $post_id, 'pdoorman', $pdoorman );
			add_post_meta( $post_id, 'pgym', $pgym );
			add_post_meta( $post_id, 'plaundry', $plaundry );
			add_post_meta( $post_id, 'pprivatespaces', $pprivatespaces );
			add_post_meta( $post_id, 'pwifi', $pwifi );
			
			// Interiors metapost for property post type
			add_post_meta( $post_id, 'pbasement', $pbasement );
			add_post_meta( $post_id, 'pbasementtype', $pbasementtype );
			add_post_meta( $post_id, 'pheating', $pheating );
			add_post_meta( $post_id, 'pconstruction', $pconstruction );
			
			// Exteriors metapost for property post type
			add_post_meta( $post_id, 'proof', $proof );
			add_post_meta( $post_id, 'pfoundation', $pfoundation );
			add_post_meta( $post_id, 'pexteriorfeature', $pexteriorfeature );
			add_post_meta( $post_id, 'pexteriorsliding', $pexteriorsliding );
			add_post_meta( $post_id, 'pfrontagemeter', $pfrontagemeter );
			add_post_meta( $post_id, 'pfruntexposure', $pfruntexposure );
			
			// Diemension metapost for property post type
			add_post_meta( $post_id, 'proomdiemension', $proomdiemension );
			add_post_meta( $post_id, 'pbathdiemension', $pbathdiemension );
			add_post_meta( $post_id, 'pkitchendiemension', $pkitchendiemension );
			add_post_meta( $post_id, 'ppooldiemension', $ppooldiemension );
			add_post_meta( $post_id, 'proomdiemension', $proomdiemension );
			
			// Address metapost for property post type
			add_post_meta( $post_id, 'paddress', $paddress );
			add_post_meta( $post_id, 'pstate', $pstate );
			add_post_meta( $post_id, 'pcity', $pcity );
			add_post_meta( $post_id, 'platitude', $platitude );
			add_post_meta( $post_id, 'plognitude', $plognitude );
			add_post_meta( $post_id, 'pzipcode', $pzipcode );
			
			// Agent metapost for property post type
			add_post_meta( $post_id, 'pagentfname', $pagentfname );
			add_post_meta( $post_id, 'pagentlname', $pagentlname );
			add_post_meta( $post_id, 'pagentmobilr', $pagentmobilr );
			add_post_meta( $post_id, 'pagentoffice', $pagentoffice );
			add_post_meta( $post_id, 'pagentemail', $pagentemail );
			add_post_meta( $post_id, 'pagentmlsid', $pagentmlsid );
			add_post_meta( $post_id, 'pagentlistingoffice', $pagentlistingoffice );
			
			// Other fields
			add_post_meta( $post_id, 'pfencing', $pfencing );
			add_post_meta( $post_id, 'pwaterdesc', $pwaterdesc );
			add_post_meta( $post_id, 'pnoofaircondition', $pnoofaircondition );	
				
				
			//Save images
			if(isset($property->pba__PropertyMedia__r))
			{
				//Delete all images first
				$sql = "DELETE FROM {$wpdb->prefix}postmeta where post_id=%s";
				$sql = $wpdb->prepare($sql, $post_id);
				$result = $wpdb->query($sql);
				
				// Image Gallery array
				$pgalleryarray = array();
				
				foreach($property->pba__PropertyMedia__r->records as $key => $media):
				
					$pgalleryarray[$key] =  array( 'pgallery' => $media->pba__URL__c );
					
				endforeach;
				
				// Add image gallery in to metapost table
				add_post_meta($post_id, 'propertygallery', $pgalleryarray);
			}
                                }
                            }
                            
                            }
                            catch(Exception $ex)
                            {
                                //todo log error
                            }
                        }
                    }
                    else
                        $done = true;
                }

                $settings["LastRun"] = $currRun;
                $settings["Status"] = "Idle";
                update_option("WebListing_Settings", $settings);
                self::$isRunning = false;
            }
            }
            catch(Exception $ex)
            {
                //todo log error
                $settings["Status"]="Idle";
                update_option("WebListing_Settings",$settings);
                self::$isRunning=false;
            }
			
		}else if((isset($settings["Status"]) || $settings["Status"]!="Idle"))
        {
            $lastRun=date("Y-m-d H:i:s",strtotime($settings["LastRun"]));
            $today = date("Y-m-d H:i:s");

            if($lastRun->modify("+2 hours")<$today)
            {
                $settings["Status"] = "Idle";
                update_option("WebListing_Settings",$settings);
                self::$isRunning = false;
            }
        }
        
        
    }
	
	
	 public static function removeExpired()
    {
        try
        {
            global $wpdb;
            $settings=get_option('WebListing_Settings');

            $end=date("Y-m-d\Th:i:s",strtotime("+2 days"));
            $start=date("Y-m-d\Th:i:s",strtotime("-29 days"));
            $lastRun=date("Y-m-d\Th:i:s",strtotime("January 1 1900"));

            if(isset($settings["LastRun"]))
                $lastRun=strtotime("-2 days", strtotime($settings["LastRun"]));

            $query="start=" . urlencode($start . "Z") . "&end=" . urlencode($end . "Z");

            $methodUrl="/services/data/v29.0/sobjects/pba__listing__c/deleted/?" . $query;

            require_once( i5PBIntegration__PLUGIN_DIR . 'class.i5PBIntegration-Functions.php' );
            $results =i5PBIntegration_Functions::GetAPI($settings["OAuth"],$methodUrl);
            $current=0;

            foreach($results->deletedRecords as $del)
            {
                try
                {
                    $current+=1;

                    $settings["Status"]="Processing Deleted Listings " . $current . " of " . sizeof($results->deletedRecords);
                    update_option("WebListing_Settings",$settings);

                    //Get the listing
                    $sql="SELECT propertyid from {$wpdb->prefix}i5listings where pbid=%s";
                    $sql=$wpdb->prepare($sql,$del->id);

                    $list = $wpdb->get_row($sql);

                    if($list!=null)
                    {
                        //Remove listings
                        $sql="DELETE FROM {$wpdb->prefix}i5listings where pbid=%s";

                        $sql=$wpdb->prepare($sql,$del->id);

                        $result = $wpdb->query($sql);

                        //Check to see if there are any more listings with the same property id
                        $sql="SELECT count(*) FROM {$wpdb->prefix}i5listings where propertyid=%s";

                        $sql=$wpdb->prepare($sql,$list->propertyid);

                        $ttl = $wpdb->get_var($sql);

                        if($ttl==0)
                        {
                            //Remove images
                            $sql="DELETE FROM {$wpdb->prefix}i5Images where propertyid=%s";

                            $sql=$wpdb->prepare($sql,$list->propertyid);

                            $result = $wpdb->query($sql);
                        }
                    }
                }
                catch(Exception $ex)
                {
                    $err=$ex;
                }
            }

            $current=0;

            //Remove Delete Condition listings
            if(isset($settings["DELETE"]))
            {
                $where=" where LastModifiedDate>" . $lastRun . "Z and ";
                $first=true;

                if(isset($settings["DELETE"]["Condition1"]))
                {
                    $values = explode(";",$settings["DELETE"]["Condition1"]["Value"]);

                    foreach($values as $value)
                    {
                        if(!$first)
                            $where .= " or ";
                        else
                        {
                            $where .= "(";
                            $first=false;
                        }

                        $where .= $settings["DELETE"]["Condition1"]["Field"] . $settings["DELETE"]["Condition1"]["Type"] . ((strtolower($value)=="true"||strtolower($value)=="false")?"":"'") . $value . ((strtolower($value)=="true"||strtolower($value)=="false")?"":"'");
                    }
                }

                $first=true;

                if(isset($settings["DELETE"]["Condition2"]))
                {
                    $values = explode(";",$settings["DELETE"]["Condition2"]["Value"]);

                    if(sizeof($values)>1)
                    {
                        $where .= " or " . $settings["DELETE"]["Condition2"]["Field"];

                        if($settings["DELETE"]["Condition2"]["Type"]=="!=")
                            $where .= " not";

                        $where .= " in (";

                        foreach($values as $value)
                            $where .= ((strtolower($value)=="true"||strtolower($value)=="false")?"":"'") . $value . ((strtolower($value)=="true"||strtolower($value)=="false")?"":"'") . ",";

                        $where = rtrim($where,",") . ")";
                    }
                    else
                        $where .= " or " . $settings["DELETE"]["Condition2"]["Field"] . $settings["DELETE"]["Condition2"]["Type"] . ((strtolower($values[0])=="true"||strtolower($values[0])=="false")?"":"'") . $values[0] . ((strtolower($values[0])=="true"||strtolower($values[0])=="false")?"":"'");
                }

                $first=true;

                if(isset($settings["DELETE"]["Condition3"]))
                {
                    $values = explode(";",$settings["DELETE"]["Condition3"]["Value"]);

                    if(sizeof($values)>1)
                    {
                        $where .= " or " . $settings["DELETE"]["Condition3"]["Field"];

                        if($settings["DELETE"]["Condition3"]["Type"]=="!=")
                            $where .= " not";

                        $where .= " in (";

                        foreach($values as $value)
                            $where .= ((strtolower($value)=="true"||strtolower($value)=="false")?"":"'") . $value . ((strtolower($value)=="true"||strtolower($value)=="false")?"":"'") . ",";

                        $where = rtrim($where,",") . ")";
                    }
                    else
                        $where .= " or " . $settings["DELETE"]["Condition3"]["Field"] . $settings["DELETE"]["Condition3"]["Type"] . ((strtolower($values[0])=="true"||strtolower($values[0])=="false")?"":"'") . $values[0] . ((strtolower($values[0])=="true"||strtolower($values[0])=="false")?"":"'");
                }

                if(isset($settings["DELETE"]["Condition1"]))
                    $where .= ")";


                $query = "select id,pba__property__c from pba__listing__c" . $where;

                $done=false;
                $methodUrl="/services/data/v20.0/query/?q=" . urlencode($query);
                $current=0;

                while(!$done)
                {
                    $results =i5PBIntegration_Functions::GetAPI($settings["OAuth"],$methodUrl);

                    if(isset($results->totalSize) && $results->totalSize>0)
                    {
                        $done=$results->done;

                        if(!$done)
                            $methodUrl=$results->nextRecordsUrl;

                        foreach($results->records as $listing)
                        {
                            $current+=1;

                            $settings["Status"]="Processing Expired Listings " . $current . " of " . $results->totalSize;
                            update_option("WebListing_Settings",$settings);

                            //Delete the listing
                            $sql="DELETE FROM {$wpdb->prefix}i5listings where pbid=%s";

                            $sql=$wpdb->prepare($sql,$listing->Id);

                            $result = $wpdb->query($sql);

                            //Check to see if there are any more listings with this property id
                            $sql="SELECT count(*) FROM {$wpdb->prefix}i5listings where propertyid=%s";

                            $sql=$wpdb->prepare($sql,$listing->pba__Property__c);

                            $ttl = $wpdb->get_var($sql);

                            if($ttl==0)
                            {
                                //Remove images
                                $sql="DELETE FROM {$wpdb->prefix}i5Images where propertyid=%s";

                                $sql=$wpdb->prepare($sql,$del->pba__Property__c);

                                $result = $wpdb->query($sql);
                            }
                        }
                    }
                    else
                        $done=true;
                }
            }

        }
        catch(Exception $ex)
        {
            $err=$ex;
        }
    }
}