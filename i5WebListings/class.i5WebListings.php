<?php

class i5WebListings
{
    private static $initiated = false;
    private static $settings;

    public static function init(){
        if(!self::$initiated){
            self::$settings=get_option('WebListing_Settings');
            self::init_hooks();
            self::$initiated=true;
        }

        wp_register_script( "WebListings_Script", WP_PLUGIN_URL.'/i5WebListings/i5WebListings.js', array('jquery') );
        wp_localize_script( 'WebListings_Script', 'i5WebListingsAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ),'nonce' => wp_create_nonce('i5WebListings')));

        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'WebListings_Script' );
    }
    public static function init_hooks(){
        if(is_user_logged_in())
      		add_action('wp_ajax_queryListingsAjax',array('i5WebListings', 'queryListingsAjax'));
      	else
      		add_action('wp_ajax_nopriv_queryListingsAjax',array('i5WebListings', 'queryListingsAjax'));
    }
    public static function queryListingsAjax(){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], "i5WebListings")) {
            exit("Denied");
        }

        $results =json_encode(self::query_listings($_REQUEST['fields'],$_REQUEST['recordTypes'],$_REQUEST['sort'],$_REQUEST['page'],$_REQUEST['pageSize']));

        echo $results;

        die();
    }
    public static function query_listings($fields,$recordtypes="",$sort="",$page=0,$page_size=10){
	    if($page_size>200)
            $page_size=200;

        if(self::$settings)
		{
            $url=self::$settings["EndPoint"] ."/pba__webservicelistingsquery?token=" . self::$settings["Token"] . "&fields=" . $fields . "&page=" . $page . "&itemsperpage=" .  $page_size . (self::$settings["Debug"] ? "&debugmode=true":"");

			if($recordtypes!="")
				$url .= "&recordtypes=" . $recordtypes;

			if($sort!="")
				$url .= "&orderby=" . $sort;

			if($settings["Debug"]){
				echo($url);
      }
      
     // echo $url;
      
      $listingData=simplexml_load_file($url);

			if($listingData && isset($listingData->numberOfListings) && (int)$listingData->numberOfListings>0)
			{
				return $listingData;
			}
		}

		return null;
	}
}
