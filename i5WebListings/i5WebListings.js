function getListings(fields, recordTypes, sort, page, pageSize,callback) {
    jQuery.ajax({
        type: "post",
        dataType: "json",
        url: i5WebListingsAjax.ajaxurl,
        data: { action: "queryListingsAjax", nonce: i5WebListingsAjax.nonce, fields: fields, recordTypes: recordTypes, sort: sort, page: page, pageSize: pageSize },
        success: function (response) {
            callback(response);
        },
        error: function (err) {
            alert(err);
        }
    });
}