<?php

class i5WebListings_Prospect
{
    private static $initiated = false;
    private static $settings;

    public static function init(){
        if(!self::$initiated){
            self::$settings=get_option('WebListing_Settings');
            self::init_hooks();
            self::$initiated=true;
        }
      
      	//echo WP_PLUGIN_URL . '/i5WebListings/i5PBprospect.js';
      	wp_deregister_script('Prospect_Script');
      
        wp_register_script( "Prospect_Script", WP_PLUGIN_URL . '/i5WebListings/i5PBprospect.js', array('jquery') );
        wp_localize_script( 'Prospect_Script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ),'nonce' => wp_create_nonce('i5prospect')));        
			
        wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'Prospect_Script' );
    }

    public static function init_hooks(){
        add_action('wp_ajax_saveProspect',array('i5WebListings_Prospect', 'SaveProspect'));

        if(isset(self::$settings["Prospect"]) && self::$settings["Prospect"])
            add_action('user_register',array( 'i5WebListings_Prospect', 'ProcessProspect' ));
    }
    public static function SaveProspect(){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], "i5prospect")) {
            exit("Denied");
        }

        self::ProcessProspect(0);
    }
    public static function ProcessProspect($newId){
        if(self::$settings){
          	session_start();
          
            $url= self::$settings["EndPoint"] . "/services/apexrest/pba/webtoprospect/v1/";
            $redirect="";

            $userID = get_current_user_id();

            $contact=array();

            if($newId!=0)
            {
                $contact["pba__SystemExternalId__c"] = "WordPress" . $newId;
								$contact["Stage__c"]="Unclaimed Lead";
              
                if(isset(self::$settings["ProspectMappings"]["FirstName"]))
                    $contact["FirstName"] = $_REQUEST[self::$settings["ProspectMappings"]["FirstName"]];

                if(isset(self::$settings["ProspectMappings"]["LastName"]))
                    $contact["LastName"] = $_REQUEST[self::$settings["ProspectMappings"]["LastName"]];

                if(isset(self::$settings["ProspectMappings"]["Email"]))
                    $contact["Email"] = $_REQUEST[self::$settings["ProspectMappings"]["Email"]];

                if(isset(self::$settings["ProspectMappings"]["Phone"]))
                    $contact["Phone"] = $_REQUEST[self::$settings["ProspectMappings"]["Phone"]];

                if(isset(self::$settings["ProspectMappings"]["MapCust1"]))
                    $contact[self::$settings["ProspectMappings"]["MapCust1"][0]] = $_REQUEST[self::$settings["ProspectMappings"]["MapCust1"][1]];

                if(isset(self::$settings["ProspectMappings"]["MapCust2"]))
                    $contact[self::$settings["ProspectMappings"]["MapCust2"][0]] = $_REQUEST[self::$settings["ProspectMappings"]["MapCust2"][1]];

                if(isset(self::$settings["ProspectMappings"]["MapCust3"]))
                    $contact[self::$settings["ProspectMappings"]["MapCust3"][0]] = $_REQUEST[self::$settings["ProspectMappings"]["MapCust3"][1]];

                if(isset(self::$settings["ProspectMappings"]["MapCust4"]))
                    $contact[self::$settings["ProspectMappings"]["MapCust4"][0]] = $_REQUEST[self::$settings["ProspectMappings"]["MapCust4"][1]];

                if(isset(self::$settings["ProspectMappings"]["MapCust5"]))
                    $contact[self::$settings["ProspectMappings"]["MapCust5"][0]] = $_REQUEST[self::$settings["ProspectMappings"]["MapCust5"][1]];
              
              if(isset(self::$settings["ProspectMappings"]["MapCust6"]))
                    $contact[self::$settings["ProspectMappings"]["MapCust6"][0]] = $_REQUEST[self::$settings["ProspectMappings"]["MapCust6"][1]];

                if(isset(self::$settings["ProspectMappings"]["MapCust7"]))
                    $contact[self::$settings["ProspectMappings"]["MapCust7"][0]] = $_REQUEST[self::$settings["ProspectMappings"]["MapCust7"][1]];

                if(isset($_REQUEST["redirect_to"]) && $_REQUEST["redirect_to"]!="")
                    $redirect=$_REQUEST["redirect_to"];
                else if(isset($_SERVER["HTTP_REFERER"]))
                    $redirect=$_SERVER["HTTP_REFERER"];
                else
                    $redirect="/";
            }
            else
            {
                if($userID)
                    $contact["pba__SystemExternalId__c"] = "WordPress" . $userID;

                if(isset($_REQUEST["FirstName"]) && $_REQUEST["FirstName"]!="")
                    $contact["FirstName"] = $_REQUEST["FirstName"];

                if(isset($_REQUEST["LastName"]) && $_REQUEST["LastName"]!="")
                    $contact["LastName"]=$_REQUEST["LastName"];
                
                if(isset($_REQUEST["Email"]) && $_REQUEST["Email"]!="")
                    $contact["Email"]=$_REQUEST["Email"];

                if(isset($_REQUEST["Phone"]) && $_REQUEST["Phone"]!="")
                    $contact["Phone"]=$_REQUEST["Phone"];
            }

            $contact["Company_Lead__c"]="Yes";
            $contact["LeadSource"]="Web";
            
            $contactFields=array("id","Lastname","Firstname");

            $prospect = array(
                "token"=> self::$settings["Token"],
                "contact" => $contact,
                "contactFields"=>$contactFields
                );

            //Add favorites
            if(isset($_REQUEST["Favorite"]) && $_REQUEST["Favorite"]!=""){
                //Check to see if the user has this favorite
                $favs=get_user_meta($userID,'Favorites',true);

                if(!isset($favs) || $favs==""){
                    $favs=array();
                }

                if(!in_array($_REQUEST["Favorite"],$favs))
                {
                    array_push($favs,$_REQUEST["Favorite"]);

                    $favorite=array($_REQUEST["Favorite"]);

                    $prospect["favoriteListings"]=$favorite;

                    update_user_meta($userID,'Favorites',$favs);
                }
              	else
                {
                	//Remove the favorite
                  $newFavs=array();
                  
                  foreach($favs as $fav)
                  {
                    if($fav!=$_REQUEST["Favorite"])
                      array_push($newFavs,$fav);
                    
                    	update_user_meta($userID,'Favorites',$newFavs);
                  }
                }
            }

            $data=array(
               "prospect"=>$prospect
               );

            $json = json_encode($data);

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");  
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$json);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',                                                                                
                'Content-Length: ' . strlen($json))                                                                       
            ); 
            $result = curl_exec($ch);

            if(FALSE==$result)
                echo curl_error($ch);

            curl_close($ch);

          
          	if(isset($_SESSION["redirpage"]))
            {
              header("location:" . $_SESSION["redirpage"]);
            }
            else if($redirect=="")
            {
                echo json_encode(true);
            }
            else
            {
                header("location:" . $redirect);
            }

            die();
        }
        
    }
}
