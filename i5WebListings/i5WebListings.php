<?php
	/*
	 Plugin Name:i5WebListings
	 Plugin URI: http://www.i5fusion.com
	 Description: Displays listings from Propertybase and Setups Webtoprospect for Propertybase
	 Author: Jarrett Fisher
	 Version: 2.0
	 Author URI:http://www.i5fusion.com
	 */
     
     
	 define( 'WEBLISTING__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

     if ( is_admin() ) {
         require_once( WEBLISTING__PLUGIN_DIR . 'class.i5WebListings-Admin.php' );
         add_action( 'init', array( 'i5WebListings_Admin', 'init' ) );
     }

     require_once( WEBLISTING__PLUGIN_DIR . 'class.i5WebListings-Prospect.php' );
     add_action( 'init',array('i5WebListings_Prospect','init'));

     require_once( WEBLISTING__PLUGIN_DIR . 'class.i5WebListings.php' );
     add_action( 'init',array('i5WebListings','init'));
     
?>