<?php
class i5WebListings_Admin
{
    private static $initiated = false;
    
    public static function init(){
        if(!self::$initiated){
            self::init_hooks();
        }
    }
    public static function init_hooks(){
        add_action('admin_menu',array('i5WebListings_Admin','admin_menu'));
    }

    public static function admin_menu(){
		 add_options_page('Web Listings','Web Listings','manage_options',__FILE__,array('i5WebListings_Admin', 'admin_interface'));
	}

    public static function admin_interface(){
         $settings=get_option('WebListing_Settings');

         if(!isset($settings))
             $settings =array('Prospect'=>false,'EndPoint'=>'','Token'=>'','Debug'=>false);
		 
		 if(isset($_POST['submit']))
		 {
			 $ok=true;
			 $message='<p>Please Correct the Following Errors</p>';

			 if(trim($_POST['WebListingEndPoint'])==""){
				 $ok=false;
				 $message.='<li>Please Specify Webservice EndPoint</li>';
			 }
			 if(trim($_POST['WebListingToken'])==""){
				 $ok=false;
				 $message.='<li>Please Specify Propertybase Token</li>';
			 }

			 if($ok)
			 {
				 $settings=array('Prospect'=>isset($_POST["EnableWebProspect"])?true:false,'EndPoint'=>rtrim(trim($_POST["WebListingEndPoint"]),"/"),'Token'=>trim($_POST["WebListingToken"]),'Debug'=>isset($_POST["WebListingDebug"])?true:false);

                 $settings["ProspectMappings"]=array();

                 if(isset($_POST["MapFirstName"]) && $_POST["MapFirstName"]!="")
                     $settings["ProspectMappings"]["FirstName"]=$_POST["MapFirstName"];
                 
                 if(isset($_POST["MapLastName"]) && $_POST["MapLastName"]!="")
                     $settings["ProspectMappings"]["LastName"]=$_POST["MapLastName"];

                 if(isset($_POST["MapEmail"]) && $_POST["MapEmail"] !="")
                     $settings["ProspectMappings"]["Email"]=$_POST["MapEmail"];

                 if(isset($_POST["MapPhone"]) && $_POST["MapPhone"]!="")
                     $settings["ProspectMappings"]["Phone"]=$_POST["MapPhone"];

                 if(isset($_POST["MapCust1PB"]) && isset($_POST["MapCust1WP"]) && $_POST["MapCust1PB"]!="" && $_POST["MapCust1WP"]!="")
                     $settings["ProspectMappings"]["MapCust1"]=array($_POST["MapCust1PB"], $_POST["MapCust1WP"]);

                 if(isset($_POST["MapCust2PB"]) && isset($_POST["MapCust2WP"]) && $_POST["MapCust2PB"]!="" && $_POST["MapCust2WP"]!="")
                     $settings["ProspectMappings"]["MapCust2"]=array($_POST["MapCust2PB"], $_POST["MapCust2WP"]);

                 if(isset($_POST["MapCust3PB"]) && isset($_POST["MapCust3WP"]) && $_POST["MapCust3PB"]!="" && $_POST["MapCust3WP"]!="")
                     $settings["ProspectMappings"]["MapCust3"]=array($_POST["MapCust3PB"], $_POST["MapCust3WP"]);

                 if(isset($_POST["MapCust4PB"]) && isset($_POST["MapCust4WP"]) && $_POST["MapCust4PB"]!="" && $_POST["MapCust4WP"]!="")
                     $settings["ProspectMappings"]["MapCust4"]=array($_POST["MapCust4PB"], $_POST["MapCust4WP"]);

                 if(isset($_POST["MapCust5PB"]) && isset($_POST["MapCust5WP"]) && $_POST["MapCust5PB"]!="" && $_POST["MapCust5WP"]!="")
                     $settings["ProspectMappings"]["MapCust5"]=array($_POST["MapCust5PB"], $_POST["MapCust5WP"]);

                 if(isset($_POST["MapCust6PB"]) && isset($_POST["MapCust6WP"]) && $_POST["MapCust6PB"]!="" && $_POST["MapCust6WP"]!="")
                     $settings["ProspectMappings"]["MapCust6"]=array($_POST["MapCust6PB"], $_POST["MapCust6WP"]);

                 if(isset($_POST["MapCust7PB"]) && isset($_POST["MapCust7WP"]) && $_POST["MapCust7PB"]!="" && $_POST["MapCust7WP"]!="")
                     $settings["ProspectMappings"]["MapCust7"]=array($_POST["MapCust7PB"], $_POST["MapCust7WP"]);

				 update_option("WebListing_Settings",$settings);
				 echo "<p>Settings Updated</p>";
			 }
			 else
			 {
				 echo $message;
			 }

		 }
?>
		<div class="wrap">
			<h1>Web Listings Settings</h1>
			<form method="post" action="" novalidate="novalidate">
                <table class="form-table">
                    <tbody>
						<tr>
							<th scope="row">Debug</th>
							<td><input name="WebListingDebug" type="checkbox" <?php echo $settings["Debug"]? 'checked="true"':'' ?> /></td>
						</tr>
						<tr>
                            <th scope="row">Webservice EndPoint:</th>
							<td><input style="width:400px;" name="WebListingEndPoint" type="text" value="<?php echo $settings["EndPoint"] ?>" /></td>
						</tr>
                        <tr>
							<th scope="row">Propertybase Token:</th>
                            <td><input style="width:400px;" name="WebListingToken" type="text" value="<?php echo $settings["Token"]?>" /></td>
                        </tr>
                        <tr>
                            <th scope="row">Save Contacts on Registration:</th>
                            <td><input name="EnableWebProspect" type="checkbox" <?php echo (isset($settings["Prospect"]) && $settings["Prospect"])?'checked="true"':'' ?>/></td>
                        </tr>
                        <tr>
                            <th colspan="2" scope="row">Prospect Mappings</th>
                        </tr>
                        <tr>
                            <td>PropertyBase Field</td>
                            <td>WordPress Field</td>
                        </tr>
                        <tr>
                            <td>
                                FirstName
                            </td>
                            <td>
                                <input name="MapFirstName" type="text" value="<?php echo isset($settings["ProspectMappings"]["FirstName"])?$settings["ProspectMappings"]["FirstName"]:""?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                LastName
                            </td>
                            <td>
                                <input name="MapLastName" type="text" value="<?php echo isset($settings["ProspectMappings"]["LastName"])?$settings["ProspectMappings"]["LastName"]:""?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Email
                            </td>
                            <td>
                                <input name="MapEmail" type="text" value="<?php echo isset($settings["ProspectMappings"]["Email"])?$settings["ProspectMappings"]["Email"]:""?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Phone
                            </td>
                            <td>
                                <input name="MapPhone" type="text" value="<?php echo isset($settings["ProspectMappings"]["Phone"])?$settings["ProspectMappings"]["Phone"]:""?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input name="MapCust1PB" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust1"][0])?$settings["ProspectMappings"]["MapCust1"][0]:""?>" />
                            </td>
                            <td>
                                <input name="MapCust1WP" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust1"][1])?$settings["ProspectMappings"]["MapCust1"][1]:""?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input name="MapCust2PB" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust2"][0])?$settings["ProspectMappings"]["MapCust2"][0]:""?>" />
                            </td>
                            <td>
                                <input name="MapCust2WP" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust2"][1])?$settings["ProspectMappings"]["MapCust2"][1]:""?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input name="MapCust3PB" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust3"][0])?$settings["ProspectMappings"]["MapCust3"][0]:""?>" />
                            </td>
                            <td>
                                <input name="MapCust3WP" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust3"][1])?$settings["ProspectMappings"]["MapCust3"][1]:""?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input name="MapCust4PB" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust4"][0])?$settings["ProspectMappings"]["MapCust4"][0]:""?>" />
                            </td>
                            <td>
                                <input name="MapCust4WP" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust4"][1])?$settings["ProspectMappings"]["MapCust4"][1]:""?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input name="MapCust5PB" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust5"][0])?$settings["ProspectMappings"]["MapCust5"][0]:""?>" />
                            </td>
                            <td>
                                <input name="MapCust5WP" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust5"][1])?$settings["ProspectMappings"]["MapCust5"][1]:""?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input name="MapCust6PB" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust6"][0])?$settings["ProspectMappings"]["MapCust6"][0]:""?>" />
                            </td>
                            <td>
                                <input name="MapCust6WP" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust6"][1])?$settings["ProspectMappings"]["MapCust6"][1]:""?>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input name="MapCust7PB" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust7"][0])?$settings["ProspectMappings"]["MapCust7"][0]:""?>" />
                            </td>
                            <td>
                                <input name="MapCust7WP" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust7"][1])?$settings["ProspectMappings"]["MapCust7"][1]:""?>" />
                            </td>
                        </tr>
                    </tbody>
                </table>
				<?php submit_button(); ?>
			</form>
		</div>
<?php
	 }
}
?>
