﻿function saveProspect(fname, lname, email, phone) {
    jQuery.ajax({
        type: "post",
        dataType: "json",
        url: i5PBAjax.ajaxurl,
        data: { action: "saveProspect", nonce: i5PBAjax.nonce, FirstName: fname, LastName: lname, Email: email, Phone: phone },
        success: function (response) {
            
        },
        error:function(err) {
            alert(err);
        }
    });
}
function saveFavorite(id,on) {
    jQuery.ajax({
        type: "post",
        dataType: "json",
        url: i5PBAjax.ajaxurl,
        data: { action: "saveFavorite", nonce: i5PBAjax.nonce, FavMLSID: id, FavOn: on },
        success: function (response) {
            //alert("worked");
        },
        error: function (err) {
            //Revert to non ajax
            var join = "&";

            if (window.location.href.indexOf("?") == -1)
                join = "?";

            window.location=window.location.href.replace(window.location.hash,"") + join + "fav=" + id + "&on=" + on;
        }
    });
}
