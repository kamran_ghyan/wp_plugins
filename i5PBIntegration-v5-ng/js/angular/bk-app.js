var listingApp = angular.module('listingApp', ['ngStorage', 'ngAnimate'])
listingApp.directive('fadeIn', function($timeout){
        return {
            restrict: 'A',
            link: function($scope, $element, attrs){
                $element.addClass("ng-hide-remove");
                $element.on('load', function() {
                    $element.addClass("ng-hide-add");
                });
            }
        };
    });

listingApp.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

listingApp.filter('ceil', function() {
    return function(input) {
        return Math.ceil(input);
    };
});

listingApp.controller('dropDownController', ['$scope', '$http' , '$localStorage', '$filter', function($scope, $http, $localStorage, $filter) {
    // Local Storage Variable
    $scope.searchTerm   = '';
    $scope.sortingBy      = '';


    // Local storage variables more option
    $scope.minSqft      = '';
    $scope.maxSqft      = '';
    $scope.listedSince  = '';
    $scope.bathrooms    = '';
    $scope.parking      = '';
    $scope.openHouse    = '';
    $scope.garage       = '';

    // Local storage variables house filter
    $scope.single           = '';
    $scope.singleBasement   = '';
    $scope.duplex           = '';
    $scope.triplex          = '';
    $scope.fourplex         = '';


    // Build filter
    // Home Types
    $scope.homeTypesAttribute = [
        {
            "name" : "home",
            "label" : "Home",
            "value" : true
        },{
            "name" : "condo",
            "label" : "Condo",
            "value" : true
        },{
            "name" : "apartment",
            "label" : "Apartment",
            "value" : true
        }
    ];

    // Price Range Min
    $scope.minPriceRange= [
        {"value" : "0"},{"value" : "50000"},{"value" : "100000"},{"value" : "200000"},{"value" : "400000"},{"value" : "600000"},{"value" : "800000"},{"value" : "1000,000"},{"value" : "1250,000"},{"value" : "1500,000"},
    ];

    // Price Range Max
    $scope.maxPriceRange= [
        {"value" : "150000"},{"value" : "250000"},{"value" : "350000"},{"value" : "450000"},{"value" : "550000"},{"value" : "650000"},{"value" : "750000"},{"value" : "850000"},{"value" : "950000"},{"value" : "Any Price"},
    ];

    // Number of beds
    $scope.numBeds= [
        {"value" : "0"},{"value" : "1"},{"value" : "2"},{"value" : "3"},{"value" : "4"},{"value" : "5"},{"value" : "6"},
    ];

    // Number of Bathrooms
    $scope.numBaths= [
        {"value" : "1"},{"value" : "2"},{"value" : "3"},{"value" : "4"},{"value" : "5"},{"value" : "6"},
    ];

    // Number of Parking
    $scope.numParking= [
       {"value" : "1"},{"value" : "2"},{"value" : "3"},{"value" : "4"},{"value" : "5"},
    ];

    // House Filters
    $scope.houseFilters= [
        {
            "name" : "single-family",
            "label" : "Single family",
            "value" : false
        },{
            "name" : "single-family-apartment",
            "label" : "Single family with basement apartment",
            "value" : false
        },{
            "name" : "duplex",
            "label" : "Duplex",
            "value" : false
        },{
            "name" : "triplex",
            "label" : "Triplex",
            "value" : false
        },{
            "name" : "fourplex",
            "label" : "Fourplex +",
            "value" : false
        }
    ];

    // Sort By Filter
    $scope.typeSort= [
        {
            "name" : "ascprice",
            "label" : "Price (Low to High)‎"
        },{
            "name" : "descprice",
            "label" : "Price (High to Low)‎"
        },{
            "name" : "desclastUpdated",
            "label" : "Newest to Oldest‎"
        },{
            "name" : "asclastUpdated",
            "label" : "Oldest to Newest‎"
        },{
            "name" : "descbedrooms",
            "label" : "Bedrooms (High to Low)‎"
        },{
            "name" : "ascbedrooms",
            "label" : "Bedrooms (Low to High)‎"
        },{
            "name" : "descfullBaths",
            "label" : "Bathrooms (High to Low)‎"
        },{
            "name" : "ascfullBaths",
            "label" : "Bathrooms (Low to High)‎"
        }
    ];

    $scope.infoWindows = [];

    // Checkbox checks
    $scope.getCheckedTrue = function(args){
        if(args == true){
            return true;
        }
    };

    // Home type size
    $scope.homeType = [];
    $scope.updateTypes = function () {
        $scope.homeType = $filter("filter")( $scope.homeTypesAttribute , {value:true} );
        return $scope.homeType.length;
    };

    // Home type size
    $scope.houseFilter = [];
    $scope.houseFilterSelect = function () {
        $scope.houseFilter = $filter("filter")( $scope.houseFilters , {value:true} );

    };

    // Bed type size
    $scope.selectedBed = 0;
    $scope.changeBeds = function () {
        if($scope.selectedBed == 0){
            return $scope.selectedBed;
        } else{
            return $scope.selectedBed;
        }
    };

    // Min price selection
    $scope.minPrice = [];
    $scope.addMinPrice = function(args){
        $scope.minPrice = [];
        $scope.minPrice.push(args);
        $scope.showPriceRange('maxprice');
    };

    // Max price selection
    $scope.maxPrice = [];
    $scope.addMaxPrice = function(args){
        $scope.maxPrice = [];
        $scope.maxPrice.push(args);

        $scope.showPriceRange('null');
    };

    // Show / Hide price range panels
    $scope.showPrice = 'minprice';

    $scope.showPriceRange = function(args){
        return $scope.showPrice = args;
    }


    // price range subset
    $scope.priceRangeValue = 'Any Price';
    $scope.priceRange = function(){
         if($scope.minPrice[0] == undefined && $scope.maxPrice[0] == undefined){
             return $scope.priceRangeValue;
        } else if($scope.minPrice[0] != '' && $scope.maxPrice[0] == undefined) {
             var subset = "Above " + $scope. minPrice[0];
             $scope.priceRangeValue = subset;
             return $scope.priceRangeValue;
         }else if($scope.minPrice[0] != '' && $scope.maxPrice[0] != ''){
            $scope.priceRangeValue = '';
            var subset = $scope. minPrice[0] + " - " + $scope.maxPrice[0];
            $scope.priceRangeValue = subset;
            return $scope.priceRangeValue;
        } else{
             return $scope.priceRangeValue;
        }
    }

    // Drop Down Value
    $scope.showDrop = '';

    // Drop Down Hide / Show panel
    $scope.showDropDown = function(arg){
        $scope.showDrop = arg;
    }


    // Reset Form
    $scope.resetForm = function (){

        if($localStorage != ''){

            delete $localStorage.searchTerm
            delete $scope.searchTerm;

            delete $localStorage.sortingBy;
            delete $scope.sortingBy;

            delete $localStorage.selectedBed;
            delete $scope.selectedBed;

            delete $localStorage.homeType;
            delete $scope.homeType;

            delete $localStorage.minPrice;
            $scope.minPrice[0] = '';

            delete $localStorage.maxPrice;
            $scope.maxPrice[0] = '';

            delete $localStorage.minSqft;
            delete $scope.minSqft;

            delete $localStorage.maxSqft;
            delete $scope.maxSqft;

            delete $localStorage.listingSince;
            delete $scope.listingSince;

            delete $localStorage.bathrooms;
            delete $scope.bathrooms;

            delete $localStorage.parking;
            delete $scope.parking;

            delete $localStorage.openHouse;
            delete $scope.openHouse;

            delete $localStorage.garage;
            delete $scope.garage;

            delete $localStorage.houseFilter;
            delete $scope.houseFilter;

            delete $scope.single;
            delete $localStorage.single;

            delete $scope.singleBasement
            delete $localStorage.singleBasement;

            delete $scope.duplex;
            delete $localStorage.duplex;

            delete $scope.triplex;
            delete $localStorage.triplex;

            delete $scope.fourplex;
            delete $localStorage.fourplex;
        }

        $scope.showDrop = '';
        $scope.propertyListing();
    };

    // Search Filter Storage
    $scope.storeData = function(){
        $localStorage.searchTerm    = $scope.searchTerm;
        $localStorage.sortingBy     = $scope.sortingBy;

        $localStorage.selectedBed   = $scope.selectedBed;
        $localStorage.homeType      = $scope.homeType;
        $localStorage.minPrice      = $scope.minPrice[0];
        $localStorage.maxPrice      = $scope.maxPrice[0];

        $localStorage.minSqft       = $scope.minSqft;
        $localStorage.maxSqft       = $scope.maxSqft;
        $localStorage.listingSince  = $scope.listingSince;
        $localStorage.bathrooms     = $scope.bathrooms;
        $localStorage.parking       = $scope.parking;
        $localStorage.openHouse     = $scope.openHouse;
        $localStorage.garage        = $scope.garage;
        $localStorage.single           = $scope.single;
        $localStorage.singleBasement   = $scope.singleBasement;
        $localStorage.duplex           = $scope.duplex;
        $localStorage.triplex          = $scope.triplex;
        $localStorage.fourplex         = $scope.fourplex;
        $scope.propertyListing();

    }

    if($localStorage != ''){
        $scope.searchTerm       = $localStorage.searchTerm;
        $scope.sortingBy        = $localStorage.sortingBy;

        $scope.selectedBed      = $localStorage.selectedBed;
        $scope.homeType         = $localStorage.homeType;
        $scope.minPrice[0]      = $localStorage.minPrice;
        $scope.maxPrice[0]      = $localStorage.maxPrice;

        $scope.minSqft          = $localStorage.minSqft;
        $scope.maxSqft          = $localStorage.maxSqft;
        $scope.listingSince     = $localStorage.listingSince;
        $scope.bathrooms        = $localStorage.bathrooms;
        $scope.parking          = $localStorage.parking;
        $scope.openHouse        = $localStorage.openHouse;
        $scope.garage           = $localStorage.garage;

        $scope.single           = $localStorage.single;
        $scope.singleBasement   = $localStorage.singleBasement;
        $scope.duplex           = $localStorage.duplex;
        $scope.triplex          = $localStorage.triplex;
        $scope.fourplex         = $localStorage.fourplex;
    }
    // Load Search Filter Values
    $scope.loadData = function(){
        $scope.filterStorage = $localStorage.filterStorage;
    }
    // Reset Search Filter
    $scope.resetData = function (){
        $scope.filterStorage = "";
    }

    // Pagination
    $scope.currentPage = 0;
    $scope.pageSize = 10;
    $scope.paginationStart = 10;

    $scope.totalPages=function(arg){
        //$scope.listLength = Object.keys(arg).length;
        return 3;
    }

    $scope.totalListLength=function(arg){
        return Object.keys(arg).length;
    }

    // HTTP call
    $scope.listing = [];

    $scope.propertyListing = function(){
        $scope.showDrop = '';
        $scope.loading = true;
        $http({
            url: i5PBAjax.ajaxurl,
            method: "POST",
            params: {
                action:         'propertyListing',
                term:           $scope.searchTerm,
                sorting:        $scope.sortingBy,
                propertytype:   JSON.stringify($scope.homeType),
                minprice:       $scope.minPrice[0],
                maxprice:       $scope.maxPrice[0],
                bedrooms:       $scope.selectedBed,
                minlotsize:     $scope.minSqft,
                maxlotsize:     $scope.maxSqft,
                lastUpdated:    $scope.listingSince,
                fullBaths:      $scope.bathrooms,
                parking:        $scope.garage,
                homespecs:      $scope.openHouse,
                single:         $scope.single,
                singleBasement: $scope.singleBasement,
                duplex:         $scope.duplex,
                triplex:        $scope.triplex,
                fourplex:      $scope.fourplex
            }
        })
        .then(function(response) {
            //console.log(response.data);
            $scope.listing = response.data;

            $scope.initialize(response.data);
            $scope.loading = false;

            $scope.listtingVal = Object.keys(response.data).length;
            $scope.totalPages(response.data);
            $scope.totalListLength(response.data);


        },
        function(response) { // optional
            //console.log(response);
        });
    }

    $scope.favClass = "";
    $scope.saveFav = function(id, on){
        $http({
            url: i5PBAjax.ajaxurl,
            method: "POST",
            params: {action: "saveFavorite", nonce: i5PBAjax.nonce, FavMLSID: id, FavOn: on}
        })
        .then(function(response) {
            $scope.favClass = "";
        },
        function(response) { // optional
            //console.log(response);

        });
    }

    $scope.initialize = function (loc) {

        var data = loc;
        // Center Object
        var center = new google.maps.LatLng(37.76506000000000, -122.43617000000000);
        // Map Object
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: center,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            cluster:{
                options:{
                    styles:[{
                        url: 'http://pbdemo.i5fusion.com/wp-content/plugins/i5PBIntegration/images/icon-map-cluster.png',
                        height: 42,
                        width:42,
                        textColor: '#fff'
                    }]
                }
            }
        });

        // Custom marker icon
        var iconBase = "http://pbdemo.i5fusion.com/wp-content/plugins/i5PBIntegration/images/";
        var icons = {
            cluster: {
                icon: iconBase + 'icon-map-cluster-marker.png'
            }
        };

        var markerClusterer = null;

        var bulle = 0;

       /* if (markerClusterer) {
            markerClusterer.clearMarkers();
        }*/

        var markers = [];

        var infowindow = new google.maps.InfoWindow();

        var bounds = new google.maps.LatLngBounds();

        for (var i = 0; i < data.length; i++) {

            var listingData = data[i];
            var title = listingData.title;

            var infoTemplate = '<div class="cluster-popup"><div class="listing"><div class="image-wrapper"><img src="' + listingData.image + '" /></div><div class="listing-info"><div class="listing-street">' + listingData.address + '</div><div class="listing-price">$' + listingData.price +'</div><div class="listing-details"><span><strong>' + listingData.bedrooms + '</strong> beds</span><span><strong>' + listingData.fullBaths + '</strong> baths</span></div><div class="listing-sqft">'+ listingData.size + 'sq ft</div></div></div></div>';
            //var infoTemplate = '<div class="cluster-popup"><img src="' + listingData.image + '" /></div>';
            //var infoTemplate = "Test";
            var latLng = new google.maps.LatLng(listingData.latitude,
                listingData.longitude);

            bounds.extend(latLng);

            var marker = new google.maps.Marker({
                position: latLng,
                map: map,
                html : infoTemplate,
                icon: icons.cluster.icon
            });
            markers.push(marker)
           //markers.push(marker);
            google.maps.event.addListener(marker, 'mousemove', (function (marker, infoTemplate, infowindow) {
                return function () {
                    infowindow.setContent(infoTemplate);
                    infowindow.open(map, marker);

                    google.maps.event.addListenerOnce(map, 'mousemove', function(){
                        infowindow.close();
                    });
                };

            })(marker, infoTemplate, infowindow));
        }

        $scope.clusterList(map, markers, infowindow);

    }

    $scope.clusterList = function(map, markers, infowindow){
        var clusterOptions = {
            zoomOnClick: false,
            gridSize: 60,
            styles: [{
                url: 'http://pbdemo.i5fusion.com/wp-content/plugins/i5PBIntegration/images/icon-map-cluster.png',
                height: 42,
                width:42,
                textColor: '#fff'
            }]
        }

        var markerClusterer = new MarkerClusterer(map, markers, clusterOptions);

        google.maps.event.addListener(markerClusterer, 'clustermouseover', function(cluster){

            var content ='';
            var clickedMarkers = cluster.getMarkers();
            for (var i = 0; i < clickedMarkers.length; i++) {
                if(i==0){
                    var var_pos = clickedMarkers[i];
                }
                var html = clickedMarkers[i].html;
                content +=html;

                infowindow.close();
                infowindow.setPosition(cluster.getCenter());
                infowindow.setContent(content);
                infowindow.open(map,var_pos);
            }

            console.log(content);
            //$scope.showInfoWindow = false;
            //$scope.openInfoWindow();
            //infowindow.close();
            //infowindow.setPosition(cluster.getCenter());
            //infowindow.setContent(content);
            //infowindow.open(map,var_pos);

           /* google.maps.event.addListenerOnce(map, 'clustermouseover', function(){
             infowindow.close();
             });*/

            bulle=1;
        });
    }

    $scope.InfoWindow = [];
    $scope.openInfoWindow = function () {
        $scope.showInfoWindow = true;
    }

    $scope.propertyListing();

    }]);