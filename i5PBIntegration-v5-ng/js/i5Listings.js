﻿function queryListings(queryFields,whereFields,callback,groupFields,orderFields,limit) {
    jQuery.ajax({
        type: "post",
        dataType: "json",
        url: i5PBAjax.ajaxurl,
        data: { action: "queryListings", nonce: i5PBAjax.nonce, query: queryFields, where: whereFields, group: groupFields, order: orderFields, limit: limit },
        success: function (response) {
            callback(response);
        },
        error: function (err) {
            alert(err);
        }
    });
}
function autoComplete(term, limit,callback) {
    jQuery.ajax({
        type: "post",
        dataType: "json",
        url: i5PBAjax.ajaxurl,
        data: { action: "autoCompleteListings", nonce: i5PBAjax.nonce, limit: limit, kw: term },
        success: function (response) {
            callback(response);
        },
        error: function (err) {
            alert(err);
        }
    });
}