
<style>
    .slick-prev:before, .slick-next:before {
        color: #fff !important;
        font-size: 30px !important;
    }
</style>
<?php
wp_enqueue_style( 'property-detail5', i5PBIntegration__PLUGIN_URL . 'css/property-detail5.css', array(), '1.1', 'all');
wp_enqueue_style( 'property-search-header', i5PBIntegration__PLUGIN_URL . 'css/search-header2.css', array(), '1.1', 'all');
wp_enqueue_style( 'slick-styles', i5PBIntegration__PLUGIN_URL . 'css/slick.css', array(), '1.1', 'all');
wp_enqueue_style( 'slick-theme', i5PBIntegration__PLUGIN_URL . 'css/slick-theme.css', array(), '1.1', 'all');
wp_enqueue_script( 'slick', i5PBIntegration__PLUGIN_URL . 'js/slick.min.js', array('jquery'), '1.1', true );
wp_enqueue_script( 'PrintElement', i5PBIntegration__PLUGIN_URL . 'js/PrintElement.js', array('jquery'), '1.1', true );
/*
Template Name: Property Detail 5
 */
$showSave=false;
get_header();
include(i5PBIntegration__PLUGIN_DIR . 'Includes/Prospect-Functions.php');
include(i5PBIntegration__PLUGIN_DIR . 'Includes/PropertyDetail-Functions.php');
include(i5PBIntegration__PLUGIN_DIR . 'Includes/PropertySearch-Functions.php');
//include(i5PBIntegration__PLUGIN_DIR . "Includes/HeaderSearch.php");
include(i5PBIntegration__PLUGIN_DIR . 'Includes/Global-Functions.php');
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<div class="printContainer">
    <div class="detailsHeader">
        <div class="container">

            <div class="detailsAddress div2-3">
                <h1 itemprop="address" itemtype="http://schema.org/PostalAddress">
                    <?php echo $property->address . ($property->unitNumber==""?"":" " . $property->unitNumber) ?>
                </h1>
                <div id="fav<?php echo $property->mlsid?>" class="<?php echo ($favs=='' || !in_array($property->mlsid,$favs)?'detailsFavorite':'detailsFavoriteSelected')?>">
                    <a href="#" style="margin-right: 10px;" id="show-map"><i class="fa fa-map-marker" aria-hidden="true"></i></a>
                    <a href="javascript:toggleFav('<?php echo $property->mlsid ?>');setFavorite('<?php echo $property->mlsid ?>',<?php echo ($favs=='' || !in_array($property->mlsid,$favs)?'1':'0')?>);">

                        <i class="fa fa-heart" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <!-- end detailsAddress -->
            <div class="div1-3 printHide">
                <div class="detailsButtons"></div>
            </div>
        </div>
        <!-- end contain -->
    </div>
    <!-- end detailsHeader -->
    <div class="detailsBody">
        <div class="container">
            <div class="div2-3">
                <div class="propertyImages" id="slider">
                    <?php
                    if(sizeof($images)==0){
                        echo " <div><img src='" . i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png' /></div>";
                    }
                    else{
                        $index=0;

                        foreach($images as $image){
                            if($index==0)
                                echo " <div><img onerror='fixImage(this);' src='" . $image->url . "' /></div>";
                            else
                                echo " <div><img style='display:none;' onerror='fixImage(this);' src='" . $image->url . "' /></div>";

                            $index+=1;
                        }
                    }
                    ?>
                </div>
                <div id="map" style="display: none;">
                    <div class="detailsMap">
                        <a id="map"></a>
                        <p style="text-align:center;">
                            <?php
                            do_shortcode('[i5ListingMap maxzoom="100" minzoom="1" style="display:block;width: 100%; height: 418px" showdetails="false" showpolygon="false" zoom="15" lat="' . $property->latitude . '" long="' . $property->longitude . '" ]');
                            echo "<script>markAddress(" . json_encode($property) . ");</script>";
                            ?>
                        </p>

                        <div class="Disclaimer">
                            <?php echo htmlspecialchars_decode(get_post_meta( get_the_ID(), 'disclaimer', true )); ?>
                        </div>

                    </div>
                </div>
            </div>

            <!-- end div 2-3 -->
            <div class="div1-3">
                <div class="request-information">
                    <h4>Request Information</h4>
                    <p>Complete the form and we’ll be in touch.</p>
                    <?php echo do_shortcode( '[contact-form-7 id="6642" title="Request Information"]' ); ?>
                </div>
            </div>
            <!-- end div1-3 -->
            <div class="detailsDescriptionHolder">
                <div class="div2-3">
                    <div class="price-section padded">

      <span class="price ember-view"><?php echo "$" . number_format($property->price,0); ?></span>
      <span class="mortgage-price">est. <a style="cursor: pointer;"><?php echo "$" . round(number_format($property->price,0)/12 , 2). " /month"; ?></a>
      </span>
                        <!---->  </div>
                    <div class="beds-baths padded">
                        <span><?php echo number_format($property->bedrooms,0) ?> beds</span><span><?php echo number_format($property->fullBaths,0) ?> baths</span><span> <?php echo number_format($property->lotsize,0) ?> sq ft</span><span>no parking</span>
                    </div>
                </div>
                <div class="div2-3">
                   <!-- <div class="detailsDescriptionHeader">
                        Description
                    </div>-->
                    <p>
                        <?php echo $property->description?>
                    </p>
                </div>
                <!-- end div2-3 -->
                <div class="div1-3">
                    <div class="agentTile">
                        <?php
                        $agent;
                        if(post_type_exists("i5agents")){
                            global $wpdb;
                            $sql="select p.id from {$wpdb->prefix}postmeta pm join {$wpdb->prefix}posts p on p.id=pm.post_id where meta_key='domain' and p.post_status='publish' and p.post_type='i5agents' and pm.meta_value='" . $_SERVER['HTTP_HOST'] . "'";
                            $agent=$wpdb->get_row($sql);
                        }

                        if($agent!=null){
                            echo do_shortcode("[i5AgentData show_social_links='false' office_phone_label='O: ' mobile_phone_label='M: ']");
                        } else{?>
                            <div class="div1-3">
                                <div class="padding10">

                                </div>
                            </div>
                            <!-- end div1-3 -->
                            <div class="div2-3">
                                <div class="agentTitle">
                                    <?php echo esc_attr( get_post_meta( get_the_ID(), 'companyname', true ) ); ?>
                                </div>
                                <div class="agentPhone">
                                    <a href="tel:<?php echo esc_attr( get_post_meta( get_the_ID(), 'companyphone', true ) ); ?>">
                                        <span class="red">O:</span>
                                        <?php echo esc_attr( get_post_meta( get_the_ID(), 'companyphone', true ) ); ?>
                                    </a>
                                </div>
                            </div>
                            <!-- end div2-3 -->
                        <?php }?>
                    </div>
                    <!-- end agentTile -->
                </div>
                <!-- end div1-3 -->
            </div>
            <div class="featuresHolder">
                <div class="div2-3">
                    <div id="accordion">
                        <h3>Property Details</h3>
                        <div>
                            <p>Mauris mauris ante, blandit et, ultrices a, susceros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.</p>
                        </div>
                        <h3>Neighbourhood Demographics</h3>
                        <div>
                            <p>Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna. </p>
                        </div>
                        <h3>Mortgages</h3>
                        <div>
                            <p>Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui. </p>
                        </div>
                    </div>
                </div>
            </div>
            <?php if(isset($property->listingOffice) && $property->listingOffice!=""):?>
                <div style="width:100%">
                    Listing provided courtesy of <?php echo $property->listingOffice; ?>
                </div>
            <?php endif;?>
            <!-- end featuresHolder -->


        </div>
        <!-- end container -->
    </div>
</div>
<!-- end detailsBody -->


<script>
    jQuery("#show-map").click(function(e){

        e.preventDefault();
        var center = new google.maps.LatLng(<?php echo $property->latitude; ?>, <?php echo $property->longitude; ?>);
        //jQuery('#map').fadeIn(1000);
        google.maps.event.trigger(document.getElementById('divListings'), "resize");
        map.setCenter(center);

        jQuery("#map").toggle("slow");
        jQuery("#slider").toggle();


    });

    jQuery( function() {
        jQuery( "#accordion" ).accordion({
            collapsible: true,
            active: false
        });
    } );

    jQuery(function () {
        jQuery(".propertyImages").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true
        });

        jQuery("img").show();
    });
    function toggleFav(id){
        if (jQuery("#fav" + id).hasClass("detailsFavoriteSelected"))
        {
            jQuery("#fav" + id).removeClass("detailsFavoriteSelected");
            jQuery("#fav" + id).addClass("detailsFavorite");
            jQuery(jQuery("#fav" + id).children()[0]).prop("href","javascript:toggleFav('" + id + "');setFavorite('" + id + "',1);");
        }
        else {
            jQuery("#fav" + id).addClass("detailsFavoriteSelected");
            jQuery("#fav" + id).removeClass("detailsFavorite");
            jQuery(jQuery("#fav" + id).children()[0]).prop("href", "javascript:toggleFav('" + id + "');setFavorite('" + id + "',0);");
        }

    }

    function printListing(){
        jQuery(".printHide").hide();
        jQuery(".printContainer").printElement();
        jQuery(".printHide").show();
    }

    function showSharing() {
        jQuery(".dropdownContent").removeClass("show");
        document.getElementById("ddShare").classList.toggle("show");
    }

    // Close the dropdown if the user clicks outside of it
    window.onclick = function(event) {
        if (!event.target.matches('.dropBtn')) {

            var dropdowns = document.getElementsByClassName("dropdownContent");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }

</script>

<?php
get_footer();

?>
