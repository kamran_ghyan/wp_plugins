<style xmlns="http://www.w3.org/1999/html">
    .slick-prev:before, .slick-next:before {
        color: #fff !important;
    }
</style>
<style>[ng-cloak] { display: none; }</style>

<?php
wp_enqueue_style( 'property-search3', i5PBIntegration__PLUGIN_URL . 'css/search-results3.css', array(), '1.1', 'all');
wp_enqueue_style( 'property-search-header', i5PBIntegration__PLUGIN_URL . 'css/search-header3.css', array(), '1.1', 'all');
wp_enqueue_script('angularjs', i5PBIntegration__PLUGIN_URL . 'js/angular/angular.min.js');
wp_enqueue_script('angular-animation',i5PBIntegration__PLUGIN_URL . 'js/angular/angular-animate.js', array( 'angularjs' ));
wp_enqueue_script('angular-local-storage',i5PBIntegration__PLUGIN_URL . 'js/angular/ngStorage.min.js', array( 'angularjs' ));
wp_enqueue_script('angular-app',i5PBIntegration__PLUGIN_URL . 'js/angular/app.js', array( 'angularjs' ));


get_header();

$maxResults=24;

include i5PBIntegration__PLUGIN_DIR . 'Includes/HeaderSearch.php';
//include 'includes/HeaderSearch3.php';

include(i5PBIntegration__PLUGIN_DIR . 'Includes/Prospect-Functions.php');
include(i5PBIntegration__PLUGIN_DIR . 'Includes/PropertySearch-Functions.php');
/*
Template Name: search results 3
 */

?>

<section class="main-area width-100" ng-app="listingApp" ng-controller="dropDownController">
    <div class="input-bar" >
       <div class="autocomplete-input" >
           <input type="text" name="search" id="search" ng-model="searchTerm" value="{{searchTerm}}" >
           <button type="submit" ng-click="storeData()">Search</button>

       </div>
        <div class="home drop-down">
            <label  ng-click="showDropDown('home')">
                Home Type ({{updateTypes()}})
                <span class="icon-arrow"></span>
            </label>
            <div class="panel" ng-show="showDrop == 'home'">
                <div class="checkbox-input">
                    <label  ng-repeat="type in homeTypesAttribute" >
                        <input type="checkbox" ng-model="type.label"  ng-checked="getCheckedTrue(type.value)" value="{{type.value}}">{{type.label}}
                    </label>
                </div>
            </div>
        </div>
        <div class="price drop-down">
            <label ng-click="showDropDown('price')">
                {{priceRange()}}
                <span class="icon-arrow"></span>
            </label>
            <div class="panel" ng-show="showDrop == 'price'">
                <input type="text" id="number" size="20" name="minprice" ng-model="minPrice[0]" value="{{minPrice[0]}}" ng-click="showPriceRange('minprice')">
                <span>to</span>
                <input type="text" id="number" size="20" name="maxprice" ng-model="maxPrice[0]" value="{{maxPrice[0]}}">
                <div class="price-options" style=" cursor: pointer;">
                    <div class="min-price-options" ng-show="showPrice == 'minprice'">
                        <span ng-repeat="range in minPriceRange" ng-click="addMinPrice(range.value)">${{range.value}}</span>
                    </div>
                    <div class="max-price-options" ng-show="showPrice == 'maxprice'">
                        <span ng-repeat="range in maxPriceRange" ng-click="addMaxPrice(range.value)">${{range.value}}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="beds drop-down">
            <label ng-click="showDropDown('beds')">
                {{changeBeds()}} + Beds
                <span class="icon-arrow"></span>
            </label>
            <div class="panel" ng-show="showDrop == 'beds'">
                <select class="bed-range" ng-model="selectedBed">
                    <option ng-repeat="bed in numBeds" value="{{bed.value}}"> +{{bed.value}} Beds</option>
                </select>
            </div>
        </div>
        <div class="more-option drop-down">
            <label ng-click="showDropDown('more')">
                More
                <span class="icon-arrow"></span>
            </label>
            <div class="panel" ng-show="showDrop == 'more'">
                    <section>
                        <header>More Options</header>
                        <div class="section-content">
                            <div class="sqrt-control">
                                <label>
                                    Square Feet
                                </label>
                                <div class="sqrt-control-group">
                                    <input type="number" ng-model="minSqft" id="sqft" size="20" name="minsqft">
                                    <span>to</span>
                                    <input type="number" ng-model="maxSqft" id="sqft" size="20" name="maxsqft">
                                </div>
                            </div>
                            <div class="select-field">
                                <label>
                                    Listed Since
                                </label>
                                <input type="text" ng-model="listingSince" id="listed-since" size="20" name="listed">
                            </div>
                            <div class="select-field">
                                <label>
                                    Bathrooms
                                </label>
                                <select id="select-field-bathrooms" ng-model="bathrooms">
                                    <option ng-repeat="bath in numBaths" value="{{bath.value}}"> +{{bath.value}} Baths</option>
                                </select>
                            </div class="select-field">
                            <div class="select-field">
                                <label>
                                    Parking
                                </label>
                                <select id="select-field-parking" ng-model="parking">
                                    <option ng-repeat="parking in numParking" value="{{parking.value}}"> +{{parking.value}} Garage</option>
                                </select>
                            </div>
                            <div class="select-field">
                                <label>
                                    <input type="checkbox" name="open-house" ng-model="openHouse">
                                    <span>Open House</span>
                                </label>
                                <label>
                                    <input type="checkbox" name="garage" ng-model="garage" >
                                    <span>Garage</span>
                                </label>
                            </div>
                        </div>
                    </section>

                    <section>
                        <header>House Filters</header>
                        <div class="section-content">
                            <div class="select-field">
                                <label class="house-filter">
                                    <input type="checkbox" name="single" ng-model="single" >
                                    <span>Single family</span>
                                </label>
                                <label class="house-filter">
                                    <input type="checkbox" name="singleBasement" ng-model="singleBasement" >
                                    <span>Single family with basement apartment</span>
                                </label>
                                <label class="house-filter">
                                    <input type="checkbox" name="duplex" ng-model="duplex" >
                                    <span>Duplex</span>
                                </label>
                                <label class="house-filter">
                                    <input type="checkbox" name="triplex" ng-model="triplex" >
                                    <span>Triplex</span>
                                </label>
                                <label class="house-filter">
                                    <input type="checkbox" name="fourplex" ng-model="fourplex" >
                                    <span>Fourplex +</span>
                                </label>
                            </div>
                        </div>
                    </section>
            </div>
        </div>
        <div class="reset-search">
            <label ng-click="resetForm()">Reset Filters</label>
        </div>
        <div class="save-search">
            <button onClick="showSaveSearch();" class="secondary">Save Search</button>
        </div>
    </div>
    <div class="listing-area">
        <div class="listing-sidebar scroll-reset">
            <div class="select-option">
                <label>Sort by:</label>
                <select class="sort-by" ng-model="sortingBy">
                    <option ng-repeat="listingSort in typeSort" ng-selected="sortingBy == listingSort.name"  value="{{listingSort.name}}">{{listingSort.label}}‎</option>
                </select>
            </div>
            <div ng-show="loading" class="loading"><img src="<?php echo i5PBIntegration__PLUGIN_URL; ?>images/loading.gif"></div>
            <div class="listing-property"  ng-animate="'animate'" ng-repeat="propertylisting in listing  | startFrom:currentPage*pageSize | limitTo:pageSize">
                <div class="listing">
                    <div class="image-wrapper">
                        <a href="/property-details/{{propertylisting.url}}"> <img ng-src="{{propertylisting.image}}" class="animate-show" fade-in /> </a>
                    </div>
                    <div class="listing-info">

                        <div class="listing-street">
                            {{propertylisting.address}}
                        </div>
                        <div class="listing-price">
                            $ {{propertylisting.price}}
                        </div>
                        <div class="listing-details">
                            <span><strong>{{propertylisting.bedrooms}}</strong> beds</span>
                            <span><strong>{{propertylisting.fullBaths}}</strong> baths</span>
                        </div>
                        <div class="listing-sqft">
                            {{propertylisting.size}} sq ft
                        </div>
                        <div class="listing-added-date">
                            Added {{propertylisting.lastUpdated}}
                        </div>
                    </div>
                    <div class="favorite">
                        <a href="javascript:toggleFav('{{propertylisting.mlsid}}');setFavorite('propertylisting.mlsid',1);">
                            <i class="fa fa-heart"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="searchFilter" id="pagi_block">
                <button ng-disabled="currentPage == 0" ng-click="currentPage=currentPage-1" class="prev" >
                    <i class="fa fa-angle-left" aria-hidden="true" style="padding-right:15px;"></i>
                </button>
                <span> {{currentPage+1}} - {{listtingVal/pageSize | ceil}}</span>
                <button ng-disabled="currentPage >= listtingVal/pageSize - 1" ng-click="currentPage=currentPage+1" class="next" >
                    <i class="fa fa-angle-right" aria-hidden="true" style="padding-left:15px;"></i>
                </button>
            </div>
        </div>
        <div class="map-container">
            <div ng-show="loading" class="loading-map"><img src="<?php echo i5PBIntegration__PLUGIN_URL; ?>images/loading.gif"></div>
            <div id="map-container"><div id="map"></div></div>
        </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyDdKFY7uK1hS4zqPja6sERrKhEtvOikNmc"></script>
    <script type="text/javascript" src="<?php echo i5PBIntegration__PLUGIN_URL; ?>js/markerclusterer.js"></script>
</section>


<script>
    jQuery(document).ready(function(){
        jQuery('#main').addClass('width-100');
        jQuery('#main div:first-child').removeClass('fusion-row');
        jQuery('#main').css('padding-left','0');
        jQuery('#main').css('padding-right', '0');

        // remove select option #1
        jQuery(".bed-range").find("option").eq(0).remove();
    });


</script>
<?php
include(i5PBIntegration__PLUGIN_DIR . 'Includes/Global-Functions.php');
get_footer();
?>