<?php
/*
Template Name: Property Detail Theme 1
 */

global $wpdb;
wp_enqueue_style( 'property-detail1', plugin_dir_url() . 'i5PBIntegration/css/property-detail1.css', array(), '1.1', 'all');
wp_enqueue_style( 'slick-styles', plugin_dir_url() . 'i5PBIntegration/css/slick.css', array(), '1.1', 'all');
wp_enqueue_style( 'slick-theme', plugin_dir_url() . 'i5PBIntegration/css/slick-theme.css', array(), '1.1', 'all');
wp_enqueue_script( 'slick', plugin_dir_url() . 'i5PBIntegration/js/slick.min.js', array('jquery'), '1.1', true );

get_header();

/* Theme Listing Page Module */
$theme_listing_module = get_option('theme_listing_module');

/* Only for demo purpose only */
if(isset($_GET['module'])){
    $theme_listing_module = $_GET['module'];
}

switch($theme_listing_module){
    case 'properties-map':
        get_template_part('banners/map_based_banner');
        break;

    default:
        get_template_part('banners/default_page_banner');
        break;
}

include(i5PBIntegration__PLUGIN_DIR . 'Includes/Prospect-Functions.php');
include(i5PBIntegration__PLUGIN_DIR . 'Includes/PropertyDetail-Functions.php');


?>
<!-- Content -->
<div class="container contents lisitng-grid-layout">
    <div class="row">
        <div class="span9 main-wrap">

            <!-- Main Content -->
            <div class="main">

                <section class="listing-layout">

                    <div class="list-container clearfix" style="padding:0;">

                        <div class="propertyOverview">
                            <div class="propertyName">
                                <h4 class="title">
                                    <?php echo $property->title ?>
                                </h4>
                                <h5 class="price">
                                    <?php echo "$" . number_format($property->price,0)?>
                                </h5>
                            </div>
                            <div class="propertyImages">
                                <?php
                                foreach($images as $image)
                                    echo " <div><img src='" . $image->url . "' /></div>";
                                ?>
                            </div>
                            <div class="propertyThumbs">
                                <?php
                                foreach($images as $image)
                                    echo " <div><img src='" . $image->url . "' /></div>";
                                ?>
                            </div>
                            <script>
                                jQuery(window).ready(function () {
                                    jQuery(".propertyImages").slick({
                                        slidesToShow: 1,
                                        slidesToScroll: 1,
                                        arrows: false,
                                        fade: true,
                                        asNavFor: '.propertyThumbs'
                                    });
                                    jQuery(".propertyThumbs").slick({
                                        slidesToShow: 3,
                                        slidesToScroll: 1,
                                        asNavFor: '.propertyImages',
                                        dots: false,
                                        centerMode: true,
                                        focusOnSelect: true
                                    });
                                });
                            </script>
                        </div>
                        <div class="propertyDetails">
                            <div class="<?php echo ($favs=='' || !in_array($result->mlsid,$favs)?'favorites':'favoritesSelected')?>">
                                <a href="javascript:setFavorite('<?php echo $result->mlsid ?>',<?php echo ($favs=='' || !in_array($result->mlsid,$favs)?'1':'0')?>);">
                                    <i class="fa fa-heart">&nbsp;</i>
                                </a>
                            </div>
                            <div class="propBox">
                                <div class="propValue">
                                    <?php echo $property->bedrooms?>
                                </div>
                                <div class="propLabel">Bedrooms</div>
                            </div>
                            <div class="propBox">
                                <div class="propValue">
                                    <?php
                                    if($property->halfBaths!=0 && $property->fullBaths!=0)
                                        echo $property->fullBaths + ($property->halfBaths/2);
                                    else if($property->halfBaths!=0 && $property->fullBaths==0)
                                        echo ($property->halfBaths/2);
                                    else
                                        echo number_format($property->fullBaths,0);
                                    ?>
                                </div>
                                <div class="propLabel">Bathrooms</div>
                            </div>
                            <div class="propBox">
                                <?php if ( $property->size!=0 ) : ?>
                                <div class="propValue">
                                    <?php echo $property->size?>
                                </div>
                                <div class="propLabel">Size</div>
                                <?php elseif($property->lotsize!=0) : ?>
                                <div class="propValue">
                                    <?php echo $property->lotsize?>
                                </div>
                                <div class="propLabel">Lot Size</div>
                                <?php else :?>
                                <div class="propValue">
                                    <?php echo $property->mlsid;?>
                                </div>
                                <div class="propLabel">Listing Id</div>
                                <?php endif; ?>
                            </div>
                            <div class="propBox">
                                <div class="propValue">
                                    <?php echo $property->yearbuilt==""?"Unknown":$property->yearbuilt?>
                                </div>
                                <div class="propLabel">Year Built</div>
                            </div>
                            <div class="propBox scheduleShowing">
                                <a href="#">
                                    Schedule
                                    <br />
                                    Showing
                                </a>
                            </div>
                        </div>
                        <div class="fieldsWrapper">
                            <div class="propertyDesc">
                                <h3 class="propertyTitle">Description</h3>
                                <p>
                                    <?php echo $property->description?>
                                </p>
                            </div>
                            <div class="detailsContainer">
                                <h3 class="propertyTitle">Features</h3>
                                <div class="detailsColumn">
                                    <div id="fieldCity">
                                        <span class="fieldLabel">City:</span>
                                        <span class="fieldData">
                                            <?php echo $property->city?>
                                        </span>
                                    </div>
                                    <div id="fieldPrice">
                                        <span class="fieldLabel">Price:</span>
                                        <span class="fieldData">
                                            <?php echo "$" . number_format($property->price,0)?>
                                        </span>
                                    </div>
                                    <?php if($property->view!="") :?>
                                    <div id="fieldView">
                                        <span class="fieldLabel">View:</span>
                                        <span class="fieldData">
                                            <?php echo $property->view?>
                                        </span>
                                    </div>
                                    <?php endif;?>
                                </div>
                                <div class="detailsColumn">
                                    <div id="fieldStatus">
                                        <span class="fieldLabel">Status:</span>
                                        <span class="fieldData">
                                            <?php echo $property->status?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <!-- end detailsContainer -->
                            <div class="detailsDisclaimer">
                                Listing provided courtesy of Chantel Ray Real Estate Inc.
                            � 2016 REIN, Inc. Information Deemed Reliable But Not Guaranteed.
                            </div>
                        </div>
                        <!-- end fields wrapper -->
                        <?php if($property->latitude!=0):?>
                        <div class="propertyMap">
                            <?php
                                  do_shortcode('[i5ListingMap maxzoom="100" minzoom="1" style="display:block;width: 100%; height: 418px" showdetails="false" showpolygon="false" zoom="15" lat="' . $property->latitude . '" long="' . $property->longitude . '" ]');
                                  $img="/wp-content/plugins/i5PBIntegration/images/imgUnavailable.png";

                                  if(isset($images) && sizeof($images)>0)
                                      $img=$images[0]->url;

                                  echo "<script>markAddress(" . json_encode($property) . ");</script>";
                            ?>
                        </div>
                        <?php endif;?>
                    </div>
                    <!-- end list container -->

                    <?php //theme_pagination( $property_listing_query->max_num_pages); ?>

                </section>

            </div>
            <!-- End Main Content -->

        </div>
        <!-- End span9 -->
        <div class="span3">
            <div class="propertySidebar">
                <?php if ( is_active_sidebar( 'property_detail_right' ) ) : ?>
                <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
                    <?php dynamic_sidebar( 'property_detail_right' ); ?>
                </div>
                <!-- #primary-sidebar -->
                <?php endif; ?>
            </div>
        </div>

    </div>
    <!-- End contents row -->
</div>
<!-- End Content -->
<?php get_footer(); ?>