<?php 
wp_enqueue_script("Listings_Script",i5PBIntegration__PLUGIN_URL . 'js/i5Listings.js');
include(i5PBIntegration__PLUGIN_DIR . 'Includes/SaveSearch-Popup.php');
?>

<script type="text/javascript">
    jQuery(function () {
        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();

        jQuery("#inputSearch").keyup(function () {
            delay(function () {
                doAutoComplete();
            }, 200);
        });

        var mnu = getCookie("openMenu");

        if (mnu != "") {
            jQuery("." + mnu).addClass("show");

            if (mnu == 'ddPrice') {
                jQuery("#maxprice").focus();
                jQuery("#selMinPrice").hide();
                jQuery("#selMaxPrice").show();
            }

            document.cookie = "openMenu=";
        }
    });
    function doAutoComplete() {
        if (jQuery("#searchtype").val() == "Address" || jQuery("#searchtype").val() == "MLSID")
            queryListings("url as redirect,'" + jQuery("#searchtype").val() + "' as section," + jQuery("#searchtype").val() + " as result", jQuery("#searchtype").val().replace(" ", "") + " like '%" + jQuery("#inputSearch").val() + "%'", populateSearch, null, jQuery("#searchtype").val().replace(" ", ""), "10");
        else if (jQuery("#searchtype").val() == "School")
            queryListings("count(*),'" + jQuery("#searchtype").val() + "' as section," + jQuery("#searchtype").val().replace(" ", "") + " as result", "elementaryschool like '%" + jQuery("#inputSearch").val() + "%' or middleschool like '%" + jQuery("#inputSearch").val() + "%' or highschool like '%" + jQuery("#inputSearch").val() + "%'", populateSearch, jQuery("#searchtype").val().replace(" ", "") + ",section", jQuery("#searchtype").val().replace(" ", ""), "10");
        else if(jQuery("#searchtype").val()=="" || jQuery("#searchtype").val()=="all")
            autoComplete(jQuery("#inputSearch").val(), 4, populateSearch);
        else
            queryListings("count(*),'" + jQuery("#searchtype").val() + "' as section," + jQuery("#searchtype").val().replace(" ", "") + " as result", jQuery("#searchtype").val().replace(" ", "") + " like '%" + jQuery("#inputSearch").val() + "%'", populateSearch, jQuery("#searchtype").val().replace(" ", "") + ",section", jQuery("#searchtype").val().replace(" ", ""), "10");
    }
    var lastSection = "";
    function populateSearch(list) {
        jQuery(".ulSearchResults").empty();
        var html = "";

        if(list!=null && list.length>0)
        {
            for(var i=0;i<list.length;i++)
            {
                if (lastSection != list[i].section || i==0) {
                    html += "<li class='resultsHeader'>" + list[i].section + "</span></li>";
                    lastSection = list[i].section;
                }

                if (list[i].redirect == null || list[i].redirect=="")
                    html += "<li class='resultsItem'><a href='javascript:doSearch(null,null,null,\"" + lastSection + "\",\"" + list[i].result + "\");'>" + list[i].result + "</a></li>";
                else
                    html += "<li class='resultsItem'><a href='javascript:redirect(\"" + list[i].redirect + "\");'>" + list[i].result + "</a></li>";
            }
        }
        else
        {
            html += "<li class='resultsHeader'>" + lastSection + "<br/>No Results</span></li>";
        }

        jQuery(".ulSearchResults").append(html);
    }
    function redirect(url)
    {
        window.location = "/property-details/" + url;
    }
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    function setPrice(cnt,vl)
    {
        if(vl!=0)
            jQuery("#" + cnt).val(vl);
        else
            jQuery("#" + cnt).val("");

        if(cnt=="minprice")
        {
            jQuery("#maxprice").focus();
            jQuery("#selMinPrice").hide();
            jQuery("#selMaxPrice").show();
            menuStick("ddPrice");
            doSearch();
        }
        else
        {
            jQuery("#minprice").focus();
            jQuery("#selMinPrice").show();
            jQuery("#selMaxPrice").hide();
            doSearch();
        }
    }
    function setSearchType(t,label) {
        jQuery("#searchtype").val(t);
        jQuery("#inputSearch").prop("placeholder", label);
    }
    function menuStick(vl) {
        document.cookie = "openMenu=" + vl;
    }
    function showSearchBy() {
        jQuery(".dropdownContent").removeClass("show");
        document.getElementById("dropdown").classList.toggle("show");
  }
    function showPriceFilters() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddPrice").classList.toggle("show");
  }
    function showTypeFilters() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddType").classList.toggle("show");
  }
    function showBedFilters() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddBeds").classList.toggle("show");
  }
    function showBathFilters() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddBaths").classList.toggle("show");
  }
    function showMoreFilters() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddMore").classList.toggle("show");
  }
  	function showSort() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddSort").classList.toggle("show");
  }
    function showSearchResults() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddSearchResults").classList.toggle("show");
  }
  // Close the dropdown if the user clicks outside of it
  window.onclick = function(event) {
  if (!event.target.matches('.dropBtn')) {

  var dropdowns = document.getElementsByClassName("dropdownContent");
  var i;
  for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
  function showSaveSearch() {
      var filters="";
      var display = "";
      var propType="";
        <?php if(isset($_REQUEST["minprice"]))
              {
                  echo "filters+='pba__ListingPrice_pb_min__c:" . $_REQUEST["minprice"] . ";';";
                  echo "display+='<div class=\"filter\">Price Min: " . $_REQUEST["minprice"] . "</div>';";
              }
              if(isset($_REQUEST["maxprice"]))
              {
                  echo "filters+='pba__ListingPrice_pb_max__c:" . $_REQUEST["maxprice"] . ";';";
                  echo "display+='<div class=\"filter\">Price Max: " . $_REQUEST["maxprice"] . "</div>';";
              }
              if(isset($_REQUEST["maxbedrooms"]))
              {
                  echo "filters+='pba__Bedrooms_pb_max__c:" . $_REQUEST["maxbedrooms"] . ";';";
                  echo "display+='<div class=\"filter\">Bed(s) Max: " . $_REQUEST["maxbedrooms"] . "</div>';";
              }
              if(isset($_REQUEST["minbedrooms"]))
              {
                  echo "filters+='pba__Bedrooms_pb_min__c:" . $_REQUEST["minbedrooms"] . ";';";
                  echo "display+='<div class=\"filter\">Bed(s) Min: " . $_REQUEST["minbedrooms"] . "</div>';";
              }
              if(isset($_REQUEST["minfullBaths"]))
              {
                  echo "filters+='pba__FullBathrooms_pb_min__c:" . $_REQUEST["minfullBaths"] . ";';";
                  echo "display+='<div class=\"filter\">Bath(s) Min: " . $_REQUEST["minfullBaths"] . "</div>';";
              }
              if(isset($_REQUEST["maxfullBaths"]))
              {
                  echo "filters+='pba__FullBathrooms_pb_max__c:" . $_REQUEST["maxfullBaths"] . ";';";
                  echo "display+='<div class=\"filter\">Bath(s) Max: " . $_REQUEST["maxfullBaths"] . "</div>';";
              }
              if(isset($_REQUEST["minsize"]))
              {
                  echo "filters+='pba__TotalArea_pb_min__c:" . $_REQUEST["minsize"] . ";';";
                  echo "display+='<div class=\"filter\">Size Min: " . $_REQUEST["minsize"] . "</div>';";
              }
              if(isset($_REQUEST["maxsize"]))
              {
                  echo "filters+='pba__TotalArea_pb_max__c:" . $_REQUEST["maxsize"] . ";';";
                  echo "display+='<div class=\"filter\">Size Max: " . $_REQUEST["maxsize"] . "</div>';";
              }
              if(isset($_REQUEST["minlotsize"]))
              {
                  echo "filters+='pba__LotSize_pb_min__c:" . $_REQUEST["minlotsize"] . ";';";
                  echo "display+='<div class=\"filter\">Acres Min: " . $_REQUEST["minlotsize"] . "</div>';";
              }
              if(isset($_REQUEST["maxlotsize"]))
              {
                  echo "filters+='pba__LotSize_pb_max__c:" . $_REQUEST["maxlotsize"] . ";';";
                  echo "display+='<div class=\"filter\">Acres Max: " . $_REQUEST["maxlotsize"] . "</div>';";
              }
              if(isset($_REQUEST["minyearbuilt"]))
              {
                  echo "filters+='pba__YearBuilt_pb_min__c:" . $_REQUEST["minyearbuilt"] . ";';";
                  echo "display+='<div class=\"filter\">Year Built Min: " . $_REQUEST["minyearbuilt"] . "</div>';";
              }
              if(isset($_REQUEST["maxyearbuilt"]))
              {
                  echo "filters+='pba__YearBuilt_pb_min__c:" . $_REQUEST["maxyearbuilt"] . ";';";
                  echo "display+='<div class=\"filter\">Year Built Max: " . $_REQUEST["maxyearbuilt"] . "</div>';";
              }
              if(isset($_REQUEST["minparkingSpaces"]))
              {
                  echo "filters+='ParkingSpaces__c:" . $_REQUEST["minparkingSpaces"] . ";';";
                  echo "display+='<div class=\"filter\">Parking Spaces Min: " . $_REQUEST["minparkingSpaces"] . "</div>';";
              }
              if(isset($_REQUEST["maxparkingSpaces"]))
              {
                  echo "filters+='Parking_Spaces_Max__c:" . $_REQUEST["maxparkingSpaces"] . ";';";
                  echo "display+='<div class=\"filter\">Parking Spaces Max: " . $_REQUEST["maxparkingSpaces"] . "</div>';";
              }
              if(isset($_REQUEST["minstories"]))
              {
                  echo "filters+='Stories_Min__c:" . $_REQUEST["minstories"] . ";';";
                  echo "display+='<div class=\"filter\">Stories Min: " . $_REQUEST["minstories"] . "</div>';";
              }
              if(isset($_REQUEST["maxstories"]))
              {
                  echo "filters+='Stories_Max__c:" . $_REQUEST["maxstories"] . ";';";
                  echo "display+='<div class=\"filter\">Stories Max: " . $_REQUEST["maxstories"] . "</div>';";
              }
              if(isset($_REQUEST["searchtype"]) && isset($_REQUEST["search"]))
              {
                  if($_REQUEST["searchtype"]=="City")
                      echo "filters+='pba__City_pb__c:" . $_REQUEST["search"] . ";';";
                  else if($_REQUEST["searchtype"]=="Zip Code")
                      echo "filters+='pba__PostalCode_pb__c:" . $_REQUEST["search"] . ";';";
                  else if($_REQUEST["searchtype"]=="area")
                      echo "filters+='pba__Area_pb__c:" . $_REQUEST["search"] . ";';";
                  else if($_REQUEST["searchtype"]=="Subdivision")
                      echo "filters+='Subdivision__c:" . $_REQUEST["search"] . ";';";

                  echo "display+='<div class=\"filter\">" . $_REQUEST["searchtype"]. ": " . $_REQUEST["search"] . "</div>';";
              }

              foreach($types as $type){
                  if(isset($type) && $type!="")
                      echo "propType+='" . $type . ",';";
                  echo "display+='<div class=\"filter\">Property Type: " . $type . "</div>';";
              }
        ?>

      if (filters != "" || propType != "")
      {
          if(filters!="")
            filters=filters.substring(0,filters.length-1);

          if (filters != "" && propType != "")
              filters += ";";

          if (propType != "")
          {
              propType = propType.substring(0, propType.length - 1);
              filters += "pba__PropertyType__c:" + propType;
              }

          jQuery("#filters").val(filters);
          jQuery('#saveSearch').modal('show');
      }
      else
          alert("Please specify your search parameters above.");
  }
</script>