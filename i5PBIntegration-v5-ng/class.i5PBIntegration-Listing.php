<?php
class i5PBIntegration_Listing
{
    private static $initiated = false;

    public static function init(){
        if(!self::$initiated){
            self::init_hooks();
            self::$initiated=true;
        }

        wp_register_script( "Listings_Script", WP_PLUGIN_URL.'/i5PBIntegration/js/i5Listings.js', array('jquery') );

        $adminUrl;

        if(post_type_exists("i5agents"))
            $adminUrl="http://" . $_SERVER["HTTP_HOST"] . "/wp-admin/admin-ajax.php";
        else
            $adminUrl=admin_url( 'admin-ajax.php' );

        wp_localize_script( 'Listings_Script', 'i5PBAjax', array( 'ajaxurl' => $adminUrl,'nonce' => wp_create_nonce('i5PBIntegration')));

        wp_enqueue_script( 'Listings_Script' );
    }
    public static function init_hooks(){
        add_action('wp_ajax_queryListings',array('i5PBIntegration_Listing', 'doQuery'));
        add_action('wp_ajax_nopriv_queryListings',array('i5PBIntegration_Listing', 'doQuery'));
        add_action('wp_ajax_autoCompleteListings',array('i5PBIntegration_Listing', 'doAutoCompleteSearch'));
        add_action('wp_ajax_nopriv_autoCompleteListings',array('i5PBIntegration_Listing', 'doAutoCompleteSearch'));

        add_action('wp_ajax_propertyListing',array('i5PBIntegration_Listing', 'propertyListing'));
        add_action('wp_ajax_nopriv_propertyListing',array('i5PBIntegration_Listing', 'propertyListing'));
    }

    public static function doAutoCompleteSearch(){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], "i5PBIntegration")) {
            exit("Denied");
        }

        global $wpdb;
        $limit=5;

        if(isset($_REQUEST["limit"]) && isset($_REQUEST["limit"])!="")
            $limit=$_REQUEST["limit"];

        $sql = "(select count(*), '' as redirect, 'City' as section,city as result from {$wpdb->prefix}i5listings where city like '" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), url as redirect, 'Address' as section,address as result from {$wpdb->prefix}i5listings where replace(address,'  ',' ') like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), url as redirect, 'MLSID' as section,mlsid as result from {$wpdb->prefix}i5listings where mlsid like '" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'Zip Code' as section,zipcode as result from {$wpdb->prefix}i5listings where zipcode like '" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'Subdivision' as section,Subdivision as result from {$wpdb->prefix}i5listings where Subdivision like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'School District' as section,schooldistrict as result from {$wpdb->prefix}i5listings where schooldistrict like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'Area' as section,Area as result from {$wpdb->prefix}i5listings where Area like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'School' as section,elementaryschool as result from {$wpdb->prefix}i5listings where elementaryschool like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'School' as section,middleschool as result from {$wpdb->prefix}i5listings where middleschool like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'School' as section,highschool as result from {$wpdb->prefix}i5listings where highschool like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                order by case when section='city' then 1 when section='address' then 2 when section='area' then 3 else 4 end,result";

        $results = $wpdb->get_results($sql);

        echo json_encode($results);

        die();

    }
    public static function doQuery(){
        global $wpdb;

        if ( !wp_verify_nonce( $_REQUEST['nonce'], "i5PBIntegration")) {
            exit("Denied");
        }

        $sql ="select " . str_replace("\'","'",$_REQUEST["query"]) . ",i.url as image from {$wpdb->prefix}i5listings as l left join {$wpdb->prefix}i5Images i on i.id = (select min(id) from {$wpdb->prefix}i5Images ii where ii.propertyid=l.propertyid)";

        if(isset($_REQUEST["where"]) && $_REQUEST["where"]!="")
            $sql .= " where " . str_replace("\'","'",$_REQUEST["where"]);

        if(isset($_REQUEST["group"]) && $_REQUEST["group"] !="")
            $sql .= " group by " . $_REQUEST["group"];

        if(isset($_REQUEST["order"]) && $_REQUEST["order"]!="")
            $sql .= " order by " . $_REQUEST["order"];

        if(isset($_REQUEST["limit"]) && $_REQUEST["limit"]!="")
            $sql .= " limit " . $_REQUEST["limit"];

        $results = $wpdb->get_results($sql);

        echo json_encode($results);

        die();
    }

    public static function propertyListing(){
        global $wpdb;
        // search fields
        $fields = array("term", "sorting", "propertytype", "minprice", "maxprice", "bedrooms", "minlotsize", "maxlotsize", "lastUpdated", "fullBaths", "parking", "homespecs", "hfilter");

        // prepare query to fetch data
        $sql = "SELECT mlsid,recordType,area,subdivision,lastUpdated,propertytype,title,status,l.url,latitude,longitude,i.url AS image,mlsid,bedrooms,address,city,state,zipCode,description,price,fullBaths,halfBaths,size,agentFirstName,agentEmail,agentMLSId FROM {$wpdb->prefix}i5listings l\r\n\tleft join {$wpdb->prefix}i5Images i on i.id = (SELECT min(id) FROM {$wpdb->prefix}i5Images ii WHERE ii.propertyid=l.propertyid)";

        $where = "";
        $join = "WHERE";
        $order = " order by case when status = 'new listing' then 1 when status='Active' then 2 else 3 end";
        $skip = 0;
        $pageSize = 25;


        foreach ($fields as $field) {
            if (isset($_REQUEST[$field]) && $_REQUEST[$field] != '') {
                if ($field == "term") {

                } else if($field == "sorting"){

                    $dir = "ASC";
                    $field = "";

                    if (substr(strtolower($_REQUEST["sorting"]), 0, 3) != "asc") {
                        $dir = "DESC";
                    }

                    if ($dir == "DESC") {
                        $field = substr($_REQUEST["sorting"], 4);
                    } else {
                        $field = substr($_REQUEST["sorting"], 3);
                    }

                    $order = " ORDER BY " . $field . " " . $dir;

                } else {

                    if($_REQUEST[$field] != '' && $_REQUEST[$field] != null){

                        if($field == "propertytype"){
                            $where .= " " . $join . " ";
                        }else {
                            $where .= " " . $join . " " . str_replace("max", "", str_replace("min", "", $field));
                        }

                        if (strlen($field) > 2 && strrpos($field, "min", -strlen($field)) !== false) {
                            $where .= " >= ";
                        } else if (strlen($field) > 2 && strrpos($field, "max", -strlen($field)) !== false) {
                            $where .= " <= ";
                        } else  {

                            if($field == "propertytype"){

                            }else {
                                $where .= " = ";
                            }

                        }

                        if($field == "propertytype"){
                            $typeValue = json_decode(stripslashes($_REQUEST["propertytype"]), true);
                            $typeSize = sizeof($typeValue);
                            foreach ($typeValue as $key => $value) {
                                if(!empty($value['label'])){

                                    $typeSize  = $typeSize - 1;
                                    if($typeSize >= 1){
                                       $where .=  ' ' . $field . ' LIKE "%' . $value['label'] . '%" OR';
                                    } else{
                                        $where .=  ' ' . $field . ' LIKE "%' . $value['label'] . '%"';
                                    }
                                }
                            }
                        } else {
                            $where .= $_REQUEST[$field];
                        }

                        $join = "and";
                    }
                }
            }
        }

        //echo $sql . $where . $order . " limit " . $skip . ", " . $pageSize;
        $sql = $sql . $where . $order . " limit " . $skip . ", " . $pageSize;
        //$test_sql = 'SELECT mlsid,recordType,area,subdivision,lastUpdated,propertytype,title,status,l.url,latitude,longitude ,i.url AS image,mlsid,bedrooms,address,city,state,zipCode,description,price,fullBaths,halfBaths,size ,agentFirstName,agentEmail,agentMLSId FROM wp_3_i5listings l left join wp_3_i5Images i on i.id = (SELECT min(id) FROM wp_3_i5Images ii WHERE ii.propertyid=l.propertyid ) WHERE propertytype LIKE "%Home%" OR propertytype LIKE "%Condo%" OR propertytype LIKE "%Apartment%" and price >= 0 and price <= 950000 and bedrooms = 1 and lotsize >= 1 and lotsize <= 200000 and fullBaths = 1 ORDER By price ASC limit 0, 25';
        $results = $wpdb->get_results($sql);

        echo json_encode($results);

        die();
    }
}