<?php
class i5PBIntegration_Admin
{
 private static $initiated = false;
    private static function getPBFields($settings){
        require_once( i5PBIntegration__PLUGIN_DIR . 'class.i5PBIntegration-Functions.php' );

        $vals = i5PBIntegration_Functions::GetApi($settings["OAuth"],"/services/data/v20.0/sobjects/pba__listing__c/describe");

        $items=array();

        $items[""]="";

        foreach($vals->fields as $field)
        {
            $items[$field->name]=$field->label;// . "(" . $field->name . ")";
        }
        
        return $items;
    }   
    public static function init(){
        if(!self::$initiated){
            self::init_hooks(); 
        }
    }
    public static function init_hooks(){
        add_action('admin_menu',array('i5PBIntegration_Admin','admin_menu'));
    }
    public static function admin_menu(){
        add_options_page('Propertybase Integration','Propertybase Integration','manage_options',__FILE__,array('i5PBIntegration_Admin', 'admin_interface'));
	}
    public static function admin_interface(){

        if ($_POST['syncNow'] == 'sync') {
           
           i5PBIntegration::mlsSync();
            
        }
      


        
             $doOAuth=false;
             $settings=get_option('WebListing_Settings');
             if(!isset($settings))
                 $settings =array('Prospect'=>false,'EndPoint'=>'','Token'=>'','Debug'=>false);

             if(isset($_REQUEST['code']) && !isset($settings["OAuth"]["OAuthToken"]))
             {

                 //Get the token
                 $url="https://login.salesforce.com/services/oauth2/token";

                 if($settings["Debug"]=="Yes")
                     $url="https://test.salesforce.com/services/oauth2/token";

                 $body="grant_type=authorization_code&client_id=" . $settings["OAuth"]["OAuthClientId"] . "&client_secret=" . $settings["OAuth"]["OAuthSecret"] . "&redirect_uri=" . urlencode('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']) . "&code=" . $_REQUEST['code'];

                 $ch=curl_init($url . "?" . $body);

                 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                 $token = curl_exec($ch);

                 if(FALSE==$token)
                 {
                     $err=curl_error($ch);
                 }
                 else
                 {
                     $var=json_decode($token);

                     if(isset($var->access_token))
                     {
                         $settings["OAuth"]["OAuthToken"]=$var->access_token;
                         $settings["OAuth"]["RefreshToken"]=$var->refresh_token;
                         $settings["OAuth"]["InstanceUrl"]=$var->instance_url;
                         update_option("WebListing_Settings",$settings);
                     }
                 }

                 curl_close($ch);
             }
    		 else if(isset($_POST['submit']))
    		 {
                 if(isset($_POST["ClientId"])) //Check to see if this is oauth
                 {
                     $doOAuth=true;
                     $settings["OAuth"]["OAuthClientId"]=$_POST["ClientId"];
                     $settings["OAuth"]["OAuthSecret"]=$_POST["Secret"];
                     $settings["Debug"]=isset($_POST["WebListingDebug"])?"Yes":"No";

                     update_option("WebListing_Settings",$settings);
                 }
                 else if(isset($_POST["ResetOAuth"]))
                 {
                     $settings["OAuth"]["OAuthToken"]=null;
                     update_option("WebListing_Settings",$settings);
                 }
                 else
                 {
                     $ok=true;
                     $message='<p>Please Correct the Following Errors</p>';
                    
                     if($ok)
                     {
                         $settings["Prospect"] =isset($_POST["EnableWebProspect"])?true:false;
                         $settings["Debug"]=isset($_POST["WebListingDebug"])?"Yes":"No";
                         $settings["EnableSync"]=isset($_POST["EnableSync"])?"Yes":"No";
                         $settings["GoogleAPIKey"]=$_POST["GoogleAPIKey"];
                         $settings["ExcludeMapsAPI"]=isset($_POST["ExcludeMapsAPI"])?"Yes":"No";

                         $settings["ProspectMappings"]=array();
                         if(isset($_POST["MapFirstName"]) && $_POST["MapFirstName"]!="")
                             $settings["ProspectMappings"]["FirstName"]=$_POST["MapFirstName"];
                         if(isset($_POST["MapLastName"]) && $_POST["MapLastName"]!="")
                             $settings["ProspectMappings"]["LastName"]=$_POST["MapLastName"];
                         if(isset($_POST["MapEmail"]) && $_POST["MapEmail"] !="")
                             $settings["ProspectMappings"]["Email"]=$_POST["MapEmail"];
                         if(isset($_POST["MapSaveSearchId"]) && $_POST["MapSaveSearchId"] !="")
                             $settings["ProspectMappings"]["MapSaveSearchId"]=$_POST["MapSaveSearchId"];
                         if(isset($_POST["MapPhone"]) && $_POST["MapPhone"]!="")
                             $settings["ProspectMappings"]["Phone"]=$_POST["MapPhone"];
                         if(isset($_POST["MapCust1PB"]) && isset($_POST["MapCust1WP"]) && $_POST["MapCust1PB"]!="" && $_POST["MapCust1WP"]!="")
                             $settings["ProspectMappings"]["MapCust1"]=array($_POST["MapCust1PB"], $_POST["MapCust1WP"]);
                         if(isset($_POST["MapCust2PB"]) && isset($_POST["MapCust2WP"]) && $_POST["MapCust2PB"]!="" && $_POST["MapCust2WP"]!="")
                             $settings["ProspectMappings"]["MapCust2"]=array($_POST["MapCust2PB"], $_POST["MapCust2WP"]);
                         if(isset($_POST["MapCust3PB"]) && isset($_POST["MapCust3WP"]) && $_POST["MapCust3PB"]!="" && $_POST["MapCust3WP"]!="")
                             $settings["ProspectMappings"]["MapCust3"]=array($_POST["MapCust3PB"], $_POST["MapCust3WP"]);
                         if(isset($_POST["MapCust4PB"]) && isset($_POST["MapCust4WP"]) && $_POST["MapCust4PB"]!="" && $_POST["MapCust4WP"]!="")
                             $settings["ProspectMappings"]["MapCust4"]=array($_POST["MapCust4PB"], $_POST["MapCust4WP"]);
                         if(isset($_POST["MapCust5PB"]) && isset($_POST["MapCust5WP"]) && $_POST["MapCust5PB"]!="" && $_POST["MapCust5WP"]!="")
                             $settings["ProspectMappings"]["MapCust5"]=array($_POST["MapCust5PB"], $_POST["MapCust5WP"]);
                         $settings["SelectFields"]=array();
                         //setup pb mappings
                         foreach($_POST as $key => $value)
                         {
                             if(substr($key,0,3)=="PBS" && isset($value) && $value!="")
                                 $settings["SelectFields"][$key]=$value;
                         }

                         //Setup where clauses
                         if(isset($_POST["WHField1"]) && $_POST["WHField1"]!="" && isset($_POST["WHValue1"]) && $_POST["WHValue1"]!="")
                         {
                             $settings["WHERE"]["Condition1"]["Field"]=$_POST["WHField1"];
                             $settings["WHERE"]["Condition1"]["Value"]=$_POST["WHValue1"];
                             $settings["WHERE"]["Condition1"]["Type"]=$_POST["WHType1"];
                         }
                         else
                         {
                             $settings["WHERE"]["Condition1"]=null;
                             $settings["WHERE"]["Condition2"]=null;
                             $settings["WHERE"]["Condition3"]=null;
                         }

                         if(isset($_POST["WHField2"]) && $_POST["WHField2"]!="" && isset($_POST["WHValue2"]) && $_POST["WHValue2"]!="")
                         {
                             $settings["WHERE"]["Condition2"]["Field"]=$_POST["WHField2"];
                             $settings["WHERE"]["Condition2"]["Value"]=$_POST["WHValue2"];
                             $settings["WHERE"]["Condition2"]["Type"]=$_POST["WHType2"];
                         }
                         else
                         {
                             $settings["WHERE"]["Condition2"]=null;
                             $settings["WHERE"]["Condition3"]=null;
                         }

                         if(isset($_POST["WHField3"]) && $_POST["WHField3"]!="" && isset($_POST["WHValue3"]) && $_POST["WHValue3"]!="")
                         {
                             $settings["WHERE"]["Condition3"]["Field"]=$_POST["WHField3"];
                             $settings["WHERE"]["Condition3"]["Value"]=$_POST["WHValue3"];
                             $settings["WHERE"]["Condition3"]["Type"]=$_POST["WHType3"];
                         }
                         else
                         {
                             $settings["WHERE"]["Condition3"]=null;
                         }

                         //Setup delete clauses
                         if(isset($_POST["DELField1"]) && $_POST["DELField1"]!="" && isset($_POST["DELValue1"]) && $_POST["DELValue1"]!="")
                         {
                             $settings["DELETE"]["Condition1"]["Field"]=$_POST["DELField1"];
                             $settings["DELETE"]["Condition1"]["Value"]=$_POST["DELValue1"];
                             $settings["DELETE"]["Condition1"]["Type"]=$_POST["DELType1"];
                         }
                         else
                         {
                             $settings["DELETE"]["Condition1"]=null;
                             $settings["DELETE"]["Condition2"]=null;
                             $settings["DELETE"]["Condition3"]=null;
                         }

                         if(isset($_POST["DELField2"]) && $_POST["DELField2"]!="" && isset($_POST["DELValue2"]) && $_POST["DELValue2"]!="")
                         {
                             $settings["DELETE"]["Condition2"]["Field"]=$_POST["DELField2"];
                             $settings["DELETE"]["Condition2"]["Value"]=$_POST["DELValue2"];
                             $settings["DELETE"]["Condition2"]["Type"]=$_POST["DELType2"];
                         }
                         else
                         {
                             $settings["DELETE"]["Condition2"]=null;
                             $settings["DELETE"]["Condition3"]=null;
                         }

                         if(isset($_POST["DELField3"]) && $_POST["DELField3"]!="" && isset($_POST["DELValue3"]) && $_POST["DELValue3"]!="")
                         {
                             $settings["DELETE"]["Condition3"]["Field"]=$_POST["DELField3"];
                             $settings["DELETE"]["Condition3"]["Value"]=$_POST["DELValue3"];
                             $settings["DELETE"]["Condition3"]["Type"]=$_POST["DELType3"];
                         }
                         else
                         {
                             $settings["DELETE"]["Condition3"]=null;
                         }

                         $result = update_option("WebListing_Settings",$settings);
                         echo "<p>Settings Updated</p>";
                     }
                     else
                     {
                         echo $message;
                     }
                 }
    		 }
         
?>
<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('#sync').click(function() {
        jQuery('#syncNow').val('sync');
    });
});
</script>
<div class="wrap">
    <form method="post" action="" novalidate="novalidate">
    <?php
        if(isset($settings["OAuth"]["OAuthToken"]))
        {
            $items=self::getPBFields($settings);
            asort($items);
    ?>
    <h1>Web Listings Settings</h1>
   
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">Status:</th>
                    <td>
                        <?php echo isset($settings["EnableSync"])?($settings["EnableSync"]=="No"?"Disabled":(isset($settings["Status"])?$settings["Status"]:"Never Run")):"Disabled"?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Last Sync:</th>
                    <td>
                        <input type="submit" id="sync" value="Sync Now" class="button button-primary" />
                        <input type="hidden" id="syncNow" name="syncNow" value="">
                        <?php //echo isset($settings["LastRun"])?$settings["LastRun"]:'Never'?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Next Run</th>
                    <td>
                        <?php //echo date("Y-m-d\Th:i:s",wp_next_scheduled("SyncListings"))?></td>
                </tr>
                <tr>
                    <th scope="row">Enable Listing Sync</th>
                    <td>
                        <input name="EnableSync" type="checkbox" <?php echo $settings["EnableSync"]=='Yes'? 'checked="true"':'' ?> />
                    </td>
                </tr>
                <tr>
                    <th scope="row">Reset OAuth</th>
                    <td>
                        <input name="ResetOAuth" type="checkbox" />
                    </td>
                </tr>
                <tr>
                    <th scope="row">Sandbox</th>
                    <td>
                        <input name="WebListingDebug" type="checkbox" <?php echo $settings["Debug"]=='Yes'? 'checked="true"':'' ?> />
                    </td>
                </tr>
                <tr>
                    <th colspan="2" scope="row">Google Maps API Settings:</th>
                </tr>
                <tr>
                    <th scope="row">API Key</th>
                    <td>
                        <input name="GoogleAPIKey" type="text" value='<?php echo isset($settings["GoogleAPIKey"])?$settings["GoogleAPIKey"]:"" ?>' />
                    </td>
                </tr>
                <tr>
                    <th scope="row">Don't Include Google Maps API Code</th>
                    <td>
                        <input name="ExcludeMapsAPI" type="checkbox" <?php echo $settings["ExcludeMapsAPI"]=='Yes'? 'checked="true"':'' ?> />
                    </td>
                </tr>
                <tr>
                    <th colspan="2" scope="row">Propertybase Field Mappings:</th>
                </tr>
                <tr>
                    <td>Listing Field</td>
                    <td>Propertybase Field</td>
                </tr>
                <tr>
                    <td>MLS Id</td>
                    <td>
                        <select name="PBSMLSID">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSMLSID"]) && $settings["SelectFields"]["PBSMLSID"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Size</td>
                    <td>
                        <select name="PBSSize">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSSize"]) && $settings["SelectFields"]["PBSSize"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Latitude</td>
                    <td>
                        <select name="PBSLatitude">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSLatitude"]) && $settings["SelectFields"]["PBSLatitude"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Longitude</td>
                    <td>
                        <select name="PBSLongitude">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSLongitude"]) && $settings["SelectFields"]["PBSLongitude"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td>
                        <select name="PBSStatus">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSStatus"]) && $settings["SelectFields"]["PBSStatus"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Bedrooms</td>
                    <td>
                        <select name="PBSBeds">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSBeds"]) && $settings["SelectFields"]["PBSBeds"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Full Baths</td>
                    <td>
                        <select name="PBSFullBaths">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSFullBaths"]) && $settings["SelectFields"]["PBSFullBaths"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Half Baths</td>
                    <td>
                        <select name="PBSHalfBaths">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSHalfBaths"]) && $settings["SelectFields"]["PBSHalfBaths"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>
                        <select name="PBSAddress">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSAddress"]) && $settings["SelectFields"]["PBSAddress"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Unit Number</td>
                    <td>
                        <select name="PBSUnitNumber">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSUnitNumber"]) && $settings["SelectFields"]["PBSUnitNumber"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>City</td>
                    <td>
                        <select name="PBSCity">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCity"]) && $settings["SelectFields"]["PBSCity"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>State</td>
                    <td>
                        <select name="PBSState">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSState"]) && $settings["SelectFields"]["PBSState"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Zipcode</td>
                    <td>
                        <select name="PBSZip">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSZip"]) && $settings["SelectFields"]["PBSZip"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Price</td>
                    <td>
                        <select name="PBSPrice">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSPrice"]) && $settings["SelectFields"]["PBSPrice"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Listing Type</td>
                    <td>
                        <select name="PBSListType">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSListType"]) && $settings["SelectFields"]["PBSListType"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Subdivision</td>
                    <td>
                        <select name="PBSSubdivision">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSSubdivision"]) && $settings["SelectFields"]["PBSSubdivision"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Year Built</td>
                    <td>
                        <select name="PBSYearBuilt">
                            <?php
                            foreach($items as $key => $value)
                            {
                                if(isset($settings["SelectFields"]["PBSYearBuilt"]) && $settings["SelectFields"]["PBSYearBuilt"]==$key)
                                    echo "<option value='$key' selected='true'>" . $value . "</option>";
                                else
                                    echo "<option value='$key'>" . $value . "</option>";
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Property Type</td>
                    <td>
                        <select name="PBSPropertyType">
                            <?php
                            foreach($items as $key => $value)
                            {
                                if(isset($settings["SelectFields"]["PBSPropertyType"]) && $settings["SelectFields"]["PBSPropertyType"]==$key)
                                    echo "<option value='$key' selected='true'>" . $value . "</option>";
                                else
                                    echo "<option value='$key'>" . $value . "</option>";
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Record Type</td>
                    <td>
                        <select name="PBSRecordType">
                            <?php
                            foreach($items as $key => $value)
                            {
                                if(isset($settings["SelectFields"]["PBSRecordType"]) && $settings["SelectFields"]["PBSRecordType"]==$key)
                                    echo "<option value='$key' selected='true'>" . $value . "</option>";
                                else
                                    echo "<option value='$key'>" . $value . "</option>";
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td>
                        <select name="PBSDescription">
                            <?php
                            foreach($items as $key => $value)
                            {
                                if(isset($settings["SelectFields"]["PBSDescription"]) && $settings["SelectFields"]["PBSDescription"]==$key)
                                    echo "<option value='$key' selected='true'>" . $value . "</option>";
                                else
                                    echo "<option value='$key'>" . $value . "</option>";
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Lot Size</td>
                    <td>
                        <select name="PBSLotSize">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSLotSize"]) && $settings["SelectFields"]["PBSLotSize"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Stories</td>
                    <td>
                        <select name="PBSStories">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSStories"]) && $settings["SelectFields"]["PBSStories"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Parking Spaces</td>
                    <td>
                        <select name="PBSParkingSpaces">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSParkingSpaces"]) && $settings["SelectFields"]["PBSParkingSpaces"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>School District</td>
                    <td>
                        <select name="PBSSchoolDistrict">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSSchoolDistrict"]) && $settings["SelectFields"]["PBSSchoolDistrict"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                 <tr>
                    <td>Elementary School</td>
                    <td>
                        <select name="PBSElementarySchool">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSElementarySchool"]) && $settings["SelectFields"]["PBSElementarySchool"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Middle School</td>
                    <td>
                        <select name="PBSMiddleSchool">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSMiddleSchool"]) && $settings["SelectFields"]["PBSMiddleSchool"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>High School</td>
                    <td>
                        <select name="PBSHighSchool">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSHighSchool"]) && $settings["SelectFields"]["PBSHighSchool"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>View</td>
                    <td>
                        <select name="PBSView">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSView"]) && $settings["SelectFields"]["PBSView"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Area</td>
                    <td>
                        <select name="PBSArea">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSArea"]) && $settings["SelectFields"]["PBSArea"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Detail URL Field</td>
                    <td>
                        <select name="PBSURL">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSURL"]) && $settings["SelectFields"]["PBSURL"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                 <tr>
                    <td>Agent First Name</td>
                    <td>
                        <select name="PBSAgentFirstName">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSAgentFirstName"]) && $settings["SelectFields"]["PBSAgentFirstName"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Agent Last Name</td>
                    <td>
                        <select name="PBSAgentLastName">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSAgentLastName"]) && $settings["SelectFields"]["PBSAgentLastName"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Agent Mobile</td>
                    <td>
                        <select name="PBSAgentMobile">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSAgentMobile"]) && $settings["SelectFields"]["PBSAgentMobile"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Agent Office Phone</td>
                    <td>
                        <select name="PBSAgentOfficePhone">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSAgentOfficePhone"]) && $settings["SelectFields"]["PBSAgentOfficePhone"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Agent Email</td>
                    <td>
                        <select name="PBSAgentEmail">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSAgentEmail"]) && $settings["SelectFields"]["PBSAgentEmail"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Agent MLS ID</td>
                    <td>
                        <select name="PBSAgentMLSID">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSAgentMLSID"]) && $settings["SelectFields"]["PBSAgentMLSID"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Listing Office Name</td>
                    <td>
                        <select name="PBSListingOffice">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSListingOffice"]) && $settings["SelectFields"]["PBSListingOffice"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				 <tr>
                    <td>Exterior Features</td>
                    <td>
                        <select name="PBSExteriorFeatures">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSExteriorFeatures"]) && $settings["SelectFields"]["PBSExteriorFeatures"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Exterior Siding</td>
                    <td>
                        <select name="PBSExteriorSiding">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSExteriorSiding"]) && $settings["SelectFields"]["PBSExteriorSiding"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Fence Description</td>
                    <td>
                        <select name="PBSFenceDesc">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSFenceDesc"]) && $settings["SelectFields"]["PBSFenceDesc"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Has Fence</td>
                    <td>
                        <select name="PBSFence">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSFence"]) && $settings["SelectFields"]["PBSFence"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Foundation Type</td>
                    <td>
                        <select name="PBSFoundationType">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSFoundationType"]) && $settings["SelectFields"]["PBSFoundationType"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Parking Description</td>
                    <td>
                        <select name="PBSParkingDesc">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSParkingDesc"]) && $settings["SelectFields"]["PBSParkingDesc"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Pool</td>
                    <td>
                        <select name="PBSPool">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSPool"]) && $settings["SelectFields"]["PBSPool"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Roof</td>
                    <td>
                        <select name="PBSRoof">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSRoof"]) && $settings["SelectFields"]["PBSRoof"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Style</td>
                    <td>
                        <select name="PBSStyle">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSStyle"]) && $settings["SelectFields"]["PBSStyle"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Waterfront Description</td>
                    <td>
                        <select name="PBSWaterfrontDesc">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSWaterfrontDesc"]) && $settings["SelectFields"]["PBSWaterfrontDesc"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Waterfront</td>
                    <td>
                        <select name="PBSWaterfront">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSWaterfront"]) && $settings["SelectFields"]["PBSWaterfront"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Appliances</td>
                    <td>
                        <select name="PBSAppliances">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSAppliances"]) && $settings["SelectFields"]["PBSAppliances"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Cooling</td>
                    <td>
                        <select name="PBSCooling">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCooling"]) && $settings["SelectFields"]["PBSCooling"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Equipment</td>
                    <td>
                        <select name="PBSEquipment">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSEquipment"]) && $settings["SelectFields"]["PBSEquipment"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Number of Fireplaces</td>
                    <td>
                        <select name="PBSFirePlaceNum">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSFirePlaceNum"]) && $settings["SelectFields"]["PBSFirePlaceNum"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Flooring</td>
                    <td>
                        <select name="PBSFlooring">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSFlooring"]) && $settings["SelectFields"]["PBSFlooring"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Heating</td>
                    <td>
                        <select name="PBSHeating">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSHeating"]) && $settings["SelectFields"]["PBSHeating"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Interior Features</td>
                    <td>
                        <select name="PBSInteriorFeat">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSInteriorFeat"]) && $settings["SelectFields"]["PBSInteriorFeat"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Number of Rooms</td>
                    <td>
                        <select name="PBSRoomNum">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSRoomNum"]) && $settings["SelectFields"]["PBSRoomNum"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Other Rooms</td>
                    <td>
                        <select name="PBSOtherRooms">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSOtherRooms"]) && $settings["SelectFields"]["PBSOtherRooms"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Water Heater Type</td>
                    <td>
                        <select name="PBSWaterHeater">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSWaterHeater"]) && $settings["SelectFields"]["PBSWaterHeater"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Disclosure</td>
                    <td>
                        <select name="PBSDisclosure">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSDisclosure"]) && $settings["SelectFields"]["PBSDisclosure"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>New Construction</td>
                    <td>
                        <select name="PBSNewConstruction">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSNewConstruction"]) && $settings["SelectFields"]["PBSNewConstruction"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Ownership Type</td>
                    <td>
                        <select name="PBSOwnershipType">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSOwnershipType"]) && $settings["SelectFields"]["PBSOwnershipType"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Sewer Type</td>
                    <td>
                        <select name="PBSSewer">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSSewer"]) && $settings["SelectFields"]["PBSSewer"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Taxes</td>
                    <td>
                        <select name="PBSTaxes">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSTaxes"]) && $settings["SelectFields"]["PBSTaxes"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Water Type</td>
                    <td>
                        <select name="PBSWater">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSWater"]) && $settings["SelectFields"]["PBSWater"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
				<tr>
                    <td>Zoning</td>
                    <td>
                        <select name="PBSZoning">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSZoning"]) && $settings["SelectFields"]["PBSZoning"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                 <tr>
                    <td>Custom 1</td>
                    <td>
                        <select name="PBSCustom1">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom1"]) && $settings["SelectFields"]["PBSCustom1"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Custom 2</td>
                    <td>
                        <select name="PBSCustom2">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom2"]) && $settings["SelectFields"]["PBSCustom2"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Custom 3</td>
                    <td>
                        <select name="PBSCustom3">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom3"]) && $settings["SelectFields"]["PBSCustom3"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Custom 4</td>
                    <td>
                        <select name="PBSCustom4">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom4"]) && $settings["SelectFields"]["PBSCustom4"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Custom 5</td>
                    <td>
                        <select name="PBSCustom5">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom5"]) && $settings["SelectFields"]["PBSCustom5"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Custom 6</td>
                    <td>
                        <select name="PBSCustom6">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom6"]) && $settings["SelectFields"]["PBSCustom6"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Custom 7</td>
                    <td>
                        <select name="PBSCustom7">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom7"]) && $settings["SelectFields"]["PBSCustom7"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Custom 8</td>
                    <td>
                        <select name="PBSCustom8">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom8"]) && $settings["SelectFields"]["PBSCustom8"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Custom 9</td>
                    <td>
                        <select name="PBSCustom9">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom9"]) && $settings["SelectFields"]["PBSCustom9"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Custom 10</td>
                    <td>
                        <select name="PBSCustom10">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["SelectFields"]["PBSCustom10"]) && $settings["SelectFields"]["PBSCustom10"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                </table>
        <table>
                <tr>
                    <th colspan="4">Where Clauses:</th>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <select name="WHField1">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["WHERE"]["Condition1"]["Field"]) && $settings["WHERE"]["Condition1"]["Field"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                        </td>
                    <td>
                        <select name="WHType1">
                            <option value="=" <?php echo (isset($settings["WHERE"]["Condition1"]["Type"]) && $settings["WHERE"]["Condition1"]["Type"]=="=")?"selected='true'":"" ?>>Equals</option>
                            <option value="!=" <?php echo (isset($settings["WHERE"]["Condition1"]["Type"]) && $settings["WHERE"]["Condition1"]["Type"]=="!=")?"selected='true'":"" ?>>Not Equal To</option>
                        </select>
                    </td>
                    <td>
                        <input name="WHValue1" type="text" value="<?php echo isset($settings["WHERE"]["Condition1"]["Value"])?$settings["WHERE"]["Condition1"]["Value"]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                       And
                        </td>
                    <td>
                        <select name="WHField2">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["WHERE"]["Condition2"]["Field"]) && $settings["WHERE"]["Condition2"]["Field"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                        </td>
                    <td>
                        <select name="WHType2">
                            <option value="=" <?php echo (isset($settings["WHERE"]["Condition2"]["Type"]) && $settings["WHERE"]["Condition2"]["Type"]=="=")?"selected='true'":"" ?>>Equals</option>
                            <option value="!=" <?php echo (isset($settings["WHERE"]["Condition2"]["Type"]) && $settings["WHERE"]["Condition2"]["Type"]=="!=")?"selected='true'":"" ?>>Not Equal To</option>
                        </select>
                    </td>
                    <td>
                        <input name="WHValue2" type="text" value="<?php echo isset($settings["WHERE"]["Condition2"]["Value"])?$settings["WHERE"]["Condition2"]["Value"]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        And
                        </td>
                    <td>
                        <select name="WHField3">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["WHERE"]["Condition3"]["Field"]) && $settings["WHERE"]["Condition3"]["Field"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                        </td>
                    <td>
                        <select name="WHType3">
                            <option value="=" <?php echo (isset($settings["WHERE"]["Condition3"]["Type"]) && $settings["WHERE"]["Condition3"]["Type"]=="=")?"selected='true'":"" ?>>Equals</option>
                            <option value="!=" <?php echo (isset($settings["WHERE"]["Condition3"]["Type"]) && $settings["WHERE"]["Condition3"]["Type"]=="!=")?"selected='true'":"" ?>>Not Equal To</option>
                        </select>
                    </td>
                    <td>
                        <input name="WHValue3" type="text" value="<?php echo isset($settings["WHERE"]["Condition3"]["Value"])?$settings["WHERE"]["Condition3"]["Value"]:""?>" />
                    </td>
                </tr>
            </table>
        <table>
                <tr>
                    <th colspan="4">Delete Rules<br/>Listings Matching The Following Conditions will Be Deleted</th>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <select name="DELField1">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["DELETE"]["Condition1"]["Field"]) && $settings["DELETE"]["Condition1"]["Field"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                        </td>
                    <td>
                        <select name="DELType1">
                            <option value="=" <?php echo (isset($settings["DELETE"]["Condition1"]["Type"]) && $settings["DELETE"]["Condition1"]["Type"]=="=")?"selected='true'":"" ?>>Equals</option>
                            <option value="!=" <?php echo (isset($settings["DELETE"]["Condition1"]["Type"]) && $settings["DELETE"]["Condition1"]["Type"]=="!=")?"selected='true'":"" ?>>Not Equal To</option>
                        </select>
                    </td>
                    <td>
                        <input name="DELValue1" type="text" value="<?php echo isset($settings["DELETE"]["Condition1"]["Value"])?$settings["DELETE"]["Condition1"]["Value"]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                       Or
                        </td>
                    <td>
                        <select name="DELField2">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["DELETE"]["Condition2"]["Field"]) && $settings["DELETE"]["Condition2"]["Field"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                        </td>
                    <td>
                        <select name="DELType2">
                            <option value="=" <?php echo (isset($settings["DELETE"]["Condition2"]["Type"]) && $settings["DELETE"]["Condition2"]["Type"]=="=")?"selected='true'":"" ?>>Equals</option>
                            <option value="!=" <?php echo (isset($settings["DELETE"]["Condition2"]["Type"]) && $settings["DELETE"]["Condition2"]["Type"]=="!=")?"selected='true'":"" ?>>Not Equal To</option>
                        </select>
                    </td>
                    <td>
                        <input name="DELValue2" type="text" value="<?php echo isset($settings["DELETE"]["Condition2"]["Value"])?$settings["DELETE"]["Condition2"]["Value"]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Or
                        </td>
                    <td>
                        <select name="DELField3">
                            <?php
                                foreach($items as $key => $value)
                                {
                                    if(isset($settings["DELETE"]["Condition3"]["Field"]) && $settings["DELETE"]["Condition3"]["Field"]==$key)
                                        echo "<option value='$key' selected='true'>" . $value . "</option>";
                                    else
                                        echo "<option value='$key'>" . $value . "</option>";
                                }
                            ?>
                        </select>
                        </td>
                    <td>
                        <select name="DELType3">
                            <option value="=" <?php echo (isset($settings["DELETE"]["Condition3"]["Type"]) && $settings["DELETE"]["Condition3"]["Type"]=="=")?"selected='true'":"" ?>>Equals</option>
                            <option value="!=" <?php echo (isset($settings["DELETE"]["Condition3"]["Type"]) && $settings["DELETE"]["Condition3"]["Type"]=="!=")?"selected='true'":"" ?>>Not Equal To</option>
                        </select>
                    </td>
                    <td>
                        <input name="DELValue3" type="text" value="<?php echo isset($settings["DELETE"]["Condition3"]["Value"])?$settings["DELETE"]["Condition3"]["Value"]:""?>" />
                    </td>
                </tr>
            </table>
<table>
                <tr>
                    <th scope="row">Save Contacts on Registration:</th>
                    <td>
                        <input name="EnableWebProspect" type="checkbox" <?php echo (isset($settings["Prospect"]) && $settings["Prospect"])?'checked="true"':'' ?> />
                    </td>
                </tr>
                <tr>
                    <th colspan="2" scope="row">Prospect Mappings</th>
                </tr>
                <tr>
                    <td>PropertyBase Field</td>
                    <td>WordPress Field</td>
                </tr>
                <tr>
                    <td>
                        FirstName
                    </td>
                    <td>
                        <input name="MapFirstName" type="text" value="<?php echo isset($settings["ProspectMappings"]["FirstName"])?$settings["ProspectMappings"]["FirstName"]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        LastName
                    </td>
                    <td>
                        <input name="MapLastName" type="text" value="<?php echo isset($settings["ProspectMappings"]["LastName"])?$settings["ProspectMappings"]["LastName"]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Email
                    </td>
                    <td>
                        <input name="MapEmail" type="text" value="<?php echo isset($settings["ProspectMappings"]["Email"])?$settings["ProspectMappings"]["Email"]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Phone
                    </td>
                    <td>
                        <input name="MapPhone" type="text" value="<?php echo isset($settings["ProspectMappings"]["Phone"])?$settings["ProspectMappings"]["Phone"]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input name="MapCust1PB" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust1"][0])?$settings["ProspectMappings"]["MapCust1"][0]:""?>" />
                    </td>
                    <td>
                        <input name="MapCust1WP" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust1"][1])?$settings["ProspectMappings"]["MapCust1"][1]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input name="MapCust2PB" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust2"][0])?$settings["ProspectMappings"]["MapCust2"][0]:""?>" />
                    </td>
                    <td>
                        <input name="MapCust2WP" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust2"][1])?$settings["ProspectMappings"]["MapCust2"][1]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input name="MapCust3PB" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust3"][0])?$settings["ProspectMappings"]["MapCust3"][0]:""?>" />
                    </td>
                    <td>
                        <input name="MapCust3WP" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust3"][1])?$settings["ProspectMappings"]["MapCust3"][1]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input name="MapCust4PB" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust4"][0])?$settings["ProspectMappings"]["MapCust4"][0]:""?>" />
                    </td>
                    <td>
                        <input name="MapCust4WP" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust4"][1])?$settings["ProspectMappings"]["MapCust4"][1]:""?>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <input name="MapCust5PB" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust5"][0])?$settings["ProspectMappings"]["MapCust5"][0]:""?>" />
                    </td>
                    <td>
                        <input name="MapCust5WP" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapCust5"][1])?$settings["ProspectMappings"]["MapCust5"][1]:""?>" />
                    </td>
                </tr>
                <tr>
                    <th colspan="2">
                        Save Search Settings
                    </th>
                </tr>
                <tr>
                    <td>Contact Form Id</td>
                    <td>
                        <input name="MapSaveSearchId" type="text" value="<?php echo isset($settings["ProspectMappings"]["MapSaveSearchId"])?$settings["ProspectMappings"]["MapSaveSearchId"]:""?>" />
                    </td>
                </tr>
            </tbody>
        </table>
        <?php submit_button(); 
        }
        else
        {
            if($doOAuth)
            {
                if($settings["Debug"]=="Yes")
                    echo "<script>window.location='https://test.salesforce.com/services/oauth2/authorize?response_type=code&client_id=" . $_POST["ClientId"] . "&redirect_uri=" . urlencode('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']) . "';</script>";
                else
                    echo "<script>window.location='https://login.salesforce.com/services/oauth2/authorize?response_type=code&client_id=" . $_POST["ClientId"] . "&redirect_uri=" . urlencode('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']) . "';</script>";
            }
        ?>
<h1>OAuth Configuration</h1>
<h3>You must authorize Wordpress to be able to access your salesforce site.</h3>
<table class="form-table">
    <tbody>
        <tr>
            <th scope="row">Sandbox</th>
            <td>
                        <input name="WebListingDebug" type="checkbox" <?php echo $settings["Debug"]=='Yes'? 'checked="true"':'' ?> />
            </td>
        </tr>
        <tr>
            <th scope="row">Client ID</th>
            <td>
                        <input name="ClientId" type="text" value="<?php echo isset($settings["OAuth"]['OAuthClientId'])?$settings["OAuth"]['OAuthClientId']:'' ?>" />
            </td>
        </tr>
        <tr>
            <th scope="row">Secret</th>
            <td>
                        <input name="Secret" type="text" value="<?php echo isset($settings["OAuth"]['OAuthSecret'])?$settings["OAuth"]['OAuthSecret']:'' ?>" />
            </td>
        </tr>
        </tbody>
    </table>
        <?php submit_button(); ?>
        
        <?php
        }
        ?>
    </form>
</div>
<?php
	 }
}
        ?>
