<?php
/*
Plugin Name:i5Rewrite
Plugin URI: http://www.i5fusion.com
Description: Allows for rewrite of URL's
Author: Jarrett Fisher
Version: 1.0
Author URI:http://www.i5fusion.com
 */

add_action( 'init', 'wp_rewrites_init' );
function wp_rewrites_init(){
    add_rewrite_rule(
        'austin_homes/(.+)/?$',
        'index.php?pagename=property-specification&property_add=$matches[1]',
        'top' );

	add_rewrite_rule(
        'austin_rentals/(.+)/?$',
        'index.php?pagename=property-specification&property_add=$matches[1]',
        'top' );

	add_rewrite_rule(
        'ut_austin_rentals/(.+)/?$',
        'index.php?pagename=property-specification&property_add=$matches[1]',
        'top' );
  
  add_rewrite_rule(
        'communities/(.+)/?$',
        'index.php?pagename=property-specification&property_add=$matches[1]',
        'top' );
}

//add_filter('init','flushRules');
add_filter( 'query_vars', 'wp_query_vars' );

function flushRules(){
 global $wp_rewrite; $wp_rewrite->flush_rules(); 
}

function wp_query_vars( $query_vars ){
    $query_vars[] = 'property_add';
    return $query_vars;
}
?>