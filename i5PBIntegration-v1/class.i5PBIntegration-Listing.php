<?php
class i5PBIntegration_Listing
{
    private static $initiated = false;
   
    public static function init(){
        if(!self::$initiated){
            self::init_hooks();
            self::$initiated=true;
        }

        wp_register_script( "Listings_Script", WP_PLUGIN_URL.'/i5PBIntegration/js/i5Listings.js', array('jquery') );

        $adminUrl;

        if(post_type_exists("i5agents"))
            $adminUrl="http://" . $_SERVER["HTTP_HOST"] . "/wp-admin/admin-ajax.php";
        else
            $adminUrl=admin_url( 'admin-ajax.php' );

        wp_localize_script( 'Listings_Script', 'myAjax', array( 'ajaxurl' => $adminUrl,'nonce' => wp_create_nonce('i5PBIntegration')));

        wp_enqueue_script( 'Listings_Script' );
    }
    public static function init_hooks(){
        add_action('wp_ajax_queryListings',array('i5PBIntegration_Listing', 'doQuery'));
        add_action('wp_ajax_nopriv_queryListings',array('i5PBIntegration_Listing', 'doQuery'));
        add_action('wp_ajax_autoCompleteListings',array('i5PBIntegration_Listing', 'doAutoCompleteSearch'));
        add_action('wp_ajax_nopriv_autoCompleteListings',array('i5PBIntegration_Listing', 'doAutoCompleteSearch'));

        // Gallery Listing
        add_action('wp_ajax_galleryListings',array('i5PBIntegration_Listing', 'galleryListings'));
        add_action('wp_ajax_nopriv_galleryListings',array('i5PBIntegration_Listing', 'galleryListings'));
    }
    public static function doAutoCompleteSearch(){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], "i5PBIntegration")) {
            exit("Denied");
        }
        
        global $wpdb;
        $limit = 5;

        if(isset($_REQUEST["limit"]) && isset($_REQUEST["limit"]) != "")
            $limit = $_REQUEST["limit"];

        $abbr = strtolower(substr($_REQUEST["kw"], 0 , 2));

        if($abbr == 'va'){
            $city = "(select count(*), '' as redirect, 'City' as section,city as result from cr_i5listings where (state like '" . $abbr . "%' AND city like '%" . substr($abbr, 0,1 ) . "%') group by result,redirect,section limit " . $limit . ")";
        } else{
            $city = "(select count(*), '' as redirect, 'City' as section,city as result from cr_i5listings where city like '" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")";
        }
        


        $sql = " ". $city ."
                union
                (select count(*), url as redirect, 'Address' as section,address as result from cr_i5listings where address like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), url as redirect, 'MLSID' as section,mlsid as result from cr_i5listings where mlsid like '" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'Zip Code' as section,zipcode as result from cr_i5listings where zipcode like '" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'Subdivision' as section,Subdivision as result from cr_i5listings where Subdivision like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'School District' as section,schooldistrict as result from cr_i5listings where schooldistrict like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'Area' as section,Area as result from cr_i5listings where Area like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'School' as section,elementaryschool as result from cr_i5listings where elementaryschool like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'School' as section,middleschool as result from cr_i5listings where middleschool like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'School' as section,highschool as result from cr_i5listings where highschool like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                order by case when section='city' then 1 when section='address' then 2 when section='area' then 3 else 4 end,result";

        $results = $wpdb->get_results($sql);

        echo json_encode($results);

        die();

    }
    public static function doQuery(){
        global $wpdb;

        if ( !wp_verify_nonce( $_REQUEST['nonce'], "i5PBIntegration")) {
            exit("Denied");
        }

        $sql ="select " . str_replace("\'","'",$_REQUEST["query"]) . ",i.url as image from {$wpdb->prefix}i5listings as l left join {$wpdb->prefix}i5Images i on i.id = (select min(id) from {$wpdb->prefix}i5Images ii where ii.propertyid=l.propertyid)";

        if(isset($_REQUEST["where"]) && $_REQUEST["where"]!="")
            $sql .= " where " . str_replace("\'","'",$_REQUEST["where"]);

        if(isset($_REQUEST["group"]) && $_REQUEST["group"] !="")
            $sql .= " group by " . $_REQUEST["group"];

        if(isset($_REQUEST["order"]) && $_REQUEST["order"]!="")
            $sql .= " order by " . $_REQUEST["order"];

        if(isset($_REQUEST["limit"]) && $_REQUEST["limit"]!="")
            $sql .= " limit " . $_REQUEST["limit"];

        $results = $wpdb->get_results($sql);
                
        echo json_encode($results);

        die();
    }

    public static function galleryListings(){

        global $wpdb;
        $PropertyTypes = array();
        $TTLResults = 0;
        $SearchResults = "";
        $pageSize = 25;
        $startPage = 1;
        $endPage = "";
        $start = 1;
        $end = 0;
        $pageNum = 1;

        $fields_ = $_REQUEST['terms'];
        $fields_ = explode('&', $fields_);

        if (isset($maxResults)) {
            $pageSize = $maxResults;
        }

        $fields = array("stories", "minstories", "maxstories", "parkingSpaces", "minparkingSpaces", "maxparkingSpaces", "lotsize", "minlotsize", "maxlotsize", "yearbuilt", "minyearbuilt", "maxyearbuilt", "minsize", "maxsize", "minprice", "maxprice", "bedrooms", "minbedrooms", "maxbedrooms", "fullBaths", "minfullBaths", "maxfullBaths", "search");

        //Get the Property Types
        $sql = "SELECT propertytype from {$wpdb->prefix}i5listings group by propertytype order by propertytype";

        $pTypes = $wpdb->get_results($sql);

        array_push($PropertyTypes, "All");

        if ($pTypes != null) {
            foreach ($pTypes as $pType) {
                if ($pType->propertytype != null && $pType->propertytype != "") {
                    array_push($PropertyTypes, $pType->propertytype);
                }
            }
        }

        $sql = "";

        //Do the search
        if (isset($_REQUEST["terms"])) {
            $sql = "SELECT propertytype,title,status,l.url,latitude,longitude,i.url AS image,mlsid,bedrooms,address,city,state,zipCode,price,fullBaths,halfBaths,size from {$wpdb->prefix}i5listings l
      left join {$wpdb->prefix}i5Images i on i.id = (select min(id) from {$wpdb->prefix}i5Images ii where ii.propertyid=l.propertyid)";
        } else {
            $sql = "SELECT propertytype,title,status,l.url,latitude,longitude,i.url AS image,mlsid,bedrooms,address,city,state,zipCode,price,fullBaths,halfBaths,size from {$wpdb->prefix}i5listings l
      left join {$wpdb->prefix}i5Images i on i.id = (select min(id) from {$wpdb->prefix}i5Images ii where ii.propertyid=l.propertyid)";
            //$sql = "SELECT l.*,i.url AS image from {$wpdb->prefix}i5listings l left join {$wpdb->prefix}i5Images i on i.id = (select min(id) from {$wpdb->prefix}i5Images ii where ii.propertyid=l.propertyid)";
        }

        $sqlCount = "SELECT count(*) from {$wpdb->prefix}i5listings";

        $where = "";
        $join = "WHERE";
        $order = " order by case when status = 'new listing' then 1 when status='Active' then 2 else 3 end";

        $types_ = 'Lots and Land;Multi Family;Apartments 3+;Rental;';
        if (strtolower(get_query_var("search_query")) != "all") {}
        $types = '';

        $typeField = $_REQUEST['terms'];
        $typeField = explode(';', $typeField);

        foreach ($fields_ as $field) {
            $field = explode('=', $field);
            if ($field[0] == "ptype") {
                $types .= $field[1];
            }
        }

        if($types != '' || $types != null){
            $pts = explode(";", urldecode($types));
            $in = "";

            foreach ($pts as $pt) {
                $in .= "'" . $pt . "',";
            }

            $in = substr($in, 0, strlen($in) - 1);

            $where = " " . $join . " propertytype in(" . $in . ")";
            $join = "and";
        }

        if(isset($_REQUEST['terms']) && $_REQUEST['terms'] != '' || $_REQUEST['terms'] != null) {
            foreach ($fields_ as $field) {
                $field = explode('=', $field);
                if ($field[0] == "search" || $field[0] == "ptype" || $field[0] == "sort" || $field[0] == "pg") {
                } else {
                    $where .= " " . $join . " " . str_replace("max", "", str_replace("min", "", $field[0]));
                    if (strlen($field[0]) > 2 && strrpos($field[0], "min", -strlen($field[0])) !== false) {
                        $where .= " >= ";
                    } else if (strlen($field[0]) > 2 && strrpos($field[0], "max", -strlen($field[0])) !== false) {
                        $where .= " <= ";
                    } else {
                        $where .= " = ";
                    }
                    $where .= $field[1];
                    $join = "and";
                }


            }
        }
        foreach ($fields_ as $field) {
            $field_ = explode('=', $field);
            if ($field_[0] == 'sort') {
                $dir = "ASC";
                $field = "";

                if (substr(strtolower($field_[1]), 0, 3) != "asc") {
                    $dir = "DESC";
                }

                if ($dir == "DESC") {
                    $field = substr($field_[1], 4);
                } else {
                    $field = substr($field_[1], 3);
                }

                $order = " ORDER By " . $field . " " . $dir;
            }
        }


        foreach ($fields_ as $field) {
            $field_ = explode('=', $field);
            if ($field_[0] == 'pg' && $field_[1] != 0) {
                $start = ($pageSize * (int) $field_[1]) + 1;
                $pageNum = (int) $field_[1];
            }
        }

        if (isset($_REQUEST["searchtype"]) && isset($_REQUEST["search"]) && $_REQUEST["search"] != "" && $_REQUEST["searchtype"] != "") {
            $where .= " " . $join . " " . str_replace(" ", "", urldecode($_REQUEST["searchtype"])) . " ='" . urldecode($_REQUEST["search"]) . "'";
            $join = "and";
        } else if (isset($_REQUEST["search"]) && $_REQUEST["search"] != "") {
            $where .= " " . $join . " (description like '" . $_REQUEST["search"] . "' or mlsid like '" . $_REQUEST["searc"] . "' or title like '" . $_REQUEST["search"] . "' or address like '" . $_REQUEST["search"] . "' or city like '" . $_REQUEST["search"] . "')";
            $join = "and";
        }

        if (isset($_REQUEST["School"]) && $_REQUEST["School"] != "") {
            $where .= " " . $join . "(elementaryschool like '%" . $_REQUEST["School"] . "%' or middleschool like '%" . $_REQUEST["School"] . "%' or highschool like '%" . $_REQUEST["School"] . "%')";
            $join = "and";
        }

        if (isset($_REQUEST["sort"])) {
            $dir = "ASC";
            $field = "";

            if (substr(strtolower($_REQUEST["sort"]), 0, 3) != "asc") {
                $dir = "DESC";
            }

            if ($dir == "DESC") {
                $field = substr($_REQUEST["sort"], 4);
            } else {
                $field = substr($_REQUEST["sort"], 3);
            }

            $order = " ORDER BY " . $field . " " . $dir;
        }

        if (isset($_REQUEST["pg"]) && (int) $_REQUEST["pg"] != 0) {
            $start = ($pageSize * (int) $_REQUEST["pg"]) + 1;
            $pageNum = (int) $_REQUEST["pg"];
        }

        $skip = 0;

        if ($pageNum > 1) {
            $skip = $pageNum * $pageSize;
        }

        $isAdjusted = false;

        if (!isset($where) || $where == "") {
            $isAdjusted = true;
            $where = " where price>=300000";
        }

        //echo $sql . $where . $order . " limit " . $skip . ", " . $pageSize;

        $SearchResults = $wpdb->get_results($sql . $where . $order . " limit " . $skip . ", " . $pageSize);

        if ($isAdjusted) {
            $where = "";
        }

        $TTLResults = $wpdb->get_var($sqlCount . $where);

        $endPage = ceil($TTLResults / $pageSize);

        if ($pageNum > 5 && $endPage > 10) {
            $startPage = $pageNum - 4;
        }

        if ($endPage > 9 && $startPage != 1 && ($startPage + 9) > $endPage) {
            while (($startPage + 9) > $endPage) {
                $startPage -= 1;
            }
        }

        if ($endPage > $startPage + 9) {
            $endPage = $startPage + 9;
        }

        $end = $start + $pageSize - 1;

        if ($end > $TTLResults) {
            $end = $TTLResults;
        }

        // Last run
        $settings = get_option('WebListing_Settings');
        $lastRun = strtotime(date("Y-m-d h:i:s"));
        $currTime = $lastRun;

        if(isset($settings["LastRun"]))
            $lastRun = strtotime($settings["LastRun"]);

        // Pagination
        $pagination = "";
        $pagination .= '<section>
        <section class="pagi">
            <div class="pagi-left">
                <span>'.$start .' - '. $end . '</span> of '.$TTLResults .' Results
            </div>
            <div class="pagi-right">';

                  if($pageNum!=1)
                  {
                      $pagination .= '<a href="javascript:changePage( '. $pageNum .' - 1);"><</a>';

                  }
                  for($x=$startPage;$x<=$endPage;$x++)
                  {
                      //$x == $pageNum?"class='active'":"href='javascript:doSearch(null," . $x . ");'";
                      if($x == $pageNum){
                          $attribute = "class='active'";
                      }
                      else{
                          $attribute = "href='javascript:doSearch(null," . $x . ");'";
                      }

                      $pagination .= '<a '. $attribute .' > ' . $x . '</a>';
               	 }
                  if($end<$TTLResults)
                  {
                      $page = $pageNum + 1;

                      $pagination .= '<a href="javascript:doSearch(null, ' . $page . ');">&raquo;</a>';
                }
        $pagination .= '</div>
        </section>
    </section>';


        $html = "";
        $html .= '<div class="ajax-loader"></div>';
        $html .= ' <div id="divFilters">';
        foreach ($fields_ as $field) {
            $field = explode('=', $field);

                if ($field[0] == "ptype"):
                    $html .= '<div class="filter">Property Type:  ' . substr(urldecode($field[1]), 0, -1) . ' <div class="remove" onclick="removeFilter(\'' . substr(urldecode($field[1]), 0, -1) . '\');">X</div></div>';
                endif;
                 if ($field[0] == "minprice"):
                     $html .= '<div class="filter">Price Min:  ' . $field[1] . ' <div class="remove" onclick="removeFilter(' . $field[1] . ');">X</div></div>';
                endif;
                if ($field[0] == "maxprice"):
                    $html .= '<div class="filter">Price Max:  ' . $field[1] . ' <div class="remove" onclick="removeFilter(\'maxprice\');">X</div></div>';
                endif;
                 if ($field[0] == "minbedrooms"):
                    $html .= '<div class="filter">Bed(s) Min:   ' .  $field[1] . ' <div class="remove" onclick="removeFilter(\'minbedrooms\');">X</div></div>';
                 endif;
                 if ($field[0] == "maxbedrooms"):
                    $html .= '<div class="filter">Bed(s) Max:  ' .  $field[1] . ' <div class="remove" onclick="removeFilter(\'maxbedrooms\');">X</div></div>';
                 endif;
                 if ($field[0] == "minfullBaths"):
                    $html .= '<div class="filter">Bath(s) Min: ' .  $field[1] . ' <div class="remove" onclick="removeFilter(\'minfullBaths\');">X</div></div>';
                 endif;
                 if ($field[0] == "maxfullBaths"):
                    $html .= '<div class="filter">Bath(s) Max: ' .  $field[1] . ' <div class="remove" onclick="removeFilter(\'maxfullBaths\');">X</div></div>';
                 endif;
                 if ($field[0] == "minsize"):
                    $html .= '<div class="filter">Square Footage Min: ' .  $field[1] . ' <div class="remove" onclick="removeFilter(\'minsize\');">X</div></div>';
                 endif;
                 if ($field[0] == "maxsize"):
                    $html .= '<div class="filter">Square Footage Max: ' .  $field[1] . ' <div class="remove" onclick="removeFilter(\'maxsize\');">X</div></div>';
                 endif;
                 if ($field[0] == "minlotsize"):
                    $html .= '<div class="filter">Acres Min: ' .  $field[1] . ' <div class="remove" onclick="removeFilter(\'minlotsize\');">X</div></div>';
                 endif;
                 if ($field[0] == "maxlotsize"):
                    $html .= '<div class="filter">Acres Max: ' .  $field[1] . ' <div class="remove" onclick="removeFilter(\'maxlotsize\');">X</div></div>';
                 endif;
                 if ($field[0] == "minyearbuilt"):
                    $html .= '<div class="filter">Year Built Min: ' .  $field[1] . ' <div class="remove" onclick="removeFilter(\'minyearbuilt\');">X</div></div>';
                 endif;
                 if ($field[0] == "maxyearbuilt"):
                    $html .= '<div class="filter">Year Built Max: ' .  $field[1] . ' <div class="remove" onclick="removeFilter(\'maxyearbuilt\');">X</div></div>';
                 endif;
                 if ($field[0] == "minparkingSpaces"):
                    $html .= '<div class="filter">Parking Spaces Min: ' .  $field[1] . ' <div class="remove" onclick="removeFilter(\'minparkingSpaces\');">X</div></div>';
                 endif;
                 if ($field[0] == "maxstories"):
                    $html .= '<div class="filter">Parking Spaces Max: ' .  $field[1] . ' <div class="remove" onclick="removeFilter(\'maxparkingSpaces\');">X</div></div>';
                 endif;
                 if ($field[0] == "maxstories"):
                    $html .= '<div class="filter">Stories Min:  ' .  $field[1] . ' <div class="remove" onclick="removeFilter(\'minstories\');">X</div></div>';
                 endif;
                 if ($field[0] == "maxstories"):
                    $html .= '<div class="filter">Stories Max: ' .  $field[1] . ' <div class="remove" onclick="removeFilter(\'maxstories\');">X</div></div>';
                 endif;
                 if (isset($_REQUEST["searchtype"]) && isset($_REQUEST["search"])):
                    $html .= '<div class="filter"> ' . $_REQUEST["searchtype"] . ' : ' . $_REQUEST["search"] . ' <div class="remove" onclick="removeFilter("' . $_REQUEST["searchtype"].'");">X</div></div>';
                 endif;

        }
        $html .= '</div>';

        $html .= '<div class="searchResultsHeader">
            '. $TTLResults .' Properties for sale
            <span class="timestamp">Updated
            <b> '. round(abs($currTime - $lastRun) / 60,0) .' MIN AGO</b>
          	</span>
              <section>';
        $html .= $pagination;
        $html .= '</section></div>';


        foreach ($SearchResults as $result) {

            // Baths
            if ($result->halfBaths != 0 && $result->fullBaths != 0)
                $baths = $result->fullBaths + ($result->halfBaths / 2);
            else if ($result->halfBaths != 0 && $result->fullBaths == 0)
                $baths = ($result->halfBaths / 2);
            else
                $baths = number_format($result->fullBaths, 0);

            // Favortite Class
            if ($favs == '' || !in_array($result->mlsid, $favs)) {
                $active = 'listingFavorite';
            } else {
                $active = 'listingFavoriteSelected';
            }

            // Img source
            if (@getimagesize($result->image) == false ) {

                $src = i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png";
            } else {
                $src = $result->image;
            }

            $html .= '<div class="col1-4">
                <div class="listing">
                    <div class="listingImage">
                        <img src="'. $src .'">
                     </div>
                    <div id="fav'.$result->mlsid.'" class="'. $active.'">
                        <a href="javascript:toggleFav(\' '.$result->mlsid .'\');setFavorite(\''.$result->mlsid .'\');">
                            <i class="fa fa-heart"></i>
                        </a>
                    </div>
                    <div class="listingAddress">
                        address locality | '.$result->city.'                    </div>
                    <div class="listingPrice">
                        $ ' . number_format($result->price,0) . '               </div>
                    <div class="listingBeds">
                        <div class="bedsValue">
                            '.$result->bedrooms.'                        </div>
                        <div class="bedsLabel">
                            Beds
                        </div>
                    </div>
                    <div class="listingBaths">
                        <div class="bathsValue">
                            '.$baths.'                        </div>
                        <div class="bathsLabel">
                            Baths
                        </div>
                    </div>
                                        <div class="listingSqft">
                        <div class="sqftValue">
                            '.number_format($result->size,0).'                       </div>
                        <div class="sqftLabel">
                            SqFt
                        </div>
                    </div>
                                                            <div class="listingAcres">
                        <div class="acresValue">
                            '.number_format($result->lotsize,0).'                        </div>
                        <div class="acresLabel">
                            Acres
                        </div>
                    </div>

                    <div class="listingStatus">
                        <!-- other status
						<div class="shortSale">Short Sale</div>
            <div class="foreclosure">Foreclosure</div>-->
                        <div class="new">
                            '.$result->status.'                        </div>
                    </div>
                    <div class="viewDetails">
                        <a href="/property-details/'.urldecode($result->url).'">View Details</a>
                    </div>
                  	<div class="Disclaimer">
                        ' . htmlspecialchars_decode(get_post_meta( get_the_ID(), 'disclaimer', true )) . '
                     </div>
                </div>
            </div>';
        }

        $html .= $pagination;

        echo $html;
        die;
    }
}

