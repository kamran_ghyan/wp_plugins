function queryListings(queryFields,whereFields,callback,groupFields,orderFields,limit) {

    jQuery.ajax({
        type: "post",
        dataType: "json",
        url: myAjax.ajaxurl,
        data: { action: "queryListings", nonce: myAjax.nonce, query: queryFields, where: whereFields,group:groupFields,order:orderFields,limit:limit },
        success: function (response) {
            callback(response);
        },
        error: function (err) {
            alert(err);
        }
    });
}

function autoComplete(term, limit,callback) {
    jQuery.ajax({
        type: "post",
        dataType: "json",
        url: myAjax.ajaxurl,
        data: { action: "autoCompleteListings", nonce: myAjax.nonce, term: terms },
        success: function (response) {
            callback(response);
        },
        error: function (err) {
            alert(err);
        }
    });
}

