<?php
class i5PBIntegration_ShortCode
{
    private static $initiated = false;
    private static $settings;

    public static function init(){
        if(!self::$initiated){
            self::$settings=get_option('WebListing_Settings');
            self::$initiated=true;
        }
    }

    public static function map_shortCode($atts){
        $vars=shortcode_atts(array(
		  'style' => '',
          'lat' => 38.980731,
          'listingtemplate'=>'',
          'long' => -107.7936206,
          'zoom' => 10,
          'maxzoom'=>12,
          'minzoom'=>8,
          'showdetails'=>true,
          'showpolygon'=>true
          ),$atts);

        if(isset(self::$settings["ExcludeMapsAPI"]) && self::$settings["ExcludeMapsAPI"]=="No")
        {
?>

<script src="https://maps.google.com/maps/api/js?v=3&key=<?php echo isset(self::$settings['GoogleAPIKey'])?self::$settings['GoogleAPIKey']:''?>&libraries=drawing"></script>
<?php }?>

<script src="<?php echo WP_PLUGIN_URL ?>/i5PBIntegration/js/i5ListingMap.js"></script>
<script src="<?php echo WP_PLUGIN_URL ?>/i5PBIntegration/js/MarkerWithLabel.js"></script>
<div style="display:none;" id="divLoading">Loading....</div>
<div id="divListings" style="<?php echo $vars["style"] ?>"></div>
<script>

    InitializeMap("divListings", <?php echo $vars["zoom"]?>, <?php echo $vars["maxzoom"]?>, <?php echo $vars["minzoom"]?>, <?php echo $vars["lat"]?>,<?php echo $vars["long"]?>,<?php echo $vars["showdetails"]?>,<?php echo $vars["showpolygon"]?>,'<?php echo $vars["listingtemplate"]?>');
</script>
<?php
    }

    public static function geosearch_shortCode($atts){
        
        $vars=shortcode_atts(array(
		    'class_name' => '',
			'title_place'=>'',
            'usebranding'=>false,
            'geocode'=>true
          ),$atts);

        wp_enqueue_script("Listings_Script",i5PBIntegration__PLUGIN_URL . 'js/i5Listings.js');

        //Geocode from ip address
        $ch = curl_init("http://freegeoip.net/json/" . $_SERVER["REMOTE_ADDR"]);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_TIMEOUT,1);
      
        $resp = curl_exec($ch);
        $sType = "all";
        $sTerm = "";
        $state = "";
        $listingCount = 0;

        if(FALSE != $resp)
        {
            $vals = json_decode($resp);
            $state = $vals->region_code;
            $sTerm = $vals->city;

            $validCity = false;
            
            if(strtolower($state) == "nc")
                $validCity = true;
            else if(strtolower($state) == "va" && $sType == "virginia beach")
                $validCity = true;
            else if(strtolower($state) == "va" && $sType == "newport news")
                $validCity = true;
            else if(strtolower($state) == "va" && $sType == "chesapeake")
                $validCity = true;
              
            if($validCity && $vars["geocode"])
            {
                $sType = "City";
                
                global $wpdb;
                $sql = "select id from {$wpdb->prefix}i5listings where city='" . $sTerm . "'";
                $listings = $wpdb->get_row($sql);

                if($listings != null)
                    $listingCount = sizeof($listings);

                //TEMP REMOVE WHEN LIVE
                $listingCount = 1;
            }
            else 
                $sTerm = "";
        }
?>

<input type="hidden" name="searchtype" id="searchtype" value="<?php echo $sType?>" />
<?php if(empty($vars['title_place'])): ?>
	<div class="row text-center"><h3 style="text-align:center;" class="find-home">Find Your Home</h3></div>
<?php endif; ?>
	<div class="geoSearchHolder">
                <!--<select id="dropdown">
                    <option value="buy">Buy</option>
                    <option value="rent">Rent</option>
                </select>-->
                <button class="button" onclick="doGeoSearch();"><i class="fa fa-search"></i></button>
                <div class="inputHolder">
                    <input id="inputSearch" value="<?php echo $sTerm;?>" onclick="showSearchResults()" class="inputSearch" placeholder="Type any Area, Address, Zip, School, etc" type="text" />
                  <div id="ddSearchResults" class="ddSearchResults dropdownContent">
                    <ul class="ulSearchResults"></ul>
                	</div>
                </div>
                <?php if($sType=="City" && $listingCount>0):?>
                <div class="listingCount">
                   <?php /*?> <a href="/listing-search/all/?searchtype=City&search=<?php echo $sTerm?>">View <?php echo $listingCount;?> listing in <?php echo $sTerm . "," . $state;?></a><?php */?>
                  </div>
                  <?php endif;?>
                 <?php if($vars["class_name"]){ ?>
                  
                  <button class="btn btn-default <?php echo $vars["class_name"]; ?>"><a href="/listing-search/all/">Advanced <br />Search</a></button>
                  <?php } else { ?>
                  <button class="button"><a href="/listing-search/all/">Advanced Search</a></button>
                </div>
                <?php } ?>
                
                
                
           </div>
<?php if(!empty($vars['title_place'])): ?>
	<div class="row text-center"><h3 style="text-align:center;" class="find-home">Find Your Home</h3></div>
<?php endif; ?>
   
           <script>
                function showSearchResults() {
                    jQuery(".dropdownContent").removeClass("show");
                    document.getElementById("ddSearchResults").classList.toggle("show");
                }
                jQuery(function () {
                    var delay = (function () {
                        var timer = 0;
                        return function (callback, ms) {
                            clearTimeout(timer);
                            timer = setTimeout(callback, ms);
                        };
                    })();

                    jQuery("#inputSearch").keyup(function () {
                        delay(function () {
                            showSearchResults();
                            doAutoComplete();
                        }, 200);
                    });
                });

                function doSearch(pg,ord,sort,type,value){
                    var ptype="all";

                    if(jQuery("#dropdown").val()=="rent")
                        ptype="Rental";

                    window.location="/listing-search/" + ptype + "/?searchtype=" + type + "&search=" + value;
                }

                function doGeoSearch(){
                    var ptype="all";

                    if(jQuery("#dropdown").val()=="rent")
                        ptype="Rental";

                    if(jQuery("#searchtype").val()=="City" && jQuery("#inputSearch").val()!="")
                    {
                        window.location="/listing-search/" + ptype + "/?searchtype=City&search=" + jQuery("#inputSearch").val();
                    }
                }

                function doAutoComplete() {
            			autoComplete(jQuery("#inputSearch").val(), 4, populateSearch);
                }

                var lastSection = "";

                function populateSearch(list) {
                    jQuery(".ulSearchResults").empty();
                    var html = "";
                    if(list!=null && list.length>0)
                    {
                        for(var i=0;i<list.length;i++)
                        {
                            if (lastSection != list[i].section || i==0) {
                                html += "<li class='resultsHeader'>" + list[i].section + "</span></li>";
                                lastSection = list[i].section;
                            }

                            if (list[i].redirect == null || list[i].redirect=="")
                                html += "<li class='resultsItem'><a href='javascript:doSearch(null,null,null,\"" + lastSection + "\",\"" + list[i].result + "\");'>" + list[i].result + "</a></li>";
                            else
                                html += "<li class='resultsItem'><a href='javascript:redirect(\"" + list[i].redirect + "\");'>" + list[i].result + "</a></li>";
                        }
                    }
                    else
                    {
                        html += "<li class='resultsHeader'>" + lastSection + "<br/>No Results</span></li>";
                    }

                    jQuery(".ulSearchResults").append(html);
                }
                function redirect(url)
                {
                    window.location = "/property-details/" + url;
                }
            </script>

        <?php
    }
}

