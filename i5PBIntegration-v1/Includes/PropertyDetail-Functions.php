<?php
	session_start();
    $relatedBaths=0;
    $relatedPropertyType="";
    $relatedSize=0;
    $relatedPrice=0;

    $property;
    $images;

    $prevSearch = $_SERVER['HTTP_REFERER'];

    if(strpos(strtolower($prevSearch),"listing-search")===false)
        $prevSearch="/listing-search/all";

    $sql = "SELECT * from {$wpdb->prefix}i5listings where url=%s";
    $sql=$wpdb->prepare($sql,urldecode(get_query_var("property_url")));

    $property=$wpdb->get_row($sql);

    $relatedBaths=$property->baths;
    $relatedPropertyType=$property->propertytype;
    $relatedSize=$property->size;
    $relatedPrice=$property->price;

    if(!isset($property))
    {
        //Todo redirect
    }
    else
    {
        $sql = "SELECT * from {$wpdb->prefix}i5Images where propertyid=%s";
        $sql=$wpdb->prepare($sql,$property->propertyid);

        $images=$wpdb->get_results($sql);
    }

if (is_user_logged_in() ) {
		unset($_SESSION['countid']);
	}
	else {
		session_start(); 
		$sid = session_id(); 
		$_SESSION['countid'] = $_SESSION['countid']+1;
    
    
		if($_SESSION['countid'] > 2 )
		{
      echo "<script>jQuery(window).ready(function(){jQuery('.registerClose').remove();jQuery('#popupHeader').text('Register to Continue');jQuery('#register').modal({backdrop:'static',keyboard:false});});</script>";
		}
	}
?>