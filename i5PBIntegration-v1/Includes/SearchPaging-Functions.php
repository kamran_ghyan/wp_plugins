  <section>
        <section class="pagi">
            <div class="pagi-left">
                <span><?php echo $start . "-" . $end?></span> of <?php echo $TTLResults ?> Results
            </div>
            <div class="pagi-right">
              	<?php
                  if($pageNum!=1)
                  {
                  ?>
              			<a href="javascript:changePage(<?php echo $pageNum - 1?>);"><</a>
              	<?php
                  }
                  for($x=$startPage;$x<=$endPage;$x++)
                  {
                  ?>
                		<a <?php echo $x==$pageNum?"class='active'":"href='javascript:doSearch(null," . $x . ");'"; ?>><?php echo $x?></a>
               	<?php } 
                  if($end<$TTLResults)
                  {
                   ?>
                <a href="javascript:doSearch(null,<?php echo $pageNum + 1?>);">&raquo;</a>
                <?php }?>
            </div>
        </section>
    </section>