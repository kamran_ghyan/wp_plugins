<?php
wp_enqueue_style( 'property-search1', plugin_dir_url() . 'i5PBIntegration/css/search-results1.css', array(), '1.1', 'all');
wp_enqueue_script('jquery');
?>
<style>
<?php include '//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'; ?>
</style>
<?php
/*
Template Name: Property Search Theme 1
 */

get_header();

/* Theme Listing Page Module */
$theme_listing_module = get_option('theme_listing_module');

/* Only for demo purpose only */
if(isset($_GET['module'])){
    $theme_listing_module = $_GET['module'];
}

switch($theme_listing_module){
    case 'properties-map':
        get_template_part('banners/map_based_banner');
        break;

    default:
        get_template_part('banners/default_page_banner');
        break;
}

include(i5PBIntegration__PLUGIN_DIR . 'Includes/Prospect-Functions.php');
include(i5PBIntegration__PLUGIN_DIR . 'Includes/PropertySearch-Functions.php');

?>

<!-- Content -->
<div class="container contents lisitng-grid-layout">
    <div class="resultsSearch">
        <div class="span3">
            <input id="search" type="text" value="<?php echo (isset($_REQUEST['kw'])?$_REQUEST['kw']:'') ?>" placeholder="Search" autocomplete="off" />
        </div>
        <div class="span2">
            <select id="propertyType">
                <?php
                foreach($PropertyTypes as $propType)
                    echo "<option value='" . str_replace(" ","-",strtolower($propType)) . "'" . (((str_replace("-"," ",strtolower(get_query_var("search_query")))==str_replace("-"," ",strtolower($propType))))?" selected":"") . ">" . $propType . "</option>";
                ?>
            </select>
        </div>
        <div class="span1">
            <input id="minprice" type="text" value="<?php echo (isset($_REQUEST['minp'])?$_REQUEST['minp']:'') ?>" placeholder="Min Price" />
        </div>
        <div class="span1">
            <input id="maxprice" value="<?php echo (isset($_REQUEST['maxp'])?$_REQUEST['maxp']:'') ?>" type="text" placeholder="Max Price" />
        </div>
        <div class="span1">
            <input id="bed" value="<?php echo (isset($_REQUEST['bed'])?$_REQUEST['bed']:'') ?>" type="text" placeholder="Bed" />
        </div>
        <div class="span1">
            <input id="bath" value="<?php echo (isset($_REQUEST['bath'])?$_REQUEST['bath']:'') ?>" type="text" placeholder="Bath" />
        </div>
        <div class="span2">
            <input id="btnSearch" class="btn btn-search btn-sm" type="submit" />
        </div>
    </div>
    <div class="row">
        <div class="span9 main-wrap">

            <!-- Main Content -->
            <div class="main">

                <section class="listing-layout">

                    <div class="list-container clearfix" style="padding:15px 0 0;">
                        <div class="navTabs">
                            <ul class="nav nav-tabs">
                                <li id="liMap" class="">
                                    <a href="javascript:showHideMap(true);">
                                        <i class="fa fa-globe"></i>
                                        View on Map
                                    </a>
                                </li>
                                <li class="<?php echo !isset($_REQUEST['ord'])?'active':''?>">
                                    <a href="javascript:doSearch()">
                                        <i class="fa fa-clock-o"></i>
                                        Newest
                                    </a>
                                </li>
                                <li class="<?php echo (isset($_REQUEST['ord']) && $_REQUEST['ord']=='pricelh')?'active':''?>">
                                    <a href="javascript:doSearch('pricelh')">
                                        <i class="fa fa-usd"></i>
                                        Lowest to Highest
                                    </a>
                                </li>
                                <li class="<?php echo (isset($_REQUEST['ord']) && $_REQUEST['ord']=='pricehl')?'active':''?>">
                                    <a href="javascript:doSearch('pricehl')">
                                        <i class="fa fa-usd"></i>
                                        Highest to Lowest
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <h3 class="title-heading">
                            <?php the_title(); ?>
                        </h3>
                        <div style="display:none;" class="mapHolder">
                            <?php 
                            $lat=0;
                            $long=0;

                            if(isset($SearchResults) && sizeof($SearchResults)>0)
                            {
                                $lat=$SearchResults[0]->latitude;
                                $long=$SearchResults[0]->longitude;
                            }

                                do_shortcode('[i5ListingMap maxzoom="100" minzoom="1" lat="' . $lat . '" long="' . $long . '" style="display:block;width: 100%; height: 418px" showPolygon="false"]');
                            ?>
                        </div>
                        <div class="listingHolder">
                        <?php
                        foreach($SearchResults as $result)
                        {
                        ?>
                        <div class="resultsContainer">
                            <a name="<?php echo $result->mlsid?>"></a>
                            <div class="resultsPhoto">
                                <a href="/property-details/<?php echo urldecode($result->url)?>">
                                    <?php if(isset($result->image) && $result->image!=""):?>
                                    <img src="<?php echo $result->image?>" />
                                    <?php else:?>
                                    <img src="<?php echo plugin_dir_url()?>" . "i5pbintegration/images/imgunavailable.png" />
                                    <?php endif;?>
                                </a>
                            </div>
                            <div class="resultsDetails">
                                <div class="<?php echo ($favs=='' || !in_array($result->mlsid,$favs)?'resultsFavorite':'resultsFavoriteSelected')?>">
                                    <a href="javascript:setFavorite('<?php echo $result->mlsid ?>',<?php echo ($favs=='' || !in_array($result->mlsid,$favs)?'1':'0')?>);">
                                        <i class="fa fa-heart">&nbsp;</i>
                                    </a>
                                </div>
                                <div class="resultsPrice">
                                    <?php echo "$" . number_format($result->price,0)?>
                                </div>
                                <div class="resultsAddress">
                                    <?php echo $result->address?>
                                    <br />
                                    <span class="resultsCityStateZip">
                                        <?php echo $result->city?>, <?php echo $result->state?><?php echo $result->zipCode?>
                                    </span>
                                </div>
                                <div class="resultsInfo">
                                    <div class="infoItem">
                                        <span class="lblBed">Bed:</span>
                                        <span class="infoBed">
                                            <?php echo $result->bedrooms?>
                                        </span>
                                    </div>
                                    <div class="infoItem">
                                        <span class="lblBath">Bath:</span>
                                        <span class="infoBed">
                                            <?php
                            if($result->halfBaths!=0 && $result->fullBaths!=0)
                                echo $result->fullBaths + ($result->halfBaths/2);
                            else if($result->halfBaths!=0 && $result->fullBaths==0)
                                echo ($result->halfBaths/2);
                            else
                                echo number_format($result->fullBaths,0);
                                            ?>
                                        </span>
                                    </div>
                                    <?php if($result->size!=0) :?>
                                    <div class="infoItem">
                                        <span class="lblSqft">Sqft:</span>
                                        <span class="infoSqft">
                                            <?php $result->size?>
                                        </span>
                                    </div>
                                    <?php endif;?>
                                </div>
                                <div class="infoButtons">
                                    <div class="btnDetails">
                                        <a href="/property-details/<?php echo urldecode($result->url)?>">
                                            <i class="fa fa-info-circle"></i>
                                            View Property Details
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        }
                        ?>
                        </div>
                    </div>
                  <!-- PAGINATION START -->
                    <?php echo include(i5PBIntegration__PLUGIN_DIR . 'Includes/SearchPaging-Functions.php');?>
                  <!-- PAGINATION END -->
                </section>
								
            </div>
            <!-- End Main Content -->
              
        </div>
        <!-- End span9 -->

        <?php get_sidebar('property-listing'); ?>
    </div>
    <!-- End contents row -->
</div>
<!-- End Content -->
<script>
    jQuery(window).ready(function () {
        jQuery("#btnSearch").click(function () { doSearch(); });
    });
    var mapLoaded = false;
    function showHideMap(show) {
        if (show) {
            jQuery(".mapHolder").show();
            jQuery(".listingHolder").hide();
            jQuery("li.active").each(function () {
                jQuery(this).removeClass("active");
            });

            jQuery("#liMap").addClass("active");

            if (!mapLoaded) {
                mapLoaded = true;
            <?php
                foreach($SearchResults as $result)
                {
                    if($result->latitude!=0)
                        echo "markAddress(" . json_encode($result) . ");";
                }
            ?>
                ReInitialize();
                
            }
        }
        else
        {
            jQuery(".mapHolder").hide();
            jQuery(".listingHolder").show();
        }
    }
    function doSearch(ord,page) {
        var loc = "/listing-search/" + encodeURI(jQuery("#propertyType").val());
        var han = "?";

        if (jQuery("#minprice").val() != "") {
            loc += han + "minp=" + encodeURI(jQuery("#minprice").val());
            han = "&";
        }

        if (jQuery("#maxprice").val() != "") {
            loc += han + "maxp=" + encodeURI(jQuery("#maxprice").val());
            han = "&";
        }

        if (jQuery("#bed").val() != "") {
            loc += han + "bed=" + encodeURI(jQuery("#bed").val());
            han = "&";
        }

        if (jQuery("#bath").val() != "") {
            loc += han + "bath=" + encodeURI(jQuery("#bath").val());
            han = "&";
        }

        if (jQuery("#search").val() != "") {
            loc += han + "kw=" + encodeURI(jQuery("#search").val());
            han = "&";
        }

        if (ord)
        {
            loc += han + "ord=" + ord;
            han = "&";
        }

        if (page)
        {
            loc += han + "pg=" + page;
            han = "&";
        }

        window.location = loc;
    }
</script>
<?php get_footer(); ?>