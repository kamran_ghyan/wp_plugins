<style>
.slick-prev:before, .slick-next:before {
    color:#fff !important;
}
</style>
<?php
wp_enqueue_style( 'property-search2', i5PBIntegration__PLUGIN_URL . 'css/search-results2.css', array(), '1.1', 'all');
wp_enqueue_style( 'property-search-header', i5PBIntegration__PLUGIN_URL . 'css/search-header2.css', array(), '1.1', 'all');

if(isset($_REQUEST["display"]) && $_REQUEST["display"]=="photo")
{
    wp_enqueue_style( 'slick-styles', i5PBIntegration__PLUGIN_URL . 'css/slick.css', array(), '1.1', 'all');
    wp_enqueue_style( 'slick-theme', i5PBIntegration__PLUGIN_URL . 'css/slick-theme.css', array(), '1.1', 'all');
    wp_enqueue_script( 'slick', i5PBIntegration__PLUGIN_URL . 'js/slick.min.js', array('jquery'), '1.1', true );
}

get_header();

$maxResults=24;
include(i5PBIntegration__PLUGIN_DIR . 'Includes/Prospect-Functions.php');
include(i5PBIntegration__PLUGIN_DIR . 'Includes/PropertySearch-Functions.php');
/*
Template Name: search results 2
 */

?>
<?php include(i5PBIntegration__PLUGIN_DIR . "Includes/HeaderSearch.php"); ?>

<div class="searchResults">
    <div class="container" id="listing">
        <div class="searchResultsHeader">
            <?php echo $TTLResults?> Properties for sale
            <span class="timestamp">Updated 
            <b>
                <?php
                $settings = get_option('WebListing_Settings');
                $lastRun = strtotime(date("Y-m-d\Th:i:s"));
								$currTime = $lastRun;

                if(isset($settings["LastRun"]))
                    $lastRun = strtotime($settings["LastRun"]);


                echo round(abs($currTime - $lastRun) / 60,0) . " MIN AGO";
                ?>
            </b>
          	</span>
            <?php include(i5PBIntegration__PLUGIN_DIR . 'Includes/SearchPaging-Functions.php');?>
        </div>
        <?php if(!isset($_REQUEST["display"]) || $_REQUEST["display"]=="gallery") :?>
        <div class="row" style="margin:0;">
            <?php foreach($SearchResults as $result){?>
            <div class="col1-4">
                <div class="listing">
                    <div class="listingImage">
                        <?php if(isset($result->image) && $result->image!=""):?>
                        <img src="<?php echo $result->image?>" />
                        <?php else:?>
                        <img src="<?php echo i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png" ?>" />
                        <?php endif;?>
                    </div>
                    <div id="fav<?php echo $result->mlsid?>" class="<?php echo ($favs=='' || !in_array($result->mlsid,$favs)?'listingFavorite':'listingFavoriteSelected')?>">
                        <a href="javascript:toggleFav('<?php echo $result->mlsid ?>');setFavorite('<?php echo $result->mlsid ?>',<?php echo ($favs=='' || !in_array($result->mlsid,$favs)?'1':'0')?>);">
                            <i class="fa fa-heart"></i>
                        </a>
                    </div>
                    <div class="listingAddress">
                        address locality | <?php echo $result->city?>
                    </div>
                    <div class="listingPrice">
                        <?php echo "$" . number_format($result->price,0)  . ($result->propertytype=="Rental"?" /Month":"")?>
                    </div>
                    <div class="listingBeds">
                        <div class="bedsValue">
                            <?php echo $result->bedrooms?>
                        </div>
                        <div class="bedsLabel">
                            Beds
                        </div>
                    </div>
                    <div class="listingBaths">
                        <div class="bathsValue">
                            <?php
                      if($result->halfBaths!=0 && $result->fullBaths!=0)
                          echo $result->fullBaths + ($result->halfBaths/2);
                      else if($result->halfBaths!=0 && $result->fullBaths==0)
                          echo ($result->halfBaths/2);
                      else
                          echo number_format($result->fullBaths,0);
                            ?>
                        </div>
                        <div class="bathsLabel">
                            Baths
                        </div>
                    </div>
                    <?php if($result->size!=0) :?>
                    <div class="listingSqft">
                        <div class="sqftValue">
                            <?php echo number_format($result->size,0);?>
                        </div>
                        <div class="sqftLabel">
                            SqFt
                        </div>
                    </div>
                    <?php endif;?>
                    <?php if($result->size!=0) :?>
                    <div class="listingAcres">
                        <div class="acresValue">
                            <?php echo number_format($result->lotsize,0);?>
                        </div>
                        <div class="acresLabel">
                            Acres
                        </div>
                    </div>
                    <?php endif;?>
                    
                    <div class="listingStatus">
                        <!-- other status
						<div class="shortSale">Short Sale</div>
            <div class="foreclosure">Foreclosure</div>-->
                        <div class="new">
                            <?php echo $result->status?>
                        </div>
                    </div>
                    <div class="viewDetails">
                        <a href="/property-details/<?php echo urldecode($result->url)?>">View Details</a>
                    </div>
                  	<div class="Disclaimer">
                        <?php echo htmlspecialchars_decode(get_post_meta( get_the_ID(), 'disclaimer', true )); ?>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
        <?php elseif($_REQUEST["display"]=="photo"): ?>
        <div class="photoLayout">
            <div class="sidebar" id="sidebar">
                <div class="listingSidebar">
                    <?php foreach($SearchResults as $result){?>
                    <div onclick="scrollToAnchor('<?php echo $result->mlsid?>')" style="cursor:pointer;" class="listingWrapper">
                        <div class="listingPhoto">
                            <?php if(isset($result->image) && $result->image!=""):?>
                            <img src="<?php echo $result->image?>" />
                            <?php else:?>
                            <img src="<?php echo i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png" ?>" />
                            <?php endif;?>
                            <div class="listingStatus">
                                <div class="new">
                                    <?php echo $result->status?>
                                </div>
                            </div>
                        </div>
                        <div class="listingDetails">
                            <div class="listingAddress">
                                <div class="listingAddress">
                                    address locality | <?php echo $result->city ?>
                                </div>
                                <div class="listingPrice">
                                    <?php echo "$" . number_format($result->price,0)  . ($result->propertytype=="Rental"?" /Month":"")?>
                                </div>
                                <div class="listingSpecs">
                                    <?php
                              $str=$result->bedrooms . " beds ";

                              if($result->halfBaths!=0 && $result->fullBaths!=0)
                                  $str .= $result->fullBaths + ($result->halfBaths/2);
                              else if($result->halfBaths!=0 && $result->fullBaths==0)
                                  $str .= ($result->halfBaths/2);
                              else
                                  $str .= number_format($result->fullBaths,0);

                              $str .= " baths " . number_format($result->size,0) . " sqft";

                              echo $str;
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div>
            </div>
            <div class="col2-3">
                <?php foreach($SearchResults as $result){?>
                <div class="listingBox">
                    <a name="<?php echo $result->mlsid?>"></a>
                    <div id="fav<?php echo $result->mlsid?>" class="<?php echo ($favs=='' || !in_array($result->mlsid,$favs)?'listingFavorite':'listingFavoriteSelected')?>">
                        <a href="javascript:toggleFav('<?php echo $result->mlsid ?>');setFavorite('<?php echo $result->mlsid ?>',<?php echo ($favs=='' || !in_array($result->mlsid,$favs)?'1':'0')?>);">
                            <i class="fa fa-heart"></i>
                        </a>
                    </div>
                    <h1 itemprop="address" itemtype="http://schema.org/PostalAddress">
                        <?php echo $result->address?>
                    </h1>
                    <div class="address2">
                        <?php echo $result->city . ", " . $result->state . " " . $result->zipcode?>
                    </div>
                    <div class="listingPhotos">
                        <p>
                            <div style="width:450px;display:none;" class="propertyImages">
                                <?php
                          $sql = "SELECT * from {$wpdb->prefix}i5Images where propertyid=%s";
                          $sql=$wpdb->prepare($sql,$result->propertyid);

                          $images=$wpdb->get_results($sql);

                          if($images==null || sizeof($images)==0)
                          {
                              echo " <div><img src='" . i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png' /></div>";
                          }
                          else
                          {
                              foreach($images as $image)
                                  echo " <div><img src='" . $image->url . "' /></div>";
                          }
                                ?>
                            </div>
                        </p>
                    </div>
                    <div class="listingDetailsContainer">
                        <div class="listingDetailsBox">
                            <div class="listingDetailsPrice">
                                <?php echo "$" . number_format($result->price,0)  . ($result->propertytype=="Rental"?" /Month":"")?>
                            </div>
                            <div class="listingDetailsBeds">
                                <div class="listingDetailsValue">
                                    <?php echo $result->bedrooms?>
                                </div>
                                <div class="listingDetailsLabel">
                                    Beds
                                </div>
                            </div>
                            <div class="listingDetailsAcres">
                                <div class="listingDetailsValue">
                                    <?php echo number_format($result->lotsize,0);?>
                                </div>
                                <div class="listingDetailsLabel">
                                    Acres
                                </div>
                            </div>
                            <div class="listingDetailsBaths">
                                <div class="listingDetailsValue">
                                    <?php echo number_format($result->fullBaths,0); ?>
                                </div>
                                <div class="listingDetailsLabel">
                                    Baths
                                </div>
                            </div>
                            <div class="listingDetails1-2Baths">
                                <div class="listingDetailsValue">
                                    <?php echo number_format($result->halfBaths,0); ?>
                                </div>
                                <div class="listingDetailsLabel">
                                    1/2 Baths
                                </div>
                            </div>
                            <div class="listingDetailsSqft">
                                <div class="listingDetailsValue">
                                    <?php echo number_format($result->size,0);?>
                                </div>
                                <div class="listingDetailsLabel">
                                    SQFT
                                </div>
                            </div>
                            <div class="listingDetailsPriceSqft">
                                <div class="listingDetailsValue">
                                    <?php
                          if(isset($result->size))
                              echo "$" . number_format($result->price/ $result->size,0)  . ($result->propertytype=="Rental"?" /Month":"");
                          else
                              echo "N/A";
                                    ?>
                                </div>
                                <div class="listingDetailsLabel">
                                    $/SQFT
                                </div>
                            </div>
                            <div class="listingDetailsSpecs">
                                <div class="specsLabel">
                                    Neighborhood:
                                </div>
                                <div class="specsValue">
                                    <?php echo ($result->subdivision==""?"&nbsp;":$result->subdivision);?>
                                </div>
                                <div class="specsLabel">
                                    Type:
                                </div>
                                <div class="specsValue">
                                    <?php echo ($result->propertytype==""?"&nbsp;":$result->propertytype);?>
                                </div>
                                <div class="specsLabel">
                                    Built:
                                </div>
                                <div class="specsValue">
                                    <?php echo ($result->yearbuilt==""?"&nbsp;":$result->yearbuilt);?>
                                </div>
                                <div class="specsLabel">
                                    Area:
                                </div>
                                <div class="specsValue">
                                    <?php echo ($result->area==""?"&nbsp;":$result->area);?>
                                </div>
                            </div>
                        </div>
                        <a href="/property-details/<?php echo urldecode($result->url)?>">
                            <button class="button fullButton">
                                View Full Details
                                <i class="fa fa-angle-right"></i>
                            </button>
                        </a>
                    </div>

                </div>
                <!-- end listingBox -->
                <?php }?>
            </div>

        </div>
        <script>
                jQuery(function () {
                    jQuery(".propertyImages").slick({
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: true,
                        fade: true
                    });
                    jQuery(".propertyImages").show();
                });
        </script>
      <script>
        jQuery(function() {

            var jQuerysidebar   = jQuery("#sidebar"), 
            jQuerywindow    = jQuery(window),
                offset     = jQuerysidebar.offset(),
                topPadding = 15;

            jQuerywindow.scroll(function() {
                if (jQuerywindow.scrollTop() > offset.top) {
                	jQuerysidebar.stop().animate({
                        marginTop: jQuerywindow.scrollTop() - offset.top + topPadding
                    });
                } else {
                	jQuerysidebar.stop().animate({
                        marginTop: 0
                    });
                }
            });
            
        });
        </script>
        <?php elseif($_REQUEST["display"]=="map"): ?>
        <div class="col1-3">
            <div class="listingSidebar mapSidebar">
                <?php foreach($SearchResults as $result){
                          $lat=0;
                          $long=0;

                          if(isset($SearchResults) && sizeof($SearchResults)>0)
                          {
                              $lat=$SearchResults[0]->latitude;
                              $long=$SearchResults[0]->longitude;
                          }

                ?>
                <div onclick="location.href='/property-details/<?php echo $result->url?>';" onmouseover="openInfoWindow('<?php echo $result->mlsid?>')" style="cursor:pointer;" class="listingWrapper">
                    <div class="listingPhoto">
                        <?php if(isset($result->image) && $result->image!=""):?>
                        <img src="<?php echo $result->image?>" />
                        <?php else:?>
                        <img src="<?php echo i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png" ?>" />
                        <?php endif;?>
                        <div class="listingStatus">
                            <div class="new">
                                <?php echo $result->status?>
                            </div>
                        </div>
                    </div>
                    <div class="listingDetails">
                        <div class="listingAddress">
                            <div class="listingAddress">
                                address locality | <?php echo $result->city ?>
                            </div>
                            <div class="listingPrice">
                                <?php echo "$" . number_format($result->price,0)  . ($result->propertytype=="Rental"?" /Month":"")?>
                            </div>
                            <div class="listingSpecs">
                                <?php
                          $str=$result->bedrooms . " beds ";

                          if($result->halfBaths!=0 && $result->fullBaths!=0)
                              $str .= $result->fullBaths + ($result->halfBaths/2);
                          else if($result->halfBaths!=0 && $result->fullBaths==0)
                              $str .= ($result->halfBaths/2);
                          else
                              $str .= number_format($result->fullBaths,0);

                          $str .= " baths " . number_format($result->size,0) . " sqft";

                          echo $str;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>
            </div>
        </div>
        <div class="col2-3">
            <div class="listingMap">
                <?php
                  do_shortcode('[i5ListingMap maxzoom="100" minzoom="1" lat="' . $lat . '" long="' . $long . '" style="display:block;width: 100%; height: 575px" showPolygon="true"]');
                ?>
                <script>
                <?php
                  foreach($SearchResults as $result)
                  {
                      if($result->latitude!=0)
                          echo "markAddress(" . json_encode($result) . ");";
                  }
                ?>
                    fitBounds();
                </script>
            </div>
        </div>
        <?php endif;?>
        <?php include(i5PBIntegration__PLUGIN_DIR . 'Includes/SearchPaging-Functions.php');?>
    </div>
    <div class="bottomDisclaimer">
        <?php echo htmlspecialchars_decode(get_post_meta( get_the_ID(), 'companylogo', true )); ?>
      	<br/>
      <div style="text-align:center"> 
      	REIN updates its listings on a daily basis.  Data last updated: 
      <?php
				
                echo Date('m-d-Y',$lastRun);

                ?>
        </div>
    </div>
</div>
<?php 
include(i5PBIntegration__PLUGIN_DIR . 'Includes/Footer-Disclaimer.php');
?>
<script>
    function toggleFav(id)
    {
        if (jQuery("#fav" + id).hasClass("listingFavoriteSelected"))
        {
            jQuery("#fav" + id).removeClass("listingFavoriteSelected");
            jQuery("#fav" + id).addClass("listingFavorite");
            jQuery(jQuery("#fav" + id).children()[0]).prop("href","javascript:toggleFav('" + id + "');setFavorite('" + id + "',1);");
        }
        else {
            jQuery("#fav" + id).addClass("listingFavoriteSelected");
            jQuery("#fav" + id).removeClass("listingFavorite");
            jQuery(jQuery("#fav" + id).children()[0]).prop("href", "javascript:toggleFav('" + id + "');setFavorite('" + id + "',0);");
        }

    }
    function scrollToAnchor(aid) {
        var aTag = jQuery("a[name='" + aid + "']");
        jQuery('html,body').animate({ scrollTop: aTag.offset().top }, 'slow');
    }
    jQuery("img").error(function () {
        jQuery(this).attr("src", '<?php echo i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png" ?>');
    });
    <?php if(isset($_REQUEST["display"]) && $_REQUEST["display"]=="photo"): ?>
    jQuery(function () {
        jQuery(".sticky").sticky({ topSpacing: 0 });
    });
    <?php endif; ?>
</script>

<?php
include(i5PBIntegration__PLUGIN_DIR . 'Includes/Global-Functions.php');
get_footer();
?>