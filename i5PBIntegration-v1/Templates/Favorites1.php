<?php
    wp_enqueue_style( 'property-search1', plugin_dir_url() . 'i5PBIntegration/css/favorites1.css', array(), '1.1', 'all');
    
/*
Template Name: Favorites Theme 1
 */

get_header();

/* Theme Listing Page Module */
$theme_listing_module = get_option('theme_listing_module');

/* Only for demo purpose only */
if(isset($_GET['module'])){
    $theme_listing_module = $_GET['module'];
}

switch($theme_listing_module){
    case 'properties-map':
        get_template_part('banners/map_based_banner');
        break;

    default:
        get_template_part('banners/default_page_banner');
        break;
}

include(i5PBIntegration__PLUGIN_DIR . 'Includes/Favorites-Functions.php');
?>

<!-- Content -->
<div class="container contents lisitng-grid-layout">
    <div class="row">
        <div class="span9 main-wrap">

            <!-- Main Content -->
            <div class="main">

                <section class="listing-layout">
                    <div class="list-container clearfix" style="padding:0;">
                        <h3 class="title-heading">
                            <?php the_title(); ?>
                        </h3>
                        <?php
                        foreach($Favorites as $result)
                        {
                        ?>
                        <div class="favoritesContainer">
                            <div class="favoritesPhoto">
                                <a href="/property-details/<?php echo urldecode($result->url)?>">
                                    <?php if(isset($result->image) && $result->image!=""):?>
                                    <img src="<?php echo $result->image?>" />
                                    <?php else:?>
                                    <img src="<?php echo plugin_dir_url()?>" . "i5pbintegration/images/imgunavailable.png" />
                                    <?php endif;?>
                                </a>
                            </div>
                            <div class="favoritesDetails">

                                <div class="favoritesPrice">
                                    <?php echo "$" . number_format($result->price,0)?>
                                </div>
                                <div class="favoritesAddress">
                                    <?php echo $result->address?>
                                    <br />
                                    <span class="favoritesCityStateZip">
                                        <?php echo $result->city?>, <?php echo $result->state?> <?php echo $result->zipCode?>
                                    </span>
                                </div>
                                <div class="favoritesInfo">
                                    <div class="infoItem">
                                        <span class="lblBed">Bed:</span>
                                        <span class="infoBed">
                                            <?php echo $result->bedrooms?>
                                        </span>
                                    </div>
                                    <div class="infoItem">
                                        <span class="lblBath">Bath:</span>
                                        <span class="infoBed">
                                            <?php
                            if($result->halfBaths!=0 && $result->fullBaths!=0)
                                echo $result->fullBaths + ($result->halfBaths/2);
                            else if($result->halfBaths!=0 && $result->fullBaths==0)
                                echo ($result->halfBaths/2);
                            else
                                echo number_format($result->fullBaths,0);
                                            ?>
                                        </span>
                                    </div>
                                    <?php if($result->size!=0) :?>
                                    <div class="infoItem">
                                        <span class="lblSqft">Sqft:</span>
                                        <span class="infoSqft"><?php $result->size?></span>
                                    </div>
                                    <?php endif;?>
                                </div>
                                <div class="favoritesButtons">
                                    <div class="btnRemove">
                                        <a href="javascript:setFavorite('<?php echo $result->mlsid ?>',0)">
                                            <i class="fa fa-trash-o"></i>
                                            Remove from Favorites
                                        </a>
                                    </div>
                                    <div class="btnDetails">
                                        <a href="/property-details/<?php echo urldecode($result->url)?>">
                                            <i class="fa fa-info-circle"></i>
                                            View Property Details
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                </section>

            </div>
            <!-- End Main Content -->

        </div>
        <!-- End span9 -->

        <?php get_sidebar('property-listing'); ?>

    </div>
    <!-- End contents row -->
</div>
<!-- End Content -->

<?php get_footer(); ?>