<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://i5fusion.com/
 * @since      1.0.0
 *
 * @package    I5pbsearch
 * @subpackage I5pbsearch/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    I5pbsearch
 * @subpackage I5pbsearch/includes
 * @author     Jarrett <jarrettf@i5fusion.com>
 */
class I5pbsearch_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
