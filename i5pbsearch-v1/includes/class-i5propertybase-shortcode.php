<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.i5fusion.com
 * @since      1.0.0
 *
 * @package    I5propertybase
 * @subpackage I5propertybase/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    I5propertybase
 * @subpackage I5propertybase/includes
 * @author     Jarret Fisher <jarret@i5fusion.com>
 */
class I5propertybase_Shortcode {
	/**
	 * Retrieve the shortcode for search.
	 *
	 * @since     1.0.0
	 */
	public static function search_shortcode($atts) {

		$vars = shortcode_atts(array(
		  'bgcolor'		=> '',
		  'display' 	=> 'map',
		  'min_price' 	=> true,
		  'max_price' 	=> true,
		  'beds' 		=> true,
		  'baths' 		=> true
          ), $atts);
	?>
	<form  method="post"  name="search_form_i5pbsearch">
	<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 form-group i5search-form">
			<div class="col-xs-12 col-sm-4 col-md-3">
				<label>Location</label>
				<input type="text" class="form-control" id="location" name="location" placeholder="Enter ZIP CODE">


			</div>
			<div class="col-xs-12 col-sm-4 col-md-2 dropdown">
				<label>Property Type</label>
				<input type="text" class="form-control" id="place_type" value="" name="propertyType" placeholder="Property Type">

				<div id="ddPropertyType" class="ddPropertyType i5DropDown">
	                <div class="input-text">
	                    <input type="radio" name="propertyType" value="Apartment"> Apartment
	                </div>
	                <div class="input-text">
	                    <input type="radio" name="propertyType" value="Condo"> Condo
	                </div>
	                <div class="input-text">
	                    <input type="radio" name="propertyType" value="House"> House
	                </div>
	            </div>

			</div>
			<div class="col-xs-12 col-sm-4 col-md-2 dropdown">
				<label>Min. Price</label>
				<input type="text" class="form-control" id="min_pr" name="minPrice" placeholder="Min. Price">

				<div id="ddMinPrice" class="ddMinPrice i5DropDown">
					<div class="input-text">
	                    <input type="radio" value="Any"> Any
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="50000" name="minprice"> $50K
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="75000" name="minprice"> $75K
	                </div>
	                <div class="input-text">
	                    <input  type="radio" value="100000" name="minprice"> $100K
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="150000" name="minprice"> $150K
	                </div>
	                <div class="input-text">
	                    <input  type="radio" value="200000" name="minprice"> $200K
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="250000" name="minprice"> $250K
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="300000" name="minprice"> $300K
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="350000" name="minprice"> $350K
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="400000" name="minprice"> $400K
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="500000" name="minprice"> $500K
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="600000" name="minprice"> $600K
	                </div>

	                
	            </div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-2 dropdown">
				<label>Max. Price</label>
				<input type="text" class="form-control" id="max_pr" name="maxPrice" placeholder="Max. Price">

				<div id="ddMaxPrice" class="ddMaxPrice i5DropDown">
	                <div class="input-text">
	                    <input type="radio" value="Any" name="maxprice"> Any
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="50000" name="maxprice"> $50K
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="75000" name="maxprice"> $75K
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="100000" name="maxprice"> $100K
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="150000" name="maxprice"> $150K
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="200000" name="maxprice"> $200K
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="250000" name="maxprice"> $250K
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="300000" name="maxprice"> $300K
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="350000" name="maxprice"> $350K
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="400000" name="maxprice"> $400K
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="500000" name="maxprice"> $500K
	                </div>
	                <div class="input-text">
	                    <input type="radio" value="600000" name="maxprice"> $600K
	                </div>
	            </div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-1 dropdown">
				<label>Beds</label>
				<input type="text" class="form-control" id="beds_" name="minBedrooms" placeholder="Beds">

				<div id="ddBeds" class="ddBeds i5DropDown">
	                <div class="input-text">
	                    <input name="minbedrooms" value="" type="radio" />Any
	                </div>
	                <div class="input-text">
	                    <input name="minbedrooms" value="2" type="radio" />2+
	                </div>
	                <div class="input-text">
	                    <input name="minbedrooms" value="3" type="radio" />3+
	                </div>
	                <div class="input-text">
	                    <input name="minbedrooms" value="4" type="radio" />4+
	                </div>
	                <div class="input-text">
	                    <input name="minbedrooms" value="5" type="radio" />5+
	                </div>
	            </div>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-1 dropdown">
				<label>Baths</label>
				<input type="text" class="form-control" id="baths_" name="minBathrooms" placeholder="Baths">

				<div id="ddBaths" class="ddBaths i5DropDown">
	                <div class="input-text">
	                    <input name="minfullBaths" value="" type="radio" />Any
	                </div>
	                <div class="input-text">
	                    <input name="minfullBaths" value="2" type="radio" />2+
	                </div>
	                <div class="input-text">
	                    <input name="minfullBaths" value="3" type="radio" />3+
	                </div>
	                <div class="input-text">
	                    <input name="minfullBaths" value="4" type="radio" />4+
	                </div>
	                <div class="input-text">
	                    <input name="minfullBaths" value="5" type="radio" />5+
	                </div>
	            </div>
			</div>
			<div class="col-xs-12 col-sm-2 col-md-1">
				<input type="hidden" value="map" name="display">
				<label> </label>
				<button type="submit" class="btn btn-default btn-space" id="submit_i5pbsearch"><i class="fa fa-search"></i></button>
			</div>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
	//Placeholdem( document.querySelectorAll( '[placeholder]' ) );
</script>
	<?php
	}

}
