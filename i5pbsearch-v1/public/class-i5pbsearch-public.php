<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://i5fusion.com/
 * @since      1.0.0
 *
 * @package    I5pbsearch
 * @subpackage I5pbsearch/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    I5pbsearch
 * @subpackage I5pbsearch/public
 * @author     Jarrett <jarrettf@i5fusion.com>
 */
class I5pbsearch_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		//self::init_shortcode();

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in I5pbsearch_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The I5pbsearch_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/i5pbsearch-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in I5pbsearch_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The I5pbsearch_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_script( 'i5pbsearch-placeholder', plugin_dir_url( __FILE__ ) . 'js/placeholdem.min.js' );
		wp_enqueue_script( 'i5pbsearch-custom-js', plugin_dir_url( __FILE__ ) . 'js/i5pbsearch-public.js');

	}
	public function init_shortcode() {
		/**
		 * This function is create shortcode.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-i5propertybase-shortcode.php';

	}
}
