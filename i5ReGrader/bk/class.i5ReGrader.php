<?php
class i5ReGrader
{
    public static function activation(){
        wp_schedule_event(time() + 60 * 5,'hourly','SyncAnalytics');

        //Setup database
        self::setupdb();
    }
    public static function deactivation(){
        wp_clear_scheduled_hook('SyncAnalytics');
    }
    private static function setupdb(){
        global $wpdb;
        global $i5PBIntegration_Version;
        $i5PBIntegration_Version="1.0";

        $charset_collate = $wpdb->get_charset_collate();

        //Setup listing table
        $tableName= $wpdb->prefix . "i5Analytics";

        $sql = "CREATE TABLE $tableName (
                metricName nvarchar(255) NOT NULL,
                metricValue nvarchar(255) NOT NULL,
                PRIMARY KEY (metricName)
                ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        $result = dbDelta( $sql );
    }
    public static function getData(){
        global $wpdb;
        require_once i5ReGrader__PLUGIN_DIR . '/google-api/vendor/autoload.php';

        $settings=get_option('ReGradder_Settings');

        if(isset($settings["OAuth1"]))
        {
            $client = new Google_Client();
            $client->setAuthConfig(i5ReGrader__PLUGIN_DIR . '/google-api/client_secrets.json');
            $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
            $client->setAccessToken($settings["OAuth1"]);
            if($client->isAccessTokenExpired())
            {
                $client->refreshToken($settings["OAuth1Refresh"]);
                $settings["OAuth1"]=$client->getAccessToken();
                update_option("ReGradder_Settings",$settings);
            }

            //Get account 1 values
            $sourceValues = array();
            $text = trim($settings["ViewIds1"]);
            $textAr = explode("\r\n", $text);
            $textAr = array_filter($textAr, 'trim');

            foreach ($textAr as $line) {
                $response = self::getReport($client,$line);
                $sourceValues = self::parseResults($response,$sourceValues);
            }

            $sourceValues["Bounce Rate"]=round($sourceValues["Bounce Rate"]/sizeof($textAr),2);
            $sourceValues["Average Pages Per Visit"]=round($sourceValues["Average Pages Per Visit"]/sizeof($textAr),2);
            $sourceValues["Percentage of New Users"]=round($sourceValues["Percentage of New Users"]/sizeof($textAr),2);
            $sourceValues["Mobile Bounce Rate"]=round($sourceValues["Mobile Bounce Rate"]/sizeof($textAr),2);
            $sourceValues["Time on Site"]=$sourceValues["Time on Site"]/sizeof($textAr);
            $sourceValues["Average Page Load Time"]=$sourceValues["Average Page Load Time"]/sizeof($textAr);
            $sourceValues["Mobile Average Session Duration"]=$sourceValues["Mobile Average Session Duration"]/sizeof($textAr);

            //Save the data to the db
            foreach($sourceValues as $key=> $value)
            {
                $sql = "INSERT INTO {$wpdb->prefix}i5Analytics (metricName,metricValue) VALUES(%s,%s) ON DUPLICATE KEY UPDATE metricValue=%s";

                $sql = $wpdb->prepare($sql,$key,$value,$value);

                $result=$wpdb->query($sql);
            }
        }
    }
    public function getReport($client,$viewId) {
        // Create an authorized analytics service object.
        $analytics = new Google_Service_AnalyticsReporting($client);

        // Replace with your view ID. E.g., XXXX.
        $VIEW_ID = $viewId;

        // Create the DateRange object.
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate("7daysAgo");
        $dateRange->setEndDate("today");

        // Create the Metrics object.
        $bounceRateMetric = new Google_Service_AnalyticsReporting_Metric();
        $bounceRateMetric->setExpression("ga:bounceRate");
        $bounceRateMetric->setAlias("Bounce Rate");

        $avgTimeMetric = new Google_Service_AnalyticsReporting_Metric();
        $avgTimeMetric->setExpression("ga:avgSessionDuration");
        $avgTimeMetric->setAlias("Time on Site");

        //??? Average Visitors

        $PagesPerVisitMetric = new Google_Service_AnalyticsReporting_Metric();
        $PagesPerVisitMetric->setExpression("ga:pageviewsPerSession");
        $PagesPerVisitMetric->setAlias("Average Pages Per Visit");

        $newUserMetric = new Google_Service_AnalyticsReporting_Metric();
        $newUserMetric->setExpression("ga:percentNewSessions");
        $newUserMetric->setAlias("Percentage of New Users");

        $AvgPagesLoadTimeMetric = new Google_Service_AnalyticsReporting_Metric();
        $AvgPagesLoadTimeMetric->setExpression("ga:avgPageLoadTime");
        $AvgPagesLoadTimeMetric->setAlias("Average Page Load Time");

        $userMetric = new Google_Service_AnalyticsReporting_Metric();
        $userMetric->setExpression("ga:users");
        $userMetric->setAlias("Users");

        $sessionMetric = new Google_Service_AnalyticsReporting_Metric();
        $sessionMetric->setExpression("ga:sessions");
        $sessionMetric->setAlias("Sessions");

        $sourceDimension = new Google_Service_AnalyticsReporting_Dimension();
        $sourceDimension->setName("ga:medium");

        $socialNetworkDimension = new Google_Service_AnalyticsReporting_Dimension();
        $socialNetworkDimension->setName("ga:socialNetwork");

        $browserDimension = new Google_Service_AnalyticsReporting_Dimension();
        $browserDimension->setName("ga:deviceCategory");

        // Create the ReportRequest object.
        $requestGlobal = new Google_Service_AnalyticsReporting_ReportRequest();
        $requestGlobal->setViewId($VIEW_ID);
        $requestGlobal->setDateRanges($dateRange);
        $requestGlobal->setMetrics(array($bounceRateMetric,$avgTimeMetric,$PagesPerVisitMetric,$AvgPagesLoadTimeMetric,$newUserMetric));

        //Get Direct Refferal and social
        $requestSource = new Google_Service_AnalyticsReporting_ReportRequest();
        $requestSource->setViewId($VIEW_ID);
        $requestSource->setDateRanges($dateRange);
        $requestSource->setMetrics(array($userMetric,$sessionMetric));
        $requestSource->setDimensions(array($sourceDimension,$socialNetworkDimension));

        //Get the Mobile Stats
        $requestMobile = new Google_Service_AnalyticsReporting_ReportRequest();
        $requestMobile->setViewId($VIEW_ID);
        $requestMobile->setDateRanges($dateRange);
        $requestMobile->setMetrics(array($avgTimeMetric,$sessionMetric,$bounceRateMetric));
        $requestMobile->setDimensions(array($browserDimension));

        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests( array( $requestGlobal,$requestSource,$requestMobile) );
        return $analytics->reports->batchGet( $body );
    }
    public function parseResults($reports,$results) {
        if($results==null)
            $results=array();

        for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
            $report = $reports[ $reportIndex ];
            $header = $report->getColumnHeader();
            $dimensionHeaders = $header->getDimensions();
            $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
            $rows = $report->getData()->getRows();

            for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
                //print("<br/><br/>");
                $row = $rows[ $rowIndex ];
                $dimensions = $row->getDimensions();
                $metrics = $row->getMetrics();
                for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
                    //print($dimensionHeaders[$i] . ": " . $dimensions[$i] . "<br/><hr>");
                }

                for ($j = 0; $j < count( $metricHeaders ); $j++) {
                    $entry = $metricHeaders[$j];
                    $values = $metrics[0];
                    //print("Metric type: " . $entry->getType() . ": " );

                    $value = $values->getValues()[ $j ];

                    if(count($dimensionHeaders)==0)
                    {
                        if($entry->getName()=="Bounce Rate" || $entry->getName()=="Average Pages Per Visit")
                            $results[$entry->getName()]+=round($value,2);
                        else
                            $results[$entry->getName()] +=$value;
                    }
                    else
                    {
                        if($dimensionHeaders[0]=="ga:medium" && $entry->getName()=="Sessions")
                        {
                            if($dimensions[0]=="(none)")
                                $results["Direct"] += $value;
                            else if($dimensions[0]=="organic")
                                $results["Organic"] += $value;
                            else if($dimensions[0]=="(not set)")
                                $results["Referral"] += $value;
                            else
                                $results["Social"] += $value;
                        }
                        else if($dimensionHeaders[0]=="ga:deviceCategory")
                        {
                            if($dimensions[0]=="mobile" && $entry->getName()=="Bounce Rate")
                                $results["Mobile Bounce Rate"]+=$value;
                            else if($dimensions[0]=="mobile" && $entry->getName()=="Time on Site")
                                $results["Mobile Average Session Duration"]+=$value;
                            else if($dimensions[0]=="mobile" && $entry->getName()=="Sessions")
                                $results["Mobile Sessions"]+=$value;
                        }
                    }

                    //print($entry->getName() . ": " . $value . "<br/>");
                }
            }
        }

        return $results;
    }
}