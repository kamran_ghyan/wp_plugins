<?php
/*
Plugin Name:i5Regrader
Plugin URI: http://www.i5fusion.com
Description: Google Adwords Integration
Author: Jarrett Fisher
Version: 1.0
Author URI:http://www.i5fusion.com
 */

define( 'i5ReGrader__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define('i5ReGrader__PLUGIN_URL',plugin_dir_url(__FILE__));

if(is_admin()){
    require_once( i5ReGrader__PLUGIN_DIR . 'class.i5ReGrader-Admin.php' );
    add_action( 'init', array( 'i5ReGrader_Admin', 'init' ) );
}

require_once( i5ReGrader__PLUGIN_DIR . 'class.i5ReGrader-Template.php' );
require_once( i5ReGrader__PLUGIN_DIR . 'class.i5ReGrader.php' );
add_action( 'plugins_loaded', array( 'i5ReGrader_Template', 'get_instance' ) );

register_deactivation_hook(__FILE__,array('i5ReGrader', 'deactivation'));
register_activation_hook(__FILE__, array('i5ReGrader', 'activation'));

add_action('SyncAnalytics',array('i5ReGrader', 'getData'));
?>