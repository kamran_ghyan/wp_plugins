<?php
class i5ReGrader_Admin
{
    private static $initiated = false;

    public static function init(){
        if(!self::$initiated){
            self::init_hooks();
        }
    }

    public static function init_hooks(){
        add_action('admin_menu',array('i5ReGrader_Admin','admin_menu'));
    }
    public static function admin_menu(){
        add_options_page('ReGrader Config','ReGrader Config','manage_options',__FILE__,array('i5ReGrader_Admin', 'admin_interface'));
	}
    public static function admin_interface(){
        session_start();
        $settings=get_option('ReGradder_Settings');

        if(isset($_REQUEST["token"]))
        {
            //TODO: Need to figure this out???
            //if($_REQUEST["a"]=="1")
                $settings["OAuth1"]=$_SESSION['access_token'];
                $settings["OAuth1Refresh"]=$_SESSION['access_token']['refresh_token'];
            //else
            //    $settings["OAuth2"]=json_decode($_SESSION['access_token']);

            update_option("ReGradder_Settings",$settings);

            echo "<script>window.location='/wp-admin/options-general.php?page=i5ReGrader%2Fclass.i5ReGrader-Admin.php'</script>";
        }
        else if(isset($_POST['Auth1']))
        {
            $redirect_url='http://' . $_SERVER['HTTP_HOST'] . '/oauth2callback?a=1';

            echo "<script>window.location='" . filter_var($redirect_url, FILTER_SANITIZE_URL) . "'</script>";
        }
        else if(isset($_POST['Auth2']))
        {
            $redirect_url='http://' . $_SERVER['HTTP_HOST'] . '/oauth2callback?a=2';

            echo "<script>window.location='" . filter_var($redirect_url, FILTER_SANITIZE_URL) . "'</script>";
        }
        else if(isset($_POST['DeAuth1']))
        {
            $settings["OAuth1"]=null;
            update_option("ReGradder_Settings",$settings);
        }
        else if(isset($_POST['DeAuth2']))
        {
            $settings["OAuth2"]=null;
            update_option("ReGradder_Settings",$settings);
        }
        else if(isset($_POST["saveIds"]))
        {
            $settings["ViewIds1"]=$_REQUEST["txtViewId1"];
            $settings["ViewIds2"]=$_REQUEST["txtViewId2"];

            update_option("ReGradder_Settings",$settings);
        }

?>
        <div class="wrap">
            <p>This page is to setup authorization with the please make sure you are logged into the correct Gmail account before clicking on the buttons below.</p>
            <form id="frmAdWords" method="post" action="" novalidate="novalidate">
                <input id="hidClickType" type="hidden" />
                <input id="btnSubmit" name="submit" hidden="submit" type="submit" style="display:none;"/>
                <table class="form-table">
                    <tr>
                        <td>Adwords Account 1:</td>
                        <td>
                            <?php if(isset($settings["OAuth1"])):?>
                            <input type="submit" name="DeAuth1" id="DeAuth1" value="De-Authorize" />
                            <?php else:?>
                            <input type="submit" name="Auth1" id="Auth1" value="Click To Authorize Account 1" />
                            <?php endif;?>
                        </td>
                    </tr>
                    <tr>
                        <td>Adwords Account 2:</td>
                        <td>
                            <?php if(isset($settings["OAuth2"])):?>
                            <input type="submit" name="DeAuth2" id="DeAuth2" value="De-Authorize" />
                            <?php else:?>
                            <input type="submit" name="Auth2" id="Auth2" value="Click To Authorize Account 2" />
                            <?php endif;?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Account 1 View Id's
                        </td>
                        <td>
                            <textarea id="txtViewId1" name="txtViewId1" cols="70" rows="15"><?php echo $settings["ViewIds1"]==null?"":$settings["ViewIds1"]?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Account 2 View Id's
                        </td>
                        <td>
                            <textarea id="txtViewId2" name="txtViewId2" cols="70" rows="15"><?php echo $settings["ViewIds2"]==null?"":$settings["ViewIds2"]?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <input id="saveIds" name="saveIds" type="submit" value="Save View Ids" />
                        </td>
                    </tr>
                </table>
            </form>
            <script>
                function setClickType(t)
                {
                    jQuery("#hidClickType").val(t);
                    jQuery("#btnSubmit").click();
                }
            </script>
        </div>
<?php
    }
}
?>