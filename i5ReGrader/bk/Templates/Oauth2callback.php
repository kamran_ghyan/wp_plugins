<?php
/**
 *Template name:Google Oauth Callback
 */
// Load the Google API PHP Client Library.
require_once i5ReGrader__PLUGIN_DIR . '/google-api/vendor/autoload.php';

// Start a session to persist credentials.
session_start();

// Create the client object and set the authorization configuration
// from the client_secrets.json you downloaded from the Developers Console.
$client = new Google_Client();
$client->setAuthConfig(i5ReGrader__PLUGIN_DIR . '/google-api/client_secrets.json');

if(isset($_REQUEST["a"]))
{
    $client->setAccessType("offline");
    $client->setApprovalPrompt("force");
    $client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/oauth2callback?a=' . $_REQUEST["a"]);
}
else
    $client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/oauth2callback');

$client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
//$client->addScope(Google_Service_Analytics::ANALYTICS);

// Handle authorization flow from the server.
if (! isset($_GET['code'])) {
    $auth_url = $client->createAuthUrl();
    header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
} else {
    $client->authenticate($_GET['code']);

    $_SESSION['access_token'] = $client->getAccessToken();

    $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/analytics-report';

    if(isset($_REQUEST["a"]))
    {
        $redirect_uri="/wp-admin/options-general.php?page=i5ReGrader%2Fclass.i5ReGrader-Admin.php&a=" . $_REQUEST["a"] . "&token=true";
    }

    header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}
?>