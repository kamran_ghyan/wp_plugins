<?php
/**
 *Template name:Adwords Report 2
 */
get_header('inner');

// Load the Google API PHP Client Library.
require_once i5ReGrader__PLUGIN_DIR . '/google-api/vendor/autoload.php';
require_once i5ReGrader__PLUGIN_DIR . '/class.i5ReGrader.php';

session_start();

if(isset($_REQUEST["subView"]))
{
    $_SESSION["selectedView"]=$_REQUEST["selView"];
}

$client = new Google_Client();
$client->setAuthConfig(i5ReGrader__PLUGIN_DIR . '/google-api/client_secrets.json');
$client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);

if(isset($_SESSION["access_token"]))
    $client->setAccessToken($_SESSION['access_token']);

// If the user has already authorized this app then get an access token
// else redirect to ask the user to authorize access to Google Analytics.
if (isset($_SESSION['access_token']) && $_SESSION['access_token'] && !$client->isAccessTokenExpired()) {

    //Get the source report
    //TODO: Get Token from DB
    $settings=get_option('ReGradder_Settings');

    //TODO: Get Source from db
    $sourceValues=array();
    $sql = "select metricName,metricValue from {$wpdb->prefix}i5Analytics";

    $data=$wpdb->get_results($sql);

    foreach($data as $metric)
        $sourceValues[$metric->metricName]=$metric->metricValue;

    //Get the compare account
    $client->setAccessToken($_SESSION['access_token']);

    if(!isset($_SESSION["accounts"]) || !isset($_SESSION["selectedView"]))
    {
        $analytics = new Google_Service_Analytics($client);
        $accounts = $analytics-> management_profiles->listManagementProfiles("~all","~all");

        $views=array();
        foreach($accounts as $acc)
        {
            if(!isset($_SESSION["selectedView"]))
                $_SESSION["selectedView"]=$acc->getId();

            $views[$acc->getId()]=$acc->getName();
        }

        $_SESSION["accounts"]=$views;
    }

    $re=new i5ReGrader();

    $response = $re->getReport($client,$_SESSION["selectedView"]);
    $compareValues= $re->parseResults($response,null);

    $compareValues["Bounce Rate"]=round($compareValues["Bounce Rate"],2);
    $compareValues["Average Pages Per Visit"]=round($compareValues["Average Pages Per Visit"],2);
    $compareValues["Percentage of New Users"]=round($compareValues["Percentage of New Users"],2);
    $compareValues["Mobile Bounce Rate"]=round($compareValues["Mobile Bounce Rate"],2);
    //$compareValues["Time on Site"]=gmdate("H:i:s",$compareValues["Time on Site"]);
    //$compareValues["Average Page Load Time"]=gmdate("H:i:s",$compareValues["Average Page Load Time"]);
    //$compareValues["Mobile Average Session Duration"]=gmdate("H:i:s",$compareValues["Mobile Average Session Duration"]);


    echo "<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.1/Chart.bundle.min.js'></script>";
    
    //echo "<div id='site_search_popup' class='site-search-popup cms-page-overlay'><div class='cms-page-overlay-inner'><div class='cms-search-popup'><div class='container'><div class='site-search-popup-inner text-center'><form method='get' class='search-form' action='http://resitegrader.i5fusion.com/'><label><span class='screen-reader-text'>Search for:</span><input type='search' class='search-field' placeholder='Type and hit enter...' value='' name='s' title='Search for:'></label><button type='submit' class='search-submit'><i class='fa fa-search'></i></button></form></div></div></div></div></div>";

    echo "<div id='content' class='site-content'><div class='container'><div id='primary' class='content-area'><form class='accountForm'><div class='accountSelect'>Select Account: <select id='selView' name='selView'>";

    foreach($_SESSION["accounts"] as $key=> $value)
    {
        if($_SESSION["selectedView"]==$key)
            echo "<option selected value='" . $key . "'>" . $value . "</option>";
        else
            echo "<option value='" . $key . "'>" . $value . "</option>";

    }

    echo "</select><input type='submit' id='subView' name='subView' value='Go'/></form>";

    echo "<table><tr><th>Metric</th><th>Our Data</th><th>Your Data</th><th>Difference</th></tr>";

    $index=0;

    foreach($sourceValues as $key=> $value)
    {
        $per="";

        if($key=="Bounce Rate" || $key=="Average Pages Per Visit" || $key=="Mobile Bounce Rate" || $key=="Percentage of New Users")
            $per="%";

        $v1=$value;
        $v2=$compareValues[$key];
        $d1=$value;
        $d2=$compareValues[$key];
        $difference=1;
        $chartDiff=1;
        $titleAdd='';

        if($key=="Time on Site" || $key=="Average Page Load Time" || $key=="Mobile Average Session Duration")
        {
            $titleAdd= " (Seconds)";
            $d1=gmdate("H:i:s",$d1);
            $d2=gmdate("H:i:s",$d2);

            if($v1>$v2)
            {
                $difference=gmdate("H:i:s",$v1-$v2);
                $chartDiff=gmdate("s",$v1-$v2);
            }
            else
            {
                $difference="-" . gmdate("H:i:s",$v2-$v1);
                $chartDiff="-" . gmdate("s",$v2-$v1);
            }

            $v1="1";
            $v2="1";
        }
        else
        {
            $difference=$d1 - $d2;
            $chartDiff=$difference;
        }

        echo "<tr><td>" . $key . "</td><td>" . $d1 . $per . "</td><td>" . $d2 . $per . "</td><td>" . $difference . $per . "</td></tr><tr><td colspan='4'>";

        //Render the chart
        echo "<script>var data" . $index . "={labels:['" . $key . "'],datasets:[{label:'Our " . $key . "', backgroundColor: 'rgba(220,220,220,0.5)',data:[" . $v1 . "]},{label: 'Your " . $key . "',backgroundColor: 'rgba(151,187,205,0.5)',data:[" . $v2 . "]},{label: 'Difference',backgroundColor: 'rgba(120,117,110,0.5)',data:[" . $chartDiff . "]}]};</script>";
        echo "<div id='canvas-holder" . $index . "' style='width:75%;'><canvas id='chart" . $index . "'/></div>";

        echo "</td></tr>";

        $index+=1;
    }

    $index=0;

    echo "<script>window.onload=function(){";
    foreach($sourceValues as $key=> $value)
    {
          $titleAdd='';

          if($key=="Time on Site" || $key=="Average Page Load Time" || $key=="Mobile Average Session Duration")
              $titleAdd= " (Seconds)";

        echo "var ctx" . $index . " = document.getElementById('chart" . $index . "').getContext('2d');
            window.myHorizontalBar = new Chart(ctx" . $index . ",
                    {type: 'horizontalBar',
                    data: data" . $index . ",
                    options: {
                    elements: {
                        rectangle: {
                            borderWidth: 0,
                            borderColor: 'rgb(0, 255, 0)',
                            borderSkipped: 'left'
                        }
                    },
                    scales: {
                        yAxes: [{barPercentage:0.5}],
                        xAxes: [{barPercentage:0.5}]
                    },
                    maintainAspectRatio:false,
                    responsive: true,
                    legend: {
                        position: 'right',
                    },
                    title: {
                        display: true,
                        text: '" . $key . $titleAdd . "'
                    }
                }
            });";

        $index+=1;
    }

    echo "}</script>";

    echo "</table></div></div></div>";

} else {
    $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/oauth2callback';
    //header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));

    echo "<script>location.href='" . filter_var($redirect_uri, FILTER_SANITIZE_URL) . "';</script>";
}





