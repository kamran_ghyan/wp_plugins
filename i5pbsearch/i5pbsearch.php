<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://i5fusion.com/
 * @since             1.0.0
 * @package           I5pbsearch
 *
 * @wordpress-plugin
 * Plugin Name:       i5pbsearch
 * Plugin URI:        http://i5fusion.com/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Jarrett
 * Author URI:        http://i5fusion.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       i5pbsearch
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-i5pbsearch-activator.php
 */
function activate_i5pbsearch() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-i5pbsearch-activator.php';
	I5pbsearch_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-i5pbsearch-deactivator.php
 */
function deactivate_i5pbsearch() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-i5pbsearch-deactivator.php';
	I5pbsearch_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_i5pbsearch' );
register_deactivation_hook( __FILE__, 'deactivate_i5pbsearch' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-i5pbsearch.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_i5pbsearch() {

	$plugin = new I5pbsearch();
	$plugin->run();

}
run_i5pbsearch();
