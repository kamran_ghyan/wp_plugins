	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	jQuery(document).ready(function () {

		/*
		* Drop down for search input types
		*/

		jQuery(".dropdown input").click(function(e) {
	    	jQuery(".dropdown").find("div.i5DropDown").hide();
	    	jQuery(this).parents(".dropdown").find("div.i5DropDown").show();
	    	e.stopPropagation();    
		});

		jQuery("div.i5DropDown").click(function(e){
			    e.stopPropagation();
		});

		jQuery(document).click(function(){
		    jQuery("div.i5DropDown").hide();
		});

		/*
		* Drop down dropdown list value into search input
		*/   

		jQuery(".dropdown div.i5DropDown input").click(function() {
		    var text = jQuery(this).val();
		      
		    var dd = jQuery(this).parents(".dropdown");
		    dd.find("input.form-control").val(text);
		    dd.find("div.i5DropDown").hide();
		});

		/*
		* Drop down dropdown list value into search input
		*/   

		jQuery("#submit_i5pbsearch").on("click", function(e) {

			var loc = "listing-search/";
			var han = "?";

			// Form input values
			var location 		= jQuery("input[name='location']").val();
			var property_type 	= jQuery("input[name='propertyType']").val();
			var min_price 		= jQuery("input[name='minPrice']").val();
			var max_price 		= jQuery("input[name='maxPrice']").val();
			var min_rooms 		= jQuery("input[name='minBedrooms']").val();
			var min_baths 		= jQuery("input[name='minBathrooms']").val();
			var display 		= jQuery("input[name='display']").val();

			// Build Query String 

			if(property_type && property_type != "") {
	            loc+=encodeURI(property_type) +'/';
	        }
	        else {
	        	loc+="all/";
	        }

	        if(location && location != "") {
	            loc+= han + "searchtype=Zip Code&search=" + location;
	            han="&";
	        }

			if(min_price && min_price != "") {
	            loc+= han + "minprice=" + min_price;
	            han="&";
	        }

	        if(max_price && max_price != "") {
	            loc+= han + "maxprice=" + max_price;
	            han="&";
	        }

	        if(min_rooms && min_rooms != "") {
	            loc+= han + "minbedrooms=" + min_rooms;
	            han="&";
	        }

	        if(min_baths && min_baths != "") {
	            loc+= han + "minfullBaths=" + min_baths;
	            han="&";
	        }
	        if(display && display != "") {
	            loc+= han + "display=" + display;
	            han="&";
	        }

	        // Redirect to search page

	        var url = window.location.href;
	        window.location = url + loc;
	        return false;
		});
	            
	});

	