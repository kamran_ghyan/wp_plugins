var map = null;
var userMarker = null;
var movLis = null;
var boundsLis = null;
var startIndex = 0;
var gmarkers = [];
var infoWindows = [];
var makerIndex = 0;
var MarkerListingHash = new Object();
var lastZip;
var mapTimerId = 0;
var mapTimmerRunning = false;
var zoomLis = null;
var dragLis = null;
var mapDiv;
var lastZoom;
var maxZoom;
var minZoom;
var currLat;
var currLong;
var locs = [];
var reInit=false;
var bounds = new google.maps.LatLngBounds();
var shapes=[];
var showPolygon=true;
var showDetails=true;

function ReInitialize()
{
  //reInit=true;
  //setTimeout(function(){
    //InitializeMap(mapDiv,lastZoom,maxZoom,minZoom,currLat,currLong,showDetails,showPolygon);
    
    //for(var i=0;i<locs.length;i++)
    	//markAddress(locs[i]);
  
    
    
  //},800);
}

function zoomBounds(zoom){
  map.fitBounds(bounds);
  forceZoom(zoom);
}

function centerListing(loc){
  var latlng=new google.maps.LatLng(loc.data.pba__latitude_pb__c,loc.data.pba__longitude_pb__c);
  map.setCenter(latlng);
}

function InitializeMap(mapControl, zoom,maxz,minz, lat,long,details,poly) {
  	mapDiv=mapControl;
  	lastZoom=zoom;
  	maxZoom=maxz;
  	minZoom=minz;
  	currLat=lat;
  	currLong=long;
  	showDetails=details;
  	showPolygon=poly;
  
    map = new google.maps.Map(document.getElementById(mapControl), {
        center: { lat: lat, lng: long },
        zoom: zoom,
      	scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        maxZoom: maxz,
        minZoom: minz
    });

    dragLis = google.maps.event.bind(this.map, 'dragend', this, this.mapMoved);
    movLis = google.maps.event.bind(this.map, 'center_changed', this, this.mapMoved);
    zoomLis = google.maps.event.addListener(map, 'zoom_changed', this.mapZoomed);
  
  	if(showPolygon)
  		map.fitBounds(bounds);
  
  	if(showPolygon){  
    var drawingManager = new google.maps.drawing.DrawingManager({
    drawingMode: null,
    drawingControl: true,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_CENTER,
      drawingModes: [
        google.maps.drawing.OverlayType.POLYGON
      ]
    }
  });
  drawingManager.setMap(map);
  }
  
  google.maps.event.addListener(drawingManager, 'circlecomplete', function (event) {

    		// Get circle center and radius
        //var center = event.getCenter();
        //var radius = event.getRadius();
    
    		//Zoom bounds
    		map.fitBounds(event.getBounds());
        
    		//Remove overlay from map
        event.setMap(null);
        drawingManager.setDrawingMode(null);

        // Create circle
        //createCircle(center, radius);
    });
  google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
       var newShape = event.overlay;
        newShape.type = event.type;
        shapes.push(newShape);
     
     		map.fitBounds(event.overlay.getBounds());
     		var coords = getPolyCoords(event);
      
     		//Add Coords to query
     		var newQ = i5ListingFields+"&lat__c=[" + coords.MinLat + ";" + coords.MaxLat + "]&long__c=[" + coords.MinLong + ";" + coords.MaxLong + "]";
     
     		jQuery("#divLoading").show();
     
     		//Todo Query for listings
     		getListings(newQ,i5ListingRecordTypes,i5ListingsSort,0,200,function(listings){listingsLoaded(listings);});
     		     		
     
        if (drawingManager.getDrawingMode()) {
            drawingManager.setDrawingMode(null);
        }
    });
  google.maps.event.addListener(drawingManager, "drawingmode_changed", function() {
    if (drawingManager.getDrawingMode() != null) {
      for (var i=0; i < shapes.length; i++) {
        shapes[i].setMap(null);
      }
      shapes = [];
    }
});

}

if (!google.maps.Polygon.prototype.getBounds) { 
  google.maps.Polygon.prototype.getBounds=function(){
    var bounds = new google.maps.LatLngBounds()
    this.getPath().forEach(function(element,index){bounds.extend(element)})
    return bounds
  }
}

function listingsLoaded(listings){
  if(listings!=null && listings!=0)
  	locs=listings.listings.listing;
  else
    locs=[];
  
  for (var i = 0; i < gmarkers.length; i++) {
    gmarkers[i].setMap(null);
  }
  
  makerIndex=0;
  gmarkers = [];
	infoWindows = [];
	MarkerListingHash = new Object();
  
  //Clear out listings
  clearListings();
  
  for(var i=0;i<locs.length;i++)
  {
    	//Render the listing
    	renderListing(locs[i]);
    
    	var latlng=new google.maps.LatLng(locs[i].data.pba__latitude_pb__c, locs[i].data.pba__longitude_pb__c);
    
    	if(google.maps.geometry.poly.containsLocation(latlng,shapes[0]))
    		markAddress(locs[i]);
  }
  
  jQuery("#divLoading").hide();
}

function getPolyCoords(event){
  var vertices=event.overlay.getPath();
  var contentString="";
  var minLat;
  var maxLat;
  var minLong;
  var maxLong;
  
  for (var i =0; i < vertices.getLength(); i++) {
    var xy = vertices.getAt(i);
    
    if(minLat==null || minLat>xy.lat())
      minLat=xy.lat();
    
    if(minLong==null || minLong>xy.lng())
      minLong=xy.lng();  
    
    if(maxLat==null || maxLat<xy.lat())
      maxLat=xy.lat();
    
    if(maxLong==null || maxLong<xy.lng())
      maxLong=xy.lng();
    
    contentString += '<br>' + 'Coordinate ' + i + ':<br>' + xy.lat() + ',' +
        xy.lng();
  }
  
  console.log(contentString);
  
  var obj={
    MaxLat:maxLat,
    MinLat:minLat,
    MinLong:minLong,
    MaxLong:maxLong
  };
  
  return obj;
}

function getDistanceToCenter(point2Lat, point2Long) {
    if (userMarker != null
        && point2Lat != null
        && point2Long != null) {
        var centerLatLng = userMarker.position;
        var point2LatLng = new google.maps.LatLng(point2Lat, point2Long);

        var distance = centerLatLng.distanceFrom(point2LatLng); //returns value in meters
        distance = distance + (distance / 7);  //add fudge factor, driving isn't a straight line - compare maps.google.com distances
        distance = distance / 1609.344; //convert miles to meters

        return distance.toFixed(1);
    }
    else
        return 0;

    return centerLatLng.distanceFrom(point2LatLng);
}

function forceZoom(radius) {
    if (map.getZoom()) {
        google.maps.event.removeListener(zoomLis);
        map.setZoom(radius);
        zoomLis = google.maps.event.addListener(map, 'zoom_changed', this.mapZoomed);
    }
}
function mapZoomed() {
    //alert("Map Zoomed zip:" + lastZip);
    if (mapTimmerRunning)
        return;
}

function markAddress(loc) {
    var myIcon="/wp-content/plugins/i5ListingsMap/Resources/mapMarker.png";

    var image = { url: myIcon, size: new google.maps.Size(30, 43) };

  	if(!loc.data)
    {
      loc.data=[];
      loc.data.pba__latitude_pb__c=loc.latitude;
      loc.data.pba__longitude_pb__c=loc.longitude;
      loc.data.pba__address_pb__c=loc.address;
      loc.data.pba__city_pb__c=loc.city;
      loc.data.state__c=loc.state;
      loc.data.pba__postalcode_pb__c=loc.zipCode;
      loc.data.pba__listingtype__c="";
      loc.data.name=loc.title;
      loc.data.search_value__c=loc.url;
    }    
  
    var getDir = "<br/><br/><a href='http://maps.google.com/maps?q=" + loc.data.pba__latitude_pb__c + "," + loc.data.pba__longitude_pb__c + "(" + loc.data.pba__address_pb__c + ' ' + loc.data.pba__city_pb__c + ' ' + loc.data.state__c + ' ' + loc.data.pba__postalcode_pb__c + ")' target='_blank'>Get Directions</a>";

    var img = "/wp-content/uploads/2015/05/noimage.jpg";
    var url;

    if (loc.data.pba__listingtype__c == "UT Area Leasing")
        url = "/ut_austin_rentals/";
    else if (loc.data.pba__listingtype__c == "Lease")
        url = "/austin_rentals/";
    else if(loc.data.pba__listingtype__c == "")
      url="/communties/";  
  	else 
      url = "/austin_homes/";
      	

    url += encodeURIComponent(loc.data.search_value__c);

    if (loc.media!=null && loc.media.images.image!=null && loc.media.images.image.length > 0)
        img = loc.media.images.image[0].url;
    
    var price;
  
  	if(loc.data.pba__listingtype__c=="Lease") 
      price=addCommas(Math.floor(loc.data.list_price__c)) + " Per month";
    else if(loc.data.pba__listingtype__c=="UT Area Leasing")  
      price=addCommas(Math.floor(loc.data.monthly_rent__c)) + " Per month";
  	else if(loc.data.pba__listingtype__c=="")  
      price="Starting At:" + addCommas(Math.floor(loc.data.agentOfficePhont));
    else
      price=addCommas(Math.floor(loc.data.pba__listingprice_pb__c)) + " List Price";
  
  var mainTab;
  
  if(loc.data.pba__listingtype__c=="") {
    mainTab = "<b>" + loc.data.name + "</b><br/><image width='144' height='109' src='" + img + "'/>";
  
  
  mainTab+= "<br/><a href='" + url + "'>View Listing</a>";
  }
  else
  {
  	mainTab = "<b>" + loc.data.name + "</b><br/><image width='144' height='109' src='" + img + "'/><br/><b>Bedrooms:</b> " + loc.data.pba__bedrooms_pb__c + "<br /><b>Bathrooms:</b> " + loc.data.pba__fullbathrooms_pb__c + "<br />";
  
  if(loc.data.pba__listingtype__c!="UT Area Leasing")
   mainTab += "<b>Sq Ft:</b> " + addCommas(Math.floor(loc.data.pba__totalarea_pb__c)) + "<br />";
  
  mainTab+= "<b>Price:</b> $" + price + "<br/><a href='" + url + "'>View Listing</a>";
}

    var infoWindow = new google.maps.InfoWindow({
        content: mainTab
    });

    infoWindows[makerIndex] = infoWindow;

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(loc.data.pba__latitude_pb__c, loc.data.pba__longitude_pb__c),
        map: map,
        icon: image,
        title: loc.Name
    });
  
  	bounds.extend(marker.position);

    MarkerListingHash[loc.MLS_ID__c] = makerIndex; //treat object like an associative array
    gmarkers[makerIndex] = marker;
    makerIndex++;

    //Close info windows
    if(showDetails)
    {
    google.maps.event.addListener(marker, 'click', function () {
        for (i = 0; i < infoWindows.length; i++) {
            infoWindows[i].close();
        }
    });

    google.maps.event.addListener(infoWindow, 'domready', function () {
    });

    google.maps.event.addListener(marker, 'click', function () {
        infoWindow.open(map, marker);
    });
		}
    //google.maps.event.bind(marker, 'click', this, function() { markerClicked(loc.DealerId); });    
}

function addCommas(nStr){
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

//Event Handlers
function startMapTimer() {
    mapTimmerRunning = true;
    mapTimerId = setTimeout('doMove()', 500);
}

function resetMapTimer() {
    clearTimeout(mapTimerId);
    startMapTimer();
}

function mapMoved() {
    if (mapTimmerRunning)
        resetMapTimer();
    else
        startMapTimer();
}
function doMove() {
    mapTimmerRunning = false;
    if (onMapMoved != null)
        onMapMoved.fire('MapMoved');
}

function markerClicked(dealerId) {
    showSelectedDealer(dealerId);
}

//Events
var MapLoaded = function () {
    this.eventName = arguments[0];
    var mEventName = this.eventName;
    var dealers;
    var eventAction = new Array(0);
    this.subscribe = function (fn) {
        if (eventAction.length == 0)
            eventAction[0] = fn;
        else
            eventAction[eventAction.length] = fn;
    };
    this.fire = function (sender, eventArgs) {
        this.eventName = 'eventName2';
        if (eventAction.length > 0) {
            for (i = 0; i < eventAction.length; i++) {
                eventAction[i](sender, eventArgs);
            }
        }
    };
};
var MapMoved = function () {
    this.eventName = arguments[0];
    var mEventName = this.eventName;
    var eventAction = new Array(0);
    this.subscribe = function (fn) {
        if (eventAction.length == 0)
            eventAction[0] = fn;
        else
            eventAction[eventAction.length] = fn;
    };
    this.fire = function (sender, eventArgs) {
        this.eventName = 'eventName2';
        if (eventAction.length > 0) {
            for (i = 0; i < eventAction.length; i++) {
                eventAction[i](sender, eventArgs);
            }
        }
    };
};
var onMapMoved = new MapMoved('MapMovedEventHandler');
var onMapLoaded = new MapLoaded('MapLoadedEventHandler');

//Object to hold Location Settings
function locationSettings() {
    this.lat;
    this.lng;
}

function openInfoWindow(MLSId) {
    for (i = 0; i < infoWindows.length; i++) {
        infoWindows[i].close();
    }

    google.maps.event.trigger(gmarkers[MarkerListingHash[MLSId]], "click");
}







