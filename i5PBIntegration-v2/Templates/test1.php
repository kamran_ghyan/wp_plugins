<style>
    .slick-prev:before, .slick-next:before {
        color: #fff !important;
        font-size: 30px !important;
    }
</style>

<?php
// Fancy box JS and CSS
wp_enqueue_style( 'fancy-box-css', i5PBIntegration__PLUGIN_URL . 'source/jquery.fancybox.css', array(), '1.1', 'all');
wp_enqueue_style( 'fancy-box-thumbs-css', i5PBIntegration__PLUGIN_URL . 'source/helpers/jquery.fancybox-thumbs.css', array(), '1.1', 'all');
wp_enqueue_script( 'fancy-box-js', i5PBIntegration__PLUGIN_URL . 'source/jquery.fancybox.js', array('jquery'), '1.1', true );
wp_enqueue_script( 'fancy-box-thumbs-js', i5PBIntegration__PLUGIN_URL . 'source/helpers/jquery.fancybox-thumbs.js', array('jquery'), '1.1', true );

wp_enqueue_style( 'property-detail3', i5PBIntegration__PLUGIN_URL . 'css/property-detail3.css', array(), '1.1', 'all');
wp_enqueue_style( 'property-search3', i5PBIntegration__PLUGIN_URL . 'css/search-search3.css', array(), '1.1', 'all');
wp_enqueue_style( 'slick-styles', i5PBIntegration__PLUGIN_URL . 'css/slick.css', array(), '1.1', 'all');
wp_enqueue_style( 'slick-theme', i5PBIntegration__PLUGIN_URL . 'css/slick-theme.css', array(), '1.1', 'all');
wp_enqueue_script( 'slick', i5PBIntegration__PLUGIN_URL . 'js/slick.min.js', array('jquery'), '1.1', true );

if(isset($_REQUEST["display"]) && $_REQUEST["display"]=="map")
{
	
    wp_enqueue_style( 'slick-styles', i5PBIntegration__PLUGIN_URL . 'css/slick.css', array(), '1.1', 'all');
    wp_enqueue_style( 'slick-theme', i5PBIntegration__PLUGIN_URL . 'css/slick-theme.css', array(), '1.1', 'all');
    wp_enqueue_script( 'slick', i5PBIntegration__PLUGIN_URL . 'js/slick.min.js', array('jquery'), '1.1', true );
}
$showSave=false;
get_header();  
get_template_part('index_header');

$maxResults=24;
include(i5PBIntegration__PLUGIN_DIR . 'Includes/Prospect-Functions.php');
include(i5PBIntegration__PLUGIN_DIR . 'Includes/PropertyDetail-Functions.php');
include(i5PBIntegration__PLUGIN_DIR . 'Includes/PropertySearch-Functions.php');
include(i5PBIntegration__PLUGIN_DIR . 'Includes/Global-Functions.php');
/*
Template Name: Property Detail Theme 3
 */

?>
<div class="clearfix"></div>
	<!-- Start inner page -->
	<div class="inner-page searchResultsPage">
		<div class="filters test">
			<?php include(i5PBIntegration__PLUGIN_DIR . 'Includes/HeaderSearch3.php'); ?>
		</div>
	</div>
<div class="fullWidth">
<div class="printContainer">
    <div id="detailsHeader" class="detailsHeader">
        <div class="container fullWidth">
            <div class="buttonBack printHide">
                <a href="<?php echo $prevSearch?>">
                    <i class="fa fa-angle-left"></i> 
                </a>
            </div>
            <div class="detailsAddress div1-1">
            	<div class="container">
            	<div class="div1-4">
            		<!-- <div class="statusUnderContract"></div> Need to show under contract status -->
                    <!-- <div class="statusSold"></div> Need to show sold status -->
                    <div class="statusActive"></div><?php echo $property->status ?><br />
                    <div class="detailsPrice">
					   <?php 
                            if($property->custom4 == 1){
                                echo "$" . number_format($property->custom5,0) . " /Month";
                            }else{
                                echo "$" . number_format($property->price,0);
                            }
                            
                       ?>
                    </div>
            	</div>
            	
            	<div class="div3-4">
            		 <div class="detailsAddress">
                     	<h1 id="property-form-address" itemprop="address" itemtype="http://schema.org/PostalAddress">
                     		<?php echo $property->address . ($property->unitNumber==""?"":" " . $property->unitNumber) ?>
                     	</h1>
                     </div>
                     <div class="detailsCity">
                      	<i class="fa fa-map-marker"></i> <?php echo $property->city ?>, <?php echo $property->state ?> <?php echo $property->zipCode ?>
                     </div>
            	</div>
                <!-- <h1>
                    <?php echo $property->address . ($property->unitNumber==""?"":" " . $property->unitNumber) ?>
                </h1>
                <div class="address2">
                    <?php echo $property->city ?>, <?php echo $property->state ?> <?php echo $property->zipCode ?>
                </div>
                <div class="addressDetails">
                    <div class="addressListingStatus">
                        <div class="new">
                            <?php echo $property->status ?>
                        </div>
                    </div>
                    <div class="addressPrice">
                        PRICE:
                        <span class="success">
                            <?php echo "$" . number_format($property->price,0) . ($result->propertytype=="Rental" || $result->recordType=="Rent"?" /Month":"") ?>
                        </span>
                    </div>
                    <div class="addressStatus">
                        STATUS:
                        <b>
                            <?php echo $property->status ?>
                        </b>
                    </div>
                    <div class="addressUpdated">
                        UPDATED:
                        <b>
                            <?php
                            $settings = get_option('WebListing_Settings');
                            $lastRun = strtotime(date("Y-m-d\Th:i:s"));
                            $currTime = $lastRun;

                            if(isset($settings["LastRun"]))
                                $lastRun = strtotime($settings["LastRun"]);


                            echo round(abs($currTime - $lastRun) / 60,0) . " MIN AGO";
                            ?>
                        </b>
                    </div>
                    <div class="addressID">
                        ID#:
                        <b>
                            <?php echo $property->mlsid ?>
                        </b>
                    </div> 
-->
                </div>
            </div>
            <!-- end detailsAddress -->
           <!-- <div class="div1-3 printHide">
                <div id="fav<?php echo $property->mlsid?>" class="<?php echo ($favs=='' || !in_array($property->mlsid,$favs)?'detailsFavorite':'detailsFavoriteSelected')?>">
                    <a href="javascript:toggleFav('<?php echo $property->mlsid ?>');setFavorite('<?php echo $property->mlsid ?>',<?php echo ($favs=='' || !in_array($property->mlsid,$favs)?'1':'0')?>);">
                        <i class="fa fa-heart"></i>
                    </a>
                </div>
                <div class="detailsShowing">
                    <a class="button" href="#showingForm">Request Showing</a>
                </div>
                <div class="detailsButtons">
                    <div class="btnShowMap">
                        <a href="#map" id="btnMap">Map</a>
                    </div>
                    
                    <div onclick="printListing();" class="btnPrint">Print</div>
                        
                    <div onclick="showSharing()" class="btnShare dropbtn">Share</div>

                    <div id="ddShare" class="ddShare dropdownContent">
                        <div class="padding10">
                            <?php echo do_shortcode( '[woocommerce_social_media_share_buttons]' ); ?>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
        <!-- end contain -->
    </div>
    <!-- end detailsHeader -->
    <div class="detailsBody">
        <div class="container">
            <div class="div2-3 propertyHolder">
            	<button class='lightbox'><i class="fa fa-expand"></i></button>
                <div class="propertyImages">
                
                    <?php
                    if(sizeof($images)==0){
                        echo " <div><img src='" . i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png' /></div>";
                    }
                    else{
                        $index=0;

                        foreach($images as $image){
                            if($index==0)
                                echo " <div><a class='fancybox-thumbs' data-fancybox-group='thumb' href='" . $image->url . "'><img onerror='fixImage(this);' src='" . $image->url . "' /></a></div>";
                            else
                                echo " <div><a class='fancybox-thumbs' data-fancybox-group='thumb' href='" . $image->url . "'><img style='display:none;' onerror='fixImage(this);' src='" . $image->url . "' /></a></div>";

                            $index+=1;
                        }
                    }
                    ?>
                </div>
                <div class="propertyDetails">
                	<div class="propertyType">
                		<?php echo $property->propertytype?>
                	</div>
                	<div class="detailsMapLink">
	                	
	                	<div class="detailsBeds">
	                        <?php 
								if($property->latitude == 0 || $property->longitude == 0){ }
								else { 
							?>
                            <div class="detailsValue">
	                           <a href="#map"><i class="fa fa-map-marker"></i></a>
	                        </div>
	                        <div class="detailsLabel">
                            	
	                            <a href="#map">view map</a>
                                
	                        </div>
                            <?php } ?>
	                    </div>
                	</div>
                	<i class="fa fa-bed"></i>
                	<div class="detailsBeds">
                        <div class="detailsValue">
                            <?php echo number_format($property->bedrooms,0) ?>
                        </div>
                        <div class="detailsLabel">
                            Beds
                        </div>
                    </div>
                    <img src="/wp-content/uploads/2016/10/bathroom-Teal.png" style="width:40px;" alt="baths" />
                    <div class="detailsBaths">
                        <div class="detailsValue">
                            <?php echo number_format($property->fullBaths,0) ?>
                        </div>
                        <div class="detailsLabel">
                            Baths
                        </div>
                    </div>
                    <i class="fa fa-map-o"></i>
                    <div class="detailsSqft">
                        <div class="detailsValue">
                            <?php echo number_format($property->size,0) ?>
                        </div>
                        <div class="detailsLabel">
                            sqft
                        </div>
                    </div>
                    <i class="fa fa-photo"></i>
                    <div class="detailsSqft">
                        <div class="detailsValue">
                           <?php  
						   	echo $images_cnt->c;
						   ?>
                        </div>
                        <div class="detailsLabel">
                            images
                        </div>
                    </div>
                </div>
            </div>
            <!-- end div 2-3 -->
            <div class="div1-3">
            	<div class="detailsFormHolder">
            		<h3>Contact an agent about this property.</h3>
            		<div class="detailsForm">
            			<?php
							if( $property->custom4 == 1 ) { 
								echo do_shortcode( '[contact-form-7 id="6281" title="Schedule a Showing"]' );
							} 
							else{ 
								echo do_shortcode( '[contact-form-7 id="6202" title="Schedule a Showing"]' ); 
							} 
                        	
							
                        ?>
            		</div>
            	</div>
                <!-- <div class="detailsBox">
                    <div class="detailsPrice">
                        <?php echo "$" . number_format($property->price,0) . ($property->propertytype=="Rental"?" /Month":"") ?>
                    </div>
                    
                    <div class="detailsAcres">
                        <div class="detailsValue">
                            <?php echo number_format($property->lotsize,0) ?>
                        </div>
                        <div class="detailsLabel">
                            Acres
                        </div>
                    </div>
                    
                    <div class="details1-2Baths">
                        <div class="detailsValue">
                            <?php echo number_format($property->halfBaths,0) ?>
                        </div>
                        <div class="detailsLabel">
                            1/2 Baths
                        </div>
                    </div>
                    
                    <div class="detailsPriceSqft">
                        <div class="detailsValue">
                            <?php
                            if(isset($property->size))
                                echo "$" . number_format($property->price/ $property->size,0)
                            ?>
                        </div>
                        <div class="detailsLabel">
                            $/SQFT
                        </div>
                    </div>
                    <div class="detailsSpecs">
                        <?php if(isset($property->subdivision) && $property->subdivision!=""):?>
                        <div class="specsLabel">
                            Neighborhood:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->subdivision) && $property->subdivision!=""?$property->subdivision:"&nbsp;")?>
                        </div>
                        <?php endif;?>
                        <div class="specsLabel">
                            Type:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->propertytype) && $property->propertytype!=""?$property->propertytype:"&nbsp;")?>
                        </div>
                        <div class="specsLabel">
                            Built:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->yearbuilt) && $property->yearbuilt!=""?$property->yearbuilt:"&nbsp;")?>
                        </div>
                        <div class="specsLabel">
                            Area:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->area) && $property->area!=""?$property->area:"&nbsp;")?>
                        </div>
                    </div>
                </div>
                
                <div class="detailsBox">
                    <?php if((isset($property->highSchool) && $property->highSchool!="") or (isset($property->elementarySchool) && $property->elementarySchool!="") or (isset($property->middleSchool) && $property->middleSchool!="")):?>
                    <div class="headerSchoolInfo">
                        School Information
                    </div>
                    <div class="detailsSpecs">
                    <?php endif;?>
                        <?php if(isset($property->elementarySchool) && $property->elementarySchool!=""):?>
                        <div class="specsLabel">
                            Elementary School:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->elementarySchool) && $property->elementarySchool!=""?$property->elementarySchool:"&nbsp;")?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->middleSchool) && $property->middleSchool!=""):?>
                        <div class="specsLabel">
                            Middle School:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->middleSchool) && $property->middleSchool!=""?$property->middleSchool:"&nbsp;")?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->highSchool) && $property->highSchool!=""):?>
                        <div class="specsLabel">
                            High School:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->highSchool) && $property->highSchool!=""?$property->highSchool:"&nbsp;")?>
                        </div>
                        <?php endif;?>
                        <?php if((isset($property->highSchool) && $property->highSchool!="") or (isset($property->elementarySchool) && $property->elementarySchool!="") or (isset($property->middleSchool) && $property->middleSchool!="")):?>
                        <div class="specsLabel"></div>
                        <?php endif;?>
                    </div>
                </div>
                 end detailsBox -->
            </div>
            <!-- end div1-3 -->
            <div class="detailsDescriptionHolder">
                <div class="div2-3">
                    <div class="detailsDescriptionHeader">
                        Description
                    </div>
                    <p>
                        <?php echo $property->description?>
                    </p>
                    <div class="detailsDescriptionHeader">
                        Details
                    </div>
                    <div class="div1-2">
                    	<p>
                    		<?php 
                            if($property->custom4 == 1){
                                echo 'For Rent:';
                                echo "$" . number_format($property->custom5,0) . " /Month";
                            }else{
                                echo 'For Sale:';
                                echo "$" . number_format($property->price,0);
                            }
							?> <br />
	                    	Assessments: <?php echo $property->custom1?><br />
	                    	Built in: <?php echo (isset($property->yearbuilt) && $property->yearbuilt!=""?$property->yearbuilt:"&nbsp;")?><br />
	                    	Cooling air: <?php echo $property->cooling?><br />
	                    	Property Type: <?php echo $property->propertytype?><br />
	                    	Pets Allowed: <?php echo $property->custom2?><br />
	                    	Taxes: <?php echo $property->taxes?>
                    	</p>
                    </div>
                    <div class="div1-2">
                    	<p>
	                    	Location: <?php echo (isset($property->area) && $property->area!=""?$property->area:"&nbsp;")?><br />
	                    	Listed for: <?php 
								 $current_date = date('Y-m-d');
								 $since_date  = $property->custom3;
								 
								$todate= strtotime($current_date);
								$fromdate= strtotime($since_date);
								$calculate_seconds = $todate- $fromdate; // Number of seconds between the two dates
								$days = floor($calculate_seconds / (24 * 60 * 60 )); // convert to days
								echo($days .' Days');
								 ?><br />
	                    	Heating: <?php echo $property->heating?><br />
	                    	MLS #: <?php echo $property->mlsid ?><br />
	                    	Beds: <?php echo number_format($property->bedrooms,0) ?><br />
	                    	Baths: <?php echo number_format($property->fullBaths,0) ?>
	                    </p>	
                    </div>
                    <div class="detailsDescriptionHeader">
                        Listing Provided by
                    </div>
                    <div class="div1-1">
                    	<?php if(isset($property->listingOffice) && $property->listingOffice!=""):?>
			            <h3 class="listingAgent">
			            	<?php echo $property->listingAgentFirstName; ?>
			            </h3>
			            <h4 class="listingOffice">
			                <?php echo $property->listingOffice; ?>
			            </h4>
			            <?php endif;?>
                    </div>
                    <div class="detailsDescriptionHeader">
                        Similar Listings
                    </div>
                    <div class="div1-1">
                    	<?php include(i5PBIntegration__PLUGIN_DIR . 'Includes/RelatedProperties2.php'); ?>
                    </div>
                    <div>
                        <p>Copyright <?php echo date("Y") ?> Midwest Real Estate Data LLC. All rights reserved. The data relating to real estate for sale on this web site comes in part from the Broker Reciprocity Program of the Midwest Real Estate Data LLC. Listing information is deemed reliable but not guaranteed.</p>
                      </div>
                </div>
                <!-- end div2-3 -->
                <div class="div1-3">
                    <!-- <div class="agentTile">
                        <?php
                        $agent;
                        if(post_type_exists("i5agents")){
                            global $wpdb;
                            $sql="select p.id from {$wpdb->prefix}postmeta pm join {$wpdb->prefix}posts p on p.id=pm.post_id where meta_key='domain' and p.post_status='publish' and p.post_type='i5agents' and pm.meta_value='" . $_SERVER['HTTP_HOST'] . "'";
                            $agent=$wpdb->get_row($sql);
                        }

                        if($agent!=null){
                            echo do_shortcode("[i5AgentData show_social_links='false' office_phone_label='O: ' mobile_phone_label='M: ']");
                        } else{?>
                        <div class="div1-3">
                            <div class="padding10">
                                
                            </div>
                        </div>
                        
                        <div class="div2-3">
                            <div class="agentTitle">
                                <?php echo esc_attr( get_post_meta( get_the_ID(), 'companyname', true ) ); ?>
                            </div>
                            <div class="agentPhone">
                                <a href="tel:<?php echo esc_attr( get_post_meta( get_the_ID(), 'companyphone', true ) ); ?>">
                                    <span class="red">O:</span>
                                    <?php echo esc_attr( get_post_meta( get_the_ID(), 'companyphone', true ) ); ?>
                                </a>
                            </div>
                        </div>
                        
                        <?php }?>
                    </div>-->
                    <!-- end agentTile -->
                </div>
                <!-- end div1-3 -->
            </div>
            <!-- <div class="featuresHolder">
                <div class="div1-3 marginRight">
                    <div class="featuresHeader">
                        Exterior Features
                    </div>
                    <div class="featuresBox">
                        <?php if(isset($property->exteriorFeatures) && $property->exteriorFeatures!=""):?>
                        <div class="detailsFeaturesLabel">
                            Exterior Features
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->exteriorFeatures?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->exteriorSiding) && $property->exteriorSiding!=""):?>
                        <div class="detailsFeaturesLabel">
                            Exterior Siding
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->exteriorSiding?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->fenceDescription) && $property->fenceDescription!=""):?>
                        <div class="detailsFeaturesLabel">
                            Fence Description
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->fenceDescription?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->hasFence) && $property->hasFence!=""):?>
                        <div class="detailsFeaturesLabel">
                            Fence
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->hasFence?"Yes":"No"?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->foundationType) && $property->foundationType!=""):?>
                        <div class="detailsFeaturesLabel">
                            Foundation Type
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->foundationType?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->parkingDescription) && $property->parkingDescription!=""):?>
                        <div class="detailsFeaturesLabel">
                            Parking
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->parkingDescription?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->pool) && $property->pool!=""):?>
                        <div class="detailsFeaturesLabel">
                            Pool
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->pool?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->roof) && $property->roof!=""):?>
                        <div class="detailsFeaturesLabel">
                            Roof
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->roof?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->style) && $property->style!=""):?>
                        <div class="detailsFeaturesLabel">
                            Style
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->style?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->waterfrontDescription) && $property->waterfrontDescription!=""):?>
                        <div class="detailsFeaturesLabel">
                            Waterfront Description
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->waterfrontDescription?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->waterfront) && $property->waterfront!=""):?>
                        <div class="detailsFeaturesLabel">
                            Waterfront
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php
                                  if($property->waterfront=="1")
                                      echo "Yes";
                                  else if($property->waterfront=="0")
                                      echo "No";
                                  else
                                    echo $property->waterfront
                            ?>
                        </div>
                        <?php endif;?>
                    </div>
                   
                </div>
                
                <div class="div1-3 marginRight">
                    <div class="featuresHeader">
                        Interior Features
                    </div>
                    <div class="featuresBox">
                        <?php if(isset($property->appliances) && $property->appliances!=""):?>
                        <div class="detailsFeaturesLabel">
                            Appliances
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo str_replace(",","<br/>",$property->appliances)?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->cooling) && $property->cooling!=""):?>
                        <div class="detailsFeaturesLabel">
                            Cooling
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->cooling?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->equipment) && $property->equipment!=""):?>
                        <div class="detailsFeaturesLabel">
                            Equipment
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->equipment?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->numberOfFireplaces) && $property->numberOfFireplaces!=""):?>
                        <div class="detailsFeaturesLabel">
                            Fireplaces Number
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->numberOfFireplaces?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->flooring) && $property->flooring!=""):?>
                        <div class="detailsFeaturesLabel">
                            Flooring
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->flooring?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->heating) && $property->heating!=""):?>
                        <div class="detailsFeaturesLabel">
                            Heating
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->heating?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->interiorFeatures) && $property->interiorFeatures!=""):?>
                        <div class="detailsFeaturesLabel">
                            Interior Features
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->interiorFeatures?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->numberOfRooms) && $property->numberOfRooms!=""):?>
                        <div class="detailsFeaturesLabel">
                            Number Of Rooms
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->numberOfRooms?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->otherRooms) && $property->otherRooms!=""):?>
                        <div class="detailsFeaturesLabel">
                            Other Rooms
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo str_replace(",","<br/>",$property->otherRooms)?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->waterHeaterType) && $property->waterHeaterType!=""):?>
                        <div class="detailsFeaturesLabel">
                            Water Heater Type
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->waterHeaterType?>
                        </div>
                        <?php endif;?>
                    </div>
                    
                </div>
                
                <div class="div1-3">
                    <div class="featuresHeader">
                        Property Features
                    </div>
                    <div class="featuresBox">
                        <?php if(isset($property->custom1) && $property->custom1!=""):?>
                        <div class="detailsFeaturesLabel">
                            Aicuz Crash
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->custom1?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->custom2) && $property->custom2!=""):?>
                        <div class="detailsFeaturesLabel">
                            Aicuz Noise
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->custom2?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->disclosure) && $property->disclosure!=""):?>
                        <div class="detailsFeaturesLabel">
                            Disclosure
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->disclosure?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->custom3) && $property->custom3!=""):?>
                        <div class="detailsFeaturesLabel">
                            Fees POA Monthly
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->custom3?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->custom4) && $property->custom4!=""):?>
                        <div class="detailsFeaturesLabel">
                            Master Model
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->custom4?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->subdivision) && $property->subdivision!=""):?>
                        <div class="detailsFeaturesLabel">
                            Neighborhood Name
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->subdivision?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->newConstruction) && $property->newConstruction!=""):?>
                        <div class="detailsFeaturesLabel">
                            New Construction
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->newConstruction?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->ownershipType) && $property->ownershipType!=""):?>
                        <div class="detailsFeaturesLabel">
                            Ownership Type
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->ownershipType?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->custom5) && $property->custom5!=""):?>
                        <div class="detailsFeaturesLabel">
                            Residential Type
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->custom5?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->sewerType) && $property->sewerType!=""):?>
                        <div class="detailsFeaturesLabel">
                            Sewer Type
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->sewerType?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->taxes) && $property->taxes!=""):?>
                        <div class="detailsFeaturesLabel">
                            Tax Amount Approx
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->taxes?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->waterType) && $property->waterType!=""):?>
                        <div class="detailsFeaturesLabel">
                            Water Type
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->waterType?>
                        </div>
                        <?php endif;?>
                        <?php if(isset($property->zoning) && $property->zoning!=""):?>
                        <div class="detailsFeaturesLabel">
                            Zoning
                        </div>
                        <div class="detailsFeaturesValue">
                            <?php echo $property->zoning?>
                        </div>
                        <?php endif;?>
                    </div>

                </div>
                
            </div>-->
            
            <!-- end featuresHolder -->
            <div class="detailsMap">
                <a id="map"></a>
                <p style="text-align:center;margin:0;">
                    <?php
					if($property->latitude == 0 || $property->longitude == 0){
					}
					else {
                    do_shortcode('[i5ListingMap maxzoom="100" minzoom="1" style="display:block;width: 100%; height: 418px" showdetails="false" showpolygon="false" zoom="15" lat="' . $property->latitude . '" long="' . $property->longitude . '" ]');
                    echo "<script>markAddress(" . json_encode($property) . ");</script>";
					}
                    ?>
                </p>
            </div>
            <!--<div class="detailsBox">
                <div class="padding10">
                    <a id="showingForm"></a>
                    <div class="col1-4">
                        <div class="agentTitle">
                            <?php echo esc_attr( get_post_meta( get_the_ID(), 'companyname', true ) ); ?>
                        </div>
                        <div class="agentPhone">
                            <a href="tel:<?php echo esc_attr( get_post_meta( get_the_ID(), 'companyphone', true ) ); ?>">
                                <span class="red">O:</span>
                                <?php echo esc_attr( get_post_meta( get_the_ID(), 'companyphone', true ) ); ?>
                            </a>
                        </div>
                    </div>
                    <div class="col1-2">
                        <div class="padding10">
                            <?php
                            echo do_shortcode( '[contact-form-7 id="6202" title="Schedule a Showing"]' );
                            ?>
                        </div>
                    </div>
                    <div class="col1-4">
                        <?php echo htmlspecialchars_decode(get_post_meta( get_the_ID(), 'companylogo', true )); ?>
                    </div>
                </div>
                  
            </div>-->
            <!-- end detailsBox -->
            
            <div class="Disclaimer">
                <?php echo htmlspecialchars_decode(get_post_meta( get_the_ID(), 'disclaimer', true )); ?>
            </div>
        </div>
        <!-- end container -->
    </div>
</div>
<!-- end detailsBody -->


<script>
    jQuery(function () {
        jQuery(".propertyImages").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true
        });

        jQuery("img").show();
    });
    function toggleFav(id)
    {
        if (jQuery("#fav" + id).hasClass("detailsFavoriteSelected"))
        {
            jQuery("#fav" + id).removeClass("detailsFavoriteSelected");
            jQuery("#fav" + id).addClass("detailsFavorite");
            jQuery(jQuery("#fav" + id).children()[0]).prop("href","javascript:toggleFav('" + id + "');setFavorite('" + id + "',1);");
        }
        else {
            jQuery("#fav" + id).addClass("detailsFavoriteSelected");
            jQuery("#fav" + id).removeClass("detailsFavorite");
            jQuery(jQuery("#fav" + id).children()[0]).prop("href", "javascript:toggleFav('" + id + "');setFavorite('" + id + "',0);");
        }

    }
    function printListing()
    {
        jQuery(".printHide").hide();
        jQuery(".printContainer").printElement();
        jQuery(".printHide").show();
    }
  function showSharing() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddShare").classList.toggle("show");
  }

  // Close the dropdown if the user clicks outside of it
  window.onclick = function(event) {
  if (!event.target.matches('.dropBtn')) {

  var dropdowns = document.getElementsByClassName("dropdownContent");
  var i;
  for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

</script>
<script>
       /* jQuery(function() {

            var jQuerydetailsHeader   = jQuery("#detailsHeader"),
            jQuerywindow    = jQuery(window),
                offset     = jQuerydetailsHeader.offset(),
                topPadding = 15;

            jQuerywindow.scroll(function() {
                if (jQuerywindow.scrollTop() > offset.top) {
                	jQuerydetailsHeader.stop().animate({
                        marginTop: jQuerywindow.scrollTop() - offset.top + topPadding
                    });
                } else {
                	jQuerydetailsHeader.stop().animate({
                        marginTop: 0
                    });
                }
            });

        });*/
</script>
<script type="text/javascript">
		jQuery(document).ready(function() {
		// hidden address for form
	var property_address = jQuery('#property-form-address').text();
	jQuery('#hdnAddress').attr('value', property_address.trim());
			/*
			 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			 */

			jQuery('.fancybox-thumbs').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : true,
				arrows    : true,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					},
					overlay : {
						css : {
							'background' : 'rgba(238,238,238,0.85)'
						}
					}
				}
			});
			
			jQuery('.lightbox').click(function(e) {
			   e.preventDefault();
				jQuery('.fancybox-thumbs:eq(0)').click();
			});

		});
	</script>
<?php  //if ( !is_user_logged_in() ) {} ?>

    <script type="text/javascript">
        /*jQuery(document).ready(function(){
          doRegister();
           jQuery("#register").css('display','block');
        });*/ 
    </script>
<?php
get_footer();
include(i5PBIntegration__PLUGIN_DIR . 'Includes/Footer-Disclaimer.php');
?>


