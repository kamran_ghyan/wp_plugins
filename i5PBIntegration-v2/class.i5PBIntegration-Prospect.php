<?php

class i5PBIntegration_Prospect
{
    private static $initiated = false;
    private static $settings;

    public static function init(){
        if(!self::$initiated){
            self::$settings=get_option('WebListing_Settings');
            self::init_hooks();
            self::$initiated=true;
        }

        $adminUrl;

        if(post_type_exists("i5agents"))
            $adminUrl="http://" . $_SERVER["HTTP_HOST"] . "/wp-admin/admin-ajax.php";
        else
            $adminUrl=admin_url( 'admin-ajax.php' );

        wp_register_script( "Prospect_Script", WP_PLUGIN_URL.'/i5PBIntegration/js/i5PBprospect.js', array('jquery') );
        wp_localize_script( 'Prospect_Script', 'i5PBAjax', array( 'ajaxurl' => $adminUrl,'nonce' => wp_create_nonce('i5PBIntegration')));

        wp_enqueue_script( 'Prospect_Script' );
    }

    public static function init_hooks(){
        add_action('wp_ajax_saveProspect',array('i5PBIntegration_Prospect', 'SaveProspect'));
        add_action('wp_ajax_saveFavorite',array('i5PBIntegration_Prospect', 'SaveFavorite'));
        add_action('wp_ajax_nopriv_saveFavorite',array('i5PBIntegration_Prospect', 'SaveFavorite'));

        if(isset(self::$settings["Prospect"]) && self::$settings["Prospect"])
        {
            //Hide Admin Bar From non Admins
            if(!current_user_can('administrator'))
                add_filter('show_admin_bar', '__return_false');

            if(!is_admin())
                add_filter('send_password_change_email', '__return_false');

            add_action('user_register',array( 'i5PBIntegration_Prospect', 'ProcessProspect' ));
            add_action('wpmu_new_user',array( 'i5PBIntegration_Prospect', 'ProcessProspect' ));
            add_action('wpcf7_before_send_mail',array( 'i5PBIntegration_Prospect', 'ProcessFormSave' ));
        }
    }
    public static function SaveProspect(){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], "i5PBIntegration")) {
            exit("Denied");
        }

        self::ProcessProspect(0);
    }
    public static function SaveFavorite(){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], "i5PBIntegration")) {
            exit("Denied");
        }

        self::processFavorite($_REQUEST["FavMLSID"],$_REQUEST["FavOn"]);

        die();
    }
    public static function processFavorite($mlsid,$on){
        if(isset(self::$settings) && isset(self::$settings["OAuth"]) && isset(self::$settings["OAuth"]["OAuthToken"]))
        {
            require_once( i5PBIntegration__PLUGIN_DIR . 'class.i5PBIntegration-Functions.php' );

            $userID = get_current_user_id();
            $favs=get_user_meta($userID,'Favorites',true);
            $pbId=get_user_meta($userID,'PBID',true);

            if(!isset($pbId))
                return;

            if($on==0)
            {
                if(in_array($mlsid,$favs))
                {
                    $newFavs=array();

                    foreach($favs as $fav)
                    {
                        if($fav!=$mlsid && $fav!="")
                            array_push($newFavs,$fav);
                    }

                    update_user_meta($userID,'Favorites',$newFavs);

                    //Delete the favorite from PB
                    $query="SELECT id from pba__favorite__c f where pba__Contact__c='" . $pbId . "' and f.pba__listing__r." . self::$settings["SelectFields"]["PBSMLSID"] . "='" . $mlsid . "'";
                    $methodUrl="/services/data/v36.0/query/?q=" . urlencode($query);

                    $results=i5PBIntegration_Functions::GetAPI(self::$settings["OAuth"],$methodUrl);

                    if(isset($results->totalSize) && $results->totalSize>0)
                    {
                        $methodUrl="/services/data/v36.0/sobjects/pba__favorite__c/" . $results->records[0]->Id;
                        $delResult=i5PBIntegration_Functions::DeleteAPI(self::$settings["OAuth"],$methodUrl);
                    }
                }
            }
            else
            {
                //Check to see if the user has the favoite
                if(!in_array($mlsid,$favs))
                {
                    $query="SELECT id from pba__favorite__c f where pba__Contact__c='" . $pbId . "' and f.pba__listing__r." . self::$settings["SelectFields"]["PBSMLSID"] . "='" . $mlsid . "'";
                    $methodUrl="/services/data/v36.0/query/?q=" . urlencode($query);

                    $results=i5PBIntegration_Functions::GetAPI(self::$settings["OAuth"],$methodUrl);

                    if(isset($results->totalSize) && $results->totalSize==0)
                    {
                        //Get the listing id
                        $query= "SELECT Id from pba__listing__c where " . self::$settings["SelectFields"]["PBSMLSID"] . "='" . $mlsid . "'";
                        $methodUrl="/services/data/v36.0/query/?q=" . urlencode($query);

                        $results=i5PBIntegration_Functions::GetAPI(self::$settings["OAuth"],$methodUrl);

                        if(isset($results->totalSize) && $results->totalSize!=0)
                        {
                            //Save a request
                            $request=array();
                            $request["pba__Contact__c"]=$pbId;

                            $reqResult=i5PBIntegration_Functions::PostAPI(self::$settings["OAuth"],"/services/data/v36.0/sobjects/pba__request__c",$request);

                            //attempt to save the favorite
                            $favorite=array();
                            $favorite["pba__Contact__c"]= $pbId;
                            $favorite["pba__Listing__c"]=$results->records[0]->Id;
                            $favorite["pba__Request__c"]=$reqResult->id;
                            //$favorite["pba__Request__r"] = array();
                            //$favorite["pba__Request__r"]["pba__Contact__c"]=$pbId;

                            $results=i5PBIntegration_Functions::PostAPI(self::$settings["OAuth"],"/services/data/v36.0/sobjects/pba__favorite__c",$favorite);
                        }
                    }

                    if(!isset($favs) || $favs==""){
                        $favs=array();
                    }

                    if(!in_array($mlsid,$favs))
                    {
                        array_push($favs,$mlsid);

                        update_user_meta($userID,'Favorites',$favs);
                    }
                }
            }
        }
    }
    public static function ProcessProspect($newId){
        if(!is_admin() && isset(self::$settings)){
            require_once( i5PBIntegration__PLUGIN_DIR . 'class.i5PBIntegration-Functions.php' );

            $userID = get_current_user_id();
            $pbId = get_user_meta($userID,'PBID',true);

            $contact = array();

            if($newId!=0)
            {
                $contact["pba__SystemExternalId__c"] = "WordPress" . $newId;
                $contact["pba__SystemIsIndividual__c"]=true;
                $userID=$newId;

                if(isset(self::$settings["ProspectMappings"]["FirstName"]))
                    $contact["FirstName"] = $_REQUEST[self::$settings["ProspectMappings"]["FirstName"]];

                if(isset(self::$settings["ProspectMappings"]["LastName"]))
                    $contact["LastName"] = $_REQUEST[self::$settings["ProspectMappings"]["LastName"]];

                if(isset(self::$settings["ProspectMappings"]["Email"]))
                    $contact["Email"] = $_REQUEST[self::$settings["ProspectMappings"]["Email"]];

                if(isset(self::$settings["ProspectMappings"]["Phone"]))
                    $contact["Phone"] = $_REQUEST[self::$settings["ProspectMappings"]["Phone"]];

                if(isset(self::$settings["ProspectMappings"]["MapCust1"]))
                    $contact[self::$settings["ProspectMappings"]["MapCust1"][0]] = $_REQUEST[self::$settings["ProspectMappings"]["MapCust1"][1]];

                if(isset(self::$settings["ProspectMappings"]["MapCust2"]))
                    $contact[self::$settings["ProspectMappings"]["MapCust2"][0]] = $_REQUEST[self::$settings["ProspectMappings"]["MapCust2"][1]];

                if(isset(self::$settings["ProspectMappings"]["MapCust3"]))
                    $contact[self::$settings["ProspectMappings"]["MapCust3"][0]] = $_REQUEST[self::$settings["ProspectMappings"]["MapCust3"][1]];

                if(isset(self::$settings["ProspectMappings"]["MapCust4"]))
                    $contact[self::$settings["ProspectMappings"]["MapCust4"][0]] = $_REQUEST[self::$settings["ProspectMappings"]["MapCust4"][1]];

                if(isset(self::$settings["ProspectMappings"]["MapCust5"]))
                    $contact[self::$settings["ProspectMappings"]["MapCust5"][0]] = $_REQUEST[self::$settings["ProspectMappings"]["MapCust5"][1]];

                if(isset($_REQUEST["redirect_to"]) && $_REQUEST["redirect_to"]!="")
                    $redirect=$_REQUEST["redirect_to"];
                else
                    $redirect="/";
            }
            else
            {
                if($userID)
                    $contact["pba__SystemExternalId__c"] = "WordPress" . $userID;

                if(isset($_REQUEST["FirstName"]) && $_REQUEST["FirstName"]!="")
                    $contact["FirstName"] = $_REQUEST["FirstName"];

                if(isset($_REQUEST["LastName"]) && $_REQUEST["LastName"]!="")
                    $contact["LastName"]=$_REQUEST["LastName"];

                if(isset($_REQUEST["Email"]) && $_REQUEST["Email"]!="")
                    $contact["Email"]=$_REQUEST["Email"];

                if(isset($_REQUEST["Phone"]) && $_REQUEST["Phone"]!="")
                    $contact["Phone"]=$_REQUEST["Phone"];
            }

            $contact["LeadSource"] 	= "cloudrealty.com";
			$contact["stage__c"] 	= "Lead";

            $methodUrl="/services/data/v31.0/sobjects/contact";

            if(isset($pbId))
                $methodUrl.="/" . $pbId;

            //TODO:Check for duplicate

            $results=i5PBIntegration_Functions::PostAPI(self::$settings["OAuth"],$methodUrl,$contact);

            if($results->success && $newId!=0)
            {
                update_user_meta($newId,"PBID",$results->id);
            }

            //Log in the user
            if(!is_admin()){
                $secure_cookie = is_ssl();
                $secure_cookie = apply_filters('secure_signon_cookie', $secure_cookie, array());
                global $auth_secure_cookie;
                $auth_secure_cookie = $secure_cookie;

                wp_set_current_user($newId);
                wp_set_auth_cookie($newId, true, $secure_cookie);
                $user_info = get_userdata($newId);
                do_action('wp_login', $user_info->user_login, $user_info);

                if(isset($redirect))
                {
                    header('Location: ' . $redirect);
                    die();
                }
            }
        }

    }
    public static function ProcessFormSave()
    {
        require_once( i5PBIntegration__PLUGIN_DIR . 'class.i5PBIntegration-Functions.php' );
        $userID = get_current_user_id();
        $pbId=get_user_meta($userID,'PBID',true);

        //Save the search
        if(isset($_REQUEST["emailFrequency"]))
        {
            $request=array();
            $method="POST";

            $query= "SELECT Id from pba__request__c where Search_Name__c='" . $_REQUEST["searchName"] . "' and pba__Contact__c='" . $pbId . "'";
            $methodUrl="/services/data/v36.0/query/?q=" . urlencode($query);
            $url="/services/data/v36.0/sobjects/pba__request__c";

            $results=i5PBIntegration_Functions::GetAPI(self::$settings["OAuth"],$methodUrl);

            if(isset($results->totalSize) && $results->totalSize!=0)
            {
                $url .= "/" . $results->records[0]->Id;
                $method="PATCH";
                $request["pba__ListingPrice_pb_max__c"]=null;
                $request["pba__Bedrooms_pb_min__c"]=null;
                $request["pba__FullBathrooms_pb_min__c"]=null;
                $request["pba__Bedrooms_pb_max__c"]=null;
                $request["pba__FullBathrooms_pb_max__c"]=null;
                $request["pba__City_pb__c"]=null;
                $request["pba__ListingType__c"]=null;
                $request["pba__LotSize_pb_max__c"]=null;
                $request["pba__LotSize_pb_min__c"]=null;
                $request["Parking_Spaces_Max__c"]=null;
                $request["ParkingSpaces__c"]=null;
                $request["pba__ListingPrice_pb_min__c"]=null;
                $request["pba__PropertyType__c"]=null;
                $request["pba__TotalArea_pb_max__c"]=null;
                $request["pba__TotalArea_pb_min__c"]=null;
                $request["Stories_Max__c"]=null;
                $request["Stories_Min__c"]=null;
                $request["Subdivision__c"]=null;
                $request["pba__YearBuilt_pb_max__c"]=null;
                $request["pba__YearBuilt_pb_min__c"]=null;
                $request["pba__PostalCode_pb__c"]=null;
            }

            $request["pba__Contact__c"]=$pbId;
            $request["Search_Name__c"]=$_REQUEST["searchName"];
            $request["Email_Frequency__c"]=$_REQUEST["emailFrequency"];

            $items=explode(";",$_REQUEST["filters"]);

            foreach($items as $item)
            {
                $pieces=explode(":",$item);

                $request[$pieces[0]]=str_replace(",",";",urldecode($pieces[1]));
            }

            $results=i5PBIntegration_Functions::PostAPI(self::$settings["OAuth"],$url,$request,$method);

        }
    }
}
