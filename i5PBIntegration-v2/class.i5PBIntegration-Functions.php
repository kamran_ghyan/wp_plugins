<?php
class i5PBIntegration_Functions
{
    public static function GetVersions($oAuth)
    {
	       $ch=curl_init($oAuth["InstanceUrl"] . "/services/data/");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array("Authorization: OAuth " . $oAuth["OAuthToken"]));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $resp=curl_exec($ch);

      	echo json_encode($resp);
    }
    public static function GetAPI($oAuth, $methodUrl)
    {
        $ch=curl_init($oAuth["InstanceUrl"] . $methodUrl);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array("Authorization: OAuth " . $oAuth["OAuthToken"]));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $resp=curl_exec($ch);

        if(FALSE==$resp)
        {
            $err=curl_error($ch);
        }

        curl_close($ch);
        
        $vals = json_decode($resp);

        if(is_array($vals) && isset($vals[0]->errorCode) && $vals[0]->errorCode=="INVALID_SESSION_ID")
        {
            $refresh = i5PBIntegration_Functions::RefreshToken($oAuth);

            if(isset($refresh->access_token))
            {
                $settings=get_option('WebListing_Settings');

                $settings["OAuth"]["OAuthToken"]=$refresh->access_token;
                $settings["OAuth"]["InstanceUrl"]=$refresh->instance_url;
                update_option("WebListing_Settings",$settings);

                return self::GetAPI($settings["OAuth"],$methodUrl);
            }
        }
        else
            return $vals;
    }
    public static function PostAPI($oAuth,$methodUrl,$postData,$method="POST")
    {
        $ch=curl_init($oAuth["InstanceUrl"] . $methodUrl);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        if(isset($postData))
        {
            $jData=json_encode($postData);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jData);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array("Authorization: OAuth " . $oAuth["OAuthToken"],'Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        $resp=curl_exec($ch);

        if(FALSE==$resp)
        {
            $err=curl_error($ch);
        }

        curl_close($ch);

        $vals = json_decode($resp);

        if(is_array($vals) && isset($vals[0]->errorCode) && $vals[0]->errorCode=="INVALID_SESSION_ID")
        {
            $refresh = i5PBIntegration_Functions::RefreshToken($oAuth);

            if(isset($refresh->access_token))
            {
                $settings=get_option('WebListing_Settings');

                $settings["OAuth"]["OAuthToken"]=$refresh->access_token;
                $settings["OAuth"]["InstanceUrl"]=$refresh->instance_url;
                update_option("WebListing_Settings",$settings);

                return self::PostAPI($settings["OAuth"],$methodUrl,$postData);
            }
        }
        else
            return $vals;
    }
    public static function DeleteAPI($oAuth,$methodUrl)
    {
        $ch=curl_init($oAuth["InstanceUrl"] . $methodUrl);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER,array("Authorization: OAuth " . $oAuth["OAuthToken"],'Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $resp=curl_exec($ch);

        if(FALSE==$resp)
        {
            $err=curl_error($ch);
        }

        curl_close($ch);

        $vals = json_decode($resp);

        if(is_array($vals) && isset($vals[0]->errorCode) && $vals[0]->errorCode=="INVALID_SESSION_ID")
        {
            $refresh = i5PBIntegration_Functions::RefreshToken($oAuth);

            if(isset($refresh->access_token))
            {
                $settings=get_option('WebListing_Settings');

                $settings["OAuth"]["OAuthToken"]=$refresh->access_token;
                $settings["OAuth"]["InstanceUrl"]=$refresh->instance_url;
                update_option("WebListing_Settings",$settings);

                return self::PostAPI($settings["OAuth"],$methodUrl,$postData);
            }
        }
        else
            return $vals;
    }
    public static function RefreshToken($oAuth)
    {
        $body="grant_type=refresh_token&client_id=" . $oAuth["OAuthClientId"] . "&client_secret=" . $oAuth["OAuthSecret"] . "&refresh_token=" . $oAuth["RefreshToken"];
        $ch=curl_init($oAuth["InstanceUrl"] . "/services/oauth2/token");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $body);

        $ref=curl_exec($ch);

        if(FALSE==$ref)
        {
            $err=curl_error($ch);
        }

        return json_decode($ref);
    }
}