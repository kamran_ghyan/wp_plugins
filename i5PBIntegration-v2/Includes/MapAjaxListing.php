<?php
function query_listings_ajax() {

	$fields = $_POST['fields'];

	$fields = explode('&', $fields);

	$min_lat = $fields[0];
	$max_lat = $fields[1];
	$min_long = $fields[2];
	$max_long = $fields[3];

	global $wpdb;
	// prepare query to fetch data
	$sql = "SELECT mlsid,recordType,area,subdivision,propertytype,title,status,l.url,latitude,longitude,i.url AS image,mlsid,bedrooms,address,city,state,zipCode,description,price,fullBaths,halfBaths,size,agentFirstName,agentEmail,agentMLSId FROM {$wpdb->prefix}i5listings l\r\n\tleft join {$wpdb->prefix}i5Images i on i.id = (SELECT min(id) FROM {$wpdb->prefix}i5Images ii WHERE ii.propertyid=l.propertyid)
	WHERE
	latitude
	BETWEEN
	" . $min_lat . " AND " . $max_lat . "
	AND
	longitude
	BETWEEN
	" . $min_long . " AND " . $max_long . "
	ORDER BY custom3 DESC
	";

	$results = $wpdb->get_results($sql);

	$total_listing = sizeof($results);

	if ($total_listing > 100) {
		$total_list_html = '<span> 1-100</span> of ' . $total_listing . ' Results';
	} else {
		$total_list_html = ' ' . $total_listing . ' Results';
	}

	// Pagination
	$pageSize = 100;
	$startPage = 1;
	$endPage;
	$start = 1;
	$end = 0;
	$pageNum = 1;

	$pagination;

	$endPage = ceil($total_listing / $pageSize);

	if ($pageNum == 1) {
		$pg_nm = $pageNum - 1;
		$pagination .= '<a href=javascript:changePage( ' . $pg_nm . ')><</a>';
	}

	for ($startPage; $startPage <= $endPage; $startPage++) {
		if ($startPage == $pageNum) {
			$active = 'active';
		}
		$pagination .= '<a class="' . $active . '" href="javascript:doSearch(null, ' . $startPage . ')">' . $startPage . '</a>';
	}
	if ($end < $total_listing) {
		$pg_nm = $pageNum + 1;
		$pagination .= '<a href=javascript:doSearch(null,' . $pg_nm . ')>&raquo;</a>';
	}

	$html .= '<div class="resultsFilters">
            <div class="resultsSort">
                <div onclick="changeDisplay("map");" class="btnMap selectedDisplay">
                    <i class="fa fa-th"></i>
                </div>
                <div onclick="changeDisplay("photo");" class="btnPhoto">
                    <i class="fa fa-list"></i>
                </div>
            </div>
            <div id="divFilters"></div>
        </div>';

	$html .= '<div class="searchResultsHeader">
            <div class="headerPaging">
            	  <section>
			        <section class="pagi">
			            <div class="pagi-left">' . $total_list_html . '</div>
			            <div class="pagi-right">
			        		<a class="active">1</a>
			        		<a href="javascript:doSearch(null,2);">2</a>
			        		<a href="javascript:doSearch(null,3);">3</a>
			        		<a href="javascript:doSearch(null,4);">4</a>
			        		<a href="javascript:doSearch(null,5);">5</a>
			        		<a href="javascript:doSearch(null,6);">6</a>
			        		<a href="javascript:doSearch(null,7);">7</a>
			        		<a href="javascript:doSearch(null,8);">8</a>
			        		<a href="javascript:doSearch(null,9);">9</a>
			        		<a href="javascript:doSearch(null,10);">10</a>
			               	<a href="javascript:doSearch(null,2);">»</a>
			            </div>
			        </section>
			    </section>
			    <ul>
					<li>Sort by:</li>
					<li class="dropbtn sortMore" onclick="showSortNew()"> <i class="fa fa-star"></i> Newest
			            <div id="ddSortNew" class="ddSort dropdownContent">
			                 <div class="sortCheckbox"><input name="sort" onchange="doSearch();" type="radio" value="descdays">Number of days on market (Newest)</div>
			             <div class="sortCheckbox"><input name="sort" onchange="doSearch();" type="radio" value="ascydays">Number of days on market (Oldest)</div>
			                  </div>
			        </li>
					<li class="dropbtn sortMore" onclick="showSortPrice()"><i class="fa fa-money"></i> Price
			        	<div id="ddSortPrice" class="ddSort dropdownContent">
			             <div class="sortCheckbox"><input name="sort" onchange="doSearch();" type="radio" value="descprice">Price (Hig to Low)</div>
			             <div class="sortCheckbox"><input name="sort" onchange="doSearch();" type="radio" value="ascprice">Price (Low to High)</div>
			        </div></li>
					<li class="dropbtn sortMore" onclick="showSort()"><i class="fa fa-plus"></i> More
					<div id="ddSort" class="ddSort dropdownContent">
			             <!--<div class="sortCheckbox"><input name="sort" onchange="doSearch();"  type="radio" value="descprice" />Price (Highest)</div>
			             <div class="sortCheckbox"><input name="sort" onchange="doSearch();"  type="radio" value="ascprice" />Price (Lowest)</div>
			             <div class="sortCheckbox"><input name="sort" onchange="doSearch();"  type="radio" />Most Popular</div>-->
			             <div class="sortCheckbox"><input name="sort" onchange="doSearch();" type="radio" value="descbedrooms">Bedrooms (Most)</div>
			             <div class="sortCheckbox"><input name="sort" onchange="doSearch();" type="radio" value="descfullBaths">Bathrooms (Most)</div>
			             <div class="sortCheckbox"><input name="sort" onchange="doSearch();" type="radio" value="desclotsize">Acreage (Highest)</div>
			             <div class="sortCheckbox"><input name="sort" onchange="doSearch();" type="radio" value="asclotsize">Acreage (Lowest)</div>


					</div></li>
					<!-- <div class="btnSort dropbtn" onclick="showSort()">
			        Sort <i class="fa fa-angle-down"></i>
			        </div> -->

				</ul>
            </div>
        </div>';

	$html .= '<div class="searchResultsMap">';
	$html .= '<div class="listingSidebar mapSidebar">';

	foreach ($results as $result) {

		$days = "";
		$lat = $result->latitude;
		$long = $result->longitude;

		// Convert into days
		$current_date = date('Y-m-d');
		$since_date = $result->custom3;

		$todate = strtotime($current_date);
		$fromdate = strtotime($since_date);
		$calculate_seconds = $todate - $fromdate; // Number of seconds between the two dates
		$days = floor($calculate_seconds / (24 * 60 * 60)); // convert to days

		// Image check
		if (isset($result->image) && $result->image != "") {
			$img_url = $result->image;
		} else {
			$img_url = i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png";
		}

		$html .= '<div onmouseover="openInfoWindow(' . $result->mlsid . ')" style="cursor:pointer;" class="listingWrapper">
			            	<div class="col1-1">
		                        <div class="listingPhoto">

			                        <img onclick="location.href="/property-details/' . $result->url . '" src="' . $img_url . '">
			                        <div class="shortcuts">
			                        	<a data-url="' . $result->url . '" data-placement="left" title="" class="share-list" data-original-title="Share"><i class="fa fa-share"></i></a>
			                        	<a href="#">
											</a>
											<div id="fav' . $result->mlsid . '" class="listingFavorite">
											<a href="#">
						                        </a>
						                        <a data-toggle="tooltip" data-placement="left" title="" href="javascript:toggleFav("' . $result->mlsid . '");setFavorite("' . $result->mlsid . '",1);" data-original-title="Favorites">
						                            <i class="fa fa-heart"></i>
						                        </a>
						                    </div>
			                        	<a class="remove-property" data-toggle="tooltip" data-placement="left" title="" data-original-title="Hide">
			                        		<i class="fa fa-ban"></i>
			                        	</a>
			                        </div>
			                        <div class="listingInfo">
			                        <div class="listingStatus">
			                             <div class="statusActive"></div>' . $result->status . '<br>
			                             Listed for: ' . $days . ' Days</div>
				                        <div class="listingSpecs">
				                        	<div class="listingSpec">
				                        		<div class="listingSpecValue">' . $result->bedrooms . '</div>
				                        		<div class="listingSpecLabel">
				                        			Bed
				                        		</div>
				                        	</div>
				                        	<div class="listingSpec">
				                        		<div class="listingSpecValue">' . $result->fullBaths . '</div>
				                        		<div class="listingSpecLabel">
				                        			Bath
				                        		</div>
				                        	</div>
				                        	<div class="listingSpec">
				                        		<div class="listingSpecValue">' . $result->size . '</div>
				                        		<div class="listingSpecLabel">
				                        			Sqft
				                        		</div>
				                        	</div>
				                       </div>
			                       </div>
			                    </div>
			                    <div class="listingDetails">
			                        <div class="listingAddress">
			                            <div onclick="location.href="/property-details/' . $result->url . '" class="listingAddress">' . $result->address . '</div>
			                            <div class="listingCity">
			                            	<i class="fa fa-map-marker"></i> ' . $result->city . ', ' . $result->state . ' 	 ' . $result->zip . ' </div>
			                            <div class="listingPrice">
			                                $' . number_format($result->price, 0) . ($result->propertytype == "Rental" ? " /Month" : "") . '
			                            </div>

			                        </div>
			                    </div>
			                </div>
		                </div>';

	}
	$html .= '<section>
		        <section class="pagi">
		            <div class="pagi-left">
		                ' . $total_list_html . '
		            </div>
		            <div class="pagi-right">' . $pagination . '</div>
		        </section>
		    </section>';

	$html .= '<script>google.maps.event.trigger(map, "resize");';
	foreach ($results as $result) {
		if ($result->latitude != 0) {
			$html .= "markAddress(" . json_encode($result) . ");";
		}
	}

	$html .= '</script>';
	$html .= '</div>';
	$html .= '</div>';

	echo $html;
	die;

}

// list view ajax
add_action('wp_ajax_nopriv_query_listings_ajax', 'query_listings_ajax');
add_action('wp_ajax_query_listings_ajax', 'query_listings_ajax');

function themex_enqeueu_scripts() {
	wp_enqueue_script('property', i5PBIntegration__PLUGIN_URL . 'js/i5WebListings.js');
	wp_enqueue_script('property-pagi', i5PBIntegration__PLUGIN_URL . 'js/jquery.simplePagination.js');
	wp_enqueue_style('property', i5PBIntegration__PLUGIN_URL . 'css/simplePagination.css');
	wp_localize_script('property', 'property', array('ajax_url' => admin_url('admin-ajax.php')));
}
add_action('wp_enqueue_scripts', 'themex_enqeueu_scripts');