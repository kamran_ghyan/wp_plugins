<?php
$PropertyTypes = array();
$ListingTypes = array();
$TTLResults = 0;
$SearchResults;
$pageSize = 100;
$startPage = 1;
$endPage;
$start = 1;
$end = 0;
$pageNum = 1;

if(isset($maxResults))
    $pageSize=$maxResults;

$fields = array(
			"stories",
			"minstories",
			"subdivision",
			"maxstories",
			"parkingSpaces",
			"minparkingSpaces",
			"maxparkingSpaces",
			"lotsize",
			"minlotsize",
			"maxlotsize",
			"yearbuilt",
			"minyearbuilt",
			"maxyearbuilt",
			"minsize", 
			"maxsize",
			"minprice",
			"maxprice",
			"bedrooms",
			"minbedrooms",
			"maxbedrooms",
			"fullBaths",
			"minfullBaths",
			"maxfullBaths",
			"search",
			"newConstruction",
			"listingType"
		);

//Get the Property Types
$sql="SELECT propertytype from {$wpdb->prefix}i5listings group by propertytype order by propertytype";

$pTypes=$wpdb->get_results($sql);

array_push($PropertyTypes,"All");

if($pTypes!=null)
{
    foreach($pTypes as $pType)
    {
        if($pType->propertytype!=null && $pType->propertytype!="")
            array_push($PropertyTypes,$pType->propertytype);
    }
}

$sql="";

//Get the Listing Types
$sql="SELECT ListingType from {$wpdb->prefix}i5listings group by ListingType order by ListingType";

$pTypes = $wpdb->get_results($sql);

array_push($ListingTypes,"All");

if($pTypes!=null)
{
    foreach($pTypes as $pType)
    {
        if($pType->ListingType!=null && $pType->ListingType!="")
            if( $pType->ListingType == "Apartment" || $pType->ListingType == "Exclusive Agency" || $pType->ListingType == "Exclusive Right To Sell" ){
			} else {
			array_push($ListingTypes,$pType->ListingType);
			}
    }
}

$sql="";

//Do the search
if(!isset($_REQUEST["display"])||$_REQUEST["display"]=="gallery")
{
    $sql="SELECT recordType,area,subdivision,propertytype,title,status,l.url,latitude,longitude,i.url AS image,mlsid,bedrooms,address,city,state,zipCode,price,fullBaths,halfBaths,size from {$wpdb->prefix}i5listings l
      left join {$wpdb->prefix}i5Images i on i.id = (select min(id) from {$wpdb->prefix}i5Images ii where ii.propertyid=l.propertyid)";
}
else
{
    $sql="SELECT l.*,i.url AS image from {$wpdb->prefix}i5listings l
          left join {$wpdb->prefix}i5Images i on i.id = (select min(id) from {$wpdb->prefix}i5Images ii where ii.propertyid=l.propertyid)";
}

$sqlCount="SELECT count(*) from {$wpdb->prefix}i5listings";

$where="";
$join="WHERE";
$order=" Order by custom3 DESC";

if(get_query_var("search_query")!="" && strtolower(get_query_var("search_query"))!="all")
{
    $pts = explode(";",urldecode(get_query_var("search_query")));
    $in="";

    foreach($pts as $pt)
    {
        $in .= "'" . $pt . "',";
    }

    $in = substr($in,0,strlen($in) -1);

    $where = " " . $join . " propertytype in(" . $in . ")";
    $join="and";
	
}



foreach($fields as $field)
{
    if(isset($_REQUEST[$field]))
    {
        if($field=="search" || $field=="listingType")
        {

        }
        else
        {
            $where .= " " . $join . " " . str_replace("max","",str_replace("min","",$field));

            if(strlen($field)>2 && strrpos($field,"min",-strlen($field))!==false)
                $where .= ">=";
            else if(strlen($field)>2 &&strrpos($field,"max",-strlen($field))!==false)
                $where .= "<=";
            else
                $where .= "=";

          	if($field == "newConstruction")
              $where .= $_REQUEST[$field]=="2"?"":$_REQUEST[$field];
			
			
			else
            	$where .= $_REQUEST[$field];

            $join="and";
        }
    }
}





if($_REQUEST["listingType"] == 'Rent'){
    
    $url="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $url = explode('/', $url);

    $p_type = explode(';', $url[4]);
    

    if(sizeof($p_type) > 0 && $p_type[0] != 'all'){
       $where .= ' AND custom4 = 1';
    }
    else if($url[4] == 'Apartment' || $url[4] == 'Condo' || $url[4] == 'House'){
         $where .= ' AND custom4 = 1';
    }    

	else if(empty($_REQUEST["minprice"]) && empty($_REQUEST["maxprice"]) && empty($_REQUEST["minbedrooms"]) && empty($_REQUEST["minfullBaths"]) && empty($_REQUEST["newConstruction"])){
		 $where .= ' WHERE custom4 = 1';
	}
	else{
         $where .= ' AND custom4 = 1';
	}

} elseif($_REQUEST["listingType"] == 'Sale'){

    $url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $url = explode('/', $url);
    $p_type = explode(';', $url[4]);
    

    if(sizeof($p_type) > 0 && $p_type[0] != 'all'){
       $where .= ' AND custom4 != 1';
    }
    else if($url[4] == 'Apartment' || $url[4] == 'Condo' || $url[4] == 'House'){
         $where .= ' AND custom4 != 1';
    }    

    else if(empty($_REQUEST["minprice"]) && empty($_REQUEST["maxprice"]) && empty($_REQUEST["minbedrooms"]) && empty($_REQUEST["minfullBaths"]) && empty($_REQUEST["newConstruction"])){
             $where .= ' WHERE custom4 != 1';
        }
        else{
             $where .= ' AND custom4 != 1';
        }
}

if(isset($_REQUEST["searchtype"]) && isset($_REQUEST["search"]) && $_REQUEST["search"]!="" && $_REQUEST["searchtype"]!="")
{
    $where .= " " . $join . " " . str_replace(" ","", urldecode($_REQUEST["searchtype"])) . " ='" . urldecode($_REQUEST["search"]) . "'";
    $join="and";
}
else if(isset($_REQUEST["search"]) && $_REQUEST["search"] !="")
{
    $where .= " " . $join . " (description like '" . $_REQUEST["search"] . "' or mlsid like '" . $_REQUEST["searc"] . "' or title like '" . $_REQUEST["search"] . "' or address like '" . $_REQUEST["search"] . "' or city like '" . $_REQUEST["search"] . "')";
    $join="and";
}

if(isset($_REQUEST["School"]) && $_REQUEST["School"]!="")
{
    $where .= " " . $join . "(elementaryschool like '%" . $_REQUEST["School"] . "%' or middleschool like '%" . $_REQUEST["School"] . "%' or highschool like '%" . $_REQUEST["School"] . "%')";
    $join="and";
}

if(isset($_REQUEST["sort"]))
{
    $dir="ASC";
    $field="";

    if(substr(strtolower($_REQUEST["sort"]),0,3)!="asc")
        $dir="DESC";

    if($dir=="DESC")
        $field=substr($_REQUEST["sort"],4);
    else
        $field=substr($_REQUEST["sort"],3);

    //$order=" ORDER By " . $field . " " . $dir;
    $order=" ORDER BY price " . $dir;
}


if(isset($_REQUEST["pg"]) && (int)$_REQUEST["pg"]!=0)
{
    $start= ($pageSize * (int)$_REQUEST["pg"]) + 1;
    $pageNum=(int)$_REQUEST["pg"];
}

$skip=0;

if($pageNum>1)
    $skip =$pageNum * $pageSize;

//echo $sql . $where . $order . " limit " . $skip . ", " . $pageSize;

$SearchResults = $wpdb->get_results($sql . $where . $order . " limit " . $skip . ", " . $pageSize);
$TTLResults=$wpdb->get_var($sqlCount . $where);

$endPage=ceil($TTLResults/$pageSize);

if($pageNum>5 && $endPage>10)
    $startPage=$pageNum-4;

if($endPage>9 && $startPage!=1 && ($startPage+9)>$endPage)
{
    while(($startPage+9)>$endPage){
        $startPage-=1;
    }
}

if($endPage>$startPage+9){
    $endPage=$startPage+9;
}

$end=$start+$pageSize -1;

if($end>$TTLResults)
    $end=$TTLResults;
?>
<script>
    function removeFilter(filter) {
        doSearch(null,null,filter);
    }
    function changeDisplay(type)
    {
        if(getParameterByName("display") !=null && getParameterByName("display")==type)
            return;

        var loc = getRedirect(null,null,"display");

        if(loc.indexOf("?")==-1)
            loc+="?display=" + type;
        else
            loc+="&display=" + type;

        window.location=loc;
    }

    function doSearch(ord,page,remove,searchType,searchKw) {
        window.location = getRedirect(ord,page,remove,searchType,searchKw);
    }
    
    function getRedirect(ord,page,remove,searchType,searchKw)
    {
		var loc = "/listing-search/";

        if (jQuery("#propertyType").val())
            loc += encodeURI(jQuery("#propertyType").val());
        else if(jQuery("input[name='propertyType']:checked").val() && jQuery("input[name='propertyType']").val() !="")
        {
            var haspt=false;

            jQuery("input[name='propertyType']:checked").each(function(){
                if(jQuery(this).val()!=remove)
                {
                    loc += encodeURI(jQuery(this).val() + ";");
                    haspt=true;
                }
            });

            if(haspt)
                loc=loc.substring(0,loc.length-1);
            else
                loc+="all";
        }
        else
            loc += "all";

        var han = "?";

        var fields = <?php echo json_encode($fields) ?>;

        for (var i = 0; i < fields.length; i++)
        {
            if(fields[i]!=remove)
            {
                if (jQuery("#" + fields[i]).val() && jQuery("#" + fields[i]).val() != "") {
                    loc += han + fields[i] + "=" + encodeURI(jQuery("#" + fields[i]).val());
                    han = "&";
                }
                else if (jQuery("input[name='" + fields[i] + "']:checked").val() && jQuery("input[name='" + fields[i] + "']:checked").val() != "") {
                    loc += han + fields[i] + "=" + encodeURI(jQuery("input[name='" + fields[i] + "']:checked").val());
                    han = "&";
                }
            }
        }
		

        if(jQuery("input[name='sort']:checked").val() && jQuery("input[name='sort']:checked").val()!="")
        {
            loc+= han + "sort=" + jQuery("input[name='sort']:checked").val();
            han="&";
        }
		
		if(jQuery("input[name='new']:checked").val() && jQuery("input[name='new']:checked").val()!="")
        {
            loc+= han + "new=" + jQuery("input[name='new']:checked").val();
            han="&";
        }
		
		if(jQuery("input[name='price']:checked").val() && jQuery("input[name='price']:checked").val()!="")
        {
            loc+= han + "price=" + jQuery("input[name='price']:checked").val();
            han="&";
        }

        if(searchType && searchType!=null && searchKw && searchKw!=null)
        {
            if(remove!=searchType)
            {
                loc += han + "searchtype=" + encodeURI(searchType) + "&search=" + encodeURI(searchKw);
                han = "&";
            }
        }
        else if(getParameterByName("searchtype")!="" && getParameterByName("searchtype") !=null && getParameterByName("search")!="" && getParameterByName("search")!= null)
        {
            if(remove!=getParameterByName("searchtype"))
            {
                loc += han + "searchtype=" + encodeURI(getParameterByName("searchtype")) + "&search=" + encodeURI(getParameterByName("search"));
                han = "&";
            }
        }

        if(getParameterByName("display")!="" && getParameterByName("display") !=null && remove !="display")
        {
            loc += han + "display=" + getParameterByName("display");
            han="&";
        }

        if (ord)
        {
            loc += han + "sort=" + ord;
            han = "&";
        }

        if (page)
        {
            loc += han + "pg=" + page;
            han = "&";
        }

        return loc;
    }
	
		
        
       
		
	
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
</script>
