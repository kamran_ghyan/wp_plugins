function getListings(fields, recordTypes, sort, page, pageSize,callback) {
    jQuery.ajax({
        type: "post",
        url: property.ajax_url,
        data: { 
            action: "query_listings_ajax", 
            fields: fields, 
            recordTypes: recordTypes, 
            sort: sort, 
            page: page, 
            pageSize: pageSize 
        },
        success: function (response) {
            jQuery('.searchResults').html(response);
            //callback(response);
        },
        error: function (err) {
            alert(err);
        }
    });
}