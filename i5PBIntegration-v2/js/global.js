﻿jQuery("img").error(function () {
    fixImage(jQuery(this)[0]);
});
function fixImage(image) {
    image.onerror = "";
    image.src = '<?php echo i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png" ?>';
    return true;
}