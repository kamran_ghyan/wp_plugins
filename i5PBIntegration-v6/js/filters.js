jQuery(document).ready(function(){
	
	/*
		Sorting drop down
	*/
	
    jQuery("#sorting").click(function(){
		// show sorting dropdown
		jQuery("#ddview").hide();
        jQuery("#ddsort").toggle("slow", function() {
    		jQuery("#sorting i").toggleClass('fa-angle-up');
			jQuery("#sorting i").toggleClass('fa-angle-down');
  		});
    });
	
	/*
		Show selected sorting type
	*/
	
	jQuery("#sorting-val li").on("click", function(){	
			
			// remove active class
			jQuery(this).siblings().removeClass("active");
			
			// add active to this element
			jQuery(this).addClass("active");
			
			// get id and insert into hidden field
			var sort_value = jQuery(this).attr("id");
			jQuery("#sort_value").val(sort_value);
			
			// hide dropdowm
			jQuery("#ddview").hide("slow");
			jQuery("#ddsort").toggle("slow");
			
			// toggle select arrow
			jQuery("#sorting i").toggleClass('fa-angle-up');
			jQuery("#sorting i").toggleClass('fa-angle-down');
			
			// replace dropdown with selected li html
			var htmlString = jQuery(this).text();
			jQuery("#sorting").html(htmlString + "<i class='fa fa-angle-down' aria-hidden='true'></i>");
			
			
			
			// call ajax function
			if(sort_value === "default"){ }
			else{ ajax_search(); }
	});
	
	
	/*
	*	Sorting for listing map view
	*/
	
    jQuery("#sorting-map").click(function(){
		// show sorting dropdown
		jQuery("#ddview").hide();
        jQuery("#sort-map-view #ddsort").toggle("slow", function() {
    		jQuery("#sorting-map i").toggleClass('fa-angle-up');
			jQuery("#sorting-map i").toggleClass('fa-angle-down');
  		});
    });
	
	/*
		Show selected sorting type
	*/
	
	jQuery("#sorting-val-map li").on("click", function(){	
			
			// remove active class
			jQuery(this).siblings().removeClass("active");
			
			// add active to this element
			jQuery(this).addClass("active");
			
			// get id and insert into hidden field
			var sort_value = jQuery(this).attr("id");
			jQuery("#filter_sale_map #sort_value").val(sort_value);
			
			// hide dropdowm
			jQuery("#ddview").hide("slow");
			jQuery("#sort-map-view #ddsort").toggle("slow");
			
			// toggle select arrow
			jQuery("#sorting-map i").toggleClass('fa-angle-up');
			jQuery("#sorting-map i").toggleClass('fa-angle-down');
			
			// replace dropdown with selected li html
			var htmlString = jQuery(this).text();
			jQuery("#sorting-map").html(htmlString + "<i class='fa fa-angle-down' aria-hidden='true'></i>");
			
			
			
			// call ajax function
			if(sort_value === "default"){ }
			else{ ajax_search_map(); }
	});
	
	
	
	
	
	/*
		View drop down
	*/
	
	jQuery("#view").click(function(){
		
		jQuery("#ddsort").hide();
        jQuery("#ddview").toggle("slow", function() {
    		jQuery("#view i").toggleClass('fa-angle-up');
			jQuery("#view i").toggleClass('fa-angle-down');
  		});
    });
	
	
	
	jQuery("#list-view li").on("click", function(){	
		
		
      	//console.log('test');
		var exist_view = jQuery("#view").text();
		var current_view = jQuery(this).text();
		if(exist_view == current_view){
			return false;
		}
		else {
		// remove active class
		jQuery(this).siblings().removeClass("active");
		
		// add active to this element
		jQuery(this).addClass("active");
		
		// hide dropdowm
		jQuery("#ddsort").hide("slow");
		jQuery("#ddview").toggle("slow");
		
		// toogle sort option for list / map view
		jQuery("#sort-list-view").toggle();
		jQuery("#sort-map-view").toggle();
		
		// toogle pagination section
		jQuery("#pagi_block").toggle();
		jQuery("#pagi_block_map").toggle();
		
		// toggle select arrow
		jQuery("#view i").toggleClass('fa-angle-up');
		jQuery("#view i").toggleClass('fa-angle-down');
		
		// replace dropdown with selected li html
		var htmlString = jQuery(this).text();
		jQuery("#view").html(htmlString + "<i class='fa fa-angle-down' aria-hidden='true'></i>");
		
		// toggle list / map view
		jQuery(".search-display").toggle();
		jQuery(".search-display-map").toggle();
		
		// toggle sidebar list/ map view
		jQuery("#list-view-form").toggle();
		jQuery("#map-view-form").toggle();
		//jQuery("#saleFiltersMap").toggle();
		jQuery("#map-listing-sidebar").toggle();
		
		// set value for map view
		var view = jQuery("#view_type").val();
		if(view == 'list'){
			jQuery("#view_type").val('map');
		}
		if(view == 'map'){
			jQuery("#view_type").val('list');
		}
		}
		var center = new google.maps.LatLng(61.1076819, -149.9335184);
		jQuery('.search-display-map').fadeIn(2000);
      	google.maps.event.trigger(document.getElementById('divListings'), "resize");
      	map.setCenter(center);
      	fitBounds();
	});
	
	
	/*
		Sorting drop down lease view
	*/
	
    jQuery("#sorting-lease").click(function(){
		// show sorting dropdown
		jQuery("#ddview-lease").hide();
        jQuery("#ddsort-lease").toggle("slow", function() {
    		jQuery("#sorting-lease i").toggleClass('fa-angle-up');
			jQuery("#sorting-lease i").toggleClass('fa-angle-down');
  		});
    });
	
	/*
		Show selected sorting type
	*/
	
	jQuery("#sorting-val-lease li").on("click", function(){	
			
			// remove active class
			jQuery(this).siblings().removeClass("active");
			
			// add active to this element
			jQuery(this).addClass("active");
			
			// get id and insert into hidden field
			var sort_value = jQuery(this).attr("id");
			jQuery("#sort_value_lease").val(sort_value);
			
			// hide dropdowm
			jQuery("#ddview-lease").hide("slow");
			jQuery("#ddsort-lease").toggle("slow");
			
			// toggle select arrow
			jQuery("#sorting-lease i").toggleClass('fa-angle-up');
			jQuery("#sorting-lease i").toggleClass('fa-angle-down');
			
			// replace dropdown with selected li html
			var htmlString = jQuery(this).text();
			jQuery("#sorting-lease").html(htmlString + "<i class='fa fa-angle-down' aria-hidden='true'></i>");
			
			
			
			// call ajax function
			if(sort_value === "default"){ }
			else{ ajax_search_lease(); }
	});
	
	
	// lease map view sorting
	jQuery("#sorting-lease-map").click(function(){
		// show sorting dropdown
		jQuery("#ddview-lease").hide();
        jQuery("#sort-lease-map-view #ddsort-lease").toggle("slow", function() {
    		jQuery("#sorting-lease-map i").toggleClass('fa-angle-up');
			jQuery("#sorting-lease-map i").toggleClass('fa-angle-down');
  		});
    });
	
	/*
		Show selected sorting type
	*/
	
	jQuery("#sorting-val-lease-map li").on("click", function(){	
			
			// remove active class
			jQuery(this).siblings().removeClass("active");
			
			// add active to this element
			jQuery(this).addClass("active");
			
			// get id and insert into hidden field
			var sort_value = jQuery(this).attr("id");
			jQuery("#filter_lease_map  #sort_value_lease").val(sort_value);
			
			// hide dropdowm
			jQuery("#ddview-lease").hide("slow");
			jQuery("#sort-lease-map-view #ddsort-lease").toggle("slow");
			
			// toggle select arrow
			jQuery("#sorting-lease-map i").toggleClass('fa-angle-up');
			jQuery("#sorting-lease-map i").toggleClass('fa-angle-down');
			
			// replace dropdown with selected li html
			var htmlString = jQuery(this).text();
			jQuery("#sorting-lease-map").html(htmlString + "<i class='fa fa-angle-down' aria-hidden='true'></i>");
			
			
			
			// call ajax function
			if(sort_value === "default"){ }
			else{ ajax_search_lease_map(); }
	});
	
	
	/*
		Lease view drop down
	*/
	
	jQuery("#view-lease").click(function(){
		
		jQuery("#ddsort-lease").hide();
        jQuery("#ddview-lease").toggle("slow", function() {
    		jQuery("#view-lease i").toggleClass('fa-angle-up');
			jQuery("#view-lease i").toggleClass('fa-angle-down');
  		});
    });
	
	
	
	jQuery("#list-view-lease li").on("click", function(){	
		var exist_view = jQuery("#view-lease").text();
		var current_view = jQuery(this).text();
		if(exist_view == current_view){
			return false;
		}
		else {
		// remove active class
		jQuery(this).siblings().removeClass("active");
		
		// add active to this element
		jQuery(this).addClass("active");
		
		// hide dropdowm
		jQuery("#ddsort-lease").hide("slow");
		jQuery("#ddview-lease").toggle("slow");
		
		// toogle sort option for lease / map view
		jQuery("#sort-lease-view").toggle();
		jQuery("#sort-lease-map-view").toggle();
		
		// toogle pagination section
		jQuery("#pagi_block_lease").toggle();
		jQuery("#pagi_block_lease_map").toggle();
		
		// toggle select arrow
		jQuery("#view-lease i").toggleClass('fa-angle-up');
		jQuery("#view-lease i").toggleClass('fa-angle-down');
		
		// replace dropdown with selected li html
		var htmlString = jQuery(this).text();
		jQuery("#view-lease").html(htmlString + "<i class='fa fa-angle-down' aria-hidden='true'></i>");
		
		// toggle list / map view
		jQuery(".search-display-lease").toggle();
		jQuery(".search-display-lease-map").toggle();
		
		// toggle sidebar list/ map view
		jQuery("#lease-view-form").toggle();
		jQuery("#map-lease-view-form").toggle();
		//jQuery("#saleFiltersLeaseMap").toggle();
		jQuery("#lease-map-listing-sidebar").toggle();
		
		// set value for map view
		var view = jQuery("#view_type").val();
		if(view == 'list'){
			jQuery("#view_type").val('map');
		}
		if(view == 'map'){
			jQuery("#view_type").val('list');
		}
		}
	});
	
	
	/*
		Reset Fields
	*/
	
	// Reset building size
	jQuery('#reset_bs').on('click', function() {
    	jQuery('.rangebs').find('input[type=number]').val('');
		ajax_search();
	});
	
	// Reset price range
	jQuery('#reset_p').on('click', function() {
    	jQuery('.rangep').find('input[type=number]').val('');
		ajax_search();
	});
	
	// Rest property types
	jQuery('#type_hide').on('click', function() {
    	jQuery('#property_type').find('input[type=checkbox]').removeAttr('checked');
		ajax_search();
	});
	
	// Rest property types
	jQuery('#type_show').on('click', function() {
    	jQuery('#property_type').find('input[type=checkbox]').attr('checked','checked');
		ajax_search();
	});	
	
	// Rest property types
	jQuery('#type_hide_map').on('click', function() {
    	jQuery('#property_type_map').find('input[type=checkbox]').removeAttr('checked');
		ajax_search_map();
	});
	
	// Rest property types
	jQuery('#type_show_map').on('click', function() {
    	jQuery('#property_type_map').find('input[type=checkbox]').attr('checked','checked');
		ajax_search_map();
	});


	// Rest property types
	jQuery('#type_hide_loc').on('click', function() {
		console.log('hide');
		jQuery('#location_type').find('input[type=checkbox]').removeAttr('checked');
		ajax_search();
	});

	// Rest property types
	jQuery('#type_show_loc').on('click', function() {
		console.log('hide');
		jQuery('#location_type').find('input[type=checkbox]').attr('checked','checked');
		ajax_search();
	});

	// Rest property types
	jQuery('#type_hide_map_loc').on('click', function() {
		jQuery('#property_type_map_loc').find('input[type=checkbox]').removeAttr('checked');
		ajax_search_map();
	});

	// Rest property types
	jQuery('#type_show_map_loc').on('click', function() {
		jQuery('#property_type_map_loc').find('input[type=checkbox]').attr('checked','checked');
		ajax_search_map();
	});






	/*
	*	Reset fields for lease view
	*/

	// Rest property types lease list view
	jQuery('#type_hide_lease_loc').on('click', function() {
		jQuery('#lease_location').find('input[type=checkbox]').removeAttr('checked');
		ajax_search_lease();
	});

	// Rest property types lease list view
	jQuery('#type_show_lease_loc').on('click', function() {
		jQuery('#lease_location').find('input[type=checkbox]').attr('checked','checked');
		ajax_search_lease();
	});



	// Rest property types lease list view
	jQuery('#type_hide_lease').on('click', function() {
    	jQuery('#property_type_lease').find('input[type=checkbox]').removeAttr('checked');
		ajax_search_lease();
	});
	
	// Rest property types lease list view
	jQuery('#type_show_lease').on('click', function() {
    	jQuery('#property_type_lease').find('input[type=checkbox]').attr('checked','checked');
		ajax_search_lease();
	});	
	
	// Reset building size
	jQuery('#reset_bs_lease').on('click', function() {
    	jQuery('.rangebs').find('input[type=number]').val('');
		ajax_search_lease();
	});
	
	// Reset price range
	jQuery('#reset_p_lease').on('click', function() {
    	jQuery('.rangep').find('input[type=number]').val('');
		ajax_search_lease();
	});
	
	// Rest property types lease map view
	jQuery('#type_hide_lease_map').on('click', function() {
		jQuery('#property_type_lease_map').find('input[type=checkbox]').removeAttr('checked');
		ajax_search_lease_map();
	});

	// Rest property types lease map view
	jQuery('#type_show_lease_map').on('click', function() {
		jQuery('#property_type_lease_map').find('input[type=checkbox]').attr('checked','checked');
		ajax_search_lease_map();
	});



	// Rest property types lease map view
	jQuery('#type_hide_lease_map_loc').on('click', function() {
		jQuery('#lease_map_loc').find('input[type=checkbox]').removeAttr('checked');
		ajax_search_lease_map();
	});

	// Rest property types lease map view
	jQuery('#type_show_lease_map_loc').on('click', function() {
		jQuery('#lease_map_loc').find('input[type=checkbox]').attr('checked','checked');
		ajax_search_lease_map();
	});




	// Reset building size
	jQuery('#reset_bs_lease_map').on('click', function() {
    	jQuery('.rangebs').find('input[type=number]').val('');
		ajax_search_lease_map();
	});
	
	// Reset price range
	jQuery('#reset_p_lease_map').on('click', function() {
    	jQuery('.rangep').find('input[type=number]').val('');
		ajax_search_lease_map();
	});
	
	/*
	*	Toggle sale / lease options
	*/
	jQuery('#sale-tab').on('click', function() {
    	jQuery('#lease-options').toggle();
		jQuery('#sale-options').toggle();
	});
	jQuery('#lease-tab').on('click', function() {
    	jQuery('#sale-options').toggle();
		jQuery('#lease-options').toggle();
	});
	
});