function autocomplete_ajax_search(){
	var searchTerm = jQuery('#autocomplete').val();
	if(searchTerm === ""){
		jQuery('#autocomplete').addClass('form-input-validate');
	} else{
		jQuery('#autocomplete').removeClass('form-input-validate');
		jQuery("input[name='auto_complete_term']").val(searchTerm);
		ajax_search();
	}
	
}
(function( $ ) {
	$(function() {
        
		var url = SearchAutocomplete.url + "?action=autocomplete_search";
        
		$( "#autocomplete" ).autocomplete({
			source: url,
			delay: 500,
			minLength: 3
		});	
	});
})( jQuery );