// Sale list ajax call
function ajax_search(){
	// serialize form data
	var formdata = jQuery( 'form#filter_sale' ).serialize();
	// ajax call
	jQuery.ajax({
		url : property.ajax_url,
		type : 'post',
		data : {
			action : 'filter_search',
			data_form : formdata
		},
		beforeSend: function(){
			jQuery('.ajax-loader').addClass('loading');
			jQuery('.search-item').css('opacity','0.5');
			jQuery('#pagi_block').remove();
		},
		success: function( response ){
			jQuery('.search-display').html(response);
		},
		complete: function() {
			jQuery('.ajax-loader').removeClass('loading');
			jQuery('.search-item').removeAttr('style');
		}
	});
}
// Sale map type list ajax call
function ajax_search_map(){
	// serialize form data
	var formdata = jQuery( 'form#filter_sale_map' ).serialize();
	// ajax call
	jQuery.ajax({
		url : property.ajax_url,
		type : 'post',
		data : {
			action : 'filter_search_map',
			data_form : formdata
		},
		beforeSend: function(){
			jQuery('.ajax-loader').addClass('loading');
			jQuery('.map-sidebar-item').css('opacity','0.5');
			jQuery('#pagi_block').remove();
		},
		success: function( response ){
			jQuery('#map-listing-sidebar').html(response);
		},
		complete: function() {
			jQuery('.ajax-loader').removeClass('loading');
			jQuery('.map-sidebar-item').removeAttr('style');
		}
	});
}

// Lease list ajax call
function ajax_search_lease(){
	// serialize form data
	var formdata = jQuery( 'form#filter_lease' ).serialize();
	// ajax call
	jQuery.ajax({
		url : property.ajax_url,
		type : 'post',
		data : {
			action : 'filter_search_lease',
			data_form : formdata
		},
		beforeSend: function(){
			jQuery('.ajax-loader').addClass('loading');
			jQuery('.search-item').css('opacity','0.5');
			jQuery('#pagi_block_lease').remove();
		},
		success: function( response ){
			jQuery('.search-display-lease').html(response);
		},
		complete: function() {
			jQuery('.ajax-loader').removeClass('loading');
			jQuery('.search-item').removeAttr('style');
		}
	});
}

// Lease map type list ajax call
function ajax_search_lease_map(){
	// serialize form data
	var formdata = jQuery( 'form#filter_lease_map' ).serialize();
	// ajax call
	jQuery.ajax({
		url : property.ajax_url,
		type : 'post',
		data : {
			action : 'filter_search_lease_map',
			data_form : formdata
		},
		beforeSend: function(){
			jQuery('.ajax-loader').addClass('loading');
			jQuery('.map-sidebar-item').css('opacity','0.5');
			jQuery('#pagi_block').remove();
		},
		success: function( response ){
			jQuery('#lease-map-listing-sidebar').html(response);
		},
		complete: function() {
			jQuery('.ajax-loader').removeClass('loading');
			jQuery('.map-sidebar-item').removeAttr('style');
		}
	});
}

// Pagination ajax call
function pagination(limit, nav, type){
	
	// Sale view ajax call
	if(type == 'sale'){
		if(nav == 'next'){
			jQuery("#pagi_value").val(limit);
			jQuery("#pagi_value_p").val('');
			ajax_search();
		}
		if(nav == 'prev'){
			jQuery("#pagi_value_p").val(limit);
			jQuery("#pagi_value").val('');
			ajax_search();
		}
	}
	
	// Sale map view ajax call
	if(type == 'salemap'){
		if(nav == 'next'){
			jQuery("#pagi_value_map").val(limit);
			jQuery("#pagi_value_map_p").val('');
			ajax_search_map();
		}
		if(nav == 'prev'){
			jQuery("#pagi_value_map_p").val(limit);
			jQuery("#pagi_value_map").val('');
			ajax_search_map();
		}
	}
	
	// Lease view ajax call
	if(type == 'lease'){
		if(nav == 'next'){
			jQuery("#pagi_value_lease").val(limit);
			jQuery("#pagi_value_lease_p").val('');
			ajax_search_lease();
		}
		if(nav == 'prev'){
			jQuery("#pagi_value_lease_p").val(limit);
			jQuery("#pagi_value_lease").val('');
			ajax_search_lease();
		}
	}
	
	// Lease view ajax call
	if(type == 'leasemap'){
		if(nav == 'next'){
			jQuery("#pagi_value_lease_map").val(limit);
			jQuery("#pagi_value_lease_map_p").val('');
			ajax_search_lease_map();
		}
		if(nav == 'prev'){
			jQuery("#pagi_value_lease_map_p").val(limit);
			jQuery("#pagi_value_lease_map").val('');
			ajax_search_lease_map();
		}
	}
	
}


// Pagination lease type ajax call
function pagi_ajax_lease_map(limit){
	jQuery("#pagi_value_lease_map").val(limit);
	jQuery("#pagi_value_lease_map_p").val('');
	ajax_search_lease_map();
}

function pagi_ajax_lease_map_p(limit){
	jQuery("#pagi_value_lease_map_p").val(limit);
	jQuery("#pagi_value_lease_map").val('');
	ajax_search_lease_map();
}
