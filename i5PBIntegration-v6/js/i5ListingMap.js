﻿﻿var map = null;
var userMarker = null;
var movLis = null;
var boundsLis = null;
var startIndex = 0;
var gmarkers = [];
var infoWindows = [];
var makerIndex = 0;
var MarkerListingHash = new Object();
var lastZip;
var mapTimerId = 0;
var mapTimmerRunning = false;
var zoomLis = null;
var dragLis = null;
var mapDiv;
var lastZoom;
var maxZoom;
var minZoom;
var currLat;
var currLong;
var locs = [];
var reInit = false;
var bounds = new google.maps.LatLngBounds();
var shapes = [];
var showPolygon = true;
var showDetails = true;
var listingTemplate = "<div class='mapListingHolder'><div class='listingPhoto'><img onerror='fixImage(this);' src='%image%'/><div class='listingStatus'><div class='new'>%status%</div></div></div><div class='listingDetails'><div class='listingAddress'><div class='listingAddress'>%area% | %city%</div><div class='listingPrice'><span class='number'>$%price%</div></div><div class='listingSpecs'><span class='number'>%bedrooms%</span> beds <span class='number'>%fullBaths%</span> baths <span class='number'>%size%</div> sqft</div><div class='listingUrl'><a href='%url%'>View Listing</a></div></div></div></div>";

function ReInitialize() {
    reInit = true;
    google.maps.event.trigger(map, 'resize');
    //setTimeout(function () {
    //InitializeMap(mapDiv, lastZoom, maxZoom, minZoom, currLat, currLong, showDetails, showPolygon);

    for (var i = 0; i < locs.length; i++)
        markAddress(locs[i]);

    map.fitBounds(bounds);

    //}, 800);


}
function showInfoWindow(mlsid) {
    for (i = 0; i < infoWindows.length; i++) {
        infoWindows[i].close();
    }

    google.maps.event.trigger(gmarkers[mlsid], "click");
}
function zoomBounds(zoom) {
    fitBounds();
    forceZoom(zoom);
}
function fitBounds() {
    //map.fitBounds(bounds);
}
function centerListing(loc) {
    var latlng = new google.maps.LatLng(loc.data.pba__latitude_pb__c, loc.data.pba__longitude_pb__c);
    map.setCenter(latlng);
}
function InitializeMap(mapControl, zoom, maxz, minz, lat, long, details, poly, listingTemp) {
    mapDiv = mapControl;
    lastZoom = zoom;
    maxZoom = maxz;
    minZoom = minz;
    currLat = lat;
    currLong = long;
    showDetails = details;
    showPolygon = poly;

    if (listingTemp != "")
        listingTemplate = listingTemp;

    map = new google.maps.Map(document.getElementById(mapControl), {
        center: { lat: lat, lng: long },
        zoom: zoom,
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        maxZoom: maxz,
        minZoom: minz
    });

    dragLis = google.maps.event.bind(this.map, 'dragend', this, this.mapMoved);
    movLis = google.maps.event.bind(this.map, 'center_changed', this, this.mapMoved);
    zoomLis = google.maps.event.addListener(map, 'zoom_changed', this.mapZoomed);

    if (showPolygon) {}
        var drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: null,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: [
                  google.maps.drawing.OverlayType.POLYGON
                ]
            }
        });
        drawingManager.setMap(map);
    

    google.maps.event.addListener(drawingManager, 'circlecomplete', function (event) {

        // Get circle center and radius
        //var center = event.getCenter();
        //var radius = event.getRadius();

        //Zoom bounds
        map.fitBounds(event.getBounds());

        //Remove overlay from map
        event.setMap(null);
        drawingManager.setDrawingMode(null);

        // Create circle
        //createCircle(center, radius);
    });
    google.maps.event.addListener(drawingManager, 'overlaycomplete', function (event) {
        var newShape = event.overlay;
        newShape.type = event.type;
        shapes.push(newShape);

        map.fitBounds(event.overlay.getBounds());
        var coords = getPolyCoords(event);

        //Add Coords to query
        var newQ = i5ListingFields + "&lat__c=[" + coords.MinLat + ";" + coords.MaxLat + "]&long__c=[" + coords.MinLong + ";" + coords.MaxLong + "]";

        jQuery("#divLoading").show();

        //Todo Query for listings
        getListings(newQ, i5ListingRecordTypes, i5ListingsSort, 0, 200, function (listings) { listingsLoaded(listings); });


        if (drawingManager.getDrawingMode()) {
            drawingManager.setDrawingMode(null);
        }
    });
    google.maps.event.addListener(drawingManager, "drawingmode_changed", function () {
        if (drawingManager.getDrawingMode() != null) {
            for (var i = 0; i < shapes.length; i++) {
                shapes[i].setMap(null);
            }
            shapes = [];
        }
    });

}
if (!google.maps.Polygon.prototype.getBounds) {

    google.maps.Polygon.prototype.getBounds = function () {
        var bounds = new google.maps.LatLngBounds();
        this.getPath().forEach(function (element, index) { bounds.extend(element) });
        return bounds;
    }

}
function listingsLoaded(listings) {
    if (listings != null)
        locs = listings.listings.listing;
    else
        locs = [];

    for (var i = 0; i < gmarkers.length; i++) {
        gmarkers[i].setMap(null);
    }

    makerIndex = 0;
    gmarkers = [];
    infoWindows = [];
    MarkerListingHash = new Object();

    for (var i = 0; i < locs.length; i++) {
        var latlng = new google.maps.LatLng(locs[i].data.pba__latitude_pb__c, locs[i].data.pba__longitude_pb__c);

        if (google.maps.geometry.poly.containsLocation(latlng, shapes[0]))
            markAddress(locs[i]);
    }

    jQuery("#divLoading").hide();
}
function getPolyCoords(event) {
    var vertices = event.overlay.getPath();
    var contentString = "";
    var minLat;
    var maxLat;
    var minLong;
    var maxLong;

    for (var i = 0; i < vertices.getLength() ; i++) {
        var xy = vertices.getAt(i);

        if (minLat == null || minLat > xy.lat())
            minLat = xy.lat();

        if (minLong == null || minLong > xy.lng())
            minLong = xy.lng();

        if (maxLat == null || maxLat < xy.lat())
            maxLat = xy.lat();

        if (maxLong == null || maxLong < xy.lng())
            maxLong = xy.lng();

        contentString += '<br>' + 'Coordinate ' + i + ':<br>' + xy.lat() + ',' +
            xy.lng();
    }

    console.log(contentString);

    var obj = {
        MaxLat: maxLat,
        MinLat: minLat,
        MinLong: minLong,
        MaxLong: maxLong
    };

    return obj;
}
function getDistanceToCenter(point2Lat, point2Long) {
    if (userMarker != null
        && point2Lat != null
        && point2Long != null) {
        var centerLatLng = userMarker.position;
        var point2LatLng = new google.maps.LatLng(point2Lat, point2Long);

        var distance = centerLatLng.distanceFrom(point2LatLng); //returns value in meters
        distance = distance + (distance / 7);  
		//add fudge factor, driving isnot a straight line - compare maps.google.com distances
        distance = distance / 1609.344; 
		//convert miles to meters

        return distance.toFixed(1);
    }
    else
        return 0;

    return centerLatLng.distanceFrom(point2LatLng);
}
function forceZoom(radius) {
    if (map.getZoom()) {
        google.maps.event.removeListener(zoomLis);
        map.setZoom(radius);
        zoomLis = google.maps.event.addListener(map, 'zoom_changed', this.mapZoomed);
    }
}
function mapZoomed() {
    //alert("Map Zoomed zip:" + lastZip);
    if (mapTimmerRunning)
        return;
}
function injectTemplate(search, replacement,template) {
    if (search == "%price%" || search == "%size%" || search == "%bedrooms%" || search == "%fullBaths%")
        template = template.replace(new RegExp(search, 'g'), addCommas(replacement, false));
    else
        template = template.replace(new RegExp(search, 'g'), replacement);

    return template;
}
function markAddress(loc) {
    if (!reInit)
        locs.push(loc);

    /*TODO: include in config settings
    var myIcon = "/wp-content/plugins/i5ListingsMap/Resources/mapMarker.png";
    var image = { url: myIcon, size: new google.maps.Size(30, 43) };
    console.log(loc);*/
    loc.url = "/property-details/" + encodeURIComponent(loc.url);
    if (loc.image == null || loc.image == "")
        loc.image = "/wp-content/plugins/i5PBIntegration/images/imgUnavailable.png";

    var temp=listingTemplate;

    jQuery.each(loc, function (key, value) {
        temp = injectTemplate("%" + key + "%", value,temp);
    });

    var infoWindow = new google.maps.InfoWindow({
        content: temp
    });

    infoWindows[makerIndex] = infoWindow;

      var marker = new MarkerWithLabel({
        position: new google.maps.LatLng(loc.latitude, loc.longitude),
        map: map,
        labelContent: "$" + addCommas(loc.price, false),
        labelClass: "markerLabel",
        labelAnchor: new google.maps.Point(22, 0),
        //icon: image,  //TODO: code to pull image
        title: loc.Name
    });

    bounds.extend(marker.position);

    MarkerListingHash[loc.mlsid] = makerIndex; //treat object like an associative array
    gmarkers[makerIndex] = marker;
    makerIndex++;

    //Close info windows
    if (showDetails) {
        google.maps.event.addListener(marker, 'click', function () {
            for (i = 0; i < infoWindows.length; i++) {
                infoWindows[i].close();
            }
        });

        google.maps.event.addListener(infoWindow, 'domready', function () {
        });

        google.maps.event.addListener(marker, 'click', function () {
            infoWindow.open(map, marker);
        });
    }
    //google.maps.event.bind(marker, 'click', this, function() { markerClicked(loc.DealerId); });    
}
function addCommas(nStr, showDecimal) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }

    if (!showDecimal)
        return x1;
    else
        return x1 + x2;
}
//Event Handlers
function startMapTimer() {
    mapTimmerRunning = true;
    mapTimerId = setTimeout('doMove()', 500);
}

function resetMapTimer() {
    clearTimeout(mapTimerId);
    startMapTimer();
}

function mapMoved() {
    if (mapTimmerRunning)
        resetMapTimer();
    else
        startMapTimer();
}
function doMove() {
    mapTimmerRunning = false;
    if (onMapMoved != null)
        onMapMoved.fire('MapMoved');
}
//Events
var MapLoaded = function () {
    this.eventName = arguments[0];
    var mEventName = this.eventName;
    var dealers;
    var eventAction = new Array(0);
    this.subscribe = function (fn) {
        if (eventAction.length == 0)
            eventAction[0] = fn;
        else
            eventAction[eventAction.length] = fn;
    };
    this.fire = function (sender, eventArgs) {
        this.eventName = 'eventName2';
        if (eventAction.length > 0) {
            for (i = 0; i < eventAction.length; i++) {
                eventAction[i](sender, eventArgs);
            }
        }
    };
};
var MapMoved = function () {
    this.eventName = arguments[0];
    var mEventName = this.eventName;
    var eventAction = new Array(0);
    this.subscribe = function (fn) {
        if (eventAction.length == 0)
            eventAction[0] = fn;
        else
            eventAction[eventAction.length] = fn;
    };
    this.fire = function (sender, eventArgs) {
        this.eventName = 'eventName2';
        if (eventAction.length > 0) {
            for (i = 0; i < eventAction.length; i++) {
                eventAction[i](sender, eventArgs);
            }
        }
    };
};
var onMapMoved = new MapMoved('MapMovedEventHandler');
var onMapLoaded = new MapLoaded('MapLoadedEventHandler');

//Object to hold Location Settings
function locationSettings() {
    this.lat;
    this.lng;
}
function openInfoWindow(MLSId) {
    for (i = 0; i < infoWindows.length; i++) {
        infoWindows[i].close();
    }

    google.maps.event.trigger(gmarkers[MarkerListingHash[MLSId]], "click");
}







