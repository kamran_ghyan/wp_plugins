<?php

/*
Plugin Name:i5Propertybase Integration
Plugin URI: http://www.i5fusion.com
Description: Integrates Propertybase
Author: Jarrett Fisher
Version: 2.0
Author URI:http://www.i5fusion.com
*/
$time = microtime(true);
define('i5PBIntegration__PLUGIN_DIR', plugin_dir_path(__FILE__));
define('i5PBIntegration__PLUGIN_URL', plugin_dir_url(__FILE__));
if (is_admin()) {
    require_once i5PBIntegration__PLUGIN_DIR . 'class.i5PBIntegration-Admin.php';
    add_action('init', array('i5PBIntegration_Admin', 'init'));
}
require_once i5PBIntegration__PLUGIN_DIR . 'class.i5PBIntegration.php';
require_once i5PBIntegration__PLUGIN_DIR . 'class.i5PBIntegration-Template.php';
add_action('init', array('i5PBIntegration', 'init'));
register_deactivation_hook(__FILE__, array('i5PBIntegration', 'deactivation'));
register_activation_hook(__FILE__, array('i5PBIntegration', 'activation'));
add_action('SyncListings', array('i5PBIntegration', 'mlsSync'));
//Initialize Templates
add_action('plugins_loaded', array('i5PBIntegration_Template', 'get_instance'));
//add_filter('init',array('i5PBIntegration', 'flushRules'));
require_once i5PBIntegration__PLUGIN_DIR . 'class.i5PBIntegration-Prospect.php';
add_action('init', array('i5PBIntegration_Prospect', 'init'));
require_once i5PBIntegration__PLUGIN_DIR . 'class.i5PBIntegration-Listing.php';
add_action('init', array('i5PBIntegration_Listing', 'init'));
//Setup Redirects
add_filter('query_vars', array('i5PBIntegration', 'wp_query_vars'));
//Short Code
require_once i5PBIntegration__PLUGIN_DIR . 'class.i5PBIntegration-ShortCode.php';
add_shortcode("i5ListingMap", array('i5PBIntegration_ShortCode', 'map_shortCode'));
add_shortcode("i5GeoSearch", array('i5PBIntegration_ShortCode', 'geosearch_shortCode'));
add_action('init', array('i5PBIntegration_ShortCode', 'init'));
require_once i5PBIntegration__PLUGIN_DIR . '/Includes/MetaBoxSettings.php';
add_action('load-post.php', array('MetaBoxSettings', 'setupMetaBoxes'));
add_action('load-post-new.php', array('MetaBoxSettings', 'setupMetaBoxes'));
add_action('wp_enqueue_scripts', array('i5PBIntegration', 'add_scripts'));
add_action('wp_footer', array('i5PBIntegration', 'init_registerPop'));
//echo "Time Elapsed i5PBIntegration Loading: ".(microtime(true) - $time)."s";
require_once i5PBIntegration__PLUGIN_DIR . '/Includes/AjaxSearch.php';