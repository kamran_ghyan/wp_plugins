<?php
class i5PBIntegration
{
    private static $initiated=false;
    private static $isRunning=false;

    public static function init(){
        if(!self::$initiated){
            self::init_hooks();
            self::$initiated=true;
        }
    }
    public function init_registerPop(){
        require_once(i5PBIntegration__PLUGIN_DIR . '/Includes/register-popup.php');
    }
    public static function init_hooks(){
        //Register Property Detail Sidebar widget
        register_sidebar( array(
            'name'          => 'Property Detail Right Sidebar',
            'id'            => 'property_detail_right',
            'before_widget' => '<div>',
            'after_widget'  => '</div>',
            'before_title'  => '<h2 class="rounded">',
            'after_title'   => '</h2>',
        ) );

        //Add the property details rewrites
        add_rewrite_rule(
           '^property-details/(.+)/?$',
           'index.php?pagename=property-detail&property_url=$matches[1]',
           'top' );

        //Add the search rewrites
        add_rewrite_rule(
           '^listing-search/(.+)/?$',
           'index.php?pagename=property-search&search_query=$matches[1]',
           'top' );

    }
    function i5_register_url($link){
        //return $_SERVER['REQUEST_URI'] . "#register";
    }
    function i5_fix_register_urls($url, $path, $orig_scheme){
        if ($orig_scheme !== 'login')
            return $url;

        if ($path == 'wp-login.php?action=register')
            return $_SERVER['REQUEST_URI'] . "#register";

        return $url;
    }
    function wp_query_vars( $query_vars ){
        $query_vars[] = 'property_url';
        $query_vars[] .= 'search_query';
        return $query_vars;
    }
    function flushRules(){
        global $wp_rewrite; $wp_rewrite->flush_rules();
    }
    public static function activation(){
        wp_schedule_event(time() + 60 * 5,'hourly','SyncListings');

        $settings=get_option('WebListing_Settings');

        if(isset($settings)&& isset($settings["Status"]))
        {
            $settings["Status"]="Idle";
            update_option("WebListing_Settings",$settings);
        }

        self::$isRunning=false;

        //Setup database
        self::setupdb();
        //self::mlsSync();
    }
    public static function deactivation(){
        $settings=get_option('WebListing_Settings');
        wp_clear_scheduled_hook('SyncListings');
        self::$isRunning=false;
        $settings["LastRun"]=null;
        update_option("WebListing_Settings",$settings);
    }
    private static function setupdb(){
        global $wpdb;
        global $i5PBIntegration_Version;
        $i5PBIntegration_Version="1.0";

        $charset_collate = $wpdb->get_charset_collate();

        //Setup listing table
        $listingTableName= $wpdb->prefix . "i5listings";

        $sql = "CREATE TABLE $listingTableName (
                mlsid nvarchar(255) NOT NULL,
                title nvarchar(255) NOT NULL,
                pbid nvarchar(100) NOT NULL,
                propertyid nvarchar(100) NULL,
                lastUpdated datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                created datetime DEFAULT '0000-00-00 00:00:00',
                latitude decimal(18,14) NULL,
                longitude decimal(18,14) NULL,
                size int,
                status nvarchar(100),
                bedrooms int,
                fullBaths decimal(6,2),
                halfBaths decimal(6,2),
                parkingSpaces int,
                stories decimal(2,2),
                address nvarchar(255),
                city nvarchar(255),
                state nvarchar(100),
                zipCode nvarchar(20),
                price decimal(12,2),
                lotsize decimal(12,2),
                listingType nvarchar(255),
                subdivision nvarchar(255),
                propertytype nvarchar(255),
                schoolDistrict nvarchar(255),
                elementarySchool nvarchar(255),
                middleSchool nvarchar(255),
                highSchool nvarchar(255),
                agentFirstName nvarchar(255),
                agentLastName nvarchar(255),
                agentMobile nvarchar(255),
                agentOfficePhone nvarchar(255),
                agentEmail nvarchar(255),
                agentMLSId nvarchar(255),
                exteriorFeatures nvarchar(255),
                exteriorSiding nvarchar(255),
                fenceDescription nvarchar(255),
                hasFence nvarchar(255),
                foundationType nvarchar(255),
                parkingDescription nvarchar(255),
                pool nvarchar(255),
                roof nvarchar(255),
                style nvarchar(255),
                listingOffice nvarchar(255),
                waterfrontDescription nvarchar(255),
                waterfront nvarchar(255),
                appliances nvarchar(255),
                cooling nvarchar(255),
                equipment nvarchar(255),
                numberOfFireplaces int,
                flooring nvarchar(255),
                heating nvarchar(255),
                interiorFeatures nvarchar(255),
                numberOfRooms int,
                otherRooms nvarchar(1000),
                waterHeaterType nvarchar(255),
                disclosure nvarchar(255),
                newConstruction nvarchar(255),
                ownershipType nvarchar(255),
                sewerType nvarchar(255),
                taxes nvarchar(255),
                waterType nvarchar(255),
                zoning nvarchar(255),
                area nvarchar(255),
                custom1 nvarchar(255),
                custom2 nvarchar(255),
                custom3 nvarchar(255),
                custom4 nvarchar(255),
                custom5 nvarchar(255),
                custom6 nvarchar(255),
                custom7 nvarchar(255),
                custom8 nvarchar(255),
                custom9 nvarchar(255),
                custom10 nvarchar(255),
                description text,
                view text,
                url nvarchar(255),
                yearbuilt nvarchar(4),
                PRIMARY KEY (pbid),
                INDEX listing_mlsid (mlsid),
                INDEX listing_price (price),
                INDEX listing_size (size),
                INDEX listing_lat (latitude),
                INDEX listing_long (longitude),
                INDEX listing_type (listingType)
                ) $charset_collate;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        $result = dbDelta( $sql );

        $imageTableName=$wpdb->prefix . "i5Images";

        //Create images table
        $sql= "CREATE TABLE $imageTableName (
               id bigint(2) NOT NULL AUTO_INCREMENT,
               url nvarchar(255) NOT NULL,
               propertyid nvarchar(255) NOT NULL,
               PRIMARY KEY (id),
                INDEX Image_Property (propertyid)
               ) $charset_collate;";

        $result = dbDelta( $sql );

        add_option('i5PBIntegration_Version',$i5PBIntegration_Version);
    }
    public static function mlsSync()
    {
        $settings=get_option('WebListing_Settings');

        if(!self::$isRunning && isset($settings) && isset($settings["EnableSync"]) && $settings["EnableSync"]=="Yes" && isset($settings["OAuth"]) && isset($settings["OAuth"]["OAuthToken"]) && (!isset($settings["Status"]) || $settings["Status"]=="Idle"))
        {
            try
            {

            self::$isRunning=true;
            $settings["Status"]="Running";

            self::removeExpired();

            global $wpdb;

            if(sizeof($settings["SelectFields"])>0)
            {
                $currRun=date("Y-m-d\Th:i:s");
                $lastRun=date("Y-m-d\Th:i:s",strtotime("January 1 1900"));

                if(isset($settings["LastRun"]))
                    $lastRun=$settings["LastRun"];

                require_once( i5PBIntegration__PLUGIN_DIR . 'class.i5PBIntegration-Functions.php' );
                $query="SELECT Id,(Select pba__ExternalLink__c,pba__url__c From pba__PropertyMedia__r),(select Id,Name,CreatedDate,LastModifiedDate,";
                $where=" where LastModifiedDate>" . $lastRun . "Z";

                foreach($settings["SelectFields"] as $key=>$field)
                {
                    if($field!="Id")
                        $query.= $field . ",";
                }

                if(isset($settings["WHERE"]))
                {
                    if(isset($settings["WHERE"]["Condition1"]))
                    {
                        $values = explode(";",$settings["WHERE"]["Condition1"]["Value"]);

                        foreach($values as $value)
                            $where .= " and " . $settings["WHERE"]["Condition1"]["Field"] . $settings["WHERE"]["Condition1"]["Type"] . ((strtolower($value)=="true"||strtolower($value)=="false")?"":"'") . $value . ((strtolower($value)=="true"||strtolower($value)=="false")?"":"'");
                    }

                    if(isset($settings["WHERE"]["Condition2"]))
                    {
                        $values = explode(";",$settings["WHERE"]["Condition2"]["Value"]);

                        foreach($values as $value)
                            $where .= " and " . $settings["WHERE"]["Condition2"]["Field"] . $settings["WHERE"]["Condition2"]["Type"] . ((strtolower($value)=="true"||strtolower($value)=="false")?"":"'") . $value . ((strtolower($value)=="true"||strtolower($value)=="false")?"":"'");
                    }

                    if(isset($settings["WHERE"]["Condition3"]))
                    {
                        $values = explode(";",$settings["WHERE"]["Condition3"]["Value"]);

                        foreach($values as $value)
                            $where .= " and " . $settings["WHERE"]["Condition3"]["Field"] . $settings["WHERE"]["Condition3"]["Type"] . ((strtolower($value)=="true"||strtolower($value)=="false")?"":"'") . $value . ((strtolower($value)=="true"||strtolower($value)=="false")?"":"'");
                    }
                }

                $query = rtrim($query,",");

                $query .= " from pba__listings__r" . $where . ") from pba__property__c where id in (select pba__property__c from pba__listing__c" . $where . ")" ;

                $done=false;
                $methodUrl="/services/data/v20.0/query/?q=" . urlencode($query);
                $fields=$settings["SelectFields"];
                $current=0;

                 while(!$done)
                {
                    $results =i5PBIntegration_Functions::GetAPI($settings["OAuth"],$methodUrl);

                   

                    if(isset($results->totalSize) && $results->totalSize>0)
                    {
                        $done=$results->done;

                        if(!$done)
                            $methodUrl=$results->nextRecordsUrl;

                        foreach($results->records as $property)
                        {
                            try
                            {
                            $current+=1;
                            $settings["Status"]="Syncing Property: " . $current . " of " .$results->totalSize;
                            update_option("WebListing_Settings",$settings);

                            if(isset($property->pba__Listings__r))
                            {
                                foreach($property->pba__Listings__r->records as $record)
                                {
                                   
                                   echo '<pre>';
                                   var_dump($record);
                                   echo '</pre>';
                                    //Check to see if the record exists
                                    $sql = "INSERT INTO {$wpdb->prefix}i5listings (listingOffice,exteriorFeatures,exteriorSiding,fenceDescription,hasFence,foundationType,parkingDescription,pool,roof,style,waterfrontDescription,waterfront,appliances,cooling,equipment,numberOfFireplaces,flooring,heating,interiorFeatures,numberOfRooms,otherRooms,waterHeaterType,disclosure,newConstruction,ownershipType,sewerType,taxes,waterType,zoning,agentFirstName,agentLastName,agentMobile,agentOfficePhone,agentEmail,agentMLSId,area,schoolDistrict,elementarySchool,middleSchool,highSchool,parkingSpaces,stories,custom1,custom2,custom3,custom4,custom5,custom6,custom7,custom8,custom9,custom10,created,view,description,propertytype,lotsize,yearbuilt,url,propertyid,mlsid,title,pbid,lastUpdated,latitude,longitude,size,status,bedrooms,fullBaths,halfBaths,address,city,zipCode,state,price,listingType,subdivision,ownerId,ownerName,ownerTitle,ownerEmail,ownerPhone,ownerMobile,ownerFax,ownerPosition,leaseType,propertySubType,minContiguous,maxContiguous)
                                VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%s,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%01.2f,%s,%s,%s,%s,%s,%s,%s,%01.14f,%01.14f,%d,%s,%d,%01.2f,%01.2f,%s,%s,%s,%s,%01.2f,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ON DUPLICATE KEY UPDATE title=%s,pbid=%s,lastUpdated=%s,latitude=%01.14f,longitude=%01.14f,size=%d,status=%s,bedrooms=%d,fullBaths=%01.2f,halfBaths=%01.2f,address=%s,city=%s,zipCode=%s,state=%s,price=%01.2f,listingType=%s,subdivision=%s,url=%s,yearbuilt=%s,lotsize=%01.2f,propertytype=%s,description=%s,view=%s,created=%s,custom1=%s,custom2=%s,custom3=%s,custom4=%s,custom5=%s,custom6=%s,custom7=%s,custom8=%s,custom9=%s,custom10=%s,mlsid=%s,parkingSpaces=%d,stories=%01.2f,schoolDistrict=%s,elementarySchool=%s,middleSchool=%s,highSchool=%s,area=%s,agentFirstName=%s,agentLastName=%s,agentMobile=%s,agentOfficePhone=%s,agentEmail=%s,agentMLSId=%s,exteriorFeatures=%s,exteriorSiding=%s,fenceDescription=%s,hasFence=%s,foundationType=%s,parkingDescription=%s,pool=%s,roof=%s,style=%s,waterfrontDescription=%s,waterfront=%s,appliances=%s,cooling=%s,equipment=%s,numberOfFireplaces=%d,flooring=%s,heating=%s,interiorFeatures=%s,numberOfRooms=%d,otherRooms=%s,waterHeaterType=%s,disclosure=%s,newConstruction=%s,ownershipType=%s,sewerType=%s,taxes=%s,waterType=%s,zoning=%s,listingOffice=%s,ownerId=%s,ownerName=%s,ownerTitle=%s,ownerEmail=%s,ownerPhone=%s,ownerMobile=%s,ownerFax=%s,ownerPosition=%s,leaseType=%s,propertySubType=%s,minContiguous=%s,maxContiguous=%s";

                                    var_dump($sql);

                                    $sql=$wpdb->prepare($sql,
                                        isset($fields["PBSListingOffice"])?$record->$fields["PBSListingOffice"]:null,
                                        isset($fields["PBSExteriorFeatures"])?$record->$fields["PBSExteriorFeatures"]:null,
                                        isset($fields["PBSExteriorSiding"])?$record->$fields["PBSExteriorSiding"]:null,
                                        isset($fields["PBSFenceDesc"])?$record->$fields["PBSFenceDesc"]:null,
                                        isset($fields["PBSFence"])?$record->$fields["PBSFence"]:null,
                                        isset($fields["PBSFoundationType"])?$record->$fields["PBSFoundationType"]:null,
                                        isset($fields["PBSParkingDesc"])?$record->$fields["PBSParkingDesc"]:null,
                                        isset($fields["PBSPool"])?$record->$fields["PBSPool"]:null,
                                        isset($fields["PBSRoof"])?$record->$fields["PBSRoof"]:null,
                                        isset($fields["PBSStyle"])?$record->$fields["PBSStyle"]:null,
                                        isset($fields["PBSWaterfrontDesc"])?$record->$fields["PBSWaterfrontDesc"]:null,
                                        isset($fields["PBSWaterfront"])?$record->$fields["PBSWaterfront"]:null,
                                        isset($fields["PBSAppliances"])?$record->$fields["PBSAppliances"]:null,
                                        isset($fields["PBSCooling"])?$record->$fields["PBSCooling"]:null,
                                        isset($fields["PBSEquipment"])?$record->$fields["PBSEquipment"]:null,
                                        isset($fields["PBSFirePlaceNum"])?$record->$fields["PBSFirePlaceNum"]:null,
                                        isset($fields["PBSFlooring"])?$record->$fields["PBSFlooring"]:null,
                                        isset($fields["PBSHeating"])?$record->$fields["PBSHeating"]:null,
                                        isset($fields["PBSInteriorFeat"])?$record->$fields["PBSInteriorFeat"]:null,
                                        isset($fields["PBSRoomNum"])?$record->$fields["PBSRoomNum"]:null,
                                        isset($fields["PBSOtherRooms"])?$record->$fields["PBSOtherRooms"]:null,
                                        isset($fields["PBSWaterHeater"])?$record->$fields["PBSWaterHeater"]:null,
                                        isset($fields["PBSDisclosure"])?$record->$fields["PBSDisclosure"]:null,
                                        isset($fields["PBSNewConstruction"])?$record->$fields["PBSNewConstruction"]:null,
                                        isset($fields["PBSOwnershipType"])?$record->$fields["PBSOwnershipType"]:null,
                                        isset($fields["PBSSewer"])?$record->$fields["PBSSewer"]:null,
                                        isset($fields["PBSTaxes"])?$record->$fields["PBSTaxes"]:null,
                                        isset($fields["PBSWater"])?$record->$fields["PBSWater"]:null,
                                        isset($fields["PBSZoning"])?$record->$fields["PBSZoning"]:null,
                                        isset($fields["PBSAgentFirstName"])?$record->$fields["PBSAgentFirstName"]:null,
                                        isset($fields["PBSAgentLastName"])?$record->$fields["PBSAgentLastName"]:null,
                                        isset($fields["PBSAgentMobile"])?$record->$fields["PBSAgentMobile"]:null,
                                        isset($fields["PBSAgentOfficePhone"])?$record->$fields["PBSAgentOfficePhone"]:null,
                                        isset($fields["PBSAgentEmail"])?$record->$fields["PBSAgentEmail"]:null,
                                        isset($fields["PBSAgentMLSID"])?$record->$fields["PBSAgentMLSID"]:null,
                                        isset($fields["PBSArea"])?$record->$fields["PBSArea"]:null,
                                        isset($fields["PBSSchoolDistrict"])?$record->$fields["PBSSchoolDistrict"]:null,
                                        isset($fields["PBSElementarySchool"])?$record->$fields["PBSElementarySchool"]:null,
                                        isset($fields["PBSMiddleSchool"])?$record->$fields["PBSMiddleSchool"]:null,
                                        isset($fields["PBSHighSchool"])?$record->$fields["PBSHighSchool"]:null,
                                        isset($fields["PBSParkingSpaces"])?$record->$fields["PBSParkingSpaces"]:null,
                                        isset($fields["PBSStories"])?$record->$fields["PBSStories"]:null,
                                        isset($fields["PBSCustom1"])?$record->$fields["PBSCustom1"]:null,
                                        isset($fields["PBSCustom2"])?$record->$fields["PBSCustom2"]:null,
                                        isset($fields["PBSCustom3"])?$record->$fields["PBSCustom3"]:null,
                                        isset($fields["PBSCustom4"])?$record->$fields["PBSCustom4"]:null,
                                        isset($fields["PBSCustom5"])?$record->$fields["PBSCustom5"]:null,
                                        isset($fields["PBSCustom6"])?$record->$fields["PBSCustom6"]:null,
                                        isset($fields["PBSCustom7"])?$record->$fields["PBSCustom7"]:null,
                                        isset($fields["PBSCustom8"])?$record->$fields["PBSCustom8"]:null,
                                        isset($fields["PBSCustom9"])?$record->$fields["PBSCustom9"]:null,
                                        isset($fields["PBSCustom10"])?$record->$fields["PBSCustom10"]:null,
                                        $record->CreatedDate,
                                         isset($fields["PBSView"])?$record->$fields["PBSView"]:null,
                                         isset($fields["PBSDescription"])?$record->$fields["PBSDescription"]:null,
                                         isset($fields["PBSPropertyType"])?$record->$fields["PBSPropertyType"]:null,
                                         isset($fields["PBSLotSize"])?$record->$fields["PBSLotSize"]:null,
                                         isset($fields["PBSYearBuilt"])?$record->$fields["PBSYearBuilt"]:null,
                                         isset($fields["PBSURL"])?$record->$fields["PBSURL"]:null,
                                         $property->Id,
                                         $record->$fields["PBSMLSID"],
                                         $record->Name,
                                         $record->Id,
                                         $record->LastModifiedDate,
                                         isset($fields["PBSLatitude"])?$record->$fields["PBSLatitude"]:null,
                                         isset($fields["PBSLongitude"])?$record->$fields["PBSLongitude"]:null,
                                         isset($fields["PBSSize"])?$record->$fields["PBSSize"]:null,
                                         isset($fields["PBSStatus"])?$record->$fields["PBSStatus"]:null,
                                         isset($fields["PBSBeds"])?$record->$fields["PBSBeds"]:null,
                                         isset($fields["PBSFullBaths"])?$record->$fields["PBSFullBaths"]:null,
                                         isset($fields["PBSHalfBaths"])?$record->$fields["PBSHalfBaths"]:null,
                                         isset($fields["PBSAddress"])?$record->$fields["PBSAddress"]:null,
                                         isset($fields["PBSCity"])?$record->$fields["PBSCity"]:null,
                                         isset($fields["PBSZip"])?$record->$fields["PBSZip"]:null,
                                         isset($fields["PBSState"])?$record->$fields["PBSState"]:null,
                                         isset($fields["PBSPrice"])?$record->$fields["PBSPrice"]:null,
                                         isset($fields["PBSListType"])?$record->$fields["PBSListType"]:null,
                                         isset($fields["PBSSubdivision"])?$record->$fields["PBSSubdivision"]:null,
                                         isset($fields["PBSOwnerId"])?$record->$fields["PBSOwnerId"]:null,
                                         isset($fields["PBSOwnerName"])?$record->$fields["PBSOwnerName"]:null,
                                         isset($fields["PBSOwnerTitle"])?$record->$fields["PBSOwnerTitle"]:null,
                                         isset($fields["PBSOwnerEmail"])?$record->$fields["PBSOwnerEmail"]:null,
                                         isset($fields["PBSOwnerPhone"])?$record->$fields["PBSOwnerPhone"]:null,
                                         isset($fields["PBSOwnerMobile"])?$record->$fields["PBSOwnerMobile"]:null,
                                         isset($fields["PBSOwnerFax"])?$record->$fields["PBSOwnerFax"]:null,
                                         isset($fields["PBSOwnerCompany"])?$record->$fields["PBSOwnerCompany"]:null,
                                         isset($fields["PBSLeaseType"])?$record->$fields["PBSLeaseType"]:null,
                                         isset($fields["PBSPropertySubType"])?$record->$fields["PBSPropertySubType"]:null,
                                         isset($fields["PBSMinContiguous"])?$record->$fields["PBSMinContiguous"]:null,
                                         isset($fields["PBSMaxContiguous"])?$record->$fields["PBSMaxContiguous"]:null,

                                         $record->Name,
                                         $record->Id,
                                         $record->LastModifiedDate,
                                         isset($fields["PBSLatitude"])?$record->$fields["PBSLatitude"]:null,
                                         isset($fields["PBSLongitude"])?$record->$fields["PBSLongitude"]:null,
                                         isset($fields["PBSSize"])?$record->$fields["PBSSize"]:null,
                                         isset($fields["PBSStatus"])?$record->$fields["PBSStatus"]:null,
                                         isset($fields["PBSBeds"])?$record->$fields["PBSBeds"]:null,
                                         isset($fields["PBSFullBaths"])?$record->$fields["PBSFullBaths"]:null,
                                         isset($fields["PBSHalfBaths"])?$record->$fields["PBSHalfBaths"]:null,
                                         isset($fields["PBSAddress"])?$record->$fields["PBSAddress"]:null,
                                         isset($fields["PBSCity"])?$record->$fields["PBSCity"]:null,
                                         isset($fields["PBSZip"])?$record->$fields["PBSZip"]:null,
                                         isset($fields["PBSState"])?$record->$fields["PBSState"]:null,
                                         isset($fields["PBSPrice"])?$record->$fields["PBSPrice"]:null,
                                         isset($fields["PBSListType"])?$record->$fields["PBSListType"]:null,
                                         isset($fields["PBSSubdivision"])?$record->$fields["PBSSubdivision"]:null,
                                         isset($fields["PBSURL"])?$record->$fields["PBSURL"]:null,
                                         isset($fields["PBSYearBuilt"])?$record->$fields["PBSYearBuilt"]:null,
                                         isset($fields["PBSLotSize"])?$record->$fields["PBSLotSize"]:null,
                                         isset($fields["PBSPropertyType"])?$record->$fields["PBSPropertyType"]:null,
                                         isset($fields["PBSDescription"])?$record->$fields["PBSDescription"]:null,
                                         isset($fields["PBSView"])?$record->$fields["PBSView"]:null,
                                         $record->CreatedDate,
                                         isset($fields["PBSCustom1"])?$record->$fields["PBSCustom1"]:null,
                                        isset($fields["PBSCustom2"])?$record->$fields["PBSCustom2"]:null,
                                        isset($fields["PBSCustom3"])?$record->$fields["PBSCustom3"]:null,
                                        isset($fields["PBSCustom4"])?$record->$fields["PBSCustom4"]:null,
                                        isset($fields["PBSCustom5"])?$record->$fields["PBSCustom5"]:null,
                                        isset($fields["PBSCustom6"])?$record->$fields["PBSCustom6"]:null,
                                        isset($fields["PBSCustom7"])?$record->$fields["PBSCustom7"]:null,
                                        isset($fields["PBSCustom8"])?$record->$fields["PBSCustom8"]:null,
                                        isset($fields["PBSCustom9"])?$record->$fields["PBSCustom9"]:null,
                                        isset($fields["PBSCustom10"])?$record->$fields["PBSCustom10"]:null,
                                        isset($fields["PBSMLSID"])?$record->$fields["PBSMLSID"]:null,
                                        isset($fields["PBSParkingSpaces"])?$record->$fields["PBSParkingSpaces"]:null,
                                        isset($fields["PBSStories"])?$record->$fields["PBSStories"]:null,
                                        isset($fields["PBSSchoolDistrict"])?$record->$fields["PBSSchoolDistrict"]:null,
                                        isset($fields["PBSElementarySchool"])?$record->$fields["PBSElementarySchool"]:null,
                                        isset($fields["PBSMiddleSchool"])?$record->$fields["PBSMiddleSchool"]:null,
                                        isset($fields["PBSHighSchool"])?$record->$fields["PBSHighSchool"]:null,
                                        isset($fields["PBSArea"])?$record->$fields["PBSArea"]:null,
                                        isset($fields["PBSAgentFirstName"])?$record->$fields["PBSAgentFirstName"]:null,
                                        isset($fields["PBSAgentLastName"])?$record->$fields["PBSAgentLastName"]:null,
                                        isset($fields["PBSAgentMobile"])?$record->$fields["PBSAgentMobile"]:null,
                                        isset($fields["PBSAgentOfficePhone"])?$record->$fields["PBSAgentOfficePhone"]:null,
                                        isset($fields["PBSAgentEmail"])?$record->$fields["PBSAgentEmail"]:null,
                                        isset($fields["PBSAgentMLSID"])?$record->$fields["PBSAgentMLSID"]:null,
                                        isset($fields["PBSExteriorFeatures"])?$record->$fields["PBSExteriorFeatures"]:null,
                                        isset($fields["PBSExteriorSiding"])?$record->$fields["PBSExteriorSiding"]:null,
                                        isset($fields["PBSFenceDesc"])?$record->$fields["PBSFenceDesc"]:null,
                                        isset($fields["PBSFence"])?$record->$fields["PBSFence"]:null,
                                        isset($fields["PBSFoundationType"])?$record->$fields["PBSFoundationType"]:null,
                                        isset($fields["PBSParkingDesc"])?$record->$fields["PBSParkingDesc"]:null,
                                        isset($fields["PBSPool"])?$record->$fields["PBSPool"]:null,
                                        isset($fields["PBSRoof"])?$record->$fields["PBSRoof"]:null,
                                        isset($fields["PBSStyle"])?$record->$fields["PBSStyle"]:null,
                                        isset($fields["PBSWaterfrontDesc"])?$record->$fields["PBSWaterfrontDesc"]:null,
                                        isset($fields["PBSWaterfront"])?$record->$fields["PBSWaterfront"]:null,
                                        isset($fields["PBSAppliances"])?$record->$fields["PBSAppliances"]:null,
                                        isset($fields["PBSCooling"])?$record->$fields["PBSCooling"]:null,
                                        isset($fields["PBSEquipment"])?$record->$fields["PBSEquipment"]:null,
                                        isset($fields["PBSFirePlaceNum"])?$record->$fields["PBSFirePlaceNum"]:null,
                                        isset($fields["PBSFlooring"])?$record->$fields["PBSFlooring"]:null,
                                        isset($fields["PBSHeating"])?$record->$fields["PBSHeating"]:null,
                                        isset($fields["PBSInteriorFeat"])?$record->$fields["PBSInteriorFeat"]:null,
                                        isset($fields["PBSRoomNum"])?$record->$fields["PBSRoomNum"]:null,
                                        isset($fields["PBSOtherRooms"])?$record->$fields["PBSOtherRooms"]:null,
                                        isset($fields["PBSWaterHeater"])?$record->$fields["PBSWaterHeater"]:null,
                                        isset($fields["PBSDisclosure"])?$record->$fields["PBSDisclosure"]:null,
                                        isset($fields["PBSNewConstruction"])?$record->$fields["PBSNewConstruction"]:null,
                                        isset($fields["PBSOwnershipType"])?$record->$fields["PBSOwnershipType"]:null,
                                        isset($fields["PBSSewer"])?$record->$fields["PBSSewer"]:null,
                                        isset($fields["PBSTaxes"])?$record->$fields["PBSTaxes"]:null,
                                        isset($fields["PBSWater"])?$record->$fields["PBSWater"]:null,
                                        isset($fields["PBSZoning"])?$record->$fields["PBSZoning"]:null,
                                        isset($fields["PBSListingOffice"])?$record->$fields["PBSListingOffice"]:null,
                                        isset($fields["PBSOwnerId"])?$record->$fields["PBSOwnerId"]:null,
                                        isset($fields["PBSOwnerName"])?$record->$fields["PBSOwnerName"]:null,
                                        isset($fields["PBSOwnerTitle"])?$record->$fields["PBSOwnerTitle"]:null,
                                        isset($fields["PBSOwnerEmail"])?$record->$fields["PBSOwnerEmail"]:null,
                                        isset($fields["PBSOwnerPhone"])?$record->$fields["PBSOwnerPhone"]:null,
                                        isset($fields["PBSOwnerMobile"])?$record->$fields["PBSOwnerMobile"]:null,
                                        isset($fields["PBSOwnerFax"])?$record->$fields["PBSOwnerFax"]:null,
                                        isset($fields["PBSOwnerCompany"])?$record->$fields["PBSOwnerCompany"]:null,
                                        isset($fields["PBSLeaseType"])?$record->$fields["PBSLeaseType"]:null,
                                        isset($fields["PBSPropertySubType"])?$record->$fields["PBSPropertySubType"]:null,
                                        isset($fields["PBSMinContiguous"])?$record->$fields["PBSMinContiguous"]:null,
                                        isset($fields["PBSMaxContiguous"])?$record->$fields["PBSMaxContiguous"]:null
                                        );

                                    var_dump($sql);

                                    $result = $wpdb->query($sql);

                                    if(FALSE==$result)
                                    {
                                        $err=$wpdb->last_error;
                                    }
                                }
                            }
                            //Save any images
                            if(isset($property->pba__PropertyMedia__r))
                            {
                                //Delete all images first
                                $sql="DELETE FROM {$wpdb->prefix}i5Images where propertyid=%s";

                                $sql=$wpdb->prepare($sql,$property->Id);

                                $result = $wpdb->query($sql);

                                foreach($property->pba__PropertyMedia__r->records as $media)
                                {
                                    $sql = "INSERT INTO {$wpdb->prefix}i5Images(url,propertyid) VALUES(%s,%s)";

                                    $sql=$wpdb->prepare($sql,$media->pba__URL__c,$property->Id);

                                    $result = $wpdb->query($sql);

                                    if(FALSE==$result)
                                    {
                                        $err=$wpdb->last_error;
                                    }
                                }
                            }

                            //TEMP Remove
                            //$done=true;
                            //return;
                            }
                            catch(Exception $ex)
                            {
                                //todo log error
                            }
                        }
                    }
                    else
                        $done=true;
                }

                $settings["LastRun"]=$currRun;
                $settings["Status"]="Idle";
                update_option("WebListing_Settings",$settings);
                self::$isRunning=false;
            }
            }
            catch(Exception $ex)
            {
                //todo log error
                $settings["Status"]="Idle";
                update_option("WebListing_Settings",$settings);
                self::$isRunning=false;
            }
        }
    }
    public static function removeExpired()
    {
        try
        {
            global $wpdb;
            $settings=get_option('WebListing_Settings');

            $end=date("Y-m-d\Th:i:s",strtotime("+2 days"));
            $start=date("Y-m-d\Th:i:s",strtotime("-29 days"));
            $lastRun=date("Y-m-d\Th:i:s",strtotime("January 1 1900"));

            if(isset($settings["LastRun"]))
                $lastRun=strtotime("-2 days", strtotime($settings["LastRun"]));

            $query="start=" . urlencode($start . "Z") . "&end=" . urlencode($end . "Z");

            $methodUrl="/services/data/v29.0/sobjects/pba__listing__c/deleted/?" . $query;

            require_once( i5PBIntegration__PLUGIN_DIR . 'class.i5PBIntegration-Functions.php' );
            $results =i5PBIntegration_Functions::GetAPI($settings["OAuth"],$methodUrl);
            $current=0;

            foreach($results->deletedRecords as $del)
            {
                try
                {
                    $current+=1;

                    $settings["Status"]="Processing Deleted Listings " . $current . " of " . sizeof($results->deletedRecords);
                    update_option("WebListing_Settings",$settings);

                    //Get the listing
                    $sql="SELECT propertyid from {$wpdb->prefix}i5listings where pbid=%s";
                    $sql=$wpdb->prepare($sql,$del->id);

                    $list = $wpdb->get_row($sql);

                    if($list!=null)
                    {
                        //Remove listings
                        $sql="DELETE FROM {$wpdb->prefix}i5listings where pbid=%s";

                        $sql=$wpdb->prepare($sql,$del->id);

                        $result = $wpdb->query($sql);

                        //Check to see if there are any more listings with the same property id
                        $sql="SELECT count(*) FROM {$wpdb->prefix}i5listings where propertyid=%s";

                        $sql=$wpdb->prepare($sql,$list->propertyid);

                        $ttl = $wpdb->get_var($sql);

                        if($ttl==0)
                        {
                            //Remove images
                            $sql="DELETE FROM {$wpdb->prefix}i5Images where propertyid=%s";

                            $sql=$wpdb->prepare($sql,$list->propertyid);

                            $result = $wpdb->query($sql);
                        }
                    }
                }
                catch(Exception $ex)
                {
                    $err=$ex;
                }
            }

            $current=0;

            //Remove Delete Condition listings
            if(isset($settings["DELETE"]))
            {
                $where=" where LastModifiedDate>" . $lastRun . "Z and ";
                $first=true;

                if(isset($settings["DELETE"]["Condition1"]))
                {
                    $values = explode(";",$settings["DELETE"]["Condition1"]["Value"]);

                    foreach($values as $value)
                    {
                        if(!$first)
                            $where .= " or ";
                        else
                        {
                            $where .= "(";
                            $first=false;
                        }

                        $where .= $settings["DELETE"]["Condition1"]["Field"] . $settings["DELETE"]["Condition1"]["Type"] . ((strtolower($value)=="true"||strtolower($value)=="false")?"":"'") . $value . ((strtolower($value)=="true"||strtolower($value)=="false")?"":"'");
                    }

                    $where .= ")";
                }

                $first=true;

                if(isset($settings["DELETE"]["Condition2"]))
                {
                    $values = explode(";",$settings["DELETE"]["Condition2"]["Value"]);

                    if(!$first)
                        $where .= " or ";
                    else
                    {
                        $where .= " and (";
                        $first=false;
                    }

                    foreach($values as $value)
                        $where .= $settings["DELETE"]["Condition2"]["Field"] . $settings["DELETE"]["Condition2"]["Type"] . ((strtolower($value)=="true"||strtolower($value)=="false")?"":"'") . $value . ((strtolower($value)=="true"||strtolower($value)=="false")?"":"'");

                    $where .= ")";
                }

                $first=true;

                if(isset($settings["DELETE"]["Condition3"]))
                {
                    $values = explode(";",$settings["DELETE"]["Condition3"]["Value"]);

                    if(!$first)
                        $where .= " or ";
                    else
                    {
                        $where .= " and (";
                        $first=false;
                    }

                    foreach($values as $value)
                        $where .= $settings["DELETE"]["Condition3"]["Field"] . $settings["DELETE"]["Condition3"]["Type"] . ((strtolower($value)=="true"||strtolower($value)=="false")?"":"'") . $value . ((strtolower($value)=="true"||strtolower($value)=="false")?"":"'");

                    $where .= ")";
                }

                $query = "select id,pba__property__c from pba__listing__c" . $where;

                $done=false;
                $methodUrl="/services/data/v20.0/query/?q=" . urlencode($query);
                $current=0;

                while(!$done)
                {
                    $results =i5PBIntegration_Functions::GetAPI($settings["OAuth"],$methodUrl);

                    if(isset($results->totalSize) && $results->totalSize>0)
                    {
                        $done=$results->done;

                        if(!$done)
                            $methodUrl=$results->nextRecordsUrl;

                        foreach($results->records as $listing)
                        {
                            $current+=1;

                            $settings["Status"]="Processing Expired Listings " . $current . " of " . $results->totalSize;
                            update_option("WebListing_Settings",$settings);

                            //Delete the listing
                            $sql="DELETE FROM {$wpdb->prefix}i5listings where pbid=%s";

                            $sql=$wpdb->prepare($sql,$listing->Id);

                            $result = $wpdb->query($sql);

                            //Check to see if there are any more listings with this property id
                            $sql="SELECT count(*) FROM {$wpdb->prefix}i5listings where propertyid=%s";

                            $sql=$wpdb->prepare($sql,$listing->pba__Property__c);

                            $ttl = $wpdb->get_var($sql);

                            if($ttl==0)
                            {
                                //Remove images
                                $sql="DELETE FROM {$wpdb->prefix}i5Images where propertyid=%s";

                                $sql=$wpdb->prepare($sql,$del->pba__Property__c);

                                $result = $wpdb->query($sql);
                            }
                        }
                    }
                    else
                        $done=true;
                }
            }

        }
        catch(Exception $ex)
        {
            $err=$ex;
        }
    }
}