<?php
class i5PBIntegration_Listing
{
    private static $initiated = false;
   
    public static function init(){
        if(!self::$initiated){
            self::init_hooks();
            self::$initiated=true;
        }

        wp_register_script( "Listings_Script", WP_PLUGIN_URL.'/i5PBIntegration/js/i5Listings.js', array('jquery') );

        $adminUrl;

        if(post_type_exists("i5agents"))
            $adminUrl="http://" . $_SERVER["HTTP_HOST"] . "/wp-admin/admin-ajax.php";
        else
            $adminUrl=admin_url( 'admin-ajax.php' );

        wp_localize_script( 'Listings_Script', 'i5PBAjax', array( 'ajaxurl' => $adminUrl,'nonce' => wp_create_nonce('i5PBIntegration')));

        wp_enqueue_script( 'Listings_Script' );
    }
    public static function init_hooks(){
        add_action('wp_ajax_queryListings',array('i5PBIntegration_Listing', 'doQuery'));
        add_action('wp_ajax_nopriv_queryListings',array('i5PBIntegration_Listing', 'doQuery'));
        add_action('wp_ajax_autoCompleteListings',array('i5PBIntegration_Listing', 'doAutoCompleteSearch'));
        add_action('wp_ajax_nopriv_autoCompleteListings',array('i5PBIntegration_Listing', 'doAutoCompleteSearch'));
    }
    public static function doAutoCompleteSearch(){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], "i5PBIntegration")) {
            exit("Denied");
        }
        
        global $wpdb;
        $limit=5;

        if(isset($_REQUEST["limit"]) && isset($_REQUEST["limit"])!="")
            $limit=$_REQUEST["limit"];

        $sql = "(select count(*), '' as redirect, 'City' as section,city as result from {$wpdb->prefix}i5listings where city like '" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), url as redirect, 'Address' as section,address as result from {$wpdb->prefix}i5listings where address like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), url as redirect, 'MLSID' as section,mlsid as result from {$wpdb->prefix}i5listings where mlsid like '" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'Zip Code' as section,zipcode as result from {$wpdb->prefix}i5listings where zipcode like '" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'Subdivision' as section,Subdivision as result from {$wpdb->prefix}i5listings where Subdivision like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'School District' as section,schooldistrict as result from {$wpdb->prefix}i5listings where schooldistrict like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'Area' as section,Area as result from {$wpdb->prefix}i5listings where Area like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'School' as section,elementaryschool as result from {$wpdb->prefix}i5listings where elementaryschool like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'School' as section,middleschool as result from {$wpdb->prefix}i5listings where middleschool like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'School' as section,highschool as result from {$wpdb->prefix}i5listings where highschool like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                order by section,result";

        $results = $wpdb->get_results($sql);

        echo json_encode($results);

        die();

    }
    public static function doQuery()
    {
        global $wpdb;

        if ( !wp_verify_nonce( $_REQUEST['nonce'], "i5PBIntegration")) {
            exit("Denied");
        }

        $sql ="select " . str_replace("\'","'",$_REQUEST["query"]) . ",i.url as image from {$wpdb->prefix}i5listings as l left join {$wpdb->prefix}i5Images i on i.id = (select min(id) from {$wpdb->prefix}i5Images ii where ii.propertyid=l.propertyid)";

        if(isset($_REQUEST["where"]) && $_REQUEST["where"]!="")
            $sql .= " where " . str_replace("\'","'",$_REQUEST["where"]);

        if(isset($_REQUEST["group"]) && $_REQUEST["group"] !="")
            $sql .= " group by " . $_REQUEST["group"];

        if(isset($_REQUEST["order"]) && $_REQUEST["order"]!="")
            $sql .= " order by " . $_REQUEST["order"];

        if(isset($_REQUEST["limit"]) && $_REQUEST["limit"]!="")
            $sql .= " limit " . $_REQUEST["limit"];
        //echo $sql;
        $results = $wpdb->get_results($sql);
                
        echo json_encode($results);

        die();
    }
}