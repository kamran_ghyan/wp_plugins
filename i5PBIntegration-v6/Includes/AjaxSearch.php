<?php

function autocomplete_search(){
    global $wpdb;
    $data = array();
    $term = strtolower($_GET['term']);
    // prepare query to fetch data
    $sql = "SELECT title FROM {$wpdb->prefix}i5listings WHERE (title LIKE '%" . $_GET['term'] . "%') OR (mlsid LIKE '%" . $_GET['term'] . "%') OR (description LIKE '%" . $_GET['term'] . "%')";
    $search_result = $wpdb->get_results($sql, ARRAY_A);
    foreach ($search_result as $key => $row ) {
        $data[] = $row['title'];
    }
    //$response = json_encode($search_result);
    $suggest = array(
        'abc',
        'def',
        'ghi'
    );
    echo json_encode($data);
    die;
}
add_action('wp_ajax_autocomplete_search', 'autocomplete_search');
add_action('wp_ajax_nopriv_autocomplete_search', 'autocomplete_search');
function filter_search(){
    global $wpdb;
    // prepare query to fetch data
    $sql = "SELECT mlsid,recordType,area,subdivision,propertytype,title,status,l.url,latitude,longitude,i.url AS image,mlsid,bedrooms,address,city,state,zipCode,description,price,fullBaths,halfBaths,size,agentFirstName,agentEmail,agentMLSId FROM {$wpdb->prefix}i5listings l\r\n\tleft join {$wpdb->prefix}i5Images i on i.id = (SELECT min(id) FROM {$wpdb->prefix}i5Images ii WHERE ii.propertyid=l.propertyid)";
    $where = "";
    $where_limit = "";
    $order = "";
    $limit = "";
    $where .= " WHERE ( listingType LIKE '%sale%' ) AND ";
    $where_limit .= " WHERE ( listingType LIKE '%sale%' ) AND ";
    //$order .= " ORDER BY ";
    // Ajax pagination
    $PropertyTypes = array();
    $TTLResults = 0;
    $start_page = 0;
    $end_page = 30;
    if (isset($_POST['data_form'])) {
        $form_data = explode("&", $_POST['data_form']);

        foreach ($form_data as $data) {
            $perf_key_values = explode("=", $data);
            $key = urldecode($perf_key_values[0]);
            $values = urldecode($perf_key_values[1]);
            if ($key == "auto_complete_term"){
                if (!empty($values)) {                 
                        $address_term = 'title LIKE "%' . $values . '%" AND '; 
                        $city_term = explode(",", $values);
                        $c_term = 'city LIKE "' . $city_term[1] . '%" AND ';
                        $zip_term = explode(" ", $city_term[2]);
                        $z_term = 'zipCode LIKE "' . $zip_term[2] . '%" '; 
                        $search_top_term = ' AND ' . $address_term . $c_term . $z_term;
                }
            }
            if ($key == "type_Office" || $key == "type_Office/Retail" || $key == "type_Multi-Family" || $key == "type_Retail" || $key == "type_Hospitality" || $key == "type_Commercial" || $key == "type_Land" || $key == "type_Industrial" || $key == "type_Condo" || $key == "type_Business Only" || $key == "type_Special Purpose" || $key == "type_Warehouse/Shop" || $key == "type_Other" || $key == "type_Office/Warehouse") {
                if (!empty($values)) {
                    if ($values == 'on') {
                        $term_search = explode("_", $key);
                        $property_type .= 'propertytype ="' . $term_search[1] . '" OR ';
                    }
                }
            }
            if ($key == "type_Anchorage" || $key == "type_Eagle River" || $key == "type_Chugiak" || $key == "type_Wasilla" || $key == "type_Palmer") {
                if (!empty($values)) {
                    if ($values == 'on') {
                        $term_search = explode("_", $key);
                        $city_type .= 'city ="' . $term_search[1] . '" OR ';
                    }
                }
            }
            if ($key == "type_smin" || $key == "type_smax") {
                if (!empty($values)) {
                    if ($key == "type_smin") {
                        $min_size = $values;
                    }
                    if ($key == "type_smax") {
                        $max_size = $values;
                    }
                    $size_range = "AND size BETWEEN " . $min_size . " AND " . $max_size;
                }
            }
            if ($key == "type_nnn") {
                if (!empty($values)) {
                    $add_option = " custom8 = 1";
                }
            }
            if ($key == "type_pmin" || $key == "type_pmax") {
                if (!empty($values)) {
                    if ($key == "type_pmin") {
                        $min_price = $values;
                    }
                    if ($key == "type_pmax") {
                        $max_price = $values;
                    }
                    $price_range = " AND price BETWEEN " . $min_price . " AND " . $max_price;
                }
            }
            if ($key == "sort") {
                if (!empty($values)) {
                    $term_sort = explode("_", $values);
                    if ($term_sort[0] == "alpha") {
                       // $order_by = " ORDER BY title " . $term_sort[1];
                        $order_by = " ORDER BY CAST(title AS UNSIGNED), title " . $term_sort[1];
                    }
                    if ($term_sort[0] == "price") {
                        $order_by = " ORDER BY price " . $term_sort[1];
                    }
                    if ($term_sort[0] == "size") {
                        $order_by = " ORDER BY size " . $term_sort[1];
                    }
                    $order .= $order_by;
                } else {
                    $order .= " ORDER BY created";
                }
            }
            if ($key == "pagi") {
                if (!empty($values)) {
                    $start_page = $values;
                    $end_page = $values + 30;
                    $limit = " LIMIT " . $start_page . ", 30";
                } else{
                    $limit = " LIMIT 0, 30 ";
                }
            }
            if ($key == "pagi_p") {
                if (!empty($values)) {
                    $start_page = $values - 30;
                    $end_page = $values;
                    $limit = " LIMIT " . $start_page . ", 30";
                }
            }
        }
        /*$limit .= " LIMIT 0, 30";*/
        //$city = substr($city_type, 0, -4);
        $city_ = " AND (" . substr($city_type, 0, -4) . ")";
        if(!empty($add_option)){
            $where .= "(" . substr($property_type, 0, -4) . $size_range . $price_range . $search_top_term . ")" . $city_ . "AND (" . $add_option . ")" . $order . $limit;
            $where_limit .= "(" .substr($property_type, 0, -4) . $size_range . $price_range . ")" . $city_ . " AND (" . $add_option . ")" . $order . ' LIMIT 0, 30';
        } else {
            $where .= "(" . substr($property_type, 0, -4)  . $size_range . $price_range . $search_top_term . ")" . $city_ .  $order . $limit;
            $where_limit .= "(" . substr($property_type, 0, -4) . $size_range . $price_range . ")" . $city_ . $order . ' LIMIT 0, 30';
        }

    }
    //echo $sql . $where;
    //die();
    $search_result = $wpdb->get_results($sql . $where);
    // Ajax pagination
    $sqlCount = "SELECT count(*) from {$wpdb->prefix}i5listings";
    $TTLResults = $wpdb->get_var($sqlCount . $where_limit);
    //echo $sqlCount . $where_limit;
    // Pagination html
    if (!empty($TTLResults)) {
        $pagination = '<div class="searchFilter ajax_pagi" id="pagi_block">' . pagination_ajax($start_page, $end_page, $TTLResults, "next", "prev", "sale") . '</div>';
        $pagination_bottom = '<div class="searchFilter" id="pagi_block">' . pagination_ajax($start_page, $end_page, $TTLResults, "next", "prev", "sale") . '</div>';
        $html = "";
        $html .= '<div class="ajax-loader"></div>';
        $html .= $pagination;
        foreach ($search_result as $result) {
            $html .= '<div class="row search-item">
            	<div class="col-md-2 col-xs-2  no-padding">
                	                            <a class="thumb" href="/property-details/' . $result->url . '" target="_top">
                                <img alt="' . $result->title . '" src="' . $result->image . '" height="76px" width="90px">
                            </a>
                                            </div>
                <div class="col-md-8 col-xs-8">
                	<span class="list-title">
                    <a href="/property-details/' . $result->url . '" target="_top">' . $result->title . '</a>
                    </span>
                    <span class="list-address"><i class="fa fa-map-marker" aria-hidden="true"></i> ' . $result->city . ', ' . $result->state . '</span>
                    <span class="list-description">' . $result->description . '</span>
					<span class="list-description list-description-responsive">' . substr($result->description, 0, 100) . '</span>
                </div>
                <div class="col-md-2 col-xs-2  no-padding">
                	<span class="list-price">$' . number_format($result->price, 0) . '</span>
                    <span class="list-size">' . $result->size . ' Sqft</span>
                    <span class="list-type">' . $result->propertytype . '</span>
                    <div class="row" style="margin-top:20px;">
                        <div class="col-md-offset-5">
                            <img style=" height:50px; width:50px; " alt="Alaska MLS" title="Alaska MLS" src=' . i5PBIntegration__PLUGIN_URL .'images/idx-large.gif />
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 no-padding">
                    <span class="list-links">
                    <a href="#" class="map" target="_top"><i class="fa fa-map-marker" aria-hidden="true"></i> Map</a>
 					<a href="/property-details/' . $result->url . '" class="more-details" target="_top">More Details <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    </span>
                </div>
            </div>';
        }
        $html .= $pagination_bottom;
    } else {
        $html .= '<div class="ajax-loader"></div>';
        $html .= '<h4 class="text-center"><i class="fa fa-search" aria-hidden="true"></i><br />No Result Found!</h4>';
    }
    echo $html;
    die;
}
function filter_search_map(){
    global $wpdb;
    // prepare query to fetch data
    $sql = "SELECT mlsid,recordType,area,subdivision,propertytype,title,status,l.url,latitude,longitude,i.url AS image,mlsid,bedrooms,address,city,state,zipCode,description,price,fullBaths,halfBaths,size FROM {$wpdb->prefix}i5listings l\r\n\tleft join {$wpdb->prefix}i5Images i on i.id = (SELECT min(id) FROM {$wpdb->prefix}i5Images ii WHERE ii.propertyid=l.propertyid)";
    $where = "";
    $where_limit = "";
    $order = "";
    $limit = "";
    $where .= " WHERE ( listingType LIKE '%sale%' ) AND ";
    $where_limit .= " WHERE ( listingType LIKE '%sale%' ) AND ";
    //$order .= " ORDER BY ";
    // Ajax pagination
    $PropertyTypes = array();
    $TTLResults = 0;
    $start_page = 0;
    $end_page = 30;
    if (isset($_POST['data_form'])) {
        $form_data = explode("&", $_POST['data_form']);
        foreach ($form_data as $data) {
            $perf_key_values = explode("=", $data);
            $key = urldecode($perf_key_values[0]);
            $values = urldecode($perf_key_values[1]);
            if ($key == "type_Office" || $key == "type_Office/Retail" || $key == "type_Multi-Family" || $key == "type_Retail" || $key == "type_Hospitality" || $key == "type_Commercial" || $key == "type_Land" || $key == "type_Industrial" || $key == "type_Condo" || $key == "type_Business Only" || $key == "type_Special Purpose" || $key == "type_Warehouse/Shop" || $key == "type_Other" || $key == "type_Office/Warehouse") {
                if (!empty($values)) {
                    if ($values == 'on') {
                        $term_search = explode("_", $key);
                        $property_type .= 'propertytype ="' . $term_search[1] . '" OR ';
                    }
                }
            }
            if ($key == "type_Anchorage" || $key == "type_Eagle River" || $key == "type_Chugiak" || $key == "type_Wasilla" || $key == "type_Palmer") {
                if (!empty($values)) {
                    if ($values == 'on') {
                        $term_search = explode("_", $key);
                        $city_type .= 'city ="' . $term_search[1] . '" OR ';
                    }
                }
            }
            if ($key == "type_smin" || $key == "type_smax") {
                if (!empty($values)) {
                    if ($key == "type_smin") {
                        $min_size = $values;
                    }
                    if ($key == "type_smax") {
                        $max_size = $values;
                    }
                    $size_range = "AND size BETWEEN " . $min_size . " AND " . $max_size;
                }
            }
            if ($key == "type_nnn") {
                if (!empty($values)) {
                    $add_option = " custom8 = 1";
                }
            }
            if ($key == "type_pmin" || $key == "type_pmax") {
                if (!empty($values)) {
                    if ($key == "type_pmin") {
                        $min_price = $values;
                    }
                    if ($key == "type_pmax") {
                        $max_price = $values;
                    }
                    $price_range = " AND price BETWEEN " . $min_price . " AND " . $max_price;
                }
            }
            if ($key == "sort") {
                if (!empty($values)) {
                    $term_sort = explode("_", $values);
                    if ($term_sort[0] == "alpha") {
                        $order_by = " ORDER BY CAST(title AS UNSIGNED), title " . $term_sort[1];
                    }
                    if ($term_sort[0] == "price") {
                        $order_by = " ORDER BY price " . $term_sort[1];
                    }
                    if ($term_sort[0] == "size") {
                        $order_by = " ORDER BY size " . $term_sort[1];
                    }
                    $order .= $order_by;
                } else {
                    $order .= " ORDER BY created";
                }
            }
            if ($key == "pagi") {
                if (!empty($values)) {
                    $start_page = $values;
                    $end_page = $values + 30;
                    $limit = " LIMIT " . $start_page . ", 30";
                }
            }
            if ($key == "pagi_p") {
                if (!empty($values)) {
                    $start_page = $values - 30;
                    $end_page = $values;
                    $limit = " LIMIT " . $start_page . ", 30";
                }
            }
        }
        /*$limit .= " LIMIT 0, 30";*/
        $city_ = " AND (" . substr($city_type, 0, -4) . ")";
        if(!empty($add_option)){
            $where .= "(" . substr($property_type, 0, -4) . $size_range . $price_range . $search_top_term . ")" . $city_ . "AND (" . $add_option . ")" . $order . $limit;
            $where_limit .= "(" .substr($property_type, 0, -4) . $size_range . $price_range . ")" . $city_ . " AND (" . $add_option . ")" . $order . ' LIMIT 0, 30';
        } else {
            $where .= "(" . substr($property_type, 0, -4)  . $size_range . $price_range . $search_top_term . ")" . $city_ .  $order . $limit;
            $where_limit .= "(" . substr($property_type, 0, -4) . $size_range . $price_range . ")" . $city_ . $order . ' LIMIT 0, 30';
        }
    }
    //echo $sql . $where;
    //die();
    $search_result = $wpdb->get_results($sql . $where);
    // Ajax pagination
    $sqlCount = "SELECT count(*) from {$wpdb->prefix}i5listings";
    $TTLResults = $wpdb->get_var($sqlCount . $where_limit);
    //echo $sqlCount . $where_limit;
    // Pagination html
    if (!empty($TTLResults)) {
        $html_pagination = '<div class="searchFilter" id="pagi_block">' . pagination_ajax($start_page, $end_page, $TTLResults, "next", "prev", "salemap") . '</div>';
        $html = "";
        $html .= '<div class="ajax-loader"></div>';
        $html .= '<div id="map-listing-inner">';
        foreach ($search_result as $result) {
            $html .= '<div onmouseover="openInfoWindow("' . $result->mlsid . '")" style="cursor:pointer;" class="row map-sidebar-item">
					<div class="col-md-5" style="padding-left:5px;">
						<a class="thumb" href="/property-details/' . $result->url . '" target="_top">
							<img alt="' . $result->title . '" src="' . $result->image . '" height="76px" width="90px">
						</a>
					</div>
					<div class="col-md-7 no-padding">
						<span class="list-title">
							<a href="/property-details/' . $result->url . '" target="_top">' . substr($result->title, 0, 20) . '</a>
						</span>
						<span class="list-address"> ' . $result->city . ', ' . $result->state . '</span>
						<span class="list-price"><strong>$ ' . number_format($result->price, 0) . ' </strong></span>
						<span class="list-size"> ' . $result->size . ' Sqft</span>
						<span class="list-type">' . $result->propertytype . '</span>
						
					</div>
             	</div>
			 ';
        }
        $html .= '</div>';
        $html .= $html_pagination;
    } else {
        $html .= '<div class="ajax-loader"></div>';
        $html .= '<h4 class="text-center"><i class="fa fa-search" aria-hidden="true"></i><br />No Result Found!</h4>';
    }
    echo $html;
    die;
}
function filter_search_lease(){
    global $wpdb;
    // prepare query to fetch data
    $sql = "SELECT mlsid,recordType,area,subdivision,propertytype,title,status,l.url,latitude,longitude,i.url AS image,mlsid,bedrooms,address,city,state,zipCode,description,price,fullBaths,halfBaths,size FROM {$wpdb->prefix}i5listings l\r\n\tleft join {$wpdb->prefix}i5Images i on i.id = (SELECT min(id) FROM {$wpdb->prefix}i5Images ii WHERE ii.propertyid=l.propertyid)";
    $where = "";
    $where_limit = "";
    $order = "";
    $limit = "";
    $where .= " WHERE (listingType = 'lease' OR listingType = 'commercial lease') AND ";
    $where_limit .= " WHERE (listingType = 'lease' OR listingType = 'commercial lease') AND ";
    //$order .= " ORDER BY ";
    // Ajax pagination
    $PropertyTypes = array();
    $TTLResults = 0;
    $start_page = 0;
    $end_page = 30;
    if (isset($_POST['data_form'])) {
        $form_data = explode("&", $_POST['data_form']);
        foreach ($form_data as $data) {
            $perf_key_values = explode("=", $data);
            $key = urldecode($perf_key_values[0]);
            $values = urldecode($perf_key_values[1]);
            if ($key == "type_Office" || $key == "type_Office/Retail" || $key == "type_Multi-Family" || $key == "type_Retail" || $key == "type_Hospitality" || $key == "type_Commercial" || $key == "type_Land" || $key == "type_Industrial" || $key == "type_Condo" || $key == "type_Business Only" || $key == "type_Special Purpose" || $key == "type_Warehouse/Shop" || $key == "type_Other" || $key == "type_Office/Warehouse") {
                if (!empty($values)) {
                    if ($values == 'on') {
                        $term_search = explode("_", $key);
                        $property_type .= 'propertytype ="' . $term_search[1] . '" OR ';
                    }
                }
            }
            if ($key == "type_Anchorage" || $key == "type_Eagle River" || $key == "type_Chugiak" || $key == "type_Wasilla" || $key == "type_Palmer") {
                if (!empty($values)) {
                    if ($values == 'on') {
                        $term_search = explode("_", $key);
                        $city_type .= 'city ="' . $term_search[1] . '" OR ';
                    }
                }
            }
            if ($key == "type_spmin" || $key == "type_spmax") {
                if (!empty($values)) {
                    if ($key == "type_spmin") {
                        $min_space = $values;
                    }
                    if ($key == "type_spmax") {
                        $max_space = $values;
                    }
                    $space_range = "AND size BETWEEN " . $min_space . " AND " . $max_space;
                }
            }
            if ($key == "type_sublease") {
                if (!empty($values)) {
                    $add_option = " leaseType = ''";
                }
            }
            if ($key == "type_rmin" || $key == "type_rmax") {
                if (!empty($values)) {
                    if ($key == "type_rmin") {
                        $min_rate = $values;
                    }
                    if ($key == "type_rmax") {
                        $max_rate = $values;
                    }
                    $rate_range = " AND price BETWEEN " . $min_rate . " AND " . $max_rate;
                }
            }
            if ($key == "sort") {
                if (!empty($values)) {
                    $term_sort = explode("_", $values);
                    if ($term_sort[0] == "alpha") {
                        $order_by = " ORDER BY CAST(title AS UNSIGNED), title " . $term_sort[1];
                    }
                    if ($term_sort[0] == "price") {
                        $order_by = " ORDER BY price " . $term_sort[1];
                    }
                    if ($term_sort[0] == "size") {
                        $order_by = " ORDER BY size " . $term_sort[1];
                    }
                    $order .= $order_by;
                } else {
                    $order .= " ORDER BY created";
                }
            }
            if ($key == "pagi") {
                if (!empty($values)) {
                    $start_page = $values;
                    $end_page = $values + 30;
                    $limit = " LIMIT " . $start_page . ", 30";
                }
            }
            if ($key == "pagi_p") {
                if (!empty($values)) {
                    $start_page = $values - 30;
                    $end_page = $values;
                    $limit = " LIMIT " . $start_page . ", 30";
                }
            }
        }
        /*$limit .= " LIMIT 0, 30";*/
        /*if(!empty($add_option)){
            $where .= "(" .substr($property_type, 0, -4) . $space_range . $rate_range . ") AND (" . $add_option . ")" . $order . $limit;
            $where_limit .= "(" .substr($property_type, 0, -4) . $space_range . $rate_range . ") AND (" . $add_option . ")" . $order . ' LIMIT 0, 30';
        } else {
            $where .= '(' . substr($property_type, 0, -4) . $space_range . $rate_range . ')' . $order . $limit;
            $where_limit .= '(' . substr($property_type, 0, -4) . $space_range . $rate_range . ')' . $order . ' LIMIT 0, 30';
        }*/

        /*$limit .= " LIMIT 0, 30";*/
        $city_ = " AND (" . substr($city_type, 0, -4) . ")";
        if(!empty($add_option)){
            $where .= "(" . substr($property_type, 0, -4) . $space_range . $rate_range . $search_top_term . ")" . $city_ . "AND (" . $add_option . ")" . $order . $limit;
            $where_limit .= "(" .substr($property_type, 0, -4) . $space_range . $rate_range . ")" . $city_ . " AND (" . $add_option . ")" . $order . ' LIMIT 0, 30';
        } else {
            $where .= "(" . substr($property_type, 0, -4)  . $space_range . $rate_range . $search_top_term . ")" . $city_ .  $order . $limit;
            $where_limit .= "(" . substr($property_type, 0, -4) . $space_range . $rate_range . ")" . $city_ . $order . ' LIMIT 0, 30';
        }
        /*$limit .= " LIMIT 0, 30";
        $where .= '(' . substr($property_type, 0, -4) . $space_range . $rate_range . ')' . $order . $limit;
        $where_limit .= '(' . substr($property_type, 0, -4) . $space_range . $rate_range . ')' . $order . ' LIMIT 0, 30';*/
    }
    $sql . $where;
    $search_result = $wpdb->get_results($sql . $where);
    // Ajax pagination
    $sqlCount = "SELECT count(*) from {$wpdb->prefix}i5listings";
    $sqlCount . $where_limit;
    $TTLResults = $wpdb->get_var($sqlCount . $where_limit);
    //echo $sqlCount . $where_limit;
    // Pagination html
    if (!empty($TTLResults)) {
        $html_pagination = '<div class="searchFilter ajax_pagi" id="pagi_block">' . pagination_ajax($start_page, $end_page, $TTLResults, "next", "prev", "lease") . '</div>';
        $html_pagination_bottom = '<div class="searchFilter" id="pagi_block">' . pagination_ajax($start_page, $end_page, $TTLResults, "next", "prev", "lease") . '</div>';
        $html = "";
        $html .= '<div class="ajax-loader"></div>';
        $html .= $html_pagination;
        foreach ($search_result as $result) {
            $html .= '<div class="row search-item">
            	<div class="col-md-2  no-padding">
                	                            <a class="thumb" href="/property-details/' . $result->url . '" target="_top">
                                <img alt="' . $result->title . '" src="' . $result->image . '" height="76px" width="90px">
                            </a>
                                            </div>
                <div class="col-md-8">
                	<span class="list-title">
                    <a href="/property-details/' . $result->url . '" target="_top">' . $result->title . '</a>
                    </span>
                    <span class="list-address"><i class="fa fa-map-marker" aria-hidden="true"></i> ' . $result->city . ', ' . $result->state . '</span>
                    <span class="list-description">' . $result->description . '</span>
                </div>
                <div class="col-md-2  no-padding">
                	<span class="list-price">$' . number_format($result->price, 0) . '</span>
                    <span class="list-size">' . $result->size . ' Sqft</span>
                    <span class="list-type">' . $result->propertytype . '</span>
                    <div class="row" style="margin-top:20px;">
                        <div class="col-md-offset-5">
                            <img style=" height:50px; width:50px; " alt="Alaska MLS" title="Alaska MLS" src=' . i5PBIntegration__PLUGIN_URL .'images/idx-large.gif />
                        </div>
                    </div>
                </div>
                <div class="col-md-12 no-padding">

                    <span class="list-links">                    	
                    
                    <a href="#" class="map" target="_top"><i class="fa fa-map-marker" aria-hidden="true"></i> Map</a>
 					<a href="/property-details/' . $result->url . '" class="more-details" target="_top">More Details <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    </span>
                </div>
            </div>';
        }
        $html .= $html_pagination_bottom;
    } else {
        $html .= '<div class="ajax-loader"></div>';
        $html .= '<h4 class="text-center"><i class="fa fa-search" aria-hidden="true"></i><br />No Result Found!</h4>';
    }
    echo $html;
    die;
}
function filter_search_lease_map(){
    global $wpdb;
    // prepare query to fetch data
    $sql = "SELECT mlsid,recordType,area,subdivision,propertytype,title,status,l.url,latitude,longitude,i.url AS image,mlsid,bedrooms,address,city,state,zipCode,description,price,fullBaths,halfBaths,size FROM {$wpdb->prefix}i5listings l\r\n\tleft join {$wpdb->prefix}i5Images i on i.id = (SELECT min(id) FROM {$wpdb->prefix}i5Images ii WHERE ii.propertyid=l.propertyid)";
    $where = "";
    $where_limit = "";
    $order = "";
    $limit = "";
    $where .= " WHERE (listingType = 'lease' OR listingType = 'commercial lease') AND ";
    $where_limit .= " WHERE (listingType = 'lease' OR listingType = 'commercial lease') AND ";
    //$order .= " ORDER BY ";
    // Ajax pagination
    $PropertyTypes = array();
    $TTLResults = 0;
    $start_page = 0;
    $end_page = 30;
    if (isset($_POST['data_form'])) {
        $form_data = explode("&", $_POST['data_form']);
        foreach ($form_data as $data) {
            $perf_key_values = explode("=", $data);
            $key = urldecode($perf_key_values[0]);
            $values = urldecode($perf_key_values[1]);
            if ($key == "type_Office" || $key == "type_Office/Retail" || $key == "type_Multi-Family" || $key == "type_Retail" || $key == "type_Hospitality" || $key == "type_Commercial" || $key == "type_Land" || $key == "type_Industrial" || $key == "type_Condo" || $key == "type_Business Only" || $key == "type_Special Purpose" || $key == "type_Warehouse/Shop" || $key == "type_Other" || $key == "type_Office/Warehouse") {
                if (!empty($values)) {
                    if ($values == 'on') {
                        $term_search = explode("_", $key);
                        $property_type .= 'propertytype ="' . $term_search[1] . '" OR ';
                    }
                }
            }
            if ($key == "type_Anchorage" || $key == "type_Eagle River" || $key == "type_Chugiak" || $key == "type_Wasilla" || $key == "type_Palmer") {
                if (!empty($values)) {
                    if ($values == 'on') {
                        $term_search = explode("_", $key);
                        $city_type .= 'city ="' . $term_search[1] . '" OR ';
                    }
                }
            }
            if ($key == "type_spmin" || $key == "type_spmax") {
                if (!empty($values)) {
                    if ($key == "type_spmin") {
                        $min_space = $values;
                    }
                    if ($key == "type_spmax") {
                        $max_space = $values;
                    }
                    $space_range = "AND size BETWEEN " . $min_space . " AND " . $max_space;
                }
            }
            if ($key == "type_sublease") {
                if (!empty($values)) {
                    $add_option = " leaseType = ''";
                }
            }
            if ($key == "type_rmin" || $key == "type_rmax") {
                if (!empty($values)) {
                    if ($key == "type_rmin") {
                        $min_rate = $values;
                    }
                    if ($key == "type_rmax") {
                        $max_rate = $values;
                    }
                    $rate_range = " AND price BETWEEN " . $min_rate . " AND " . $max_rate;
                }
            }
            if ($key == "sort") {
                if (!empty($values)) {
                    $term_sort = explode("_", $values);
                    if ($term_sort[0] == "alpha") {
                        $order_by = " ORDER BY CAST(title AS UNSIGNED), title " . $term_sort[1];
                    }
                    if ($term_sort[0] == "price") {
                        $order_by = " ORDER BY price " . $term_sort[1];
                    }
                    if ($term_sort[0] == "size") {
                        $order_by = " ORDER BY size " . $term_sort[1];
                    }
                    $order .= $order_by;
                } else {
                    $order .= " ORDER BY created";
                }
            }
            if ($key == "pagi") {
                if (!empty($values)) {
                    $start_page = $values;
                    $end_page = $values + 30;
                    $limit = " LIMIT " . $start_page . ", 30";
                }
            }
            if ($key == "pagi_p") {
                if (!empty($values)) {
                    $start_page = $values - 30;
                    $end_page = $values;
                    $limit = " LIMIT " . $start_page . ", " . $end_page;
                }
            }
        }
        /*$limit .= " LIMIT 0, 30";*/
       /* if(!empty($add_option)){
            $where .= "(" .substr($property_type, 0, -4) . $space_range . $rate_range . ") AND (" . $add_option . ")" . $order . $limit;
            $where_limit .= "(" .substr($property_type, 0, -4) . $space_range . $rate_range . ") AND (" . $add_option . ")" . $order . ' LIMIT 0, 30';
        } else {
            $where .= '(' . substr($property_type, 0, -4) . $space_range . $rate_range . ')' . $order . $limit;
            $where_limit .= '(' . substr($property_type, 0, -4) . $space_range . $rate_range . ')' . $order . ' LIMIT 0, 30';
        }*/
        /*$limit .= " LIMIT 0, 30";*/
        $city_ = " AND (" . substr($city_type, 0, -4) . ")";
        if(!empty($add_option)){
            $where .= "(" . substr($property_type, 0, -4) . $space_range . $rate_range . $search_top_term . ")" . $city_ . "AND (" . $add_option . ")" . $order . $limit;
            $where_limit .= "(" .substr($property_type, 0, -4) . $space_range . $rate_range . ")" . $city_ . " AND (" . $add_option . ")" . $order . ' LIMIT 0, 30';
        } else {
            $where .= "(" . substr($property_type, 0, -4)  . $space_range . $rate_range . $search_top_term . ")" . $city_ .  $order . $limit;
            $where_limit .= "(" . substr($property_type, 0, -4) . $space_range . $rate_range . ")" . $city_ . $order . ' LIMIT 0, 30';
        }
    }
    $sql . $where;
    $search_result = $wpdb->get_results($sql . $where);
    // Ajax pagination
    $sqlCount = "SELECT count(*) from {$wpdb->prefix}i5listings";
    $sqlCount . $where_limit;
    $TTLResults = $wpdb->get_var($sqlCount . $where_limit);
    //echo $sqlCount . $where_limit;
    // Pagination html
    if (!empty($TTLResults)) {
        $html_pagination = '<div class="searchFilter" id="pagi_block">' . pagination_ajax($start_page, $end_page, $TTLResults, "next", "prev", "leasemap") . '</div>';
        $html = "";
        $html .= '<div class="ajax-loader"></div>';
        $html .= '<div id="map-listing-inner">';
        foreach ($search_result as $result) {
            $html .= '<div onmouseover="openInfoWindow("' . $result->mlsid . '")" style="cursor:pointer;" class="row map-sidebar-item">
					<div class="col-md-5" style="padding-left:5px;">
						<a class="thumb" href="/property-details/' . $result->url . '" target="_top">
							<img alt="' . $result->title . '" src="' . $result->image . '" height="76px" width="90px">
						</a>
					</div>
					<div class="col-md-7 no-padding">
						<span class="list-title">
							<a href="/property-details/' . $result->url . '" target="_top">' . substr($result->title, 0, 20) . '</a>
						</span>
						<span class="list-address"> ' . $result->city . ', ' . $result->state . '</span>
						<span class="list-price"><strong>$ ' . number_format($result->price, 0) . ' </strong></span>
						<span class="list-size"> ' . $result->size . ' Sqft</span>
						<span class="list-type">' . $result->propertytype . '</span>
						
					</div>
             	</div>
			 ';
        }
        $html .= '</div>';
        $html .= $html_pagination;
    } else {
        $html .= '<div class="ajax-loader"></div>';
        $html .= '<h4 class="text-center"><i class="fa fa-search" aria-hidden="true"></i><br />No Result Found!</h4>';
    }
    echo $html;
    die;
}
// pagination
function pagination_ajax($start, $end, $count, $next, $prev, $type){
    $html = '';
    if ($start <= 1) {
        $html .= '<a onClick="javascript:return false;"><i class="fa fa-angle-left" aria-hidden="true" style="padding-right:15px;"></i></a>';
    } else {
        $html .= '<a onClick="pagination(' . $start . ', \'' . $prev . '\',  \'' . $type . '\');"><i class="fa fa-angle-left" aria-hidden="true" style="padding-right:15px;"></i></a>';
    }
    if ($end > $count) {
        $html .= '<span> ' . $start . ' - ' . $count . '</span> of  ' . $count . ' <a onClick="javascript:return false;"><i class="fa fa-angle-right" aria-hidden="true" style="padding-left:15px;"></i> </a>';
    } else {
        $html .= '<span> ' . $start . ' - ' . $end . '</span> of  ' . $count . ' <a onClick="pagination(' . $end . ', \'' . $next . '\',  \'' . $type . '\');"><i class="fa fa-angle-right" aria-hidden="true" style="padding-left:15px;"></i> </a>';
    }
    return $html;
}
// list view ajax
add_action('wp_ajax_nopriv_filter_search', 'filter_search');
add_action('wp_ajax_filter_search', 'filter_search');
// sale list map view ajax
add_action('wp_ajax_nopriv_filter_search_map', 'filter_search_map');
add_action('wp_ajax_filter_search_map', 'filter_search_map');
// lease list ajax
add_action('wp_ajax_nopriv_filter_search_lease', 'filter_search_lease');
add_action('wp_ajax_filter_search_lease', 'filter_search_lease');
// lease map view list ajax
add_action('wp_ajax_nopriv_filter_search_lease_map', 'filter_search_lease_map');
add_action('wp_ajax_filter_search_lease_map', 'filter_search_lease_map');
function themex_enqeueu_scripts()
{
    wp_enqueue_style('bootstrap-css', i5PBIntegration__PLUGIN_URL . 'css/bootstrap.min.css', array(), '1.1', 'all');
    wp_enqueue_style('property-search3', i5PBIntegration__PLUGIN_URL . 'css/search-results4.css', array(), '1.1', 'all');
    wp_enqueue_script('bootstrap-js', i5PBIntegration__PLUGIN_URL . 'js/bootstrap.min.js');
    wp_enqueue_script('filters-js', i5PBIntegration__PLUGIN_URL . 'js/filters.js');
    wp_enqueue_script('property', i5PBIntegration__PLUGIN_URL . 'js/ajax.js');
    wp_localize_script('property', 'property', array('ajax_url' => admin_url('admin-ajax.php')));
}
add_action('wp_enqueue_scripts', 'themex_enqeueu_scripts');