<style>
.modal {
    max-height: 480px !important;
}
.modal-dialog {
    width: 100% !important;
    margin:0 auto !important;
}
.modal-content {
    box-shadow: none !important;
    background-color: Transparent;
    border: 0 !important;
    width: 100% !important;
    max-height: inherit !important;
}
.modal-content label {
    font-size:12px !important;
    line-height:12px !important;
    font-weight:normal !important;
}
.myAccountTab {
    position:relative;
    cursor:pointer;
}
.myAccountTab a {
    color:#fff;
}
.myAccountSub {
    background-color: #CE1919;
    color: #FFFFFF !important;
    position: absolute;
    top: 50px;
    width: 150px;
    z-index: 10000000;
    display: none;
    right: 0;
}
.myAccountSub ul {
    margin:0;
    padding:0;
}
.myAccountSub ul li {
    display: block;
    list-style-type: none;
    padding: 0 10px;
    background-color:#CE1919;
}
.myAccountSub ul li a {
    color: #FFFFFF !important;
    display: table-row-group;
    float: left;
    line-height: 35px;
    width: 100%;
    text-decoration:none;
    cursor:pointer;
    font-size:14px;
}
.myAccountTab a:hover, .myAccountSub ul li a:hover {
    opacity: 0.8;
    text-decoration:none;
    cursor:pointer;
}

.myAccount:hover .myAccountSub, .myAccountTab:hover .myAccountSub {
    display:block !important;
}
/** hiding main header **/
nav.navbar {
	display:none !important;
}
.topBar {
	width:100%;
	background-color:#e10019;
	color:#fff;
	display:inline-block;
	padding:10px;
}
.searchMenu {
	width:50%;
	float:left;
}
.searchMenu ul {
	float:right;
	list-style-type:none;
	margin:0;
	padding:0;
}
.searchMenu li {
	float: left;
    padding: 0px 30px;
    line-height: 50px;
    font-size: 16px;
    font-weight: normal;
}
.searchHeader {
  backface-visibility: hidden;
  background: none repeat scroll 0 0 #f8f8f8;
  padding: 5px 0;
  z-index: 1000;
  width:100%;
  display:inline-block;
}
.searchResultsPage {
    padding: 0 !important;
}
.fullWidth {
    width: 100% !important;
    display:inline-block;
}
.searchFilters {
  float:left;
  position:relative;
  width:100%;
}

.searchFilters button {
    color: #aaa !important;
    padding: 0;
    display: block;
    border: 0;
    /* float: left; */
    outline: none;
    border-bottom: 1px solid #ccc;
    margin-right: 20px;
    background-color:Transparent;
}
.searchFilters button:hover {
    background-color:Transparent !important;
}
.searchFilters button:hover {
  background-color: #222; /*dark grey*/
}
.searchFilter {
	float:left;
	margin:0 15px 0 0;
}
button.dropbtnMore, button.btnSaveSearch {
    border: 1px solid #ccc;
    padding: 5px 10px !important;
    float: left;
    background-color: #fff;
    margin: 10px 0 0 25px;
    border-radius: 5px;
}
button.btnSaveSearch {
    background-color:#e10019 !important;
    color:#fff !important;
    border:1px solid #aa2425 !important;
}
button.btnSaveSearch:hover {
    background-color:#aa2425 !important;
}
/* search bar css */
.searchBar {
  float:left;
  position: relative;
  width:50%;
}
.searchBar .navbar-brand {
	padding:5px 15px !important;
}
.searchBy {
  text-transform: uppercase;
  padding: 10px 10px;
  border: 1px solid #ccc;
  display: inline-block;
  cursor: pointer;
  position: relative;
}
.searchBy:hover {
  background-color:#ececec;
}
@-webkit-keyframes mymove {
0% {
    opacity: 0;
    -webkit-transform: translateY(-5px);
    transform: translateY(-5px);
}
100% {
    opacity: 1;
    -webkit-transform: translateY(0);
    transform: translateY(0);
}
}
@keyframes mymove
0% {
    opacity: 0;
    -webkit-transform: translateY(-5px);
    transform: translateY(-5px);
}
100% {
    opacity: 1;
    -webkit-transform: translateY(0);
    transform: translateY(0);
}
.ddSearchBy, .ddPrice, .ddType, .ddBeds, .ddBaths, .ddMore, .ddSearchResults, .ddNewConstruction, .ddSort {
  position: absolute;
  top: 52px;
  width: 150px;
  list-style-type: none;
  left: 0;
  border: 1px solid #ccc;
  background-color: #fff;
  -webkit-animation: mymove .3s cubic-bezier(.215,.61,.355,1);
    animation: mymove .3s cubic-bezier(.215,.61,.355,1);
  z-index:1000;
}
.ddSort {
  padding: 10px;
  width: 175px;
  top: 26px;
  text-align:left;
}
.ddType {
  width: 175px;
  padding: 10px;
  left:0;
}
.ddBeds {
  left:197px;
  padding: 10px;
  width:130px;
}
.ddBaths {
	width:130px;
  left:292px;
  padding: 10px;
}
.ddMore {
  padding: 10px;
  width: 400px;
  left: 540px !important;
  top: 45px;
}
.ddMoreFooter .button {
    border: 1px solid #ccc !important;
    border-radius: 3px !important;
    padding: 0 10px !important;
}
.ddMoreFooter .button:hover {
    background-color:#e10019 !important;
    color:#fff !important;
}
.ddNewConstruction {
	left: 391px;
    width: 130px;
    padding:10px;
}
.ddPrice {
	width: 170px !important;
    left: 95px;
}
.ddPrice input {
    width: 67px;
    margin: 5px 0 0 5px;
}
.ddPrice ul {
    margin: 0;
    padding: 0;
}
.ddSearchResults {
    left: 170px;
    width: 575px;s
    padding: 10px;
}
#ddMore label {
    width: 100%;
}
.ddSearchBy:before, .ddPrice:before, .ddType:before, .ddBeds:before, .ddBaths:before, .ddMore:before, .ddSearchResults:before, .ddNewConstruction:before, .ddSort:before {
  content: '';
  width: 10px;
  height: 10px;
  position: absolute;
  top: -5px;
  left: 10px;
  background: #fff;
  -webkit-transform: rotate(135deg);
  -ms-transform: rotate(135deg);
  transform: rotate(135deg);
  border-bottom-left-radius: 3px;
  border: 1px solid #c8c8c8;
  border-right: none;
  border-top: none;

}
.ddMore:before {
 
}
.ulPrice, .ulSearchBy, .ulType, .ulBeds, .ulBaths, .ulMore, .ulSearchResults {
  padding-top:15px;
}
.searchHeader ul {
  margin: 0 !important;
  list-style-type: none !important
}
.ulSearchBy a, .ddPrice li, .ulSearchResults li a {
  display:block;
  color:#191919;
  padding:5px 10px;
}
.ulSearchBy a:hover, .ddPrice li:hover, .ulSearchResults li a:hover {
  color:#fff !important;
  background-color:#222; /*dk grey*/
}
.ddSearchResults {
  display:none;
}
input.inputSearch {
  padding: 10px 5px !important;
  height: 42px !important;
  display: inline-block;
  margin: 0 !important;
  margin-top: -2px !important;
  width: 350px !important;
  border-radius:4px;
  border: 1px solid #fff;
  color:#333;
}
.input.inputSeach:focus label {
  display:none;
}
.lblSearch {
  display: block;
  margin: 0;
  position: absolute;
  top: 7px;
  left: 10px;
  color: #bbb !important;
  cursor: inherit;
}
.inputHolder {
    display:inline-block;
    position:relative;
    margin: 5px 0 0 30px;
}

.dropdownContent {
  display:none;
}
.show {
  display:block !important;
}
.typeCheckbox, .bedsCheckbox {
    margin: 5px 0 0;
    color:#191919;
}
.typeCheckbox input[type="checkbox"], .bedsCheckbox input[type="checkbox"] {
    margin: 0 5px 0 0;
}
.selectHolder, .searchTo {
  float:left; 
  width:40%;
}
.searchTo {
  width: 10% !important;
  text-align: center;
  padding-top: 5px;
}
.selectHolder select {
  width:100% !important;
}
.ddMoreFooter {
  width:100%;
  padding:10px 0;
  margin-top:10px;
  border-top:1px solid #ccc;
  float:left;
}

/* end search bar css */
</style>
<?php 
wp_enqueue_script("Listings_Script",i5PBIntegration__PLUGIN_URL . 'js/i5Listings.js');
//include(i5PBIntegration__PLUGIN_DIR . 'Includes/SaveSearch-Popup.php');
?>
<div class="topBar">
	
	<div class="searchBar">
		<?php if ( ot_get_option( 'ninetheme_cobian_logovisibility' )!='off' ) : ?>	
		<?php if ( ot_get_option( 'ninetheme_cobian_logoimg' ) ) : ?>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand"><img src="<?php echo esc_url( ot_get_option( 'ninetheme_cobian_logoimg' ) ) ?>" alt="Cobian Logo"></a> <!-- Your Logo -->
			<?php  else : ?>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand"><img src="<?php echo get_template_directory_uri() . '/images/brand/logo.png';?>" alt="Cobian Logo"></a> <!-- Your Logo -->
		<?php endif; ?>	
		<?php endif; ?>	
            <!-- <div class="searchBy dropBtn" onclick="showSearchBy()">
                Search By <i class="fa fa-angle-down"></i>
            </div>
            <input type="hidden" name="searchtype" id="searchtype" />
            <div id="dropdown" class="ddSearchBy dropdownContent">
                <ul class="ulSearchBy">
                    
                    <li>
                        <a href="javascript:setSearchType('byme');">Nearby Me</a>
                    </li>
                        
                    <li>
                        <a href="javascript:setSearchType('all','Type any Area, Address, Zip, School, etc');">Search All</a>
                    </li>
                    <li>
                        <a href="javascript:setSearchType('City','Type any City');">Cities</a>
                    </li>
                    <li>
                        <a href="javascript:setSearchType('Subdivision','Type any Subdivision');">Subdivisions</a>
                    </li>
                    <li>
                        <a href="javascript:setSearchType('Address','Type any Address');">Address</a>
                    </li>
                    <li>
                        <a href="javascript:setSearchType('MLSID','Type any MLS#');">MLS#</a>
                    </li>
                    <li>
                        <a href="javascript:setSearchType('School','Type any School');">Schools</a>
                    </li>
                    <li>
                        <a href="javascript:setSearchType('School District','Type any School District');">School Districts</a>
                    </li>
                    <li>
                        <a href="javascript:setSearchType('Zip Code','Type any Zipcode');">Zip</a>
                    </li>
                    <li>
                        <a href="javascript:setSearchType('Area','Type any Area');">Area</a>
                    </li>
                    <li>
                        <a href="javascript:setSearchType('kw','Type any Keyword');">Keyword</a>
                    </li>
                    <li>
                        <a href="#">Feature</a>
                    </li>
                </ul>
            </div>-->
            
            <div class="inputHolder">
                <input id="inputSearch" onclick="showSearchResults()" class="inputSearch" placeholder="Seach by Chicago neighborhood, zip or address" type="text" />
            </div>

            <div id="ddSearchResults" class="ddSearchResults">
                <ul class="ulSearchResults">
                    
                </ul>
            </div>
            
        </div>
        <!-- end searchBar -->
        <div class="searchMenu">
            	<ul>
            		<li class="myAccountTab">
	            	<?php
						if ( is_user_logged_in() ) {
			              echo "<div class='myAccount'><i class='fa fa-user'></i> My Account</div><div class='myAccountSub'><ul><li><a href='/favorites'><i class='fa fa-heart'></i> My Favorites</a></li><li><a href='/logout'><i class='fa fa-sign-out'></i> Logout</a></div>";
			            }
						else
	              			echo "<a href='javascript:doRegister();'><i class='fa fa-lock'></i> Login/Register</a>";
					?>
					</li>
            	</ul>
            	 
            </div>
    </div>
<div class="searchHeader">
    <div class="container fullWidth">
        <div class="searchFilters">
        	<div class="searchFilter">
	        	Listing Type<br />
	        	<button onclick="showTypeFilters()" class="dropbtn">
	                For Sale <i class="fa fa-angle-down"></i>
	            </button>
	            <div id="ddType" class="ddType dropdownContent">
	                 <?php
	                 
	                 $types=explode(";",urldecode(get_query_var("search_query")));
	
	                 if(strtolower(get_query_var("search_query"))=="all")
	                     $types=array();
	
	                 foreach($PropertyTypes as $propType)
	                 {
	                     if($propType!="All")
	                         echo "<div class='typeCheckbox'><input " . (in_array($propType,$types)?" checked ":"") . "onclick='menuStick(\"ddType\");doSearch();' type='checkbox' name='propertyType' value='" . $propType . "' />" . $propType . "</div>";
	                 }
	                 ?>
	            </div>
        	</div>
        	<div class="searchFilter">
	            Price<br />
	            <button onclick="showPriceFilters()" class="dropbtn">
	                Any Price <i class="fa fa-angle-down"></i>
	            </button>
	            <div id="ddPrice" class="ddPrice dropdownContent">
	                <input class="dropBtn" type="number" onchange="menuStick('ddPrice');doSearch();" <?php echo (isset($_REQUEST['minprice'])?" value='" . $_REQUEST['minprice'] ."' ":"") ?> placeholder="Min Price" id="minprice" />
	                <input class="dropBtn" type="number" onchange="doSearch();" <?php echo (isset($_REQUEST['minprice'])?" value='" . $_REQUEST['maxprice'] . "' ":"") ?> placeholder="Max Price" id="maxprice" />
	                <ul id="selMinPrice" class="ulPrice">
	                    <li class="dropBtn" onclick="setPrice('minprice', 0);" data-price="Any Price">Any</li>
	                    <li class="dropBtn" onclick="setPrice('minprice', 50000);" data-price="50000">$50K</li>
	                    <li class="dropBtn" onclick="setPrice('minprice', 75000);" data-price="75000">$75K</li>
	                    <li class="dropBtn" onclick="setPrice('minprice', 100000);" data-price="100000">$100K</li>
	                    <li class="dropBtn" onclick="setPrice('minprice', 150000);" data-price="150000">$150K</li>
	                    <li class="dropBtn" onclick="setPrice('minprice', 200000);" data-price="200000">$200K</li>
	                    <li class="dropBtn" onclick="setPrice('minprice', 250000);" data-price="250000">$250K</li>
	                    <li class="dropBtn" onclick="setPrice('minprice', 300000);" data-price="300000">$300K</li>
	                    <li class="dropBtn" onclick="setPrice('minprice', 350000);" data-price="350000">$350K</li>
	                    <li class="dropBtn" onclick="setPrice('minprice', 400000);" data-price="400000">$400K</li>
	                    <li class="dropBtn" onclick="setPrice('minprice', 500000);" data-price="400000">$500K</li>
	                    <li class="dropBtn" onclick="setPrice('minprice', 600000);" data-price="400000">$600K</li>
	                </ul>
	                <ul id="selMaxPrice" style="display:none;" class="ulPrice">
	                    <li class="dropBtn" onclick="setPrice('maxprice', 0);" data-price="Any Price">Any</li>
	                    <li class="dropBtn" onclick="setPrice('maxprice', 50000);" data-price="50000">$50K</li>
	                    <li class="dropBtn" onclick="setPrice('maxprice', 75000);" data-price="75000">$75K</li>
	                    <li class="dropBtn" onclick="setPrice('maxprice', 100000);" data-price="100000">$100K</li>
	                    <li class="dropBtn" onclick="setPrice('maxprice', 150000);" data-price="150000">$150K</li>
	                    <li class="dropBtn" onclick="setPrice('maxprice', 200000);" data-price="200000">$200K</li>
	                    <li class="dropBtn" onclick="setPrice('maxprice', 250000);" data-price="250000">$250K</li>
	                    <li class="dropBtn" onclick="setPrice('maxprice', 300000);" data-price="300000">$300K</li>
	                    <li class="dropBtn" onclick="setPrice('maxprice', 350000);" data-price="350000">$350K</li>
	                    <li class="dropBtn" onclick="setPrice('maxprice', 400000);" data-price="400000">$400K</li>
	                    <li class="dropBtn" onclick="setPrice('maxprice', 500000);" data-price="400000">$500K</li>
	                    <li class="dropBtn" onclick="setPrice('maxprice', 600000);" data-price="400000">$600K</li>
	                </ul>
	            </div>
            </div>
            <div class="searchFilter">
            Beds<br />
            <button onclick="showBedFilters()" class="dropbtn">
                All Beds <i class="fa fa-angle-down"></i>
            </button>
            <div id="ddBeds" class="ddBeds dropdownContent">
                <div class="bedsCheckbox">
                    <input name="minbedrooms" onchange="doSearch();" <?php echo (!isset($_REQUEST['minbedrooms'])?" checked ":"") ?> value="" type="radio" />Any
                </div>
                <div class="bedsCheckbox">
                    <input name="minbedrooms" onchange="doSearch();" <?php echo (isset($_REQUEST['minbedrooms']) && $_REQUEST['minbedrooms']=="2" ?" checked ":"") ?> value="2" type="radio" />2+
                </div>
                <div class="bedsCheckbox">
                    <input name="minbedrooms" onchange="doSearch();" <?php echo (isset($_REQUEST['minbedrooms']) && $_REQUEST['minbedrooms']=="3" ?" checked ":"") ?> value="3" type="radio" />3+
                </div>
                <div class="bedsCheckbox">
                    <input name="minbedrooms" onchange="doSearch();" <?php echo (isset($_REQUEST['minbedrooms']) && $_REQUEST['minbedrooms']=="4" ?" checked ":"") ?> value="4" type="radio" />4+
                </div>
                <div class="bedsCheckbox">
                    <input name="minbedrooms" onchange="doSearch();" <?php echo (isset($_REQUEST['minbedrooms']) && $_REQUEST['minbedrooms']=="5" ?" checked ":"") ?> value="5" type="radio" />5+
                </div>
            </div>
            </div>
            <div class="searchFilter">
            Baths
            <button onclick="showBathFilters()" class="dropbtn">
                All Baths <i class="fa fa-angle-down"></i>
            </button>
            <div id="ddBaths" class="ddBaths dropdownContent">
                <div class="bedsCheckbox">
                    <input name="minfullBaths" onchange="doSearch();" value="" type="radio" <?php echo (!isset($_REQUEST['minfullBaths'])?" checked ":"") ?> />Any
                </div>
                <div class="bedsCheckbox">
                    <input name="minfullBaths" onchange="doSearch();" <?php echo (isset($_REQUEST['minfullBaths']) && $_REQUEST['minfullBaths']=="2" ?" checked ":"") ?> value="2" type="radio" />2+
                </div>
                <div class="bedsCheckbox">
                    <input name="minfullBaths" onchange="doSearch();" <?php echo (isset($_REQUEST['minfullBaths']) && $_REQUEST['minfullBaths']=="3" ?" checked ":"") ?> value="3" type="radio" />3+
                </div>
                <div class="bedsCheckbox">
                    <input name="minfullBaths" onchange="doSearch();" <?php echo (isset($_REQUEST['minfullBaths']) && $_REQUEST['minfullBaths']=="4" ?" checked ":"") ?> value="4" type="radio" />4+
                </div>
                <div class="bedsCheckbox">
                    <input name="minfullBaths" onchange="doSearch();" <?php echo (isset($_REQUEST['minfullBaths']) && $_REQUEST['minfullBaths']=="5" ?" checked ":"") ?> value="5" type="radio" />5+
                </div>
            </div>
          </div>
          <div class="searchFilter">
	        New Construction<br />
	        <button onclick="showNewConstructionFilters()" class="dropbtn">
                Any Type <i class="fa fa-angle-down"></i>
            </button>
            <div id="ddNewConstruction" class="ddNewConstruction dropdownContent">
                <div class="bedsCheckbox">
                    <input name="newConstruction" onchange="doSearch();" value="" type="radio" <?php echo (!isset($_REQUEST['newConstruction'])?" checked ":"") ?> />Any
                </div>
                <div class="bedsCheckbox">
                    <input name="newConstruction" onchange="doSearch();" <?php echo (isset($_REQUEST['newConstruction']) && $_REQUEST['newConstruction']=="1" ?" checked ":"") ?> value="1" type="radio" />Yes
                </div>
                <div class="bedsCheckbox">
                    <input name="newConstruction" onchange="doSearch();" <?php echo (isset($_REQUEST['NewConstruction']) && $_REQUEST['NewConstruction']=="2" ?" checked ":"") ?> value="2" type="radio" />No
	                </div>
	            </div>
          	</div>
          	
            <button onclick="showMoreFilters()" class="dropbtn dropbtnMore">
                More <i class="fa fa-angle-down"></i>
            </button>
            <div id="ddMore" class="ddMore dropdownContent">
                <div class="moreFilters">
                    <div class="col1-1">
                        <label>Square Footage</label>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="minsize" id="minsize">
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="" ?" selected ":"") ?> value="">Any</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="500" ?" selected ":"") ?> value="500">500</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="550" ?" selected ":"") ?> value="550">550</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="600" ?" selected ":"") ?> value="600">600</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="650" ?" selected ":"") ?> value="650">650</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="700" ?" selected ":"") ?> value="700">700</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="750" ?" selected ":"") ?> value="750">750</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="800" ?" selected ":"") ?> value="800">800</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="850" ?" selected ":"") ?> value="850">850</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="900" ?" selected ":"") ?> value="900">900</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="950" ?" selected ":"") ?> value="950">950</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="1000" ?" selected ":"") ?> value="1000">1,000</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="1050" ?" selected ":"") ?> value="1050">1,050</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="1100" ?" selected ":"") ?> value="1100">1,100</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="1150" ?" selected ":"") ?> value="1150">1,150</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="1200" ?" selected ":"") ?> value="1200">1,200</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="1250" ?" selected ":"") ?> value="1250">1,250</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="1500" ?" selected ":"") ?> value="1500">1,500</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="1750" ?" selected ":"") ?> value="1750">1,750</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="2000" ?" selected ":"") ?> value="2000">2,000</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="2250" ?" selected ":"") ?> value="2250">2,250</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="2500" ?" selected ":"") ?> value="2500">2,500</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="2750" ?" selected ":"") ?> value="2750">2,750</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="3000" ?" selected ":"") ?> value="3000">3,000</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="3500" ?" selected ":"") ?> value="3500">3,500</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="4000" ?" selected ":"") ?> value="4000">4,000</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="5000" ?" selected ":"") ?> value="5000">5,000</option>
                            </select>
                        </div>
                        <div class="searchTo">To</div>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="maxsize" id="maxsize">
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="" ?" selected ":"") ?> value="">Any</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="500" ?" selected ":"") ?> value="500">500</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="550" ?" selected ":"") ?> value="550">550</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="600" ?" selected ":"") ?> value="600">600</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="650" ?" selected ":"") ?> value="650">650</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="700" ?" selected ":"") ?> value="700">700</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="750" ?" selected ":"") ?> value="750">750</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="800" ?" selected ":"") ?> value="800">800</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="850" ?" selected ":"") ?> value="850">850</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="900" ?" selected ":"") ?> value="900">900</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="950" ?" selected ":"") ?> value="950">950</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="1000" ?" selected ":"") ?> value="1000">1,000</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="1050" ?" selected ":"") ?> value="1050">1,050</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="1100" ?" selected ":"") ?> value="1100">1,100</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="1150" ?" selected ":"") ?> value="1150">1,150</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="1200" ?" selected ":"") ?> value="1200">1,200</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="1250" ?" selected ":"") ?> value="1250">1,250</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="1500" ?" selected ":"") ?> value="1500">1,500</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="1750" ?" selected ":"") ?> value="1750">1,750</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="2000" ?" selected ":"") ?> value="2000">2,000</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="2250" ?" selected ":"") ?> value="2250">2,250</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="2500" ?" selected ":"") ?> value="2500">2,500</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="2750" ?" selected ":"") ?> value="2750">2,750</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="3000" ?" selected ":"") ?> value="3000">3,000</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="3500" ?" selected ":"") ?> value="3500">3,500</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="4000" ?" selected ":"") ?> value="4000">4,000</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="5000" ?" selected ":"") ?> value="5000">5,000</option>
                            </select>
                        </div>
                        <!-- <label>Acres</label>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="minlotsize" id="minlotsize">
                                <option <?php echo (isset($_REQUEST['minlotsize']) && $_REQUEST['minlotsize']=="" ?" selected ":"") ?> value="">Any</option>
                                <option <?php echo (isset($_REQUEST['minlotsize']) && $_REQUEST['minlotsize']=="0.01" ?" selected ":"") ?> value="0.01">1/100</option>
                                <option <?php echo (isset($_REQUEST['minlotsize']) && $_REQUEST['minlotsize']=="0.15" ?" selected ":"") ?> value="0.15">1/8</option>
                                <option <?php echo (isset($_REQUEST['minlotsize']) && $_REQUEST['minlotsize']=="0.25" ?" selected ":"") ?> value="0.25">1/4</option>
                                <option <?php echo (isset($_REQUEST['minlotsize']) && $_REQUEST['minlotsize']=="0.5" ?" selected ":"") ?> value="0.5">1/2</option>
                                <option <?php echo (isset($_REQUEST['minlotsize']) && $_REQUEST['minlotsize']=="0.75" ?" selected ":"") ?> value="0.75">3/4</option>
                                <option <?php echo (isset($_REQUEST['minlotsize']) && $_REQUEST['minlotsize']=="1" ?" selected ":"") ?> value="1">1</option>
                                <option <?php echo (isset($_REQUEST['minlotsize']) && $_REQUEST['minlotsize']=="2" ?" selected ":"") ?> value="2">2</option>
                            </select>
                        </div>
                        <div class="searchTo">To</div>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="maxlotsize" id="maxlotsize">
                                <option <?php echo (isset($_REQUEST['maxlotsize']) && $_REQUEST['maxlotsize']=="" ?" selected ":"") ?> value="">Any</option>
                                <option <?php echo (isset($_REQUEST['maxlotsize']) && $_REQUEST['maxlotsize']=="0.01" ?" selected ":"") ?> value="0.01">1/100</option>
                                <option <?php echo (isset($_REQUEST['maxlotsize']) && $_REQUEST['maxlotsize']=="0.15" ?" selected ":"") ?> value="0.15">1/8</option>
                                <option <?php echo (isset($_REQUEST['maxlotsize']) && $_REQUEST['maxlotsize']=="0.25" ?" selected ":"") ?> value="0.25">1/4</option>
                                <option <?php echo (isset($_REQUEST['maxlotsize']) && $_REQUEST['maxlotsize']=="0.5" ?" selected ":"") ?> value="0.5">1/2</option>
                                <option <?php echo (isset($_REQUEST['maxlotsize']) && $_REQUEST['maxlotsize']=="0.75" ?" selected ":"") ?> value="0.75">3/4</option>
                                <option <?php echo (isset($_REQUEST['maxlotsize']) && $_REQUEST['maxlotsize']=="1" ?" selected ":"") ?> value="1">1</option>
                                <option <?php echo (isset($_REQUEST['maxlotsize']) && $_REQUEST['maxlotsize']=="2" ?" selected ":"") ?> value="2">2</option>
                            </select>
                        </div> -->
                        <!-- <label>Stories</label>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="minstories" id="minstories">
                                <option <?php echo (isset($_REQUEST['minstories']) && $_REQUEST['minstories']=="" ?" selected ":"") ?> value="">Any</option>
                                <option <?php echo (isset($_REQUEST['minstories']) && $_REQUEST['minstories']=="1" ?" selected ":"") ?> value="1">1</option>
                                <option <?php echo (isset($_REQUEST['minstories']) && $_REQUEST['minstories']=="2" ?" selected ":"") ?> value="2">2</option>
                                <option <?php echo (isset($_REQUEST['minstories']) && $_REQUEST['minstories']=="3" ?" selected ":"") ?> value="3">3</option>
                                <option <?php echo (isset($_REQUEST['minstories']) && $_REQUEST['minstories']=="4" ?" selected ":"") ?> value="4">4</option>
                                <option <?php echo (isset($_REQUEST['minstories']) && $_REQUEST['minstories']=="5" ?" selected ":"") ?> value="5">5</option>
                            </select>
                        </div>
                        <div class="searchTo">To</div>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="maxstories" id="maxstories">
                                <option <?php echo (isset($_REQUEST['maxstories']) && $_REQUEST['maxstories']=="" ?" selected ":"") ?> value="">Any</option>
                                <option <?php echo (isset($_REQUEST['maxstories']) && $_REQUEST['maxstories']=="1" ?" selected ":"") ?> value="1">1</option>
                                <option <?php echo (isset($_REQUEST['maxstories']) && $_REQUEST['maxstories']=="2" ?" selected ":"") ?> value="2">2</option>
                                <option <?php echo (isset($_REQUEST['maxstories']) && $_REQUEST['maxstories']=="3" ?" selected ":"") ?> value="3">3</option>
                                <option <?php echo (isset($_REQUEST['maxstories']) && $_REQUEST['maxstories']=="4" ?" selected ":"") ?> value="4">4</option>
                                <option <?php echo (isset($_REQUEST['maxstories']) && $_REQUEST['maxstories']=="5" ?" selected ":"") ?> value="5">5</option>
                            </select>
                        </div> -->
                    </div><!-- end 1/1 -->
                    <div class="col1-1">
                        <label>Year Built</label>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="minyearbuilt" id="minyearbuilt">
                                <option value="">Any</option>
                                <?php for($i=date("Y");$i>=2005;$i--){ 
                                          echo "<option" . (isset($_REQUEST['minyearbuilt']) && $_REQUEST['minyearbuilt']==$i ?" selected ":"") . " value='$i'>$i</option>";
                                      }
                                      for($i=2000;$i>1899;$i-=10){
                                          echo "<option" . (isset($_REQUEST['minyearbuilt']) && $_REQUEST['minyearbuilt']==$i ?" selected ":"") . " value='$i'>$i</option>";
                                      }
                                ?>
                            </select>
                        </div>
                        <div class="searchTo">To</div>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="maxyearbuilt" id="maxyearbuilt">
                                <option value="">Any</option>
                                <?php for($i=date("Y");$i>=2005;$i--){ 
                                          echo "<option" . (isset($_REQUEST['maxyearbuilt']) && $_REQUEST['maxyearbuilt']==$i ?" selected ":"") . " value='$i'>$i</option>";
                                      }
                                      for($i=2000;$i>1899;$i-=10){
                                          echo "<option" . (isset($_REQUEST['maxyearbuilt']) && $_REQUEST['maxyearbuilt']==$i ?" selected ":"") . " value='$i'>$i</option>";
                                      }
                                ?>
                            </select>
                        </div>
                        <!-- <label>Parking Spaces</label>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="minparkingSpaces" id="minparkingSpaces">
                                <option <?php echo (isset($_REQUEST['minparkingSpaces']) && $_REQUEST['minparkingSpaces']=="" ?" selected ":"") ?> value="">Any</option>
                                <option <?php echo (isset($_REQUEST['minparkingSpaces']) && $_REQUEST['minparkingSpaces']=="1" ?" selected ":"") ?> value="1">1</option>
                                <option <?php echo (isset($_REQUEST['minparkingSpaces']) && $_REQUEST['minparkingSpaces']=="2" ?" selected ":"") ?> value="2">2</option>
                                <option <?php echo (isset($_REQUEST['minparkingSpaces']) && $_REQUEST['minparkingSpaces']=="3" ?" selected ":"") ?> value="3">3</option>
                                <option <?php echo (isset($_REQUEST['minparkingSpaces']) && $_REQUEST['minparkingSpaces']=="4" ?" selected ":"") ?> value="4">4</option>
                                <option <?php echo (isset($_REQUEST['minparkingSpaces']) && $_REQUEST['minparkingSpaces']=="5" ?" selected ":"") ?> value="5">5</option>
                            </select>
                        </div>
                        <div class="searchTo">To</div>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="maxparkingSpaces" id="maxparkingSpaces">
                                <option <?php echo (isset($_REQUEST['maxparkingSpaces']) && $_REQUEST['maxparkingSpaces']=="" ?" selected ":"") ?> value="">Any</option>
                                <option <?php echo (isset($_REQUEST['maxparkingSpaces']) && $_REQUEST['maxparkingSpaces']=="1" ?" selected ":"") ?> value="1">1</option>
                                <option <?php echo (isset($_REQUEST['maxparkingSpaces']) && $_REQUEST['maxparkingSpaces']=="2" ?" selected ":"") ?> value="2">2</option>
                                <option <?php echo (isset($_REQUEST['maxparkingSpaces']) && $_REQUEST['maxparkingSpaces']=="3" ?" selected ":"") ?> value="3">3</option>
                                <option <?php echo (isset($_REQUEST['maxparkingSpaces']) && $_REQUEST['maxparkingSpaces']=="4" ?" selected ":"") ?> value="4">4</option>
                                <option <?php echo (isset($_REQUEST['maxparkingSpaces']) && $_REQUEST['maxparkingSpaces']=="5" ?" selected ":"") ?> value="5">5</option>
                            </select>
                        </div> -->
                    </div><!-- end 1/1 column -->
                </div>
                <div class="ddMoreFooter">
                    <button class="button right">Show Results</button>
                </div>
            </div>
            <?php if((!isset($showSave) || (isset($showSave) && $showSave==true)) && is_user_logged_in()):?>
                <button  onClick="showSaveSearch();" class="button btnSaveSearch">Save This Search</button>
            <?php endif;?>
        </div>
        
    </div><!-- end container -->
</div><!-- end search header -->
<script type="text/javascript">
    jQuery(function () {
        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();

        jQuery("#inputSearch").keyup(function () {
            delay(function () {
                doAutoComplete();
            }, 200);
        });

        var mnu = getCookie("openMenu");

        if (mnu != "") {
            jQuery("." + mnu).addClass("show");

            if (mnu == 'ddPrice') {
                jQuery("#maxprice").focus();
                jQuery("#selMinPrice").hide();
                jQuery("#selMaxPrice").show();
            }

            document.cookie = "openMenu=";
        }
    });
    function doAutoComplete() {
        if (jQuery("#searchtype").val() == "Address" || jQuery("#searchtype").val() == "MLSID")
            queryListings("url as redirect,'" + jQuery("#searchtype").val() + "' as section," + jQuery("#searchtype").val() + " as result", jQuery("#searchtype").val().replace(" ", "") + " like '%" + jQuery("#inputSearch").val() + "%'", populateSearch, null, jQuery("#searchtype").val().replace(" ", ""), "10");
        else if (jQuery("#searchtype").val() == "School")
            queryListings("count(*),'" + jQuery("#searchtype").val() + "' as section," + jQuery("#searchtype").val().replace(" ", "") + " as result", "elementaryschool like '%" + jQuery("#inputSearch").val() + "%' or middleschool like '%" + jQuery("#inputSearch").val() + "%' or highschool like '%" + jQuery("#inputSearch").val() + "%'", populateSearch, jQuery("#searchtype").val().replace(" ", "") + ",section", jQuery("#searchtype").val().replace(" ", ""), "10");
        else if(jQuery("#searchtype").val()=="" || jQuery("#searchtype").val()=="all")
            autoComplete(jQuery("#inputSearch").val(), 4, populateSearch);
        else
            queryListings("count(*),'" + jQuery("#searchtype").val() + "' as section," + jQuery("#searchtype").val().replace(" ", "") + " as result", jQuery("#searchtype").val().replace(" ", "") + " like '%" + jQuery("#inputSearch").val() + "%'", populateSearch, jQuery("#searchtype").val().replace(" ", "") + ",section", jQuery("#searchtype").val().replace(" ", ""), "10");
    }
    var lastSection = "";
    function populateSearch(list) {
        jQuery(".ulSearchResults").empty();
        var html = "";

        if(list!=null && list.length>0)
        {
            for(var i=0;i<list.length;i++)
            {
                if (lastSection != list[i].section || i==0) {
                    html += "<li class='resultsHeader'>" + list[i].section + "</span></li>";
                    lastSection = list[i].section;
                }

                if (list[i].redirect == null || list[i].redirect=="")
                    html += "<li class='resultsItem'><a href='javascript:doSearch(null,null,null,\"" + lastSection + "\",\"" + list[i].result + "\");'>" + list[i].result + "</a></li>";
                else
                    html += "<li class='resultsItem'><a href='javascript:redirect(\"" + list[i].redirect + "\");'>" + list[i].result + "</a></li>";
            }
        }
        else
        {
            html += "<li class='resultsHeader'>" + lastSection + "<br/>No Results</span></li>";
        }

        jQuery(".ulSearchResults").append(html);
    }
    function redirect(url)
    {
        window.location = "/property-details/" + url;
    }
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    function setPrice(cnt,vl)
    {
        if(vl!=0)
            jQuery("#" + cnt).val(vl);
        else
            jQuery("#" + cnt).val("");

        if(cnt=="minprice")
        {
            jQuery("#maxprice").focus();
            jQuery("#selMinPrice").hide();
            jQuery("#selMaxPrice").show();
            menuStick("ddPrice");
            doSearch();
        }
        else
        {
            jQuery("#minprice").focus();
            jQuery("#selMinPrice").show();
            jQuery("#selMaxPrice").hide();
            doSearch();
        }
    }
    function setSearchType(t,label) {
        jQuery("#searchtype").val(t);
        jQuery("#inputSearch").prop("placeholder", label);
    }
    function menuStick(vl) {
        document.cookie = "openMenu=" + vl;
    }
    function showSearchBy() {
        jQuery(".dropdownContent").removeClass("show");
        document.getElementById("dropdown").classList.toggle("show");
  }
    function showPriceFilters() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddPrice").classList.toggle("show");
  }
    function showTypeFilters() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddType").classList.toggle("show");
  }
    function showBedFilters() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddBeds").classList.toggle("show");
  }
    function showBathFilters() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddBaths").classList.toggle("show");
  }
  function showNewConstructionFilters(){
  jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddNewConstruction").classList.toggle("show");
  }
    function showMoreFilters() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddMore").classList.toggle("show");
  }
  	function showSort() {
        jQuery(".dropdownContent").removeClass("show");
        document.getElementById("ddSort").classList.toggle("show");
  }
  function showSortPrice() {
        jQuery(".dropdownContent").removeClass("show");
        document.getElementById("ddSortPrice").classList.toggle("show");
  }
   function showSortNew() {
        jQuery(".dropdownContent").removeClass("show");
        document.getElementById("ddSortNew").classList.toggle("show");
  }
    function showSearchResults() {
        jQuery(".dropdownContent").removeClass("show");
        document.getElementById("ddSearchResults").classList.toggle("show");
  }
  // Close the dropdown if the user clicks outside of it
  window.onclick = function(event) {
  if (!event.target.matches('.dropBtn')) {

  var dropdowns = document.getElementsByClassName("dropdownContent");
  var i;
  for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
  function showSaveSearch() {
      var filters="";
      var display = "";
      var propType="";
        <?php if(isset($_REQUEST["minprice"]))
        {
            echo "filters+='pba__ListingPrice_pb_min__c:" . $_REQUEST["minprice"] . ";';";
            echo "display+='<div class=\"filter\">Price Min: " . $_REQUEST["minprice"] . "</div>';";
         }
        if(isset($_REQUEST["maxprice"]))
        {
            echo "filters+='pba__ListingPrice_pb_max__c:" . $_REQUEST["maxprice"] . ";';";
            echo "display+='<div class=\"filter\">Price Max: " . $_REQUEST["maxprice"] . "</div>';";
         }
        if(isset($_REQUEST["maxbedrooms"]))
        {
            echo "filters+='pba__Bedrooms_pb_max__c:" . $_REQUEST["maxbedrooms"] . ";';";
            echo "display+='<div class=\"filter\">Bed(s) Max: " . $_REQUEST["maxbedrooms"] . "</div>';";
        }
        if(isset($_REQUEST["minbedrooms"]))
        {
            echo "filters+='pba__Bedrooms_pb_min__c:" . $_REQUEST["minbedrooms"] . ";';";
            echo "display+='<div class=\"filter\">Bed(s) Min: " . $_REQUEST["minbedrooms"] . "</div>';";
        }
        if(isset($_REQUEST["minfullBaths"]))
        {
            echo "filters+='pba__FullBathrooms_pb_min__c:" . $_REQUEST["minfullBaths"] . ";';";
            echo "display+='<div class=\"filter\">Bath(s) Min: " . $_REQUEST["minfullBaths"] . "</div>';";
        }
        if(isset($_REQUEST["newConstruction"]))
        {
            //TODO: Implement
        }
        if(isset($_REQUEST["maxfullBaths"]))
        {
            echo "filters+='pba__FullBathrooms_pb_max__c:" . $_REQUEST["maxfullBaths"] . ";';";
            echo "display+='<div class=\"filter\">Bath(s) Max: " . $_REQUEST["maxfullBaths"] . "</div>';";
        }
        if(isset($_REQUEST["minsize"]))
        {
            echo "filters+='pba__TotalArea_pb_min__c:" . $_REQUEST["minsize"] . ";';";
            echo "display+='<div class=\"filter\">Size Min: " . $_REQUEST["minsize"] . "</div>';";
        }
        if(isset($_REQUEST["maxsize"]))
        {
            echo "filters+='pba__TotalArea_pb_max__c:" . $_REQUEST["maxsize"] . ";';";
            echo "display+='<div class=\"filter\">Size Max: " . $_REQUEST["maxsize"] . "</div>';";
        }
        if(isset($_REQUEST["minlotsize"]))
        {
            echo "filters+='pba__LotSize_pb_min__c:" . $_REQUEST["minlotsize"] . ";';";
            echo "display+='<div class=\"filter\">Acres Min: " . $_REQUEST["minlotsize"] . "</div>';";
        }
        if(isset($_REQUEST["maxlotsize"]))
        {
            echo "filters+='pba__LotSize_pb_max__c:" . $_REQUEST["maxlotsize"] . ";';";
            echo "display+='<div class=\"filter\">Acres Max: " . $_REQUEST["maxlotsize"] . "</div>';";
        }
        if(isset($_REQUEST["minyearbuilt"]))
        {
            echo "filters+='pba__YearBuilt_pb_min__c:" . $_REQUEST["minyearbuilt"] . ";';";
            echo "display+='<div class=\"filter\">Year Built Min: " . $_REQUEST["minyearbuilt"] . "</div>';";
        }
        if(isset($_REQUEST["maxyearbuilt"]))
        {
            echo "filters+='pba__YearBuilt_pb_min__c:" . $_REQUEST["maxyearbuilt"] . ";';";
            echo "display+='<div class=\"filter\">Year Built Max: " . $_REQUEST["maxyearbuilt"] . "</div>';";
        }
        if(isset($_REQUEST["minparkingSpaces"]))
        {
            echo "filters+='ParkingSpaces__c:" . $_REQUEST["minparkingSpaces"] . ";';";
            echo "display+='<div class=\"filter\">Parking Spaces Min: " . $_REQUEST["minparkingSpaces"] . "</div>';";
        }
        if(isset($_REQUEST["maxparkingSpaces"]))
        {
            echo "filters+='Parking_Spaces_Max__c:" . $_REQUEST["maxparkingSpaces"] . ";';";
            echo "display+='<div class=\"filter\">Parking Spaces Max: " . $_REQUEST["maxparkingSpaces"] . "</div>';";
        }
        if(isset($_REQUEST["minstories"]))
        {
            echo "filters+='Stories_Min__c:" . $_REQUEST["minstories"] . ";';";
            echo "display+='<div class=\"filter\">Stories Min: " . $_REQUEST["minstories"] . "</div>';";
        }
        if(isset($_REQUEST["maxstories"]))
        {
            echo "filters+='Stories_Max__c:" . $_REQUEST["maxstories"] . ";';";
            echo "display+='<div class=\"filter\">Stories Max: " . $_REQUEST["maxstories"] . "</div>';";
        }
        if(isset($_REQUEST["searchtype"]) && isset($_REQUEST["search"]))
        {
            if($_REQUEST["searchtype"]=="City")
                echo "filters+='pba__City_pb__c:" . $_REQUEST["search"] . ";';";
            else if($_REQUEST["searchtype"]=="Zip Code")
                echo "filters+='pba__PostalCode_pb__c:" . $_REQUEST["search"] . ";';";
            else if($_REQUEST["searchtype"]=="area")
                echo "filters+='pba__Area_pb__c:" . $_REQUEST["search"] . ";';";
            else if($_REQUEST["searchtype"]=="Subdivision")
                echo "filters+='Subdivision__c:" . $_REQUEST["search"] . ";';";

            echo "display+='<div class=\"filter\">" . $_REQUEST["searchtype"]. ": " . $_REQUEST["search"] . "</div>';";
        }

        foreach($types as $type){
            if(isset($type) && $type!="")
            echo "propType+='" . $type . ",';";
            echo "display+='<div class=\"filter\">Property Type: " . $type . "</div>';";
        }
        ?>

      if (filters != "" || propType != "")
      {
          if(filters!="")
            filters=filters.substring(0,filters.length-1);

          if (filters != "" && propType != "")
              filters += ";";

          if (propType != "")
          {
              propType = propType.substring(0, propType.length - 1);
              filters += "pba__PropertyType__c:" + propType;
              }

          jQuery("#filters").val(filters);
          jQuery('#saveSearch').modal('show');
      }
      else
          alert("Please specify your search parameters above.");
  }
  
  // Tooltip initaite
  jQuery(document).ready(function(){
    jQuery('[data-toggle="tooltip"]').tooltip();   
	});
</script>