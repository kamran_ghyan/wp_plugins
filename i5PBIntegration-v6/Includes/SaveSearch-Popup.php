<div class="modal fade" id="saveSearch" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h1>Save Search</h1>
            </div>
            <div class="modal-body">
                <?php echo do_shortcode("[contact-form-7 id='" . $settings["ProspectMappings"]["MapSaveSearchId"] . "' title='Save Search']"); ?>
            </div>
        </div>
    </div>
</div>
