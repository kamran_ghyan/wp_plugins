<?php
$settings=get_option('WebListing_Settings');
include(i5PBIntegration__PLUGIN_DIR . 'Includes/register-popup.php');

wp_enqueue_script('jquery');

$favs=array();
$cleanLoc=$_SERVER['REQUEST_URI'];

if(is_user_logged_in())
{
    //See if we need to set a favorite
    if(isset($_REQUEST["fav"]))
    {
        $loc=$_SERVER['REQUEST_URI'];

        echo $loc;

        require_once( i5PBIntegration__PLUGIN_DIR . 'class.i5PBIntegration-Prospect.php' );
        i5PBIntegration_Prospect::processFavorite($_REQUEST["fav"],isset($_REQUEST["on"])?$_REQUEST["on"]:1);

        if(isset($_REQUEST["on"]))
        {
            $loc = str_replace("&on=" . $_REQUEST["on"],"",$loc);
            $loc = str_replace("?on=" . $_REQUEST["on"],"",$loc);
        }

        echo $loc;

        $loc = str_replace("&fav=" . $_REQUEST["fav"],"",$loc);
        $loc = str_replace("?fav=" . $_REQUEST["fav"],"",$loc);

        echo $loc;

        $cleanLoc=$loc;

        header("location:" . $loc . "#" . $_REQUEST["fav"]);
        exit();
    }

    $favs=get_user_meta(get_current_user_id(),'Favorites',true);
}

echo "<script>var favs=" . json_encode($favs) . ";</script>";
?>
<script>
    var red;
    function setFavorite(id, on) {
        if ("<?php echo is_user_logged_in()?'true':'false' ?>" != "true") {
            var join = "&";

            if (window.location.href.indexOf("?") == -1)
                join = "?";

            var loc = "<?php echo $_SERVER['REQUEST_URI']; ?>";

            red = "<?php echo rtrim($cleanLoc,"/"); ?>" + join + "fav=" + id;
            jQuery("#redirect_to").val(red);
            jQuery(".open-registration").attr("data-id", red);
            jQuery("#doLogin").click();
        }
        else {
            saveFavorite(id, on);
        }
    }    
</script>
