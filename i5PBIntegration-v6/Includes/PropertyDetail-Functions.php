<?php
    $relatedBaths=0;
    $relatedPropertyType="";
    $relatedSize=0;
    $relatedPrice=0;

    $property;
    $images;

    $prevSearch = $_SERVER['HTTP_REFERER'];

    if( strpos( strtolower( $prevSearch ),"listing-search" ) === false)
        $prevSearch="/listing-search/all/?display=map";

    $sql = "SELECT * from {$wpdb->prefix}i5listings where url=%s";
    $sql=$wpdb->prepare($sql,urldecode(get_query_var("property_url")));

    $property = $wpdb->get_row($sql);
	
    $relatedBaths = $property->baths;
    $relatedPropertyType = $property->propertytype;
    $relatedSize = $property->size;
    $relatedPrice = $property->price;
	
	
    if(!isset($property))
    {
        //Todo redirect
    }
    else
    {
        $sql = "SELECT * from {$wpdb->prefix}i5Images where propertyid=%s";
        $sql=$wpdb->prepare($sql,$property->propertyid);

        $images=$wpdb->get_results($sql);
		
		// count query for images
		$sql_cnt = "SELECT count(*) as c from {$wpdb->prefix}i5Images where propertyid=%s";
        $sql_cnt=$wpdb->prepare($sql_cnt,$property->propertyid);

        $images_cnt=$wpdb->get_row($sql_cnt);
		
    }
?>