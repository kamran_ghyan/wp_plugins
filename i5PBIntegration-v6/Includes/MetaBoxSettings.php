<?php
class MetaBoxSettings
{
    public function setupMetaBoxes(){
        add_action( 'add_meta_boxes_page', array('MetaBoxSettings', 'add_meta_boxes' ));
        add_action( 'save_post', array('MetaBoxSettings','save_meta'), 10, 2 );
    }
    public static function add_meta_boxes() {

        global $post;
        if(!empty($post))
        {
            $pageTemplate = get_post_meta($post->ID, '_wp_page_template', true);

            if($pageTemplate == 'PropertyDetail2.php' || $pageTemplate=='PropertyDetail1.php' || $pageTemplate=='PropertySearch1.php' || $pageTemplate=='PropertySearch2.php' )
            {
                add_meta_box(
                  'i5_meta',      // Unique ID
                  esc_html__( 'Page Settings', 'example' ),    // Title
                  array('MetaBoxSettings', 'meta_box_render'),   // Callback function
                  'page',         // Admin page (or post type)
                  'normal',         // Context
                  'high',         // Priority
                  array()
                );
            }
        }
    }
    public function meta_box_render( $object, $box ) {
        wp_nonce_field( basename( __FILE__ ), 'i5MetaBox' );
?>

<table>
    <tr>
        <td>Company Name:</td>
        <td>
            <input class="widefat" type="text" name="companyname" id="companyname" value="<?php echo esc_attr( get_post_meta( $object->ID, 'companyname', true ) ); ?>" size="30" />
        </td>
    </tr>
    <tr>
        <td>Company Phone:</td>
        <td>
            <input class="widefat" type="text" name="companyphone" id="companyphone" value="<?php echo esc_attr( get_post_meta( $object->ID, 'companyphone', true ) ); ?>" size="30" />
        </td>
    </tr>
</table>

<p>
    <label for="companylogo">
        <?php _e( "Company Logo", 'example' ); ?>
    </label>
    <br />
    <?php
        wp_editor( htmlspecialchars_decode( get_post_meta( $object->ID, 'companylogo', true )),'companylogo', $settings = array('textarea_name'=>'companylogoText') );
    ?>
</p>
<p>
    <label for="disclaimer">
        <?php _e( "Disclaimer", 'example' ); ?>
    </label>
    <br />
    <?php
        wp_editor( htmlspecialchars_decode( get_post_meta( $object->ID, 'disclaimer', true )),'disclaimer', $settings = array('textarea_name'=>'disclaimerText') );
    ?>
</p>
<?php
    }
    public function save_meta($post_id,$post)
    {
        if ( !isset( $_POST['i5MetaBox'] ) || !wp_verify_nonce( $_POST['i5MetaBox'], basename( __FILE__ ) ) )
            return $post_id;

        /* Get the post type object. */
        $post_type = get_post_type_object( $post->post_type );

        /* Check if the current user has permission to edit the post. */
        if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
            return $post_id;

        update_post_meta($post_id,"companyname",$_POST["companyname"]);
        update_post_meta($post_id,"companyphone",$_POST["companyphone"]);

        if(!empty($_POST["disclaimerText"]))
            update_post_meta($post_id,"disclaimer",htmlspecialchars($_POST["disclaimerText"]));
        else
            update_post_meta($post_id,"disclaimer",null);

        if(!empty($_POST["companylogoText"]))
            update_post_meta($post_id,"companylogo",htmlspecialchars($_POST["companylogoText"]));
        else
            update_post_meta($post_id,"companylogo",null);
    }
}