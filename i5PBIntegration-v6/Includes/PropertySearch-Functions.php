<?php

global $wpdb;
// Property Type distinct query
$sql_dist       = "SELECT DISTINCT propertytype from {$wpdb->prefix}i5listings WHERE 1";
$property_types = $wpdb->get_results($sql_dist);

// City Type distinct query
$sql_city   = "SELECT DISTINCT city from {$wpdb->prefix}i5listings WHERE 1";
$cities     = $wpdb->get_results($sql_city);


$PropertyTypes=array();
$TTLResults=0;
$SearchResults;
$search_result_lease;
$pageSize=30;
$startPage=1;
$endPage;
$start=1;
$end=0;
$pageNum=1;

if(isset($maxResults))
    $pageSize=$maxResults;

$fields=array("stories","minstories","subdivision","maxstories","parkingSpaces","minparkingSpaces","maxparkingSpaces","lotsize","minlotsize","maxlotsize","yearbuilt","minyearbuilt","maxyearbuilt","minsize", "maxsize","minprice","maxprice","bedrooms","minbedrooms","maxbedrooms","fullBaths","minfullBaths","maxfullBaths","search","newConstruction");

//Get the Property Types
$sql="SELECT propertytype from {$wpdb->prefix}i5listings group by propertytype order by propertytype";

$pTypes=$wpdb->get_results($sql);

array_push($PropertyTypes,"All");

if($pTypes!=null)
{
    foreach($pTypes as $pType)
    {
        if($pType->propertytype!=null && $pType->propertytype!="")
            array_push($PropertyTypes,$pType->propertytype);
    }
}

$sql="";

//Do the search
if(!isset($_REQUEST["display"])||$_REQUEST["display"]=="gallery")
{
    $sql="SELECT mlsid,recordType,area,subdivision,propertytype,title,status,l.url,latitude,longitude,i.url AS image,mlsid,bedrooms,address,city,state,zipCode,description,price,fullBaths,halfBaths,size,agentFirstName,agentEmail,agentMLSId from {$wpdb->prefix}i5listings l
      left join {$wpdb->prefix}i5Images i on i.id = (select min(id) from {$wpdb->prefix}i5Images ii where ii.propertyid=l.propertyid)";
	  $sql_lease = "SELECT mlsid,recordType,area,subdivision,propertytype,title,status,l.url,latitude,longitude,i.url AS image,mlsid,bedrooms,address,city,state,zipCode,description,price,fullBaths,halfBaths,size,agentFirstName,agentEmail,agentMLSId from {$wpdb->prefix}i5listings l
      left join {$wpdb->prefix}i5Images i on i.id = (select min(id) from {$wpdb->prefix}i5Images ii where ii.propertyid=l.propertyid)";
}
else
{
    $sql = "SELECT l.*,i.url AS image from {$wpdb->prefix}i5listings l
          left join {$wpdb->prefix}i5Images i on i.id = (select min(id) from {$wpdb->prefix}i5Images ii where ii.propertyid=l.propertyid)";
		  
	$sql_lease = "SELECT l.*,i.url AS image from {$wpdb->prefix}i5listings l
          left join {$wpdb->prefix}i5Images i on i.id = (select min(id) from {$wpdb->prefix}i5Images ii where ii.propertyid=l.propertyid)";
}

$sqlCount = "SELECT count(*) from {$wpdb->prefix}i5listings";

$where = " WHERE listingType LIKE '%sale%' ";
$join = "WHERE";
$order = " Order by created";

$sql_count_lease = "SELECT count(*) from {$wpdb->prefix}i5listings";
$where_lease = " WHERE listingType LIKE '%lease%' ";
$join_lease = "WHERE";
$order_lease = " Order by created";

if(get_query_var("search_query")!="" && strtolower(get_query_var("search_query"))!="all")
{
    $pts = explode(";",urldecode(get_query_var("search_query")));
    $in="";

    foreach($pts as $pt)
    {
        $in .= "'" . $pt . "',";
    }

    $in = substr($in,0,strlen($in) -1);

    $where = " " . $join . " propertytype in(" . $in . ")";
    $join="and";
}

foreach($fields as $field)
{
    if(isset($_REQUEST[$field]))
    {
        if($field=="search")
        {

        }
        else
        {
            $where .= " " . $join . " " . str_replace("max","",str_replace("min","",$field));

            if(strlen($field)>2 && strrpos($field,"min",-strlen($field))!==false)
                $where .= ">=";
            else if(strlen($field)>2 &&strrpos($field,"max",-strlen($field))!==false)
                $where .= "<=";
            else
                $where .= "=";

          	if($field=="newConstruction")
              $where .= $_REQUEST[$field]=="2"?"":$_REQUEST[$field];
            else
            	$where .= $_REQUEST[$field];

            $join="and";
        }
    }
}

if(isset($_REQUEST["searchtype"]) && isset($_REQUEST["search"]) && $_REQUEST["search"]!="" && $_REQUEST["searchtype"]!="")
{
    $where .= " " . $join . " " . str_replace(" ","", urldecode($_REQUEST["searchtype"])) . " ='" . urldecode($_REQUEST["search"]) . "'";
    $join="and";
}
else if(isset($_REQUEST["search"]) && $_REQUEST["search"] !="")
{
    $where .= " " . $join . " (description like '" . $_REQUEST["search"] . "' or mlsid like '" . $_REQUEST["searc"] . "' or title like '" . $_REQUEST["search"] . "' or address like '" . $_REQUEST["search"] . "' or city like '" . $_REQUEST["search"] . "')";
    $join="and";
}

if(isset($_REQUEST["School"]) && $_REQUEST["School"]!="")
{
    $where .= " " . $join . "(elementaryschool like '%" . $_REQUEST["School"] . "%' or middleschool like '%" . $_REQUEST["School"] . "%' or highschool like '%" . $_REQUEST["School"] . "%')";
    $join="and";
}

if(isset($_REQUEST["sort"]))
{
    $dir="ASC";
    $field="";

    if(substr(strtolower($_REQUEST["sort"]),0,3)!="asc")
        $dir="DESC";

    if($dir=="DESC")
        $field=substr($_REQUEST["sort"],4);
    else
        $field=substr($_REQUEST["sort"],3);

    $order=" ORDER By " . $field . " " . $dir;
}


if(isset($_REQUEST["pg"]) && (int)$_REQUEST["pg"]!=0)
{
    $start= ($pageSize * (int)$_REQUEST["pg"]) + 1;
    $pageNum=(int)$_REQUEST["pg"];
}

//$where .= ' AND l.latitude <> 0 AND l.longitude <> 0';

$skip=0;

if($pageNum>1)
    $skip =$pageNum * $pageSize;

//echo $sql . $where . $order . " limit " . $skip . ", " . $pageSize;

$SearchResults = $wpdb->get_results($sql . $where . $order . " limit " . $skip . ", " . $pageSize);

// Lease type query
//echo $sql_lease . $where_lease . $order_lease . " limit " . $skip . ", " . $pageSize;
$search_result_lease = $wpdb->get_results($sql_lease . $where_lease . $order_lease . " limit " . $skip . ", " . $pageSize);

// sale type count
$TTLResults=$wpdb->get_var($sqlCount . $where);
// lease type count
$ttl_result_lease = $wpdb->get_var( $sql_count_lease . $where_lease );

$endPage=ceil($TTLResults/$pageSize);

if($pageNum>5 && $endPage>10)
    $startPage=$pageNum-4;

if($endPage>9 && $startPage!=1 && ($startPage+9)>$endPage)
{
    while(($startPage+9)>$endPage){
        $startPage-=1;
    }
}

if($endPage>$startPage+9){
    $endPage=$startPage+9;
}

$end=$start+$pageSize -1;

if($end>$TTLResults)
    $end=$TTLResults;
?>
<script>
    function removeFilter(filter) {
        doSearch(null,null,filter);
    }
    function changeDisplay(type)
    {
        if(getParameterByName("display") !=null && getParameterByName("display")==type)
            return;

        var loc = getRedirect(null,null,"display");

        if(loc.indexOf("?")==-1)
            loc+="?display=" + type;
        else
            loc+="&display=" + type;

        window.location=loc;
    }
    function doSearch(ord,page,remove,searchType,searchKw) {
        window.location = getRedirect(ord,page,remove,searchType,searchKw);
    }
    function getRedirect(ord,page,remove,searchType,searchKw)
    {
        var loc = "/listing-search/";

        if (jQuery("#propertyType").val())
            loc += encodeURI(jQuery("#propertyType").val());
        else if(jQuery("input[name='propertyType']:checked").val() && jQuery("input[name='propertyType']").val() !="")
        {
            var haspt=false;

            jQuery("input[name='propertyType']:checked").each(function(){
                if(jQuery(this).val()!=remove)
                {
                    loc += encodeURI(jQuery(this).val() + ";");
                    haspt=true;
                }
            });

            if(haspt)
                loc=loc.substring(0,loc.length-1);
            else
                loc+="all";
        }
        else
            loc += "all";

        var han = "?";

        var fields = <?php echo json_encode($fields) ?>;

        for (var i = 0; i < fields.length; i++)
        {
            if(fields[i]!=remove)
            {
                if (jQuery("#" + fields[i]).val() && jQuery("#" + fields[i]).val() != "") {
                    loc += han + fields[i] + "=" + encodeURI(jQuery("#" + fields[i]).val());
                    han = "&";
                }
                else if (jQuery("input[name='" + fields[i] + "']:checked").val() && jQuery("input[name='" + fields[i] + "']:checked").val() != "") {
                    loc += han + fields[i] + "=" + encodeURI(jQuery("input[name='" + fields[i] + "']:checked").val());
                    han = "&";
                }
            }
        }

        if(jQuery("input[name='sort']:checked").val() && jQuery("input[name='sort']:checked").val()!="")
        {
            loc+= han + "sort=" + jQuery("input[name='sort']:checked").val();
            han="&";
        }
		
		if(jQuery("input[name='new']:checked").val() && jQuery("input[name='new']:checked").val()!="")
        {
            loc+= han + "new=" + jQuery("input[name='new']:checked").val();
            han="&";
        }
		
		if(jQuery("input[name='price']:checked").val() && jQuery("input[name='price']:checked").val()!="")
        {
            loc+= han + "price=" + jQuery("input[name='price']:checked").val();
            han="&";
        }

        if(searchType && searchType!=null && searchKw && searchKw!=null)
        {
            if(remove!=searchType)
            {
                loc += han + "searchtype=" + encodeURI(searchType) + "&search=" + encodeURI(searchKw);
                han = "&";
            }
        }
        else if(getParameterByName("searchtype")!="" && getParameterByName("searchtype") !=null && getParameterByName("search")!="" && getParameterByName("search")!= null)
        {
            if(remove!=getParameterByName("searchtype"))
            {
                loc += han + "searchtype=" + encodeURI(getParameterByName("searchtype")) + "&search=" + encodeURI(getParameterByName("search"));
                han = "&";
            }
        }

        if(getParameterByName("display")!="" && getParameterByName("display") !=null && remove !="display")
        {
            loc += han + "display=" + getParameterByName("display");
            han="&";
        }

        if (ord)
        {
            loc += han + "sort=" + ord;
            han = "&";
        }

        if (page)
        {
            loc += han + "pg=" + page;
            han = "&";
        }

        return loc;
    }
	
		
        
       
		
	
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
</script>
