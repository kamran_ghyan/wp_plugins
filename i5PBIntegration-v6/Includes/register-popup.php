<!-- POPUP START -->
<style>
    .red_class {
        border: 1px #F00 solid;
    }

    #cimy_uef_wp_p_field_7, #cimy_uef_wp_p_field_8 {
        float: left;
        margin: 0 2% 0 0;
        width: 48%;
    }

    #cimy_uef_wp_p_field_8 {
        float: left;
        margin: 0 0 0 2% !important;
        ;
        width: 48%;
    }

    .i5Register input[type="text"], .i5Register input[type="email"], .i5Register input[type="password"], .i5Login input[type="text"], .i5Login input[type="email"], .i5Login input[type="password"] {
        border: 1px solid #CCCCCC !important;
        box-shadow: 2px 2px 5px #ddd inset;
        margin: 0 0 5px !important;
        padding: 5px !important;
        width: 100%;
    }

    .i5Register .button, .i5Login .button {
        margin: 0 !important;
    }

    .i5Register .col-lg-6, .i5Login .col-lg-6 {
        padding: 0 !important;
        text-align: center;
        width: 50%;
        float: left;
    }

    .i5Register p {
        margin: 0 !important;
        font-size: 12px;
    }

    .i5Register .haveAccount {
        text-align: left;
    }

    .i5Register .signUp {
        text-align: right;
    }

    .modal {
        max-height: 550px !important;
    }

    .modal-header h1 {
        margin: 0 !important;
        font-size: 26px;
    }
    /** Save Search Popup **/
    .hide {
        display: none;
    }

    .modal {
        position: fixed;
        top: 50%;
        left: 50%;
        z-index: 105000;
        width: 560px;
        margin: -250px 0 0 -280px;
        background-color: white;
        border: 1px solid #999;
        border: 1px solid rgba(0, 0, 0, 0.3);
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        border-radius: 6px;
        -webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
        -moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
        box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
        -webkit-background-clip: padding-box;
        -moz-background-clip: padding-box;
        background-clip: padding-box;
        outline: none;
    }

    .modal-header {
        padding: 9px 15px;
        border-bottom: 1px solid #eee;
    }

    .modal-body {
        position: relative;
        overflow-y: auto;
        max-height: 500px;
        padding: 15px;
    }
    .modal-dialog{
        margin: 0 !important;
    }
    .modal-footer {
        padding: 14px 15px 15px;
        margin-bottom: 0;
        text-align: right;
        background-color: #f5f5f5;
        border-top: 1px solid #ddd;
        -webkit-border-radius: 0 0 6px 6px;
        -moz-border-radius: 0 0 6px 6px;
        border-radius: 0 0 6px 6px;
        -webkit-box-shadow: inset 0 1px 0 white;
        -moz-box-shadow: inset 0 1px 0 white;
        box-shadow: inset 0 1px 0 white;
    }
    .modal-content {
        height: 548px;
    }
    .modal-body .wpcf7-form {
        color: #777 !important;
    }

    input[type="hidden" i] {
        display: none;
    }
    .i5Register input[type="text"], .i5Register input[type="email"], .i5Register input[type="password"], .i5Login input[type="text"], .i5Login input[type="email"], .i5Login input[type="password"]{
        height: auto !important;
        padding: 15px 5px !important;
    }
    .close {
        float: right;
        font-size: 20px;
        font-weight: bold;
        line-height: 20px;
        text-shadow: 0 1px 0 white;
        opacity: 0.3;
        filter: alpha(opacity=20);
        padding: 5px !important;
        color: #000 !important;
        background-color: transparent !important;
    }
    /* mobile css */
    @media (min-width: 0px) and (max-width: 699px) {
        .modal {
            width: 90% !important;
            margin-left: -45% !important;
        }
    }
    /* end mobile css */
</style>
<script>
function validregsiter() {
   return true;

}

function validregister(a) {

}
 jQuery(document).on("click", ".open-registration", function () {
        var red = jQuery(this).data('id');
        jQuery("input[name='redirect_to']").each(function(){
            jQuery(this).val(red);
        });
 });
 function toggleLogin(show)
 {
     jQuery(".i5Register").hide();
     jQuery(".i5Login").hide();

     if (show)
         jQuery(".i5Login").show();
     else
         jQuery(".i5Register").show();
 }
 
 function doRegister() {
     jQuery("#doLogin").click();
 }
</script>

<?php global $user_ID, $user_identity; get_currentuserinfo();
      global $RedirURL;
      if (!$user_ID) { ?>
<a type="button" data-toggle="modal" data-id="<?php echo rtrim($_SERVER['REQUEST_URI'],"/"); ?>" data-target="#register" id="doLogin" class="open-registration" style="display:none;">Register</a>
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h1>Create an Account</h1>
            </div>
            <div class="modal-body">
                <div class="i5Register">
                    <div id="loginbox"></div>
                    <?php $register = $_GET['register']; $reset = $_GET['reset'];
                          if ($register == true) { ?>

                    <h3>Success!</h3>
                    <p>Check your email for the password and then return to log in.</p>

                    <?php } ?>


                    <form method="post" onsubmit="return validregsiter()" action="/register/" class="wp-user-form">
                        <input type="hidden" name="action" value="register">
                        <input type="hidden" name="stage" value="validate-user-signup">
                        <input type="hidden" id="redirect_to" value="" name="redirect_to">
                        <input type="hidden" name="from_blog_id" value="1">
                        <input id="signupblog" type="hidden" name="signup_for" value="user">
                        <input type="hidden" name="cimy_post" value="1">
                         <p>
                            <label>Email</label>
                            <input type="email" name="user_email" id="user_email" placeholder="" tabindex="3" />
                        </p>
                        <?php do_action( 'register_form' ); ?>
                        <br>
<!--  End of code from Cimy User Extra Fields -->
                        <div class="col-lg-6 haveAccount">
                            <span>
                                Have an account?
                                <a type="button" href="javascript:toggleLogin(true)">Login here</a>
                            </span>
                        </div>
                        <div class="col-lg-6 signUp">
                            <input type="submit" class="button" value="Sign Up">
                        </div>
                    </form>

                    
                </div>
                <div style="display:none;" class="i5Login">
                    <div id="loginbox">
                        <?php
          $args = array(
          'echo'           => true,
          'redirect' =>  $_SERVER["REQUEST_URI"],
          'form_id'        => 'loginform',
          'label_username' => __( 'Email' ),
          'label_password' => __( 'Password' ),
          'label_remember' => __( 'Remember Me' ),
          'label_log_in'   => __( 'Log In' ),
          'id_username'    => 'user_loginn',
          'id_password'    => 'user_pass',
          'id_remember'    => 'rememberme',
          'id_submit'      => 'wp-submit',
          'remember'       => true,
          'value_username' => '',
          'value_remember' => false
  ); wp_login_form( $args ); ?>
                        <div class="col-lg-12" style="text-align:center;">
                            <a class="forgot" data-toggle="modal" data-target="#sendMessagepass" href="javascript:;">Lost Password?</a>
                            |
                            <a href="javascript:toggleLogin(false);">Register</a>
                        </div>

                    </div>
                    <?php /*?><div id="forgotbox">
          <form name="lostpasswordform" id="lostpasswordform" action="http://localhost/uptown/wp-login.php?action=lostpassword" method="post">


          <input placeholder="user_login" name="user_login" id="user_login" class="input" value="" size="20" type="text" style="    margin-bottom: 11px;">


          <input name="redirect_to" value="<?php bloginfo('url'); ?>" type="hidden">
          <p class="submit"><input name="wp-submit" id="wp-submit" class="button button-primary button-large" value="Get Password" type="submit"></p>
          </form><a class="loginbx" href="javascript:;">Login?</a>
          </div><?php */?>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-header"></div>
        </div>
    </div>
</div>
<?php } ?>
<!-- POPUP END -->