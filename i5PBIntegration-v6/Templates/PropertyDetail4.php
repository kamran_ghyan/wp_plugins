<?php
wp_enqueue_style('property-detail4', i5PBIntegration__PLUGIN_URL . 'css/property-detail4.css', array(), '1.1', 'all');
wp_enqueue_style('bootstrap-css', i5PBIntegration__PLUGIN_URL . 'css/bootstrap.min.css', array(), '1.1', 'all');
//wp_enqueue_script( 'bootstrap-js', i5PBIntegration__PLUGIN_URL . 'js/bootstrap.min.js');
wp_enqueue_script('slick', i5PBIntegration__PLUGIN_URL . 'js/slick.min.js', array('jquery'), '1.1', true);
wp_enqueue_script('PrintElement', i5PBIntegration__PLUGIN_URL . 'js/PrintElement.js', array('jquery'), '1.1', true);

$showSave = false;
get_header();

$maxResults = 24;

include i5PBIntegration__PLUGIN_DIR . 'Includes/Prospect-Functions.php';
include i5PBIntegration__PLUGIN_DIR . 'Includes/PropertyDetail-Functions.php';
include i5PBIntegration__PLUGIN_DIR . 'Includes/PropertySearch-Functions.php';
//include(i5PBIntegration__PLUGIN_DIR . "Includes/HeaderSearch.php");
include i5PBIntegration__PLUGIN_DIR . 'Includes/Global-Functions.php';
/*
Template Name: Property Detail Theme 4
 */
/*echo '<pre>';
print_r($property);
echo '</pre>';
echo '<pre>';
print_r($images);
echo '</pre>';*/

?>

<div class="container">
	<!--<div class="row banner">
    	<div class="banner-title">
        	<a href="<?php echo home_url() . '/property-form/' ?>">
            	<span><?php _e('FIND OUT AS SOON AS A PROPERTY WITH YOUR CRITERIA IS LISTED', 'i5pb');?></span>
            </a>
        </div>
    </div> -->
    <div class="row property-header">
    	<div class="col-md-12">
        	<div class="background">
				<img alt="" src="https://maps.googleapis.com/maps/api/staticmap?size=640x125&amp;center=<?php echo $property->address; ?>&amp;zoom=13&amp;format=png&amp;scale=2&amp;maptype=roadmap&amp;" width="960">
<img alt="Header-gradient-overlay" class="gradient" src="<?php echo i5PBIntegration__PLUGIN_URL . '/images/header-gradient-overlay.png' ?>" height="168" width="1090">
<img alt="Mm1458" class="marker" src="http://properties.nainorcal.com/go/mm1458">

            </div>
            <div class="head-content">
                <div class="circle-image">
                    <img alt="<?php echo $property->title; ?>" src="<?php echo $images[0]->url; ?>">
                </div>
                <div class="property-heading-address">
                    <?php
                    $listType = $property->listingType;
                    $findType   = 'Lease';
                    $type = strpos($listType, $findType);
                    if ($type == true) {
                        $listType_ = "Lease";
                    } else {
                        $listType_ = "Sale";

                    }
                    ?>
                    <h1 style=""><?php echo $property->propertytype; ?> <?php _e('For ' . $listType_, 'i5pb');?></h1>
                    <h3 id="property-form-address"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $property->address; ?></h3>
                    <h4><?php echo $property->city; ?>, <?php echo $property->state; ?> <?php echo $property->zip; ?></h4>
                </div>
            </div>
        </div>
    </div>
    <div class="row property-tabs-section">
    	<div class="col-md-12 no-padding">
    	  <div class="col-md-10" style="padding-right:0;">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#overview" aria-controls="overview" role="tab" data-toggle="tab"><?php _e('Overview', 'i5pb');?></a></li>
            <li role="presentation"><a href="#photos" aria-controls="photos" role="tab" data-toggle="tab"><?php _e('Photos', 'i5pb');?></a></li>
            <li role="presentation"><a href="#document" aria-controls="document" role="tab" data-toggle="tab"><?php _e('Documents', 'i5pb');?></a></li>
            <li role="presentation"><a href="#map" id="resize_map" aria-controls="map" role="tab" data-toggle="tab"><?php _e('View on Map', 'i5pb');?></a></li>
          </ul>

          </div>
          <div class="col-md-2 detail-back"><div class="pull-right"><a href="<?php echo home_url() . '/property-search/' ?>"><i class="fa fa-reply-all" aria-hidden="true"></i>&nbsp;&nbsp;<b>BACK</b></a></div></div>
          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="overview">
            	<div class="col-md-9 property-overview">

                	<h1><?php _e('Sale Overview', 'i5pb');?></h1>
                    <div class="row">
                    	<div class="col-md-6">
                        	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                              <!-- Indicators -->
                              <ol class="carousel-indicators">

                                <?php
$i = 0;
foreach ($images as $image) {
	?>
                                    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if ($i == 0) {echo "active";}?>"></li>
                                <?php $i++;}?>
                              </ol>
                              <!-- Wrapper for slides -->
                              <div class="carousel-inner" role="listbox">
                                <?php
$i = 0;
foreach ($images as $image) {
	?>
                                <div class="item <?php if ($i == 0) {echo "active";}?>">
                                  <img src="<?php echo $image->url; ?>">
                                </div>
                                <?php $i++;}?>
                              </div>
                              <!-- Controls -->
                              <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left fa fa-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only"><?php _e('Previous', 'i5pb');?></span>
                              </a>
                              <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right fa fa-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only"><?php _e('Next', 'i5pb');?></span>
                              </a>
                            </div>
                        </div>
                        <div class="col-md-6 property-atts">

                            <?php
$ptype = $property->listingType;
if (strpos(strtolower($ptype), 'lease') != false) {
	?>
                             <table>
                            <tbody>
                            <?php if (!empty($property->price)): ?>
                            <tr>
                              <td>
                                <div><?php _e('Price:', 'i5pb');?></div>
                              </td>
                              <td>$<?php echo number_format($property->price, 0); ?></td>
                            </tr>
                            <?php endif;?>
                            <?php if (!empty($property->size)): ?>
                            <tr>
                              <td>
                                <div><?php _e('Size:', 'i5pb');?></div>
                              </td>
                              <td><?php echo number_format($property->size, 0); ?> SF</td>
                            </tr>
                            <?php endif;?>
                            <?php if (!empty($property->leaseType)): ?>
                            <tr>
                              <td>
                                <div><?php _e('Lease Type:', 'i5pb');?></div>
                              </td>
                              <td>
                                <?php echo $property->leaseType; ?>
                              </td>
                            </tr>
                            <?php endif;?>
                            <?php if (!empty($property->propertytype)): ?>
                            <tr>
                              <td>
                                <div><?php _e('Property Type:', 'i5pb');?></div>
                              </td>
                              <td><?php echo $property->propertytype; ?></td>
                            </tr>
                            <?php endif;?>
                            <?php if (!empty($property->propertySubType)): ?>
                            <tr>
                              <td>
                                <div><?php _e('Property Subtype:', 'i5pb');?></div>
                              </td>
                              <td><?php echo $property->propertySubType; ?></td>
                            </tr>
                            <?php endif;?>
                            <?php if (!empty($property->buildingClass)): ?>
                            <tr>
                              <td>
                                <div><?php _e('Building Class:', 'i5pb');?></div>
                              </td>
                              <td><?php echo $property->buildingClass; ?></td>
                            </tr>
                            <?php endif;?>
                            <?php if (!empty($property->yearbuilt)): ?>
                            <tr>
                              <td>
                                <div><?php _e('Year Built:', 'i5pb');?></div>
                              </td>
                              <td><?php echo $property->yearbuilt; ?></td>
                            </tr>
                            <?php endif;?>
                            <?php if (!empty($property->zoning)): ?>
                             <tr>
                              <td>
                                <div><?php _e('Zoning:', 'i5pb');?></div>
                              </td>
                              <td><?php echo $property->zoning; ?> SF</td>
                            </tr>
                            <?php endif;?>
                            <?php if (!empty($property->area)): ?>
                            <tr>
                              <td>
                                <div><?php _e('Total Available (SF):', 'i5pb');?></div>
                              </td>
                              <td><?php echo number_format($property->area, 0); ?> SF</td>
                            </tr>
                            <?php endif;?>
                            <?php if (!empty($property->minContiguous)): ?>
                            <tr>
                              <td>
                                <div><?php _e('Min Contiguous Available (SF):', 'i5pb');?></div>
                              </td>
                              <td><?php echo number_format($property->minContiguous, 0); ?> SF</td>
                            </tr>
                            <?php endif;?>
                            <?php if (!empty($property->maxContiguous)): ?>
                            <tr>
                              <td>
                                <div><?php _e('Max Contiguous Available (SF):', 'i5pb');?></div>
                              </td>
                              <td><?php echo number_format($property->maxContiguous, 0); ?> SF</td>
                            </tr>
                            <?php endif;?>

                            </tbody></table>
                            <?php
} else {
	?>
                            <table>
                            <tbody>
                            <?php if (!empty($property->price)): ?>
                            <tr>
                              <td>
                                <div><?php _e('Price:', 'i5pb');?></div>
                              </td>
                              <td>$<?php echo number_format($property->price, 0); ?></td>
                            </tr>
                          <?php endif;?>
                          <?php if (!empty($property->propertytype)): ?>
                            <tr>
                              <td>
                                <div><?php _e('Property Type:', 'i5pb');?></div>
                              </td>
                              <td><?php echo $property->propertytype; ?></td>
                            </tr>
                            <?php endif;?>
                            <?php if (!empty($property->listingType)): ?>
                            <tr>
                              <td>
                                <div><?php _e('Property Subtype:', 'i5pb');?></div>
                              </td>
                              <td><?php echo $property->listingType; ?></td>
                            </tr>
                            <?php endif;?>
                            <?php if (!empty($property->size)): ?>
                            <tr>
                              <td>
                                <div><?php _e('Building Size:', 'i5pb');?></div>
                              </td>
                              <td><?php echo number_format($property->size, 0); ?> SF</td>
                            </tr>
                            <?php endif;?>
                            <?php if (!empty($property->yearbuilt)): ?>
                            <tr>
                              <td>
                                <div><?php _e('Year Built:', 'i5pb');?></div>
                              </td>
                              <td><?php echo $property->yearbuilt; ?></td>
                            </tr>
                            <?php endif;?>
                            <?php if (!empty($property->lotsize)): ?>
                            <tr>
                              <td>
                                <div><?php _e('Lot Size:', 'i5pb');?></div>
                              </td>
                              <td><?php echo number_format($property->lotsize, 0); ?> SF</td>
                            </tr>
                            <?php endif;?>
                            <?php if (!empty($property->zoning)): ?>
                            <tr>
                            <tr>
                              <td>
                                <div><?php _e('Zoning:', 'i5pb');?></div>
                              </td>
                              <td><?php echo $property->zoning; ?> SF</td>
                            </tr>
                            <?php endif;?>
                            </tbody></table>
                            <?php }?>
                            <?php if (!empty($property->custom2)): ?>
                        	  <div class="">
                            	<br />
                              <a href="#" class="btn btn-em " target="_blank"><?php _e('Download Offering Memorandum (L)', 'i5pb');?></a>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>

                    <div class="description">
                      <h4><?php _e('Description', 'i5pb');?></h4>
                        <p><?php echo $property->description; ?></p>
                    </div>

                    <div class="highlights">
                    	<h4><?php _e('Highlights', 'i5pb');?></h4>
                    	<?php echo $property->interiorFeatures; ?>
                    </div>


                    <?php
$ptype = $property->listingType;
if (strpos(strtolower($ptype), 'lease') != false) {

} else {
	?>
               		  <div class="financialAnalysis">
                    	<h4><?php _e('Financial Analysis', 'i5pb');?></h4>
                      <?php if (is_user_logged_in()) {?>
                      <table class="table table-striped">
                        <tbody>
                          <tr>
                            <td width="20%" class="text-right"><b><?php _e('Listing Price', 'i5pb');?></b></td>
                            <td width="20%" class="text-left">
                             <?php if (empty($property->price)) {?>
                             <?php _e('-', 'i5pb');?>
                             <?php } else {?>
                             <?php _e('$', 'i5pb');?>
                             <?php echo number_format($property->price, 0); ?>
                             <?php }?>
                            </td>
                            <td width="20%" class="text-right"><b><?php _e('Annual Gross Income (est)', 'i5pb');?></b></td>
                            <td class="text-left">
                             <?php if (empty($property->custom3)) {?>
                             <?php _e('-', 'i5pb');?>
                             <?php } else {?>
                             <?php _e('$', 'i5pb');?>
                             <?php echo number_format($property->custom3, 0); ?>
                             <?php }?>
                            </td>
                          </tr>
                          <tr>
                              <td width="20%" class="text-right"><b><?php _e('Land Value', 'i5pb');?></b></td>
                              <td width="20%" class="text-left">
                                  <?php if (empty($property->taxes)) {?>
                                      <?php _e('-', 'i5pb');?>
                                  <?php } else {?>
                                      <?php _e('$', 'i5pb');?>
                                      <?php echo number_format($property->taxes, 0); ?>
                                  <?php }?>
                              </td>
                              <td class="text-right"><b><?php _e('Annual Expense (est)', 'i5pb');?></b></td>
                              <td class="text-left">
                                  <?php if (empty($property->custom5)) {?>
                                      <?php _e('-', 'i5pb');?>
                                  <?php } else {?>
                                      <?php _e('$', 'i5pb');?>
                                      <?php echo number_format($property->custom5, 0); ?>
                                  <?php }?>
                              </td>
                          </tr>
                          <tr>
                            <td class="text-right"><b><?php _e('Overall Price / SF', 'i5pb');?></b></td>
                            <td class="text-left">
                             <?php if (empty($property->custom1)) {?>
                             <?php _e('-', 'i5pb');?>
                             <?php } else {?>
                             <?php _e('$', 'i5pb');?>
                             <?php echo number_format($property->custom1, 0); ?>
                             <?php }?>
                            </td>
                            <td class="text-right"><b><?php _e('NNN', 'i5pb');?></b></td>
                            <td class="text-left">
                             <?php if (empty($property->custom8)) {?>
                             <?php _e('-', 'i5pb');?>
                             <?php } else {?>
                             <?php _e('$', 'i5pb');?>
                             <?php echo number_format($property->custom8, 0); ?>
                             <?php }?>
                            </td>
                          </tr>
                          <tr>
                            <td class="text-right"><b><?php _e('Building Price / SF', 'i5pb');?></b></td>
                            <td class="text-left">
                             <?php if (empty($property->custom4)) {?>
                             <?php _e('-', 'i5pb');?>
                             <?php } else {?>
                             <?php _e('$', 'i5pb');?>
                             <?php echo number_format($property->custom4, 0); ?>
                             <?php }?>
                            </td>
                            <td class="text-right"><b><?php _e('NOI (est)', 'i5pb');?></b></td>
                            <td class="text-left">
                             <?php if (empty($property->custom7)) {?>
                             <?php _e('-', 'i5pb');?>
                             <?php } else {?>
                             <?php _e('$', 'i5pb');?>
                             <?php echo number_format($property->custom7, 0); ?>
                             <?php }?>
                            </td>
                          </tr>
                          <tr>
                            <td class="text-right"><b><?php _e('Land Price / SF', 'i5pb');?></b></td>
                            <td class="text-left">
                             <?php if (empty($property->custom2)) {?>
                             <?php _e('-', 'i5pb');?>
                             <?php } else {?>
                             <?php echo $property->custom2; ?>
                             <?php }?>
                            </td>
                            <td class="text-right"><b><?php _e('CAP Rate (calculated)', 'i5pb');?></b></td>
                            <td class="text-left">
                             <?php if (empty($property->custom9)) {?>
                             <?php _e('-', 'i5pb');?>
                             <?php } else {?>
                             <?php echo $property->custom9; ?>
                             <?php }?>
                            </td>
                          </tr>
                          <tr>
                          </tr>
                          <tr>
                              <td class="text-right"><b><?php _e('Price / Unit', 'i5pb');?></b></td>
                              <td class="text-left">
                                  <?php if (empty($property->custom6)) {?>
                                      <?php _e('-', 'i5pb');?>
                                  <?php } else {?>
                                      <?php echo $property->custom6; ?>
                                  <?php }?>
                              </td>
                            <td class="text-right"><b><?php _e('Inc/Exp updated', 'i5pb');?></b></td>
                            <td class="text-left">
                             <?php if (empty($property->custom10)) {?>
                             <?php _e('-', 'i5pb');?>
                             <?php } else {?>
                             <?php echo $property->custom10; ?>
                             <?php }?>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      <?php } else {?>
<a href="javascript:doRegister();" class="btn btn-em btn-detial-bottom" style="">Please register to view estimated financial data</a>
                      <?php }?>
                    </div>
                    <?php }?>

                </div>
                <div class="col-md-3 sidebar-agent">

                	<h4><?php _e('Contact Broker', 'i5pb');?></h4>
                	<div class="row">

                        <div class="col-md-12 agent-info">
                        <?php if (!empty($property->ownerName)): ?>
                        <span class="name"><i class="abbr"><?php _e('Name: ', 'i5pb');?></i>
                        <?php echo $property->ownerName; ?> </span>
                        <?php endif;?>
						            <?php if (!empty($property->ownerEmail)): ?>
                            <span><i class="abbr"><?php _e('Email: ', 'i5pb');?>
                            </i> <a href="mailto:<?php echo $property->ownerEmail; ?>">
                            <?php echo $property->ownerEmail; ?></a></span>
                        <?php endif;?>
                        <?php if (!empty($property->ownerPhone)): ?>
                            <span><i class="abbr"><?php _e('Phone: ', 'i5pb');?></i>
                            <?php echo $property->ownerPhone; ?></span>
						            <?php endif;?>
                        </div>
                        <div class="col-md-12">
                        <h4><?php _e('Request more information', 'i5pb');?></h4>
                        	<?php
echo do_shortcode('[contact-form-7 id="49" title="Request more information"]');
?>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="photos">
            	<div class="col-md-12 property-photos">
                	<div id="carousel-photo" class="carousel slide" data-ride="carousel">
                              <!-- Indicators -->
                              <ol class="carousel-indicators">

                                <?php
$i = 0;
foreach ($images as $image) {
	?>
                                    <li data-target="#carousel-photo" data-slide-to="<?php echo $i; ?>" class="<?php if ($i == 0) {echo "active";}?>"></li>
                                <?php $i++;}?>
                              </ol>

                              <!-- Wrapper for slides -->
                              <div class="carousel-inner" role="listbox">

                                 <?php
$i = 0;
foreach ($images as $image) {
	?>
                                <div class="item <?php if ($i == 0) {echo "active";}?>">
                                  <img src="<?php echo $image->url; ?>">
                                </div>
                                <?php
$i++;
}?>

                              </div>

                              <!-- Controls -->
                              <a class="left carousel-control" href="#carousel-photo" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left fa fa-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only"><?php _e('Previous', 'i5pb');?></span>
                              </a>
                              <a class="right carousel-control" href="#carousel-photo" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right fa fa-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only"><?php _e('Next', 'i5pb');?></span>
                              </a>
                            </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="document">
            	<div class="col-md-9 property-document">
                	<h1><?php _e('Documents', 'i5pb');?></h1>
                    <div class="row">

                    <div class="col-md-12">
                    	<a href="#"><?php _e('Offering Memorandum (L)', 'i5pb');?></a>
                        <div class="updated-at">Updated 4 days</div>

                    </div>
                    </div>
                </div>
            	<div class="col-md-3 sidebar-agent">
                	<h4><?php _e('Contact Broker', 'i5pb');?></h4>
                	<div class="row">

                        <div class="col-md-12 agent-info">
                        <?php if (!empty($property->ownerName)): ?>
                           <span class="name"><i class="abbr"><?php _e('Name: ', 'i5pb');?></i>
                           <?php echo $property->ownerName; ?> </span>
                        <?php endif;?>
                        <?php if (!empty($property->ownerEmail)): ?>
                          esre
                            <span><i class="abbr"><?php _e('Email: ', 'i5pb');?></i>
                            <a href="mailto:<?php echo $property->ownerEmail; ?>"><?php echo $property->ownerEmail; ?></a></span>
                        <?php endif;?>
                        <?php if (!empty($property->ownerPhone)): ?>
                            <span><i class="abbr"><?php _e('Phone: ', 'i5pb');?></i>
                            <?php echo $property->ownerPhone; ?></span>
                           <?php endif;?>
                        </div>
                        <div class="col-md-12">
                        <h4><?php _e('Request more information', 'i5pb');?></h4>
                        	<?php
echo do_shortcode('[contact-form-7 id="49" title="Request more information"]');
?>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="map">
            	<div class="col-md-9 no-padding">
                <div>
<?php
do_shortcode('[i5ListingMap
              maxzoom="100"
              minzoom="1"
              style="display:block;width: 100%; height: 600px"
              showdetails="false"
              showpolygon="true"
              zoom="10"
              lat="' . $property->latitude . '"
              long="' . $property->longitude . '" ]'
);
echo "<script>jQuery(window).load(function(){markAddress(" . json_encode($property) . "); });</script>";
?>
              </div>
            </div>
            <div class="col-md-3 sidebar-agent sidebar-agent-map">
                	<h4><?php _e('Contact Broker', 'i5pb');?></h4>
                	<div class="row">

                        <div class="col-md-12 agent-info">
                        <?php if (!empty($property->ownerName)): ?>
                           <span class="name"><i class="abbr"><?php _e('Name: ', 'i5pb');?></i>
                           <?php echo $property->ownerName; ?> </span>
                        <?php endif;?>
                        <?php if (!empty($property->ownerEmail)): ?>
                            <span><i class="abbr"><?php _e('Email: ', 'i5pb');?></i>
                            <a href="mailto:<?php echo $property->ownerEmail; ?>"><?php echo $property->ownerEmail; ?></a></span>
                        <?php endif;?>
                        <?php if (!empty($property->ownerPhone)): ?>
                            <span><i class="abbr"><?php _e('Phone: ', 'i5pb');?></i>
                            <?php echo $property->ownerPhone; ?></span>
                           <?php endif;?>
                        </div>
                        <div class="col-md-12">
                        <h4><?php _e('Request more information', 'i5pb');?></h4>
                        	<?php
echo do_shortcode('[contact-form-7 id="49" title="Request more information"]');
?>
                        </div>
                    </div>

                </div>
          </div>

    </div>

    </div>
</div>

<div class="row">
      <div class="col-md-12">

        <address style="margin: 30px 0;">
        <h4 style="margin: 5px 0;"><?php _e('AKMLS LISTING OFFICE', 'i5pb');?></h4>
        <span style="font-size: 12px;"><?php _e('Keller Williams Realty Alaska Group, LLC field is Listing Office Name', 'i5pb');?></span>
      </address>

      </div>
      <div class="col-md-1 no-padding">
        <img style='height:90px; width:90px;'  alt='Alaska MLS' title='Alaska MLS' src="<?php echo i5PBIntegration__PLUGIN_URL; ?>/images/idx-large.gif" />
      </div>
      <div class="col-md-11">
        <p><i><?php _e("DISCLAIMER: The listing content relating to real estate for sale or lease on this web site comes in part from the IDX Program of Alaska Multiple Listing Service, Inc. (AK MLS). Real estate listings held by brokerage firms other than Northern Edge Real Estate, LLC are marked with either the listing brokerage's logo or the AK MLS logo and information about them includes the name of the listing brokerage. All information is deemed reliable but is not guaranteed and should be independently verified for accuracy. Listing information was last", 'i5pb');?>
        <b><?php _e('updated on:', 'i5pb');?> <?php echo date('Y-m-d H:m:s', strtotime('last day')); ?></b></i></p>
      </div>
    </div>
<script type="text/javascript">
jQuery(document).ready(function(e) {
    jQuery('#carousel-photo').carousel();

	// hidden address for form
	var property_address = jQuery('#property-form-address').text();
	jQuery('#hdnAddress').attr('value', property_address);

	// active tab on URL basis
	if(window.location.hash != "") {
      jQuery('a[href="' + window.location.hash + '"]').click()
  	}
});


jQuery('#resize_map').click(
    function(e)
    {
      e.preventDefault();
      var center = new google.maps.LatLng(<?php echo $property->latitude; ?>, <?php echo $property->longitude; ?>);
      var id = jQuery(this).attr('href');
      jQuery('#map').fadeIn(2000);
      google.maps.event.trigger(document.getElementById('divListings'), "resize");
      map.setCenter(center);

    });

</script>


<?php
get_footer();

?>