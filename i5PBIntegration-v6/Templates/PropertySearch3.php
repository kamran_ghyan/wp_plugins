<style>
    .slick-prev:before, .slick-next:before {
        color: #fff !important;
    }
</style>
<?php
wp_enqueue_style( 'property-search3', i5PBIntegration__PLUGIN_URL . 'css/search-results3.css', array(), '1.1', 'all');

if(isset($_REQUEST["display"]) && $_REQUEST["display"]=="photo")
{
	
    wp_enqueue_style( 'slick-styles', i5PBIntegration__PLUGIN_URL . 'css/slick.css', array(), '1.1', 'all');
    wp_enqueue_style( 'slick-theme', i5PBIntegration__PLUGIN_URL . 'css/slick-theme.css', array(), '1.1', 'all');
    wp_enqueue_script( 'slick', i5PBIntegration__PLUGIN_URL . 'js/slick.min.js', array('jquery'), '1.1', true );
}

get_header();  
	get_template_part('index_header');

$maxResults=24;
include(i5PBIntegration__PLUGIN_DIR . 'Includes/Prospect-Functions.php');
include(i5PBIntegration__PLUGIN_DIR . 'Includes/PropertySearch-Functions.php');
/*
Template Name: Property Search Theme 3
 */

?>
<div class="clearfix"></div>
	<!-- Start inner page -->
	<div class="inner-page searchResultsPage">
		<div class="filters test">
			<?php 
				include(i5PBIntegration__PLUGIN_DIR . 'Includes/HeaderSearch3.php');
			//include 'includes/HeaderSearch3.php'; 
			?>
		</div>
	</div>
<div class="fullWidth">
<div class="searchResults">
	<div class="resultsFilters">
            <div class="resultsSort">
                <div onclick="changeDisplay('map');" class="btnMap<?php echo (isset($_REQUEST['display']) && $_REQUEST['display']=='map'?' selectedDisplay':'') ?>">
                    <i class="fa fa-th"></i>
                </div>
                <div onclick="changeDisplay('photo');" class="btnPhoto<?php echo (isset($_REQUEST['display']) && $_REQUEST['display']=='photo'?' selectedDisplay':'') ?>">
                    <i class="fa fa-list"></i>
                </div>
                <!-- <div onclick="changeDisplay('gallery');" class="btnGallery<?php echo (!isset($_REQUEST['display']) || (isset($_REQUEST['display']) && $_REQUEST['display']=='gallery')?' selectedDisplay':'') ?>">
                    <i class="fa fa-table"></i>
                </div> -->
                <div class="btnOurIcons">
                    Our Icons
                </div>
            </div>
            
            <div id="divFilters">
                <?php if(isset($_REQUEST["minprice"])) :?>
                    <div class="filter">Price Min: <?php echo $_REQUEST["minprice"]?><div class="remove" onclick="removeFilter('minprice');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["maxprice"])) :?>
                    <div class="filter">Price Max: <?php echo $_REQUEST["maxprice"]?><div class="remove" onclick="removeFilter('maxprice');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["minbedrooms"])) :?>
                    <div class="filter">Bed(s) Min: <?php echo $_REQUEST["minbedrooms"]?><div class="remove" onclick="removeFilter('minbedrooms');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["maxbedrooms"])) :?>
                    <div class="filter">Bed(s) Max: <?php echo $_REQUEST["maxbedrooms"]?><div class="remove" onclick="removeFilter('maxbedrooms');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["minfullBaths"])) :?>
                    <div class="filter">Bath(s) Min: <?php echo $_REQUEST["minfullBaths"]?><div class="remove" onclick="removeFilter('minfullBaths');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["newConstruction"])) :?>
                    <div class="filter">New Constructoin: <?php echo $_REQUEST["newConstruction"]=="2"?"No":"Yes"?><div class="remove" onclick="removeFilter('newConstruction');">X</div></div>
                <?php endif;?>
              <?php if(isset($_REQUEST["maxfullBaths"])) :?>
                    <div class="filter">Bath(s) Max: <?php echo $_REQUEST["maxfullBaths"]?><div class="remove" onclick="removeFilter('maxfullBaths');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["minsize"])) :?>
                <div class="filter">Square Footage Min: <?php echo $_REQUEST["minsize"]?><div class="remove" onclick="removeFilter('minsize');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["maxsize"])) :?>
                    <div class="filter">Square Footage Max: <?php echo $_REQUEST["maxsize"]?><div class="remove" onclick="removeFilter('maxsize');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["minlotsize"])) :?>
                    <div class="filter">Acres Min: <?php echo $_REQUEST["minlotsize"]?><div class="remove" onclick="removeFilter('minlotsize');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["maxlotsize"])) :?>
                    <div class="filter">Acres Max: <?php echo $_REQUEST["maxlotsize"]?><div class="remove" onclick="removeFilter('maxlotsize');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["minyearbuilt"])) :?>
                <div class="filter">Year Built Min: <?php echo $_REQUEST["minyearbuilt"]?><div class="remove" onclick="removeFilter('minyearbuilt');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["maxyearbuilt"])) :?>
                    <div class="filter">Year Built Max: <?php echo $_REQUEST["maxyearbuilt"]?><div class="remove" onclick="removeFilter('maxyearbuilt');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["minparkingSpaces"])) :?>
                    <div class="filter">Parking Spaces Min: <?php echo $_REQUEST["minparkingSpaces"]?><div class="remove" onclick="removeFilter('minparkingSpaces');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["maxparkingSpaces"])) :?>
                    <div class="filter">Parking Spaces Max: <?php echo $_REQUEST["maxparkingSpaces"]?><div class="remove" onclick="removeFilter('maxparkingSpaces');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["minstories"])) :?>
                    <div class="filter">Stories Min: <?php echo $_REQUEST["minstories"]?><div class="remove" onclick="removeFilter('minstories');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["maxstories"])) :?>
                    <div class="filter">Stories Max: <?php echo $_REQUEST["maxstories"]?><div class="remove" onclick="removeFilter('maxstories');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["searchtype"]) && isset($_REQUEST["search"])) :?>
                <div class="filter"><?php echo $_REQUEST["searchtype"]?>: <?php echo $_REQUEST["search"]?><div class="remove" onclick="removeFilter('<?php echo $_REQUEST["searchtype"]?>');">X</div></div>
                <?php endif;?>
                <?php foreach($types as $type){
                          if(isset($type) && $type!="")
                              echo "<div class='filter'>Property Type: " . $type . "<div class='remove' onclick='removeFilter(\"" . $type . "\");'>X</div></div>";
                      }
                ?>
            </div>
        </div>
        <div class="searchResultsHeader">
            <!-- <?php echo $TTLResults?> Properties for sale
            <span class="timestamp">
                Updated
                <b>
                    <?php
  $settings=get_option('WebListing_Settings');
                    $lastRun=strtotime(date("Y-m-d\Th:i:s"));
                    $currTime=$lastRun;

                    if(isset($settings["LastRun"]))
                        $lastRun = strtotime($settings["LastRun"]);


                    echo round(abs($currTime - $lastRun) / 60,0) . " MIN AGO";
                    ?>
                </b>
            </span>-->
            <div class="headerPaging">
            	<?php include(i5PBIntegration__PLUGIN_DIR . 'Includes/SearchPaging-Functions.php');?>
            	<ul>
            		<li>Sort by:</li>
            		<li class="dropbtn sortMore" onclick="showSortNew()"> <i class="fa fa-star"></i> Newest
                        <div id="ddSortNew" class="ddSort dropdownContent" >
                             <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="descyearbuilt" ?" checked ":"") ?> type="radio" value="descyearbuilt" />Year Built (Newest)</div>
	                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="ascyearbuilt" ?" checked ":"") ?> type="radio" value="ascyearbuilt" />Year Built (Oldest)</div>
                              </div>                    
                    </li>
            		<li class="dropbtn sortMore" onclick="showSortPrice()"><i class="fa fa-money"></i> Price
                    	<div id="ddSortPrice" class="ddSort dropdownContent" >
	                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="descprice" ?" checked ":"") ?> type="radio" value="descprice" />Price (Highest)</div>
	                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="ascprice" ?" checked ":"") ?> type="radio" value="ascprice" />Price (Lowest)</div>
                    </li>
            		<li class="dropbtn sortMore" onclick="showSort()"><i class="fa fa-plus"></i> More
            		<div id="ddSort" class="ddSort dropdownContent">
	                     <!--<div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="descprice" ?" checked ":"") ?> type="radio" value="descprice" />Price (Highest)</div>
	                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="ascprice" ?" checked ":"") ?> type="radio" value="ascprice" />Price (Lowest)</div>
	                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="5" ?" checked ":"") ?> type="radio" />Most Popular</div>-->
	                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="descbedrooms" ?" checked ":"") ?> type="radio" value="descbedrooms" />Bedrooms (Most)</div>
	                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="descfullBaths" ?" checked ":"") ?> type="radio" value="descfullBaths" />Bathrooms (Most)</div>
	                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="desclotsize" ?" checked ":"") ?> type="radio" value=desclotsize />Acreage (Highest)</div>
	                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="asclotsize" ?" checked ":"") ?> type="radio" value="asclotsize" />Acreage (Lowest)</div>
	                     
	                     
            		</li>
            		<!-- <div class="btnSort dropbtn" onclick="showSort()">
                    Sort <i class="fa fa-angle-down"></i>
	                </div> -->
	              	
            	</ul>
            </div>
        </div>
        <?php if(!isset($_REQUEST["display"]) || $_REQUEST["display"]=="gallery") :?>
        <div class="row" style="margin:0;">
            <?php foreach($SearchResults as $result){?>
            <div class="col1-2">
                <div class="listing">
                    <div class="listingImage">
                        <?php if(isset($result->image) && $result->image!=""):?>
                        <img src="<?php echo $result->image?>" />
                        <?php else:?>
                        <img src="<?php echo i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png" ?>" />
                        <?php endif;?>
                    </div>
                    <div id="fav<?php echo $result->mlsid?>" class="<?php echo ($favs=='' || !in_array($result->mlsid,$favs)?'listingFavorite':'listingFavoriteSelected')?>">
                        <a href="javascript:toggleFav('<?php echo $result->mlsid ?>');setFavorite('<?php echo $result->mlsid ?>',<?php echo ($favs=='' || !in_array($result->mlsid,$favs)?'1':'0')?>);">
                            <i class="fa fa-heart"></i>
                        </a>
                    </div>
                    <div class="listingAddress">
                        <?php echo (isset($result->subdivision) && $result->subdivision!=""?$result->subdivision . " | ":$result->area)?><?php echo $result->city?>
                    </div>
                    <div class="listingPrice">
                        <?php echo "$" . number_format($result->price,0)  . ($result->propertytype=="Rental"?" /Month":"")?>
                    </div>
                    <div class="listingBeds">
                        <div class="bedsValue">
                            <?php echo $result->bedrooms?>
                        </div>
                        <div class="bedsLabel">
                            Beds
                        </div>
                    </div>
                    <div class="listingBaths">
                        <div class="bathsValue">
                            <?php
                      if($result->halfBaths!=0 && $result->fullBaths!=0)
                          echo $result->fullBaths + ($result->halfBaths/2);
                      else if($result->halfBaths!=0 && $result->fullBaths==0)
                          echo ($result->halfBaths/2);
                      else
                          echo number_format($result->fullBaths,0);
                            ?>
                        </div>
                        <div class="bathsLabel">
                            Baths
                        </div>
                    </div>
                    <?php if($result->size!=0) :?>
                    <div class="listingSqft">
                        <div class="sqftValue">
                            <?php echo number_format($result->size,0);?>
                        </div>
                        <div class="sqftLabel">
                            SqFt
                        </div>
                    </div>
                    <?php endif;?>
                    <?php if($result->size!=0) :?>
                    <div class="listingAcres">
                        <div class="acresValue">
                            <?php echo number_format($result->lotsize,0);?>
                        </div>
                        <div class="acresLabel">
                            Acres
                        </div>
                    </div>
                    <?php endif;?>

                    <div class="listingStatus">
                        <!-- other status
						<div class="shortSale">Short Sale</div>
            <div class="foreclosure">Foreclosure</div>-->
                        <div class="new">
                            <?php echo $result->status?>
                        </div>
                    </div>
                    <div class="viewDetails">
                        <a href="/property-details/<?php echo urldecode($result->url)?>">View Details</a>
                    </div>
                    <div class="Disclaimer">
                        <?php echo htmlspecialchars_decode(get_post_meta( get_the_ID(), 'disclaimer', true )); ?>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
        <?php elseif($_REQUEST["display"]=="photo"): ?>
        <div class="searchResultsPhoto">
            <div class="listingSidebar mapSidebar">
                <?php foreach($SearchResults as $result){
                          $lat=0;
                          $long=0;

                          if(isset($SearchResults) && sizeof($SearchResults)>0)
                          {
                              $lat=$SearchResults[0]->latitude;
                          }

                ?>
                <div onclick="location.href='/property-details/<?php echo $result->url?>';" onmouseover="openInfoWindow('<?php echo $result->mlsid?>')" style="cursor:pointer;" class="listingWrapper">
	            	<div class="col1-1">
	                    <div class="listingPhoto">
	                        <?php if(isset($result->image) && $result->image!=""):?>
	                        <img src="<?php echo $result->image?>" />
	                        <?php else:?>
	                        <img src="<?php echo i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png" ?>" />
	                        <?php endif;?>
	                        
	                        <div class="shortcuts">
	                        	<a href="#" data-toggle="tooltip" data-placement="left" title="Share" ><i class="fa fa-share"></i></a>
	                        	<a href="#">
									<div id="fav<?php echo $result->mlsid?>" class="<?php echo ($favs=='' || !in_array($result->mlsid,$favs)?'listingFavorite':'listingFavoriteSelected')?>">
				                        <a data-toggle="tooltip" data-placement="left" title="Favorites" href="javascript:toggleFav('<?php echo $result->mlsid ?>');setFavorite('<?php echo $result->mlsid ?>',<?php echo ($favs=='' || !in_array($result->mlsid,$favs)?'1':'0')?>);">
				                            <i class="fa fa-heart"></i>
				                        </a>
				                    </div>
								</a>
	                        	<a data-toggle="tooltip" data-placement="left" title="Hide" href="#"><i class="fa fa-ban"></i></a>
	                        </div>
	                        
	                    </div>
	                    <div class="listingDetails">
	                        <div class="listingAddress">
	                            <div class="listingAddress">
	                                <?php echo $result->address?>
	                            </div>
	                            <div class="listingCity">
	                            	<i class="fa fa-map-marker"></i> <?php echo $result->city?>, <?php echo $result->state?> <?php echo $result->zip?>
	                            </div>
	                            
	                            <!-- 
	                            <div class="listingPpsqft">
	                            	Price per sqft<br />
	                            	<b>$500</b>
	                            </div>
	                             -->
	                        </div>
	                        <div class="listingInfo">
	                        <div class="listingPrice">
                                <?php echo "$" . number_format($result->price,0)  . ($result->propertytype=="Rental"?" /Month":"")?>
                            </div>
	                        <div class="listingStatus">
	                        	<!-- <div class="statusUnderContract"></div> Need to show under contract status -->
	                        	<!-- <div class="statusSold"></div> Need to show sold status -->
	                             <div class="statusActive"></div><?php echo $result->status?><br />
	                             Listed for:
                                 <?php 
								 $current_date = date('Y-m-d');
								 $expire_date  = $result->custom3;
								 if($current_date > $expire_date){
									echo '0 Day';
								  }
								  else{
								  	$todate= strtotime($current_date);
									$fromdate= strtotime($expire_date);
									$calculate_seconds = $todate- $fromdate; // Number of seconds between the two dates
									$days = floor($calculate_seconds / (24 * 60 * 60 )); // convert to days
									echo($days .' Days');
								  }
								?>
	                        </div>
		                        <div class="listingSpecs">
		                        	<div class="listingSpec">
		                        		<div class="listingSpecValue">
		                        			<?php echo $result->bedrooms?>
		                        		</div>
		                        		<div class="listingSpecLabel">
		                        			Bed
		                        		</div>
		                        	</div>
		                        	<div class="listingSpec">
		                        		<div class="listingSpecValue">
		                        			<?php echo $result->fullBaths?>
		                        		</div>
		                        		<div class="listingSpecLabel">
		                        			Bath
		                        		</div>
		                        	</div>
		                        	<div class="listingSpec lastSpec">
		                        		<div class="listingSpecValue">
		                        			<?php echo $result->size?>
		                        		</div>
		                        		<div class="listingSpecLabel">
		                        			Sqft
		                        		</div>
		                        	</div>
		                                <!--<?php
			                          $str=$result->bedrooms . " <div class='specLabel'>Bed</div> ";
			
			                          if($result->halfBaths!=0 && $result->fullBaths!=0)
			                              $str .= $result->fullBaths + ($result->halfBaths/2);
			                          else if($result->halfBaths!=0 && $result->fullBaths==0)
			                              $str .= ($result->halfBaths/2);
			                          else
			                              $str .= number_format($result->fullBaths,0);
			
			                          $str .= " <div class='specLabel'>Bath</div> " . number_format($result->size,0) . " <div class='specLabel'>Sqft</div>";
			
			                          echo $str;
			                                ?>-->
		                       </div>
	                       </div>
	                    </div>
	                </div>
                </div>
                <?php }?>
                <?php include(i5PBIntegration__PLUGIN_DIR . 'Includes/SearchPaging-Functions.php');?>
            </div>
        </div>
        
        </div>
        <div class="mapHolder">
            <div class="col1-1">
                <div class="listingMap">
                    <?php
                      do_shortcode('[i5ListingMap maxzoom="100" minzoom="1" lat="' . $lat . '" long="' . $long . '" style="display:block;width: 100%; position:relative; height: 100%" showPolygon="true"]');
                    ?>
                    <script>
                    <?php
                      foreach($SearchResults as $result)
                      {
                          if($result->latitude!=0)
                              echo "markAddress(" . json_encode($result) . ");";
                      }
                    ?>
                        fitBounds();
                    </script>
                </div>
            </div>
		</div>
        
        
        <?php elseif($_REQUEST["display"]=="map"): ?>
        <div class="searchResultsMap">
            <div class="listingSidebar mapSidebar">
                <?php foreach($SearchResults as $result){
                          $lat=0;
                          $long=0;

                          if(isset($SearchResults) && sizeof($SearchResults)>0)
                          {
                              $lat=$SearchResults[0]->latitude;
                              $long=$SearchResults[0]->longitude;
                          }

                ?>
                <div onmouseover="openInfoWindow('<?php echo $result->mlsid?>')" style="cursor:pointer;" class="listingWrapper">
	            	<div class="col1-1">
	                    <div class="listingPhoto">
	                        <?php if(isset($result->image) && $result->image!=""):?>
	                        <img  onclick="location.href='/property-details/<?php echo $result->url?>';" src="<?php echo $result->image?>" />
	                        <?php else:?>
	                        <img  onclick="location.href='/property-details/<?php echo $result->url?>';" src="<?php echo i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png" ?>" />
	                        <?php endif;?>
	                        
	                        <div class="shortcuts">
	                        	<a data-toggle="tooltip" data-placement="left" title="Share"  href="#"><i class="fa fa-share"></i></a>
	                        	<a href="#">
									<div id="fav<?php echo $result->mlsid; ?>" class="<?php echo ($favs=='' || !in_array($result->mlsid,$favs)?'listingFavorite':'listingFavoriteSelected')?>">
				                        <a data-toggle="tooltip" data-placement="left" title="Favorites"  href="javascript:toggleFav('<?php echo $result->mlsid; ?>');setFavorite('<?php echo $result->mlsid; ?>',<?php echo ($favs=='' || !in_array($result->mlsid,$favs)?'1':'0')?>);">
				                            <i class="fa fa-heart"></i>
				                        </a>
				                    </div>
								</a>
	                        	<a class="remove-property" data-toggle="tooltip" data-placement="left" title="Hide"><i class="fa fa-ban"></i></a>
	                        </div>
	                        <div class="listingInfo">
	                        <div class="listingStatus">
	                        	<!-- <div class="statusUnderContract"></div> Need to show under contract status -->
	                        	<!-- <div class="statusSold"></div> Need to show sold status -->
	                             <div class="statusActive"></div><?php echo $result->status?><br />
	                             Listed for: 
								 <?php 
								 $current_date = date('Y-m-d');
								 $expire_date  = $result->custom3;
								 if($current_date > $expire_date){
									echo '0 Day';
								  }
								  else{
								  	$todate= strtotime($current_date);
									$fromdate= strtotime($expire_date);
									$calculate_seconds = $todate- $fromdate; // Number of seconds between the two dates
									$days = floor($calculate_seconds / (24 * 60 * 60 )); // convert to days
									echo($days .' Days');
								  }
								 ?>
	                        </div>
		                        <div class="listingSpecs">
		                        	<div class="listingSpec">
		                        		<div class="listingSpecValue">
		                        			<?php echo $result->bedrooms?>
		                        		</div>
		                        		<div class="listingSpecLabel">
		                        			Bed
		                        		</div>
		                        	</div>
		                        	<div class="listingSpec">
		                        		<div class="listingSpecValue">
		                        			<?php echo $result->fullBaths?>
		                        		</div>
		                        		<div class="listingSpecLabel">
		                        			Bath
		                        		</div>
		                        	</div>
		                        	<div class="listingSpec">
		                        		<div class="listingSpecValue">
		                        			<?php echo $result->size?>
		                        		</div>
		                        		<div class="listingSpecLabel">
		                        			Sqft
		                        		</div>
		                        	</div>
		                                <!--<?php
			                          $str=$result->bedrooms . " <div class='specLabel'>Bed</div> ";
			
			                          if($result->halfBaths!=0 && $result->fullBaths!=0)
			                              $str .= $result->fullBaths + ($result->halfBaths/2);
			                          else if($result->halfBaths!=0 && $result->fullBaths==0)
			                              $str .= ($result->halfBaths/2);
			                          else
			                              $str .= number_format($result->fullBaths,0);
			
			                          $str .= " <div class='specLabel'>Bath</div> " . number_format($result->size,0) . " <div class='specLabel'>Sqft</div>";
			
			                          echo $str;
			                                ?>-->
		                       </div>
	                       </div>
	                    </div>
	                    <div class="listingDetails">
	                        <div class="listingAddress">
	                            <div  onclick="location.href='/property-details/<?php echo $result->url?>';" class="listingAddress">
	                                <?php echo $result->address?>
	                            </div>
	                            <div class="listingCity">
	                            	<i class="fa fa-map-marker"></i> <?php echo $result->city?>, <?php echo $result->state?> <?php echo $result->zip?>
	                            </div>
	                            <div class="listingPrice">
	                                <?php echo "$" . number_format($result->price,0)  . ($result->propertytype=="Rental"?" /Month":"")?>
	                            </div>
	                            <!-- 
	                            <div class="listingPpsqft">
	                            	Price per sqft<br />
	                            	<b>$500</b>
	                            </div>
	                             -->
	                        </div>
	                    </div>
	                </div>
                </div>
                <?php }?>
                <?php include(i5PBIntegration__PLUGIN_DIR . 'Includes/SearchPaging-Functions.php');?>
            </div>
        </div>
        
    
    </div>
    
	<div class="mapHolder">
		<div class="col1-1">
            <div class="listingMap">
                <?php
                  do_shortcode('[i5ListingMap maxzoom="100" minzoom="1" lat="' . $lat . '" long="' . $long . '" style="display:block;width: 100%; position:relative; height: 100%" showPolygon="true"]');
                ?>
                <script>
                <?php
                  foreach($SearchResults as $result)
                  {
                      if($result->latitude!=0)
                          echo "markAddress(" . json_encode($result) . ");";
                  }
                ?>
                    fitBounds();
                </script>
            </div>
        </div>
        <?php endif;?>
        
	</div>
</div>
<script>
    function toggleFav(id)
    {
        if (jQuery("#fav" + id).hasClass("listingFavoriteSelected"))
        {
            jQuery("#fav" + id).removeClass("listingFavoriteSelected");
            jQuery("#fav" + id).addClass("listingFavorite");
            jQuery(jQuery("#fav" + id).children()[0]).prop("href","javascript:toggleFav('" + id + "');setFavorite('" + id + "',1);");
        }
        else {
            jQuery("#fav" + id).addClass("listingFavoriteSelected");
            jQuery("#fav" + id).removeClass("listingFavorite");
            jQuery(jQuery("#fav" + id).children()[0]).prop("href", "javascript:toggleFav('" + id + "');setFavorite('" + id + "',0);");
        }

    }
    function scrollToAnchor(aid) {
        var aTag = jQuery("a[name='" + aid + "']");
        jQuery('html,body').animate({ scrollTop: aTag.offset().top }, 'slow');
    }
    jQuery("img").error(function () {
        jQuery(this).attr("src", '<?php echo i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png" ?>');
    });
    <?php if(isset($_REQUEST["display"]) && $_REQUEST["display"]=="photo"): ?>
    jQuery(function () {
        jQuery(".sticky").sticky({ topSpacing: 0 });
    });
    <?php endif; ?>
	
	
	jQuery('.remove-property').click(function () {
		jQuery(this).closest(".listingWrapper").remove();
	});
	
</script>

<?php
include(i5PBIntegration__PLUGIN_DIR . 'Includes/Global-Functions.php');

?>