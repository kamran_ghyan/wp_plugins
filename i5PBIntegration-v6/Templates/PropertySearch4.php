<?php
/*
Template Name: Property Search Theme 4
 */

get_header();

include i5PBIntegration__PLUGIN_DIR . 'Includes/Prospect-Functions.php';
include i5PBIntegration__PLUGIN_DIR . 'Includes/PropertySearch-Functions.php';
?>
<div class="container search-list-container">
	<div class="row search-module">
    	<div class="col-md-12 search-container">
        	<?php echo do_shortcode('[i5GeoSearch geocode="false" title_place="bottom" class_name="btn-adv-search"]'); ?>
        </div>
    </div>
    <div class="row filters">
    	<div class="col-md-3 no-padding">
        	<ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a id="sale-tab" href="#for-sale" aria-controls="sale" role="tab" data-toggle="tab">For Sale</a></li>
            <li role="presentation"><a id="lease-tab" href="#for-lease" aria-controls="lease" role="tab" data-toggle="tab">For Lease</a></li>
          </ul>
        </div>
        <div id="sale-options">
            <div class="col-md-3">
                <div class="searchFilter" id="sort-list-view">
                Sort by:
                <button  id="sorting" class="dropbtn filter-btn">
                    Default <i class="fa fa-angle-down"></i>
                </button>
                <div id="ddsort" class="ddsort dropdownContent">
                   <ul id="sorting-val" class="sorting-val">
                            <li id="default"><span><?php _e('Default', 'i5pb');?></span></li>
                            <!--<li id="alpha_asc"><span><?php /*_e('Alphabatical', 'i5pb');*/?></span> <i><?php /*_e('(A-Z)', 'i5pb');*/?></i></li>
                            <li id="alpha_desc"><span><?php /*_e('Alphabatical', 'i5pb');*/?></span> <i><?php /*_e('(Z-A)', 'i5pb');*/?></i></li>-->
                            <li id="price_desc"><span><?php _e('Price', 'i5pb');?></span> <i><?php _e('High to Low', 'i5pb');?></i></li>
                            <li id="price_asc"><span><?php _e('Price', 'i5pb');?></span> <i><?php _e('Low to High', 'i5pb');?></i></li>
                            <!--<li id="size_desc"><span><?php /*_e('Size', 'i5pb');*/?></span> <i><?php /*_e('High to Low', 'i5pb');*/?></i></li>
                            <li id="size_asc"><span><?php /*_e('Size', 'i5pb');*/?></span><i> <?php /*_e('Low to High', 'i5pb');*/?></i></li>-->
                        </ul>
                </div>

              </div>

            	<div class="searchFilter" id="sort-map-view" style="display:none;">
                Sort by:
                <button  id="sorting-map" class="dropbtn filter-btn">
                    Default <i class="fa fa-angle-down"></i>
                </button>
                <div id="ddsort" class="ddsort dropdownContent">
                   <ul id="sorting-val-map" class="sorting-val">
                            <li id="default"><span>Default</span></li>
                           <!-- <li id="alpha_asc"><span>Alphabatical</span> <i>(A-Z)</i></li>
                            <li id="alpha_desc"><span>Alphabatical</span> <i>(Z-A)</i></li>-->
                       <li id="price_desc"><span>Price</span> <i>High to Low</i></li>
                       <li id="price_asc"><span>Price</span> <i>Low to High</i></li>
                       <!--<li id="size_desc"><span>Size</span> <i>High to Low</i></li>
                       <li id="size_asc"><span>Size</span><i> Low to High</i></li>-->
                        </ul>
                </div>

              </div>
            </div>
            <div class="col-md-3">
                <div class="searchFilter">
                View:
                <button id="view" class="dropbtn filter-btn">List View <i class="fa fa-angle-down"></i></button>
                <div id="ddview" class="ddsort1 dropdownContent">
                   <ul id="list-view" class="sorting-val">
                          <li class="active"><span>List View</span> <i class="fa fa-list" aria-hidden="true"></i></li>
                          <li id="test"><span>Map View</span> <i class="fa fa-map" aria-hidden="true"></i></li>
                    </ul>
                </div>
              </div>
            </div>
            <div class="col-md-3">
                <div class="searchFilter" id="pagi_block">
                    <?php echo pagination_ajax($start, $end, $TTLResults, "next", "prev", "sale"); ?>
                </div>
                <div class="searchFilter" id="pagi_block_map" style="display:none;">
                    <?php echo pagination_ajax($start, $end, $TTLResults, "next", "prev", "salemap"); ?>
                </div>
            </div>
        </div>
        <div id="lease-options" style="display:none;" >
            <div class="col-md-3">
                <div class="searchFilter" id="sort-lease-view">
                Sort by:
                <button  id="sorting-lease" class="dropbtn filter-btn">
                    Default <i class="fa fa-angle-down"></i>
                </button>
                <div id="ddsort-lease" class="ddsort dropdownContent">
                   <ul id="sorting-val-lease" class="sorting-val">
                            <li id="default"><span>Default</span></li>
                            <!--<li id="alpha_asc"><span>Alphabatical</span> <i>(A-Z)</i></li>
                            <li id="alpha_desc"><span>Alphabatical</span> <i>(Z-A)</i></li>-->
                       <li id="price_desc"><span>Price</span> <i>High to Low</i></li>
                       <li id="price_asc"><span>Price</span> <i>Low to High</i></li>
                       <!--<li id="size_desc"><span>Size</span> <i>High to Low</i></li>
                       <li id="size_asc"><span>Size</span><i> Low to High</i></li>-->
                        </ul>
                </div>
              </div>

                <div class="searchFilter" id="sort-lease-map-view" style="display:none;">
                Sort by:
                <button id="sorting-lease-map" class="dropbtn filter-btn">
                    Default <i class="fa fa-angle-down"></i>
                </button>
                <div id="ddsort-lease" class="ddsort dropdownContent">
                   <ul id="sorting-val-lease-map" class="sorting-val">
                            <li id="default"><span>Default</span></li>
                           <!-- <li id="alpha_asc"><span>Alphabatical</span> <i>(A-Z)</i></li>
                            <li id="alpha_desc"><span>Alphabatical</span> <i>(Z-A)</i></li>-->
                       <li id="price_desc"><span>Price</span> <i>High to Low</i></li>
                       <li id="price_asc"><span>Price</span> <i>Low to High</i></li>
                      <!-- <li id="size_desc"><span>Size</span> <i>High to Low</i></li>
                       <li id="size_asc"><span>Size</span><i> Low to High</i></li>-->
                        </ul>
                </div>
              </div>
            </div>
            <div class="col-md-3">
                <div class="searchFilter">
                View:
                <button id="view-lease" class="dropbtn filter-btn">List View <i class="fa fa-angle-down"></i></button>
                <div id="ddview-lease" class="ddsort1 dropdownContent">
                   <ul id="list-view-lease" class="sorting-val">
                          <li class="active"><span>List View</span> <i class="fa fa-list" aria-hidden="true"></i></li>
                          <li><span>Map View</span> <i class="fa fa-map" aria-hidden="true"></i></li>
                    </ul>
                </div>
              </div>
            </div>
            <div class="col-md-3">
                <div class="searchFilter" id="pagi_block_lease">
                    <?php echo pagination_ajax($start, $end, $ttl_result_lease, "next", "prev", "lease"); ?>
                </div>
                <div class="searchFilter" id="pagi_block_lease_map" style="display:none;">
                    <?php echo pagination_ajax($start, $end, $ttl_result_lease, "next", "prev", "leasemap"); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row search-listing">
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="for-sale">
            	<div class="col-md-3 search-sidebar">
                    <div id="list-view-form">
                    <form name="filter_sale" id="filter_sale">
                    <div class="filterSection" id="propertyTypeFilters">
                        <div class="sectionHeader">
                            <span class="none" id="type_hide_loc">none</span>
                            <span class="sep">|</span>
                            <span class="all" id="type_show_loc">show all</span>
                            Locations
                        </div>
                        <div id="location_type">
                            <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_anchorage" name="type_Anchorage" onchange="ajax_search();" checked="checked" type="checkbox">
                                </span>
                                <label for="typeAnchorageFilter">Anchorage</label>
                            </div>
                            <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_eagle river" name="type_Eagle River" onchange="ajax_search();" checked="checked" type="checkbox">
                                </span>
                                <label for="typeEagleFilter">Eagle River</label>
                            </div>
                            <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_chugiak" name="type_Chugiak" onchange="ajax_search();" checked="checked" type="checkbox">
                                </span>
                                <label for="typeChugiakFilter">Chugiak</label>
                            </div>
                            <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_wasilla" name="type_Wasilla" onchange="ajax_search();" checked="checked" type="checkbox">
                                </span>
                                <label for="typeWasillaFilter">Wasilla</label>
                            </div>
                            <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_palmer" name="type_Palmer" onchange="ajax_search();" checked="checked" type="checkbox">
                                </span>
                                <label for="typePalmerFilter">Palmer</label>
                            </div>
                        </div>
                    </div>
                    <div class="filterSection" id="propertyTypeFilters">
                        <div class="sectionHeader">
                        <span class="none" id="type_hide">none</span>
                        <span class="sep">|</span>
                        <span class="all" id="type_show">show all</span>
                        Property Types
                        </div>

                        <div id="property_type">
                            <?php
foreach ($property_types as $type) {
	if ($type->propertytype != '' && $type->propertytype != null) {
		?>
                                <div class="propertyTypeInput" style="">
                                    <span>
                                        <input id="type_<?php echo strtolower($type->propertytype); ?>" name="type_<?php echo $type->propertytype; ?>" onChange="ajax_search();" checked="checked" type="checkbox">
                                    </span>
                                    <label for="type<?php echo $type->propertytype; ?>Filter"><?php echo $type->propertytype; ?></label>
                                </div>
                            <?php }}?>
                        </div>
                    </div>

                    <div id="saleFilters">
                        <div class="filterSection moreFilters">
                    <div id="buildingSizeFilter">
                    <div class="sectionHeader">
                    <span class="clear" id="reset_bs">clear</span>
                    Building Size (SF)
                    </div>
                    <div class="range rangebs">
                        <input type="number"  onChange="ajax_search();" value="100" id="type_size_min" name="type_smin">
                        to
                        <input type="number"  onChange="ajax_search();" value="10000" id="type_size_max" name="type_smax">
                        </div>
                    </div>

                    <div class="showingAll" id="salePriceFilter">
                    <div class="sectionHeader">
                    <span class="clear" id="reset_p">clear</span>
                    Price Range ($)
                    </div>
                    <div class="range rangep">
                    <input value="1000"  onChange="ajax_search();" type="number" id="type_price_min" name="type_pmin">
                    to
                    <input value="2000000"  onChange="ajax_search();" type="number" id="type_price_max" name="type_pmax">
                    </div>
                    </div>
                    <div class="showingAll" id="saleOptionsFilter">
                    <div class="sectionHeader">
                    Additional Options
                    </div>
                    <div class="nnnForSale">
                    <input  onChange="ajax_search();" name="type_nnn" id="type_nnn" type="checkbox">
                    <label  class="negotiable" for="showNNN">Show NNN investment property</label>
                    </div>
                    </div>
                    </div>
                    </div>
                    <input  type="hidden" id="sort_value" name="sort" value="">
                    <input  type="hidden" id="pagi_value" name="pagi" value="">
                    <input  type="hidden" id="pagi_value_p" name="pagi_p" value="">
                    <input  type="hidden" id="property_type" name="sale" value="sale">
                    <input  type="hidden" id="view_type" name="list" value="list">
                    <input type="hidden" name="ajax_call" value="ajax_call_search">
                    <input type="hidden" id="auto_complete_term" name="auto_complete_term" value="">
                    </form>
                    </div>
                    <div id="map-view-form" style="display:none;">
                    <form name="filter_sale_map" id="filter_sale_map">
                        <div class="filterSection" id="propertyTypeFilters">
                            <div class="sectionHeader">
                                <span class="none" id="type_hide_map_loc">none</span>
                                <span class="sep">|</span>
                                <span class="all" id="type_show_map_loc">show all</span>
                                Locations
                            </div>
                            <div id="property_type_map_loc">
                                <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_anchorage" name="type_Anchorage" onchange="ajax_search_map();" checked="checked" type="checkbox">
                                </span>
                                    <label for="typeAnchorageFilter">Anchorage</label>
                                </div>
                                <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_eagle river" name="type_Eagle River" onchange="ajax_search_map();" checked="checked" type="checkbox">
                                </span>
                                    <label for="typeEagleFilter">Eagle River</label>
                                </div>
                                <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_chugiak" name="type_Chugiak" onchange="ajax_search_map();" checked="checked" type="checkbox">
                                </span>
                                    <label for="typeChugiakFilter">Chugiak</label>
                                </div>
                                <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_wasilla" name="type_Wasilla" onchange="ajax_search_map();" checked="checked" type="checkbox">
                                </span>
                                    <label for="typeWasillaFilter">Wasilla</label>
                                </div>
                                <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_palmer" name="type_Palmer" onchange="ajax_search_map();" checked="checked" type="checkbox">
                                </span>
                                    <label for="typePalmerFilter">Palmer</label>
                                </div>
                            </div>
                        </div>
                    <div class="filterSection" id="propertyTypeFilters">
                        <div class="sectionHeader">
                        <span class="none" id="type_hide_map">none</span>
                        <span class="sep">|</span>
                        <span class="all" id="type_show_map">show all</span>
                        Property Types
                        </div>
                        <div id="property_type_map">
                            <?php
foreach ($property_types as $type) {
	if ($type->propertytype != '' && $type->propertytype != null) {
		?>
                                    <div class="propertyTypeInput" style="">
                                    <span>
                                        <input id="type_<?php echo strtolower($type->propertytype); ?>" name="type_<?php echo $type->propertytype; ?>" onChange="ajax_search_map();" checked="checked" type="checkbox">
                                    </span>
                                        <label for="type<?php echo $type->propertytype; ?>Filter"><?php echo $type->propertytype; ?></label>
                                    </div>
                                <?php }}?>
                        </div>
    </div>

                    <div id="saleFiltersMap">
                        <div class="filterSection moreFilters">
                    <div id="buildingSizeFilter">
                    <div class="sectionHeader">
                    <span class="clear" id="reset_bs">clear</span>
                    Building Size (SF)
                    </div>
                    <div class="range rangebs">
                        <input type="number"  onChange="ajax_search_map();" value="100" id="type_size_min" name="type_smin">
                        to
                        <input type="number"  onChange="ajax_search_map();" value="10000" id="type_size_max" name="type_smax">
                        </div>
                    </div>

                    <div class="showingAll" id="salePriceFilter">
                    <div class="sectionHeader">
                    <span class="clear" id="reset_p">clear</span>
                    Price Range ($)
                    </div>
                    <div class="range rangep">
                    <input value="1000"  onChange="ajax_search_map();" type="number" id="type_price_min" name="type_pmin">
                    to
                    <input value="2000000"  onChange="ajax_search_map();" type="number" id="type_price_max" name="type_pmax">
                    </div>
                    </div>
                    <div class="showingAll" id="saleOptionsFilter">
                    <div class="sectionHeader">
                    Additional Options
                    </div>
                    <div class="nnnForSale">
                    <input  onChange="ajax_search_map();" name="type_nnn" id="type_nnn" type="checkbox">
                    <label  class="negotiable" for="showNNN">Show NNN investment property</label>
                    </div>
                    </div>
                    </div>
                    </div>
                    <input  type="hidden" id="sort_value" name="sort" value="">
                    <input  type="hidden" id="pagi_value_map" name="pagi" value="">
                    <input  type="hidden" id="pagi_value_map_p" name="pagi_p" value="">
                    <input  type="hidden" id="property_type" name="sale" value="sale">
                    <input  type="hidden" id="view_type" name="list" value="list">
                    <input type="hidden" name="ajax_call" value="ajax_call_search">
                    <input type="hidden" id="auto_complete_term" name="auto_complete_term" value="">
                    </form>
                    </div>

                    <div id="map-listing-sidebar" style="display:none;">
                    	<div class="ajax-loader"></div>
                        <div id="map-listing-inner">
                            <?php foreach ($SearchResults as $result): ?>
                                <div onmouseover="openInfoWindow('<?php echo $result->mlsid; ?>')" style="cursor:pointer;" class="row map-sidebar-item">
                                <div class="col-md-5" style="padding-left:5px;">

                                    <?php if (isset($result->image) && $result->image != ""): ?>
                                    <a class="thumb" href="/property-details/<?php echo $result->url; ?>" target="_top">
                                        <img alt="<?php echo $result->title; ?>" src="<?php echo $result->image; ?>" height="76px" width="90px">
                                    </a>
                                    <?php else: ?>
                                    <a class="thumb" href="/property-details/<?php echo $result->url; ?>" target="_top">
                                        <img alt="<?php echo $result->title; ?>" src="<?php echo i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png" ?>" height="76px" width="90px">
                                    </a>
                                    <?php endif;?>

                                </div>
                                <div class="col-md-7 no-padding">
                                    <span class="list-title">
                                        <a href="/property-details/<?php echo $result->url; ?>" target="_top"><?php echo substr($result->title, 0, 20); ?></a>
                                    </span>
                                    <span class="list-address"><?php echo $result->city; ?>, <?php echo $result->state; ?></span>
                                    <span class="list-price"><strong>$<?php echo number_format($result->price, 0); ?></strong></span>
                                    <span class="list-size"><?php echo $result->size; ?> Sqft</span>
                                    <span class="list-type"><?php echo $result->propertytype; ?></span>

                                </div>
                            </div>
                            <?php endforeach;?>
                        </div>
                        <div class="searchFilter" id="pagi_block">
                            <?php echo pagination_ajax($start, $end, $TTLResults, "next", "prev", "salemap"); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 search-display">
                <div class="ajax-loader"></div>
        	    <?php
foreach ($SearchResults as $result):
	$lat = 0;
	$long = 0;
	if (isset($SearchResults) && sizeof($SearchResults) > 0) {
		$lat = $SearchResults[0]->latitude;
		$long = $SearchResults[0]->longitude;
	}
	?>

						            <div class="row search-item">
						            	<div class="col-md-2 col-xs-2 no-padding">
						                	<?php if (isset($result->image) && $result->image != ""): ?>
						                            <a class="thumb" href="/property-details/<?php echo $result->url; ?>" target="_top">
						                                <img alt="<?php echo $result->title; ?>" src="<?php echo $result->image; ?>" height="76px" width="90px">
						                            </a>
						                            <?php else: ?>
                            <a class="thumb" href="/property-details/<?php echo $result->image; ?>" target="_top">
                                <img alt="<?php echo $result->title; ?>" src="<?php echo i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png" ?>" height="76px" width="90px">
                            </a>
                            <?php endif;?>
                </div>
                <div class="col-md-8 col-xs-8">
                	<span class="list-title">
                    <a href="/property-details/<?php echo $result->url; ?>" target="_top"><?php echo $result->title; ?></a>
                    </span>
                    <span class="list-address"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $result->city; ?>, <?php echo $result->state; ?></span>
                    <span class="list-description">
                    	<?php echo $result->description; ?>
                    </span>
                    <span class="list-description list-description-responsive">
                    	<?php echo substr($result->description, 0, 150); ?>
                    </span>
                </div>
                <div class="col-md-2 col-xs-2  no-padding">
                	<span class="list-price">$<?php echo number_format($result->price, 0); ?></span>
                    <span class="list-size"><?php echo $result->size; ?> Sqft</span>
                    <span class="list-type"><?php echo $result->propertytype; ?></span>
                    <div class="row" style="margin-top:20px;">
                        <div class="col-md-offset-5">
                            <img style=" height:50px; width:50px; " alt="Alaska MLS" title="Alaska MLS" src="<?php echo i5PBIntegration__PLUGIN_URL; ?>/images/idx-large.gif" />
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 no-padding">
                	<!--<span class="list-contact-advisor">
                    <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;
                    <a href="mailto:<?php /*echo $result->agentEmail; */?>">Agent Email</a>

                    </span>-->
                    <span class="list-links">

                    <a href="/property-details/<?php echo $result->url; ?>#map" class="map" target="_top"><i class="fa fa-map-marker" aria-hidden="true"></i> Map</a>
                    <!--<a href="/property-details/<?php echo $result->url; ?>" class="flyer" target="_blank">
                        <i class="fa fa-random" aria-hidden="true"></i> Flyer
                    </a> -->
 					<a href="/property-details/<?php echo $result->url; ?>" class="more-details" target="_top">More Details <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    </span>
                </div>
            </div>
            <?php endforeach;?>
            <div class="searchFilter" id="pagi_block">
                <?php echo pagination_ajax($start, $end, $TTLResults, "next", "prev", "sale"); ?>
            </div>
        </div>
    			<div class="col-md-9 search-display-map" style="display:none;">
        	<div style="height: 100%;" >
			<?php
do_shortcode('[i5ListingMap zoom="5" maxzoom="100" minzoom="1" lat="' . $lat . '" long="' . $long . '" style="display:block;width: 100%; height: 100%" showPolygon="true"]');
?>
                <script>
                jQuery(window).load(function(){
                <?php
foreach ($SearchResults as $result) {
	if ($result->latitude != 0) {
		echo "markAddress(" . json_encode($result) . ");";
	}

}
?>
                });
                    fitBounds();
                </script>
        	</div>
        </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="for-lease">
            	<div class="col-md-3 search-sidebar">
                <div id="lease-view-form">
                <form name="filter_lease" id="filter_lease">
                    <div class="filterSection" id="propertyTypeFilters">
                        <div class="sectionHeader">
                            <span class="none" id="type_hide_lease_loc">none</span>
                            <span class="sep">|</span>
                            <span class="all" id="type_show_lease_loc">show all</span>
                            Locations
                        </div>
                        <div id="lease_location">
                            <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_anchorage" name="type_Anchorage" onchange="ajax_search_lease();" checked="checked" type="checkbox">
                                </span>
                                <label for="typeAnchorageFilter">Anchorage</label>
                            </div>
                            <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_eagle river" name="type_Eagle River" onchange="ajax_search_lease();" checked="checked" type="checkbox">
                                </span>
                                <label for="typeEagleFilter">Eagle River</label>
                            </div>
                            <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_chugiak" name="type_Chugiak" onchange="ajax_search_lease();" checked="checked" type="checkbox">
                                </span>
                                <label for="typeChugiakFilter">Chugiak</label>
                            </div>
                            <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_wasilla" name="type_Wasilla" onchange="ajax_search_lease();" checked="checked" type="checkbox">
                                </span>
                                <label for="typeWasillaFilter">Wasilla</label>
                            </div>
                            <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_palmer" name="type_Palmer" onchange="ajax_search_lease();" checked="checked" type="checkbox">
                                </span>
                                <label for="typePalmerFilter">Palmer</label>
                            </div>
                        </div>
                    </div>
                <div class="filterSection" id="propertyTypeFilters">
                    <div class="sectionHeader">
                    <span class="none" id="type_hide_lease">none</span>
                    <span class="sep">|</span>
                    <span class="all" id="type_show_lease">show all</span>
                    Property Types
                    </div>
                    <div id="property_type_lease">
                        <?php
foreach ($property_types as $type) {
	if ($type->propertytype != '' && $type->propertytype != null) {
		?>
                                <div class="propertyTypeInput" style="">
                                    <span>
                                        <input id="type_<?php echo strtolower($type->propertytype); ?>" name="type_<?php echo $type->propertytype; ?>" onChange="ajax_search_lease();" checked="checked" type="checkbox">
                                    </span>
                                    <label for="type<?php echo $type->propertytype; ?>Filter"><?php echo $type->propertytype; ?></label>
                                </div>
                            <?php }}?>
                    </div>
</div>

                <div id="saleFiltersLease">
                	<div class="filterSection moreFilters">
                <div id="buildingSizeFilter">
                <div class="sectionHeader">
                <span class="clear" id="reset_bs_lease">clear</span>
                Space Available (RSF)
                </div>
                <div class="range rangebs">
                    <input type="number"  onChange="ajax_search_lease();" value="100" id="type_space_min" name="type_spmin">
                    to
                    <input type="number"  onChange="ajax_search_lease();" value="1000" id="type_space_max" name="type_spmax">
                    </div>
                </div>

                <div class="showingAll" id="salePriceFilter">
                <div class="sectionHeader">
                <span class="clear" id="reset_p_lease">clear</span>
                Lease Rate ($/SF/YR)
                </div>
                <div class="range rangep">
                <input value="1000"  onChange="ajax_search_lease();" type="number" id="type_rate_min" name="type_rmin">
                to
                <input value="20000"  onChange="ajax_search_lease();" type="number" id="type_rate_max" name="type_rmax">
                </div>
                </div>
                <div class="showingAll" id="saleOptionsFilter">
                <div class="sectionHeader">
                Additional Options
                </div>
                <input  onChange="ajax_search_lease();" name="type_sublease" id="type_sublease" type="checkbox">
                <label class="negotiable" for="showInvestment">Show sublease</label>
                </div>
                </div>
                </div>
                <input  type="hidden" id="sort_value_lease" name="sort" value="">
                <input  type="hidden" id="pagi_value_lease" name="pagi" value="">
                <input  type="hidden" id="pagi_value_lease_p" name="pagi_p" value="">
                <input  type="hidden" id="property_type" name="lease" value="lease">
                <input  type="hidden" id="view_type" name="list" value="list">
                <input type="hidden" name="ajax_call" value="ajax_call_search">
                <input type="hidden" id="auto_complete_term" name="auto_complete_term" value="">
            	</form>
                </div>
                <div id="map-lease-view-form" style="display:none;">
                <form name="filter_lease_map" id="filter_lease_map">
                    <div class="filterSection" id="propertyTypeFilters">
                        <div class="sectionHeader">
                            <span class="none" id="type_hide_lease_map_loc">none</span>
                            <span class="sep">|</span>
                            <span class="all" id="type_show_lease_map_loc">show all</span>
                            Locations
                        </div>
                        <div id="lease_map_loc">
                            <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_anchorage" name="type_Anchorage" onchange="ajax_search_lease_map();" checked="checked" type="checkbox">
                                </span>
                                <label for="typeAnchorageFilter">Anchorage</label>
                            </div>
                            <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_eagle river" name="type_Eagle River" onchange="ajax_search_lease_map();" checked="checked" type="checkbox">
                                </span>
                                <label for="typeEagleFilter">Eagle River</label>
                            </div>
                            <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_chugiak" name="type_Chugiak" onchange="ajax_search_lease_map();" checked="checked" type="checkbox">
                                </span>
                                <label for="typeChugiakFilter">Chugiak</label>
                            </div>
                            <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_wasilla" name="type_Wasilla" onchange="ajax_search_lease_map();" checked="checked" type="checkbox">
                                </span>
                                <label for="typeWasillaFilter">Wasilla</label>
                            </div>
                            <div class="propertyTypeInput" style="">
                                <span>
                                    <input id="type_palmer" name="type_Palmer" onchange="ajax_search_lease_map();" checked="checked" type="checkbox">
                                </span>
                                <label for="typePalmerFilter">Palmer</label>
                            </div>
                        </div>
                    </div>
                <div class="filterSection" id="propertyTypeFilters">
                    <div class="sectionHeader">
                    <span class="none" id="type_hide_lease_map">none</span>
                    <span class="sep">|</span>
                    <span class="all" id="type_show_lease_map">show all</span>
                    Property Types
                    </div>
                    <div id="property_type_lease_map">
                        <?php
foreach ($property_types as $type) {
	if ($type->propertytype != '' && $type->propertytype != null) {
		?>
                                <div class="propertyTypeInput" style="">
                                    <span>
                                        <input id="type_<?php echo strtolower($type->propertytype); ?>" name="type_<?php echo $type->propertytype; ?>" onChange="ajax_search_lease_map();" checked="checked" type="checkbox">
                                    </span>
                                    <label for="type<?php echo $type->propertytype; ?>Filter"><?php echo $type->propertytype; ?></label>
                                </div>
                            <?php }}?>
                    </div>
</div>
                <div id="saleFiltersLeaseMap">
                	<div class="filterSection moreFilters">
                <div id="buildingSizeFilter">
                <div class="sectionHeader">
                <span class="clear" id="reset_bs_lease_map">clear</span>
                Space Available (RSF)
                </div>
                <div class="range rangebs">
                    <input type="number"  onChange="ajax_search_lease_map();" value="100" id="type_space_min" name="type_spmin">
                    to
                    <input type="number"  onChange="ajax_search_lease_map();" value="1000" id="type_space_max" name="type_spmax">
                    </div>
                </div>

                <div class="showingAll" id="salePriceFilter">
                <div class="sectionHeader">
                <span class="clear" id="reset_p_lease_map">clear</span>
                Lease Rate ($/SF/YR)
                </div>
                <div class="range rangep">
                <input value="1000"  onChange="ajax_search_lease_map();" type="number" id="type_rate_min" name="type_rmin">
                to
                <input value="20000"  onChange="ajax_search_lease_map();" type="number" id="type_rate_max" name="type_rmax">
                </div>
                </div>
                <div class="showingAll" id="saleOptionsFilter">
                <div class="sectionHeader">
                Additional Options
                </div>
                <input  onChange="ajax_search_lease_map();" name="type_sublease" id="type_sublease" type="checkbox">
                <label class="negotiable" for="showInvestment">Show sublease</label>
                </div>
                </div>
                </div>
                <input  type="hidden" id="sort_value_lease" name="sort" value="">
                <input  type="hidden" id="pagi_value_lease" name="pagi" value="">
                <input  type="hidden" id="pagi_value_lease_p" name="pagi_p" value="">
                <input  type="hidden" id="property_type" name="lease" value="lease">
                <input  type="hidden" id="view_type" name="list" value="list">
                <input type="hidden" name="ajax_call" value="ajax_call_search">
                <input type="hidden" id="auto_complete_term" name="auto_complete_term" value="">
            	</form>
                </div>
                <div id="lease-map-listing-sidebar" style="display:none;">
                		<div class="ajax-loader"></div>
                        <div id="map-listing-inner">
                            <?php foreach ($search_result_lease as $result): ?>
                                <div onmouseover="openInfoWindow('<?php echo $result->mlsid; ?>')" style="cursor:pointer;" class="row map-sidebar-item">
                                <div class="col-md-5" style="padding-left:5px;">

                                    <?php if (isset($result->image) && $result->image != ""): ?>
                                    <a class="thumb" href="/property-details/<?php echo $result->url; ?>" target="_top">
                                        <img alt="<?php echo $result->title; ?>" src="<?php echo $result->image; ?>" height="76px" width="90px">
                                    </a>
                                    <?php else: ?>
                                    <a class="thumb" href="/property-details/<?php echo $result->url; ?>" target="_top">
                                        <img alt="<?php echo $result->title; ?>" src="<?php echo i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png" ?>" height="76px" width="90px">
                                    </a>
                                    <?php endif;?>

                                </div>
                                <div class="col-md-7 no-padding">
                                    <span class="list-title">
                                        <a href="/property-details/<?php echo $result->url; ?>" target="_top"><?php echo substr($result->title, 0, 20); ?></a>
                                    </span>
                                    <span class="list-address"><?php echo $result->city; ?>, <?php echo $result->state; ?></span>
                                    <span class="list-price"><strong>$<?php echo number_format($result->price, 0); ?></strong></span>
                                    <span class="list-size"><?php echo $result->size; ?> Sqft</span>
                                    <span class="list-type"><?php echo $result->propertytype; ?></span>

                                </div>
                            </div>
                            <?php endforeach;?>
                        </div>
                        <div class="searchFilter" id="pagi_block">
                            <?php echo pagination_ajax($start, $end, $ttl_result_lease, "next", "prev", "leasemap"); ?>
                        </div>
                    </div>
                </div>
                	<div class="col-md-9 search-display-lease">
                    <div class="ajax-loader"></div>
        	 <?php
foreach ($search_result_lease as $result):
	$lat = 0;
	$long = 0;
	if (isset($search_result_lease) && sizeof($search_result_lease) > 0) {
		$lat = $search_result_lease[0]->latitude;
		$long = $search_result_lease[0]->longitude;
	}
	?>

						            <div class="row search-item">
						            	<div class="col-md-2  no-padding">
						                	<?php if (isset($result->image) && $result->image != ""): ?>
						                            <a class="thumb" href="/property-details/<?php echo $result->url; ?>" target="_top">
						                                <img alt="<?php echo $result->title; ?>" src="<?php echo $result->image; ?>" height="76px" width="90px">
						                            </a>
						                            <?php else: ?>
                            <a class="thumb" href="/property-details/<?php echo $result->image; ?>" target="_top">
                                <img alt="<?php echo $result->title; ?>" src="<?php echo i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png" ?>" height="76px" width="90px">
                            </a>
                            <?php endif;?>
                </div>
                <div class="col-md-8">
                	<span class="list-title">
                    <a href="/property-details/<?php echo $result->url; ?>" target="_top"><?php echo $result->title; ?></a>
                    </span>
                    <span class="list-address"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $result->city; ?>, <?php echo $result->state; ?></span>
                    <span class="list-description">
                    	<?php echo $result->description; ?>
                    </span>
                </div>
                <div class="col-md-2  no-padding">
                	<span class="list-price">$<?php echo number_format($result->price, 0); ?></span>
                    <span class="list-size"><?php echo $result->size; ?> Sqft</span>
                    <span class="list-type"><?php echo $result->propertytype; ?></span>
                    <div class="row" style="margin-top:20px;">
                        <div class="col-md-offset-5">
                            <img style=" height:50px; width:50px; " alt="Alaska MLS" title="Alaska MLS" src="<?php echo i5PBIntegration__PLUGIN_URL; ?>/images/idx-large.gif" />
                        </div>
                    </div>

                </div>
                <div class="col-md-12 no-padding">
                	<!--<span class="list-contact-advisor">
                    <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;
                    <a href="mailto:<?php /*echo $result->agentEmail; */?>">Agent Email</a>

                    </span>-->
                    <span class="list-links">

                    <a href="/property-details/<?php echo $result->url; ?>#map" class="map" target="_top"><i class="fa fa-map-marker" aria-hidden="true"></i> Map</a>

                    <!--<a href="/property-details/<?php echo $result->url; ?>" class="flyer" target="_blank">
                        <i class="fa fa-random" aria-hidden="true"></i> Flyer
                    </a> -->
 					<a href="/property-details/<?php echo $result->url; ?>" class="more-details" target="_top">More Details <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    </span>
                </div>
            </div>
            <?php endforeach;?>
            <div class="searchFilter" id="pagi_block">
            	 <?php echo pagination_ajax($start, $end, $ttl_result_lease, "next", "prev", "lease"); ?>
            </div>
        </div>
    				<div class="col-md-9 search-display-lease-map" style="display:none;">
        	<div style="height: 100%;" >
			<?php
//do_shortcode('[i5ListingMap maxzoom="100" minzoom="1" lat="' . $lat . '" long="' . $long . '" style="display:block;width: 100%; height: 100%" showPolygon="true"]');
?>
                <script>
                <?php
foreach ($SearchResults as $result) {
	if ($result->latitude != 0) {
		echo "markAddress(" . json_encode($result) . ");";
	}

}
?>
                    fitBounds();
                </script>

        	</div>
        </div>
            </div>
          </div>



    </div>
</div>

<script type="text/javascript">
    jQuery( '.fusion-main-menu-search .fusion-main-menu-icon' ).click(function(e) {
        e.stopPropagation();

        if( jQuery( this ).parent().find( '.fusion-custom-menu-item-contents' ).css( 'display' ) == 'block' ) {
            jQuery( this ).parent().find( '.fusion-custom-menu-item-contents' ).hide();
            jQuery( this ).parent().removeClass( 'fusion-main-menu-search-open' );

            jQuery( this ).parent().find( 'style' ).remove();
        } else {
            jQuery( this ).parent().find( '.fusion-custom-menu-item-contents' ).removeAttr( 'style' );
            jQuery( this ).parent().find( '.fusion-custom-menu-item-contents' ).show();
            jQuery( this ).parent().addClass( 'fusion-main-menu-search-open' );

            jQuery( this ).parent().append( '<style>.fusion-main-menu{overflow:visible!important;</style>' );
            jQuery( this ).parent().find( '.fusion-custom-menu-item-contents .s' ).focus();

            // position main menu search box on click positioning
            if( js_local_vars.header_position == 'Top' ) {
                if( ! jQuery( 'body.rtl' ).length && jQuery( this ).parent().find( '.fusion-custom-menu-item-contents' ).offset().left < 0 ) {
                    jQuery( this ).parent().find( '.fusion-custom-menu-item-contents' ).css({
                        'left': '0',
                        'right': 'auto'
                    });
                }

                if( jQuery( 'body.rtl' ).length && jQuery( this ).parent().find( '.fusion-custom-menu-item-contents' ).offset().left + jQuery( this ).parent().find( '.fusion-custom-menu-item-contents' ).width()  > jQuery( window ).width() ) {
                    jQuery( this ).parent().find( '.fusion-custom-menu-item-contents' ).css({
                        'left': 'auto',
                        'right': '0'
                    });
                }
            }
        }
    });
</script>