<?php
wp_enqueue_style( 'bootstrap-css', i5PBIntegration__PLUGIN_URL . 'css/bootstrap.min.css', array(), '1.1', 'all');
wp_enqueue_style( 'property-search3', i5PBIntegration__PLUGIN_URL . 'css/search-results4.css', array(), '1.1', 'all');
wp_enqueue_script( 'bootstrap-js', i5PBIntegration__PLUGIN_URL . 'js/bootstrap.min.js');
wp_enqueue_script( 'filters-js', i5PBIntegration__PLUGIN_URL . 'js/filters.js');



if(isset($_REQUEST["display"]) && $_REQUEST["display"]=="photo")
{
	
    wp_enqueue_style( 'slick-styles', i5PBIntegration__PLUGIN_URL . 'css/slick.css', array(), '1.1', 'all');
    wp_enqueue_style( 'slick-theme', i5PBIntegration__PLUGIN_URL . 'css/slick-theme.css', array(), '1.1', 'all');
    wp_enqueue_script( 'slick', i5PBIntegration__PLUGIN_URL . 'js/slick.min.js', array('jquery'), '1.1', true );
}

get_header();  
//get_template_part('index_header');

$maxResults=24;
include(i5PBIntegration__PLUGIN_DIR . 'Includes/Prospect-Functions.php');
include(i5PBIntegration__PLUGIN_DIR . 'Includes/PropertySearch-Functions.php');
/*
Template Name: Property Search Theme 3
 */

/*echo '<pre>';
print_r($SearchResults);
echo '</pre>';*/


add_action("wp_ajax_filter_search", "filter_search");
add_action("wp_ajax_nopriv_filter_search", "filter_search");

// Ajax Call
function filter_search(){
	global $wpdb;
    echo 'call back';
	die();	
}


?>


<div class="container search-list-container">
	<div class="row search-module">
    	<div class="col-md-12 search-container">
        	<?php echo do_shortcode( '[i5GeoSearch geocode="false" title_place="bottom" class_name="btn-adv-search"]' ); ?>
        </div>
    </div>
    <div class="row filters">
    	<div class="col-md-3">
        	<ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#for-sale" aria-controls="sale" role="tab" data-toggle="tab">For Sale</a></li>
            <li role="presentation"><a href="#for-lease" aria-controls="lease" role="tab" data-toggle="tab">For Lease</a></li>
          </ul>
        </div>
        <div class="col-md-3">
        	<div class="searchFilter">
            Sort by:
            <button  id="sorting" class="dropbtn filter-btn">
                Default <i class="fa fa-angle-down"></i>
            </button>
            <div id="ddsort" class="ddsort dropdownContent">
               <ul id="sorting-val" class="sorting-val">
	                    <li><span>Default</span></li>
                        <li><span>Alphabatical</span> <i>(A-Z)</i></li>
                        <li><span>Alphabatical</span> <i>(Z-A)</i></li>
                        <li><span>Price</span> <i>High to Low</i></li>
                        <li><span>Price</span> <i>Low to High</i></li>
                        <li><span>Size</span> <i>High to Low</i></li>
                        <li><span>Size</span><i> Low to High</i></li>
	                </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3">
        	<div class="searchFilter">
            View:
            <button id="view" class="dropbtn filter-btn">
                List View <i class="fa fa-angle-down"></i>
            </button>
            <div id="ddview" class="ddsort1 dropdownContent">
               <ul id="list-view" class="sorting-val">
	                  <li class="active"><span>List View</span> <i class="fa fa-list" aria-hidden="true"></i></li>
                      <li><span>Map View</span> <i class="fa fa-map" aria-hidden="true"></i></li>
	            </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3">
        	<div class="searchFilter">
            Displaying: 1 to 30            
          </div>
        </div>
    </div>
    <div class="row search-listing">
    	<div class="col-md-3 search-sidebar">
        	
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="for-sale">
            	<form name="filter_sale" id="filter_sale">
                <div class="filterSection" id="propertyTypeFilters">
                    <div class="sectionHeader">
                    <span class="none">none</span>
                    <span class="sep">|</span>
                    <span class="all">show all</span>
                    Property Types
                    </div>
                    <div class="propertyTypeInput" style=""><span>
                    <input id="type_office" name="type_office" checked type="checkbox"></span><label for="typeOfficeFilter">Office</label>
                    </div>
                    <div class="propertyTypeInput" style=""><span>
                    <input id="type_retail" name="type_retail" checked type="checkbox"></span><label for="typeRetailFilter">Retail</label>
                    </div>
                    <div class="propertyTypeInput" style=""><span><input id="type_industrial" name="type_industrial" checked type="checkbox"></span><label for="typeIndustrialFilter">Industrial</label>
                    </div>
                    <div class="propertyTypeInput" style=""><span><input id="type_land" name="type_land" checked type="checkbox"></span><label for="typeLandFilter">Land</label>
                    </div>
                    <div class="propertyTypeInput" style=""><span><input id="type_multi_family" name="type_multi_family" checked type="checkbox"></span><label for="typeMultiFamilyFilter">Multi-Family</label>
                    </div>
                    <div class="propertyTypeInput" ><span><input id="type_other" name="type_other" checked type="checkbox"></span><label for="typeSpecialPurposeFilter">Other</label>
                    </div>
                    <div class="propertyTypeInput" ><span><input id="type_hospitality" name="type_hospitality" checked type="checkbox"></span><label for="typeHospitalityFilter">Hospitality</label>
                    </div>
                    <div class="propertyTypeInput" ><span><input id="type_medical" name="type_medical" checked type="checkbox"></span><label for="typeMedicalFilter">Medical</label>
                    </div>
                    <div class="propertyTypeInput" style=""><span><input id="type_self_storage" name="type_self_storage" checked type="checkbox"></span><label for="typeSelfStorageFilter">Self Storage</label>
                    </div>
</div>
            	
                <div id="saleFilters">
                	<div class="filterSection moreFilters">
                <div id="buildingSizeFilter">
                <div class="sectionHeader">
                <span class="clear">clear</span>
                Building Size (SF)
                </div>
                <div class="range">
                
                <input id="type_size_min" name="type_size_min">
                to
                <input id="type_size_max" name="type_size_max">
                </div>
                </div>
                
                <div class="showingAll" id="salePriceFilter">
                <div class="sectionHeader">
                <span class="clear">clear</span>
                Price Range ($)
                </div>
                <div class="range">
                <input id="type_price_min" name="type_price_min">
                to
                <input id="type_price_max" name="type_price_max">
                </div>
                </div>
                <div class="showingAll" id="saleOptionsFilter">
                <div class="sectionHeader">
                Additional Options
                </div>
                <input name="type_investment" id="type_investment" type="checkbox">
                <label class="negotiable" for="showInvestment">Show investment property</label>
                <br>
                <div class="nnnForSale">
                <input name="type_nnn_investment" id="type_nnn_investment" type="checkbox">
                <label  class="negotiable" for="showNNN">Show NNN investment property</label>
                </div>
                <div class="businessForSale">
                <input name="type_business" id="type_business" type="checkbox">
                <label class="negotiable" for="showBusiness">Show business for sale</label>
                </div>
                <input name="type_auction" id="type_auction" type="checkbox">
                <label class="negotiable" for="showAuction">Show auctions</label>
                <br>
                <input name="type_distressed" id="type_distressed" type="checkbox">
                <label class="negotiable" for="showDistressed">Show distressed</label>
                </div>
                </div>
                </div>
                <input type="hidden" name="ajax_call" value="ajax_call_search">
            	</form>
                <div id="map-listing-sidebar" style="display:none;">
                	<?php foreach($SearchResults as $result): ?>

                    <div onmouseover="openInfoWindow('<?php echo $result->mlsid; ?>')" style="cursor:pointer;" class="row map-sidebar-item">
                    	<div class="col-md-5" style="padding-left:5px;">
                        	
                            <?php if(isset($result->image) && $result->image!=""):?>
                            <a class="thumb" href="/property-details/<?php echo $result->url; ?>" target="_top">
                                <img alt="<?php echo $result->title; ?>" src="<?php echo $result->image; ?>" height="76px" width="90px">
                            </a>
                            <?php else:?>
                            <a class="thumb" href="/property-details/<?php echo $result->url; ?>" target="_top">
                                <img alt="<?php echo $result->title; ?>" src="<?php echo i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png" ?>" height="76px" width="90px">
                            </a>
                            <?php endif;?>

                        </div>
                        <div class="col-md-7 no-padding">
                        	<span class="list-title">
                    			<a href="/property-details/<?php echo $result->url; ?>" target="_top"><?php  							echo substr($result->title, 0, 20); ?></a>
                    		</span>
                            <span class="list-address"><?php echo $result->city; ?>, <?php echo $result->state; ?></span>
                            <span class="list-price"><strong>$<?php echo number_format($result->price, 0); ?></strong></span>
                            <span class="list-size"><?php  $sqft = $result->size; echo $acre = round($sqft/43560, 3); ?> Acres</span>
                            <span class="list-type"><?php echo $result->propertytype; ?></span>
                            
                        </div>
                    </div>
                	<?php endforeach; ?>
                </div>
                
            </div>
            <div role="tabpanel" class="tab-pane fade" id="for-lease">for lease</div>
          </div>
        </div>
        <div class="col-md-9 search-display">
        	 <?php 
			 	foreach($SearchResults as $result):
			 	
				$lat=0;
                $long=0;

                if(isset($SearchResults) && sizeof($SearchResults)>0)
                 {
                 	$lat=$SearchResults[0]->latitude;
                 	$long=$SearchResults[0]->longitude;
                 }
			 
			 ?>
             
            <div class="row search-item">
            	<div class="col-md-2  no-padding">
                	<?php if(isset($result->image) && $result->image!=""):?>
                            <a class="thumb" href="/property-details/<?php echo $result->url; ?>" target="_top">
                                <img alt="<?php echo $result->title; ?>" src="<?php echo $result->image; ?>" height="76px" width="90px">
                            </a>
                            <?php else:?>
                            <a class="thumb" href="/property-details/<?php echo $result->image; ?>" target="_top">
                                <img alt="<?php echo $result->title; ?>" src="<?php echo i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png" ?>" height="76px" width="90px">
                            </a>
                            <?php endif;?>
                </div>
                <div class="col-md-8">
                	<span class="list-title">
                    <a href="/property-details/<?php echo $result->url; ?>" target="_top"><?php echo $result->title; ?></a>
                    </span>
                    <span class="list-address"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $result->city; ?>, <?php echo $result->state; ?></span>
                    <span class="list-description">
                    	<?php echo $result->description; ?>
                    </span>
                </div>
                <div class="col-md-2  no-padding">
                	<span class="list-price">$<?php echo number_format($result->price, 0); ?></span>
                    <span class="list-size"><?php  $sqft = $result->size; echo $acre = round($sqft/43560, 3); ?> Acres</span>
                    <span class="list-type"><?php echo $result->propertytype; ?></span>
                </div>
                <div class="col-md-12 no-padding">
                	<span class="list-contact-advisor">
                    <i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;<a href="mailto:aanicete@nainorcal.com">Email Armand Anicete, BRE #01882022</a> or 
                    <a href="mailto:dolsen@nainorcal.com">David Olsen</a> for more information

                    </span>
                    <span class="list-links">                    	
                    
                    <a href="#" class="map" target="_top"><i class="fa fa-map-marker" aria-hidden="true"></i> Map</a>
                    <a href="#" class="flyer" target="_blank"><i class="fa fa-random" aria-hidden="true"></i>
 Flyer</a>
 					<a href="/property-details/<?php echo $result->url; ?>" class="more-details" target="_top">More Details <i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                    </span>
                </div>
            </div>
            <?php endforeach; ?>
            <?php include(i5PBIntegration__PLUGIN_DIR . 'Includes/SearchPaging-Functions.php');?>
        </div>
    	<div class="col-md-9 search-display-map" style="display:none;">
        	<div style="height: 100%;" >
			<?php
                  do_shortcode('[i5ListingMap maxzoom="100" minzoom="1" lat="' . $lat . '" long="' . $long . '" style="display:block;width: 100%; height: 100%" showPolygon="true"]');
                ?>
                <script>
                <?php
                  foreach($SearchResults as $result)
                  {
                      if($result->latitude!=0)
                          echo "markAddress(" . json_encode($result) . ");";
                  }
                ?>
                    fitBounds();
                </script>
        	</div>
        </div>
    </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#sorting").click(function(){
		jQuery("#ddview").hide();
        jQuery("#ddsort").toggle();
    });
	
	jQuery("#view").click(function(){
		
		jQuery("#ddsort").hide();
        jQuery("#ddview").toggle("slow", function() {
    		jQuery("#view i").toggleClass('fa-angle-up');
			jQuery("#view i").toggleClass('fa-angle-down');
  		});
    });
	
	jQuery("#list-view li").on("click", function(){	
		
		// remove active class
		jQuery(this).siblings().removeClass("active");
		// add active to this element
		jQuery(this).addClass("active");
		// hide dropdowm
		jQuery("#ddsort").hide("slow");
		jQuery("#ddview").toggle("slow");
		// toggle select arrow
		jQuery("#view i").toggleClass('fa-angle-up');
		jQuery("#view i").toggleClass('fa-angle-down');
		// replace dropdown with selected li html
		var htmlString = jQuery(this).text();
		jQuery("#view").html(htmlString + "<i class='fa fa-angle-down' aria-hidden='true'></i>");
		// toggle list / map view
		jQuery(".search-display").toggle();
		jQuery(".search-display-map").toggle();
		// toggle sidebar list/ map view
		jQuery("#saleFilters").toggle();
		jQuery("#map-listing-sidebar").toggle();
	});
	
});
</script>

<?php
include(i5PBIntegration__PLUGIN_DIR . 'Includes/Global-Functions.php');

?>