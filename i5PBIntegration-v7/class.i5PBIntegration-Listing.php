<?php
class i5PBIntegration_Listing
{
    private static $initiated = false;
   
    public function init(){
      	self::init_hooks();
        wp_register_script( "i5PBListings_Script", WP_PLUGIN_URL.'/i5PBIntegration/js/i5Listings.js', array('jquery') );

        $adminUrl;

        if(post_type_exists("i5agents"))
            $adminUrl="http://" . $_SERVER["HTTP_HOST"] . "/wp-admin/admin-ajax.php";
        else
            $adminUrl=admin_url( 'admin-ajax.php' );
      
      wp_enqueue_script( 'i5PBListings_Script' );
        wp_localize_script( 'i5PBListings_Script', 'i5PBAjax', array( 'ajaxurl' => $adminUrl,'nonce' => wp_create_nonce('i5PBIntegration')));
        
        
    }
    public function init_hooks(){
        add_action('wp_ajax_queryListings',array('i5PBIntegration_Listing', 'doQuery'));
        add_action('wp_ajax_nopriv_queryListings',array('i5PBIntegration_Listing', 'doQuery'));
        add_action('wp_ajax_autoCompleteListings',array('i5PBIntegration_Listing', 'doAutoCompleteSearch'));
        add_action('wp_ajax_nopriv_autoCompleteListings',array('i5PBIntegration_Listing', 'doAutoCompleteSearch'));
    }
    public function doAutoCompleteSearch(){
        if ( !wp_verify_nonce( $_REQUEST['nonce'], "i5PBIntegration") && !wp_verify_nonce( $_REQUEST['nonce'], "i5WebListings")) {
          exit("Denied");
        }
        
        global $wpdb;
        $limit=5;

        if(isset($_REQUEST["limit"]) && isset($_REQUEST["limit"])!="")
            $limit=$_REQUEST["limit"];

        $sql = "(select count(*), '' as redirect, 'City' as section,city as result from {$wpdb->prefix}i5listings where custom3='" . $_REQUEST["rid"] . "' and city like '" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), url as redirect, 'Address' as section,address as result from {$wpdb->prefix}i5listings where custom3='" . $_REQUEST["rid"] . "' and address like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), url as redirect, 'MLSID' as section,mlsid as result from {$wpdb->prefix}i5listings where custom3='" . $_REQUEST["rid"] . "' and mlsid like '" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'Zip Code' as section,zipcode as result from {$wpdb->prefix}i5listings where custom3='" . $_REQUEST["rid"] . "' and zipcode like '" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'Subdivision' as section,Subdivision as result from {$wpdb->prefix}i5listings where custom3='" . $_REQUEST["rid"] . "' and Subdivision like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'School District' as section,schooldistrict as result from {$wpdb->prefix}i5listings where custom3='" . $_REQUEST["rid"] . "' and schooldistrict like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'Area' as section,Area as result from {$wpdb->prefix}i5listings where custom3='" . $_REQUEST["rid"] . "' and Area like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'Elementary School' as section,elementaryschool as result from {$wpdb->prefix}i5listings where custom3='" . $_REQUEST["rid"] . "' and elementaryschool like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'Middle School' as section,middleschool as result from {$wpdb->prefix}i5listings where custom3='" . $_REQUEST["rid"] . "' and middleschool like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")
                union
                (select count(*), '' as redirect, 'High School' as section,highschool as result from {$wpdb->prefix}i5listings where custom3='" . $_REQUEST["rid"] . "' and highschool like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")";
                

      if($_REQUEST["rid"]=="012d0000000hNU5AAM")
        {
          $sql .= " union
                (select count(*), url as redirect, 'Title' as section,title as result from {$wpdb->prefix}i5listings where custom3='" . $_REQUEST["rid"] . "' and title like '%" . $_REQUEST["kw"] . "%' group by result,redirect,section limit " . $limit . ")";
                
          }
      
      $sql .= " order by section,result";
      
        $results = $wpdb->get_results($sql);

      	//exit($sql);
      
        echo json_encode($results);

        die();

    }
    public function doQuery()
    {
        global $wpdb;

        if ( !wp_verify_nonce( $_REQUEST['nonce'], "i5PBIntegration")) {
            exit("Denied");
        }

        $sql ="select " . str_replace("\'","'",$_REQUEST["query"]) . ",i.url as image from {$wpdb->prefix}i5listings as l left join {$wpdb->prefix}i5Images i on i.id = (select min(id) from {$wpdb->prefix}i5Images ii where ii.propertyid=l.propertyid)";

        if(isset($_REQUEST["where"]) && $_REQUEST["where"]!="")
            $sql .= " where " . str_replace("\'","'",$_REQUEST["where"]);

        if(isset($_REQUEST["group"]) && $_REQUEST["group"] !="")
            $sql .= " group by " . $_REQUEST["group"];

        if(isset($_REQUEST["order"]) && $_REQUEST["order"]!="")
            $sql .= " order by " . $_REQUEST["order"];

        if(isset($_REQUEST["limit"]) && $_REQUEST["limit"]!="")
            $sql .= " limit " . $_REQUEST["limit"];

        $results = $wpdb->get_results($sql);
                
        echo json_encode($results);

        die();
    }
}