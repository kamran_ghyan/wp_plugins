<?php
/*
Plugin Name:i5Propertybase Integration
Plugin URI: http://www.i5fusion.com
Description: Integrates Propertybase
Author: Jarrett Fisher
Version: 2.0
Author URI:http://www.i5fusion.com
 */

$time = microtime(true);

define( 'i5PBIntegration__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define('i5PBIntegration__PLUGIN_URL',plugin_dir_url(__FILE__));

if ( is_admin() ) {
    require_once( i5PBIntegration__PLUGIN_DIR . 'class.i5PBIntegration-Admin.php' );
    add_action( 'init', array( 'i5PBIntegration_Admin', 'init' ) );
}

require_once( i5PBIntegration__PLUGIN_DIR . 'class.i5PBIntegration.php' );
require_once( i5PBIntegration__PLUGIN_DIR . 'class.i5PBIntegration-Template.php' );

add_action('init',array('i5PBIntegration','init'));

register_deactivation_hook(__FILE__,array('i5PBIntegration', 'deactivation'));
register_activation_hook(__FILE__, array('i5PBIntegration', 'activation'));

add_action('SyncListings',array('i5PBIntegration', 'mlsSync'));

require_once( i5PBIntegration__PLUGIN_DIR . 'class.i5PBIntegration-Listing.php' );
add_action( 'init',array('i5PBIntegration_Listing','init'));
