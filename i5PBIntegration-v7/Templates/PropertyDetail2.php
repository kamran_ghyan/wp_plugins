<style>
.slick-prev:before, .slick-next:before {
    color:#fff !important;
    font-size:30px !important;
}
</style>
<?php
wp_enqueue_style( 'property-detail2', i5PBIntegration__PLUGIN_URL . 'css/property-detail2.css', array(), '1.1', 'all');
wp_enqueue_style( 'property-search-header', i5PBIntegration__PLUGIN_URL . 'css/search-header2.css', array(), '1.1', 'all');
wp_enqueue_style( 'slick-styles', i5PBIntegration__PLUGIN_URL . 'css/slick.css', array(), '1.1', 'all');
wp_enqueue_style( 'slick-theme', i5PBIntegration__PLUGIN_URL . 'css/slick-theme.css', array(), '1.1', 'all');
wp_enqueue_script( 'slick', i5PBIntegration__PLUGIN_URL . 'js/slick.min.js', array('jquery'), '1.1', true );
wp_enqueue_script( 'PrintElement', i5PBIntegration__PLUGIN_URL . 'js/PrintElement.js', array('jquery'), '1.1', true );
/*
Template Name: Property Detail 2h
 */
$showSave=false;
get_header();
include(i5PBIntegration__PLUGIN_DIR . 'Includes/Prospect-Functions.php');
include(i5PBIntegration__PLUGIN_DIR . 'Includes/PropertyDetail-Functions.php');
include(i5PBIntegration__PLUGIN_DIR . 'Includes/PropertySearch-Functions.php');
include(i5PBIntegration__PLUGIN_DIR . "Includes/HeaderSearch.php");
include(i5PBIntegration__PLUGIN_DIR . 'Includes/Global-Functions.php');
?>
<div class="printContainer">
    <div class="detailsHeader">
        <div class="container">
            <div class="buttonBack printHide">
                <a href="<?php echo $prevSearch?>">
                    <i class="fa fa-angle-left"></i>
                </a>
            </div>
            <div class="detailsAddress div2-3">
                <h1 itemprop="address" itemtype="http://schema.org/PostalAddress">
                    <?php echo $property->address ?>
                </h1>
                <div class="address2">
                    <?php echo $property->city ?>, <?php echo $property->state ?><?php echo $property->zipCode ?>
                </div>
                <div class="addressDetails">
                    <div class="addressListingStatus">
                        <div class="new">
                            <?php echo $property->status ?>
                        </div>
                    </div>
                    <div class="addressPrice">
                        PRICE:
                        <span class="success">
                            <?php echo "$" . number_format($property->price,0) . ($property->propertytype=="Rental"?" /Month":"") ?>
                        </span>
                    </div>
                    <div class="addressStatus">
                        STATUS:
                        <b>
                            <?php echo $property->status ?>
                        </b>
                    </div>
                    <div class="addressUpdated">
                        UPDATED:
                        <b>
                            <?php
                            $settings=get_option('WebListing_Settings');
                            $lastRun=strtotime(date("Y-m-d\Th:i:s"));
                            $currTime=$lastRun;

                            if(isset($settings["LastRun"]))
                                $lastRun = strtotime($settings["LastRun"]);


                            echo round(abs($currTime - $lastRun) / 60,0) . " MIN AGO";
                            ?>
                        </b>
                    </div>
                    <div class="addressID">
                        ID#:
                        <b>
                            <?php echo $property->mlsid ?>
                        </b>
                    </div>

                </div>
            </div>
            <!-- end detailsAddress -->
            <div class="div1-3 printHide">
                <div id="fav<?php echo $property->mlsid?>" class="<?php echo ($favs=='' || !in_array($property->mlsid,$favs)?'detailsFavorite':'detailsFavoriteSelected')?>">
                    <a href="javascript:toggleFav('<?php echo $property->mlsid ?>');setFavorite('<?php echo $property->mlsid ?>',<?php echo ($favs=='' || !in_array($property->mlsid,$favs)?'1':'0')?>);">
                        <i class="fa fa-heart"></i>
                    </a>
                </div>
                <div class="detailsShowing">
                    <a class="button" href="#showingForm">Request Showing</a>
                </div>
                <div class="detailsButtons">
                    <div class="btnShowMap">
                        <a href="#map" id="btnMap">Map</a>
                    </div>
                    <div onclick="printListing();" class="btnPrint">Print</div>
                    <div onclick="showSharing()" class="btnShare dropbtn">Share</div>

                    <div id="ddShare" class="ddShare dropdownContent">
                        <div class="padding10">
                            <?php echo do_shortcode( '[woocommerce_social_media_share_buttons]' ); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end contain -->
    </div>
    <!-- end detailsHeader -->
    <div class="detailsBody">
        <div class="container">
            <div class="div2-3">
                <div class="propertyImages">
                    <?php
                    if(sizeof($images)==0){
                        echo " <div><img src='" . i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png' /></div>";
                    }
                    else{

                        foreach($images as $image)
                            echo " <div><img onerror='fixImage(this);' src='" . $image->url . "' /></div>";
                    }
                    ?>
                </div>
            </div>
            <!-- end div 2-3 -->
            <div class="div1-3">
                <div class="detailsBox">
                    <div class="detailsPrice">
                        <?php echo "$" . number_format($property->price,0) . ($property->propertytype=="Rental"?" /Month":"") ?>
                    </div>
                    <div class="detailsBeds">
                        <div class="detailsValue">
                            <?php echo number_format($property->bedrooms,0) ?>
                        </div>
                        <div class="detailsLabel">
                            Beds
                        </div>
                    </div>
                    <div class="detailsAcres">
                        <div class="detailsValue">
                            <?php echo number_format($property->lotsize,0) ?>
                        </div>
                        <div class="detailsLabel">
                            Acres
                        </div>
                    </div>
                    <div class="detailsBaths">
                        <div class="detailsValue">
                            <?php echo number_format($property->fullBaths,0) ?>
                        </div>
                        <div class="detailsLabel">
                            Baths
                        </div>
                    </div>
                    <div class="details1-2Baths">
                        <div class="detailsValue">
                            <?php echo number_format($property->halfBaths,0) ?>
                        </div>
                        <div class="detailsLabel">
                            1/2 Baths
                        </div>
                    </div>
                    <div class="detailsSqft">
                        <div class="detailsValue">
                            <?php echo number_format($property->size,0) ?>
                        </div>
                        <div class="detailsLabel">
                            SQFT
                        </div>
                    </div>
                    <div class="detailsPriceSqft">
                        <div class="detailsValue">
                            <?php
                            if(isset($property->size))
                                echo "$" . number_format($property->price/ $property->size,0)
                            ?>
                        </div>
                        <div class="detailsLabel">
                            $/SQFT
                        </div>
                    </div>
                    <div class="detailsSpecs">
                        <div class="specsLabel">
                            Neighborhood:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->subdivision) && $property->subdivision!=""?$property->subdivision:"&nbsp;")?>
                        </div>
                        <div class="specsLabel">
                            Type:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->propertytype) && $property->propertytype!=""?$property->propertytype:"&nbsp;")?>
                        </div>
                        <div class="specsLabel">
                            Built:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->yearbuilt) && $property->yearbuilt!=""?$property->yearbuilt:"&nbsp;")?>
                        </div>
                        <div class="specsLabel">
                            Area:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->area) && $property->area!=""?$property->area:"&nbsp;")?>
                        </div>
                    </div>
                </div>
                <!-- end detailsBox -->
                <div class="detailsBox">
                    <div class="headerSchoolInfo">
                        School Information
                    </div>
                    <div class="detailsSpecs">
                        <div class="specsLabel">
                            Elementary School:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->elementarySchool) && $property->elementarySchool!=""?$property->elementarySchool:"&nbsp;")?>
                        </div>
                        <div class="specsLabel">
                            Middle School:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->middleSchool) && $property->middleSchool!=""?$property->middleSchool:"&nbsp;")?>
                        </div>
                        <div class="specsLabel">
                            High School:
                        </div>
                        <div class="specsValue">
                            <?php echo (isset($property->highSchool) && $property->highSchool!=""?$property->highSchool:"&nbsp;")?>
                        </div>
                        <div class="specsLabel"></div>
                        <div class="specsValue">
                            <a href="#">School Ratings &amp; Info</a>
                        </div>
                    </div>
                </div>
                <!-- end detailsBox -->
            </div>
            <!-- end div1-3 -->
            <div class="detailsDescriptionHolder">
                <div class="div2-3">
                    <div class="detailsDescriptionHeader">
                        Description
                    </div>
                    <p>
                        <?php echo $property->description?>
                    </p>
                </div>
                <!-- end div2-3 -->
                <div class="div1-3">
                    <div class="agentTile">
                        <?php
                        $agent;
                        if(post_type_exists("i5agents")){
                            global $wpdb;
                            $sql="select p.id from {$wpdb->prefix}postmeta pm join {$wpdb->prefix}posts p on p.id=pm.post_id where meta_key='domain' and p.post_status='publish' and p.post_type='i5agents' and pm.meta_value='" . $_SERVER['HTTP_HOST'] . "'";
                            $agent=$wpdb->get_row($sql);
                        }

                        if($agent!=null){
                            echo do_shortcode("[i5AgentData show_social_links='false' office_phone_label='O: ' mobile_phone_label='M: ']");
                        } else{?>
                        <div class="div1-3">
                            <div class="padding10">
                                <img src="/wp-content/themes/realhomes/images/sellimg.jpg" />
                            </div>
                        </div>
                        <!-- end div1-3 -->
                        <div class="div2-3">
                            <div class="agentTitle">
                                <?php echo esc_attr( get_post_meta( get_the_ID(), 'companyname', true ) ); ?>
                            </div>
                            <div class="agentPhone">
                                <a href="tel:<?php echo esc_attr( get_post_meta( get_the_ID(), 'companyphone', true ) ); ?>">
                                    <span class="red">O:</span>
                                    <?php echo esc_attr( get_post_meta( get_the_ID(), 'companyphone', true ) ); ?>
                                </a>
                            </div>
                        </div>
                        <!-- end div2-3 -->
                        <?php }?>
                    </div>
                    <!-- end agentTile -->
                </div>
                <!-- end div1-3 -->
            </div>
            <div class="featuresHolder">
                <div class="div1-3 marginRight">
                    <div class="featuresHeader">
                        Exterior Features
                    </div>
                    <div class="featuresBox">
                        <div class="detailsFeaturesLabel">
                            Exterior Siding
                        </div>
                        <div class="detailsFeaturesValue">
                            Aluminum
                        </div>
                        <div class="detailsFeaturesLabel">
                            Exterior Siding
                        </div>
                        <div class="detailsFeaturesValue">
                            Aluminum
                        </div>
                    </div>
                    <!-- end featuresBox -->
                </div>
                <!-- end div1-3 -->
                <div class="div1-3 marginRight">
                    <div class="featuresHeader">
                        Interior Features
                    </div>
                    <div class="featuresBox">
                        <div class="detailsFeaturesLabel">
                            Exterior Siding
                        </div>
                        <div class="detailsFeaturesValue">
                            Aluminum
                        </div>
                        <div class="detailsFeaturesLabel">
                            Exterior Siding
                        </div>
                        <div class="detailsFeaturesValue">
                            Aluminum
                        </div>
                    </div>
                    <!-- end featuresBox -->
                </div>
                <!-- end div1-3 -->
                <div class="div1-3">
                    <div class="featuresHeader">
                        Property Features
                    </div>
                    <div class="featuresBox">
                        <div class="detailsFeaturesLabel">
                            Exterior Siding
                        </div>
                        <div class="detailsFeaturesValue">
                            Aluminum
                        </div>
                        <div class="detailsFeaturesLabel">
                            Exterior Siding
                        </div>
                        <div class="detailsFeaturesValue">
                            Aluminum
                        </div>
                    </div>
                    <!-- end featuresBox -->
                </div>
                <!-- end div1-3 -->
            </div>
            <!-- end featuresHolder -->
            <div class="detailsMap">
                <a id="map"></a>
                <p style="text-align:center; margin-top:30px;">
                    <?php
                    do_shortcode('[i5ListingMap maxzoom="100" minzoom="1" style="display:block;width: 100%; height: 418px" showdetails="false" showpolygon="false" zoom="15" lat="' . $property->latitude . '" long="' . $property->longitude . '" ]');
                    echo "<script>markAddress(" . json_encode($property) . ");</script>";
                    ?>
                </p>
            </div>
            <div class="detailsBox">
                <div class="padding10">
                    <a id="showingForm"></a>
                    <div class="col1-4">
                        <div class="agentTitle">
                            <?php echo esc_attr( get_post_meta( get_the_ID(), 'companyname', true ) ); ?>
                        </div>
                        <div class="agentPhone">
                            <a href="tel:<?php echo esc_attr( get_post_meta( get_the_ID(), 'companyphone', true ) ); ?>">
                                <span class="red">O:</span>
                                <?php echo esc_attr( get_post_meta( get_the_ID(), 'companyphone', true ) ); ?>
                            </a>
                        </div>
                    </div>
                    <div class="col1-2">
                        <div class="padding10">
                            <?php
                            echo do_shortcode( '[contact-form-7 id="6202" title="Schedule a Showing"]' );
                            ?>
                        </div>
                    </div>
                    <div class="col1-4">
                        <?php echo htmlspecialchars_decode(get_post_meta( get_the_ID(), 'companylogo', true )); ?>
                    </div>
                </div>
                <!-- end padding10 -->
            </div>
            <!-- end detailsBox -->
            <?php include(i5PBIntegration__PLUGIN_DIR . 'Includes/RelatedProperties.php'); ?>
            <div class="Disclaimer">
                <?php echo htmlspecialchars_decode(get_post_meta( get_the_ID(), 'disclaimer', true )); ?>
            </div>
        </div>
        <!-- end container -->
    </div>
</div>
<!-- end detailsBody -->


<script>
    jQuery(function () {
        jQuery(".propertyImages").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true
        });
    });
    function toggleFav(id)
    {
        if (jQuery("#fav" + id).hasClass("detailsFavoriteSelected"))
        {
            jQuery("#fav" + id).removeClass("detailsFavoriteSelected");
            jQuery("#fav" + id).addClass("detailsFavorite");
            jQuery(jQuery("#fav" + id).children()[0]).prop("href","javascript:toggleFav('" + id + "');setFavorite('" + id + "',1);");
        }
        else {
            jQuery("#fav" + id).addClass("detailsFavoriteSelected");
            jQuery("#fav" + id).removeClass("detailsFavorite");
            jQuery(jQuery("#fav" + id).children()[0]).prop("href", "javascript:toggleFav('" + id + "');setFavorite('" + id + "',0);");
        }

    }
    function printListing()
    {
        jQuery(".printHide").hide();
        jQuery(".printContainer").printElement();
        jQuery(".printHide").show();
    }
  function showSharing() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddShare").classList.toggle("show");
  }

  // Close the dropdown if the user clicks outside of it
  window.onclick = function(event) {
  if (!event.target.matches('.dropBtn')) {

  var dropdowns = document.getElementsByClassName("dropdownContent");
  var i;
  for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

</script>

<?php
get_footer();

?>
