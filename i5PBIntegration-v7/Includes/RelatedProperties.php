<?php

wp_enqueue_style( 'owlcss', i5PBIntegration__PLUGIN_URL . 'css/owl.carousel.css', array(), '1.1', 'all');
wp_enqueue_script( 'owljs', i5PBIntegration__PLUGIN_URL . 'js/owl.carousel.min.js', array('jquery'), '1.1', true );

$sql="SELECT title,status,l.url,latitude,longitude,i.url AS image,mlsid,bedrooms,address,city,state,zipCode,price,fullBaths,halfBaths,size from {$wpdb->prefix}i5listings l
      left join {$wpdb->prefix}i5Images i on i.id = (select min(id) from {$wpdb->prefix}i5Images ii where ii.propertyid=l.propertyid)
      where ";

$append="";

if(isset($relatedBaths))
{
    $sql .= " fullBaths=" . $relatedBaths;
    $append=" and ";
}

if(isset($relatedPropertyType))
{
    $sql .= $append . "propertytype='" . $relatedPropertyType . "'";
    $append=" and ";
}

if(isset($relatedSize))
{
    $sql .= $append . "size < " . ($relatedSize + 500) . " and size > " . ($relatedSize - 500);
    $append=" and ";
}

if(isset($relatedPrice))
{
    $sql .= $append . "price < " . ($relatedPrice + 50000) . " and price > " . ($relatedPrice - 50000);
    $append=" and ";
}

$RelatedResults=$wpdb->get_results($sql . " limit 10");

if(sizeof($RelatedResults)<3)
{
    $sql="SELECT title,status,l.url,latitude,longitude,i.url AS image,mlsid,bedrooms,address,city,state,zipCode,price,fullBaths,halfBaths,size from {$wpdb->prefix}i5listings l
      left join {$wpdb->prefix}i5Images i on i.id = (select min(id) from {$wpdb->prefix}i5Images ii where ii.propertyid=l.propertyid)
      where propertytype= " . $relatedPropertyType . " and size < " . $relatedSize + 500 . " and size > " . $relatedSize - 500;

    $RelatedResults=$wpdb->get_results($sql . " limit 10");
}    

?>

<div class="relatedProperties">
    <div class="relatedHeader">
        Related Properties
    </div>
    <div style="width:600px;" class="col1-1 carousel">
        <?php foreach($RelatedResults as $Related){?>
        <div class="col1-4 item">
            <div class="listing">
                <div class="listingImage">
                    <?php if(isset($Related->image) && $Related->image!=""):?>
                    <img src="<?php echo $Related->image?>" />
                    <?php else:?>
                    <img src="<?php echo i5PBIntegration__PLUGIN_URL . 'images/imgUnavailable.png' ?>" />
                    <?php endif;?>
                </div>
                <div id="fav<?php echo $Related->mlsid?>" class="<?php echo ($favs=='' || !in_array($Related->mlsid,$favs)?'listingFavorite':'listingFavoriteSelected')?>">
                    <a href="javascript:toggleFav('<?php echo $Related->mlsid ?>');setFavorite('<?php echo $Related->mlsid ?>',<?php echo ($favs=='' || !in_array($Related->mlsid,$favs)?'1':'0')?>);">
                        <i class="fa fa-heart"></i>
                    </a>
                </div>
                <div class="listingAddress">
                    address locality | <?php echo $Related->city?>
                </div>
                <div class="listingPrice">
                    <?php echo "$" . number_format($Related->price,0)?>
                </div>
                <div class="listingBeds">
                    <div class="bedsValue">
                        <?php echo $Related->bedrooms?>
                    </div>
                    <div class="bedsLabel">
                        Beds
                    </div>
                </div>
                <div class="listingBaths">
                    <div class="bathsValue">
                        <?php
                        if($Related->halfBaths!=0 && $Related->fullBaths!=0)
                            echo $Related->fullBaths + ($Related->halfBaths/2);
                        else if($Related->halfBaths!=0 && $Related->fullBaths==0)
                            echo ($Related->halfBaths/2);
                        else
                            echo number_format($Related->fullBaths,0);
                        ?>
                    </div>
                    <div class="bathsLabel">
                        Baths
                    </div>
                </div>
                <?php if($Related->size!=0) :?>
                <div class="listingSqft">
                    <div class="sqftValue">
                        <?php echo number_format($Related->size,0);?>
                    </div>
                    <div class="sqftLabel">
                        SqFt
                    </div>
                </div>
                <?php endif;?>
                <?php if($Related->size!=0) :?>
                <div class="listingAcres">
                    <div class="acresValue">
                        <?php echo number_format($Related->lotsize,0);?>
                    </div>
                    <div class="acresLabel">
                        Acres
                    </div>
                </div>
                <?php endif;?>
                <div class="listingStatus">
                    <!-- other status
						<div class="shortSale">Short Sale</div>
            <div class="foreclosure">Foreclosure</div>-->
                    <div class="new">
                        <?php echo $Related->status?>
                    </div>
                </div>
                <div class="viewDetails">
                    <a href="/property-details/<?php echo urldecode($Related->url)?>">View Details</a>
                </div>
            </div>
        </div>
        <?php }?>
    </div>
</div>
<script>
    jQuery(function () {
        jQuery('.carousel').owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            autoplay: true,
            autoplayTimeout:4000,
            items:4,
            //responsive: {
            //    0: {
            //        items: 1
            //    },
            //    600: {
            //        items: 3
            //    },
            //    1000: {
            //        items: 5
            //    }
            //}
        });
    });
</script>
