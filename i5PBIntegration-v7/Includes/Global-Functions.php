<script>
    jQuery("img").error(function () {
        jQuery(this).attr("src", '<?php echo i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png" ?>');
    });
    function fixImage(image) {
        image.onerror = "";
        image.src = '<?php echo i5PBIntegration__PLUGIN_URL . "images/imgUnavailable.png" ?>';
        return true;
    }
</script>