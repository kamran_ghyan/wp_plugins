<?php 
wp_enqueue_script("Listings_Script",i5PBIntegration__PLUGIN_URL . 'js/i5Listings.js');
include(i5PBIntegration__PLUGIN_DIR . 'Includes/SaveSearch-Popup.php');
?>
<div class="searchHeader">
    <div class="container">
        <div class="searchBar">
            <div class="searchBy dropBtn" onclick="showSearchBy()">
                Search By <i class="fa fa-angle-down"></i>
            </div>
            <input type="hidden" name="searchtype" id="searchtype" />
            <div id="dropdown" class="ddSearchBy dropdownContent">
                <ul class="ulSearchBy">
                    <!--
                    <li>
                        <a href="javascript:setSearchType('byme');">Nearby Me</a>
                    </li>
                        -->
                    <li>
                        <a href="javascript:setSearchType('all','Type any Area, Address, Zip, School, etc');">Search All</a>
                    </li>
                    <li>
                        <a href="javascript:setSearchType('City','Type any City');">Cities</a>
                    </li>
                    <li>
                        <a href="javascript:setSearchType('Subdivision','Type any Subdivision');">Subdivisions</a>
                    </li>
                    <li>
                        <a href="javascript:setSearchType('Address','Type any Address');">Address</a>
                    </li>
                    <li>
                        <a href="javascript:setSearchType('MLSID','Type any MLS#');">MLS#</a>
                    </li>
                    <li>
                        <a href="javascript:setSearchType('School','Type any School');">Schools</a>
                    </li>
                    <li>
                        <a href="javascript:setSearchType('School District','Type any School District');">School Districts</a>
                    </li>
                    <li>
                        <a href="javascript:setSearchType('Zip Code','Type any Zipcode');">Zip</a>
                    </li>
                    <li>
                        <a href="javascript:setSearchType('Area','Type any Area');">Area</a>
                    </li>
                    <li>
                        <a href="javascript:setSearchType('kw','Type any Keyword');">Keyword</a>
                    </li>
                    <li>
                        <a href="#">Feature</a>
                    </li>
                </ul>
            </div>
            <div class="inputHolder">
                <input id="inputSearch" onclick="showSearchResults()" class="inputSearch" placeholder="Type any Area, Address, Zip, School, etc" type="text" />
            </div>

            <div id="ddSearchResults" class="ddSearchResults">
                <ul class="ulSearchResults">
                    
                </ul>
            </div>
        </div>
        <!-- end searchBar -->
        <div class="searchFilters">
            <button onclick="showPriceFilters()" class="dropbtn">
                Price <i class="fa fa-angle-down"></i>
            </button>
            <div id="ddPrice" class="ddPrice dropdownContent">
                <input class="dropBtn" type="number" onchange="menuStick('ddPrice');doSearch();" <?php echo (isset($_REQUEST['minprice'])?" value='" . $_REQUEST['minprice'] ."' ":"") ?> placeholder="Min Price" id="minprice" />
                <input class="dropBtn" type="number" onchange="doSearch();" <?php echo (isset($_REQUEST['minprice'])?" value='" . $_REQUEST['maxprice'] . "' ":"") ?> placeholder="Max Price" id="maxprice" />
                <ul id="selMinPrice" class="ulPrice">
                    <li class="dropBtn" onclick="setPrice('minprice', 0);" data-price="Any Price">Any</li>
                    <li class="dropBtn" onclick="setPrice('minprice', 50000);" data-price="50000">$50K</li>
                    <li class="dropBtn" onclick="setPrice('minprice', 75000);" data-price="75000">$75K</li>
                    <li class="dropBtn" onclick="setPrice('minprice', 100000);" data-price="100000">$100K</li>
                    <li class="dropBtn" onclick="setPrice('minprice', 150000);" data-price="150000">$150K</li>
                    <li class="dropBtn" onclick="setPrice('minprice', 200000);" data-price="200000">$200K</li>
                    <li class="dropBtn" onclick="setPrice('minprice', 250000);" data-price="250000">$250K</li>
                    <li class="dropBtn" onclick="setPrice('minprice', 300000);" data-price="300000">$300K</li>
                    <li class="dropBtn" onclick="setPrice('minprice', 350000);" data-price="350000">$350K</li>
                    <li class="dropBtn" onclick="setPrice('minprice', 400000);" data-price="400000">$400K</li>
                    <li class="dropBtn" onclick="setPrice('minprice', 500000);" data-price="400000">$500K</li>
                    <li class="dropBtn" onclick="setPrice('minprice', 600000);" data-price="400000">$600K</li>
                </ul>
                <ul id="selMaxPrice" style="display:none;" class="ulPrice">
                    <li class="dropBtn" onclick="setPrice('maxprice', 0);" data-price="Any Price">Any</li>
                    <li class="dropBtn" onclick="setPrice('maxprice', 50000);" data-price="50000">$50K</li>
                    <li class="dropBtn" onclick="setPrice('maxprice', 75000);" data-price="75000">$75K</li>
                    <li class="dropBtn" onclick="setPrice('maxprice', 100000);" data-price="100000">$100K</li>
                    <li class="dropBtn" onclick="setPrice('maxprice', 150000);" data-price="150000">$150K</li>
                    <li class="dropBtn" onclick="setPrice('maxprice', 200000);" data-price="200000">$200K</li>
                    <li class="dropBtn" onclick="setPrice('maxprice', 250000);" data-price="250000">$250K</li>
                    <li class="dropBtn" onclick="setPrice('maxprice', 300000);" data-price="300000">$300K</li>
                    <li class="dropBtn" onclick="setPrice('maxprice', 350000);" data-price="350000">$350K</li>
                    <li class="dropBtn" onclick="setPrice('maxprice', 400000);" data-price="400000">$400K</li>
                    <li class="dropBtn" onclick="setPrice('maxprice', 500000);" data-price="400000">$500K</li>
                    <li class="dropBtn" onclick="setPrice('maxprice', 600000);" data-price="400000">$600K</li>
                </ul>
            </div>
            <button onclick="showTypeFilters()" class="dropbtn">
                Type <i class="fa fa-angle-down"></i>
            </button>
            <div id="ddType" class="ddType dropdownContent">
                 <?php
                 
                 $types=explode(";",urldecode(get_query_var("search_query")));

                 if(strtolower(get_query_var("search_query"))=="all")
                     $types=array();

                 foreach($PropertyTypes as $propType)
                 {
                     if($propType!="All")
                         echo "<div class='typeCheckbox'><input " . (in_array($propType,$types)?" checked ":"") . "onclick='menuStick(\"ddType\");doSearch();' type='checkbox' name='propertyType' value='" . $propType . "' />" . $propType . "</div>";
                 }
                 ?>
            </div>
            <button onclick="showBedFilters()" class="dropbtn">
                Beds <i class="fa fa-angle-down"></i>
            </button>
            <div id="ddBeds" class="ddBeds dropdownContent">
                <div class="bedsCheckbox">
                    <input name="minbedrooms" onchange="doSearch();" <?php echo (!isset($_REQUEST['minbedrooms'])?" checked ":"") ?> value="" type="radio" />Any
                </div>
                <div class="bedsCheckbox">
                    <input name="minbedrooms" onchange="doSearch();" <?php echo (isset($_REQUEST['minbedrooms']) && $_REQUEST['minbedrooms']=="2" ?" checked ":"") ?> value="2" type="radio" />2+
                </div>
                <div class="bedsCheckbox">
                    <input name="minbedrooms" onchange="doSearch();" <?php echo (isset($_REQUEST['minbedrooms']) && $_REQUEST['minbedrooms']=="3" ?" checked ":"") ?> value="3" type="radio" />3+
                </div>
                <div class="bedsCheckbox">
                    <input name="minbedrooms" onchange="doSearch();" <?php echo (isset($_REQUEST['minbedrooms']) && $_REQUEST['minbedrooms']=="4" ?" checked ":"") ?> value="4" type="radio" />4+
                </div>
                <div class="bedsCheckbox">
                    <input name="minbedrooms" onchange="doSearch();" <?php echo (isset($_REQUEST['minbedrooms']) && $_REQUEST['minbedrooms']=="5" ?" checked ":"") ?> value="5" type="radio" />5+
                </div>
            </div>
            <button onclick="showBathFilters()" class="dropbtn">
                Baths <i class="fa fa-angle-down"></i>
            </button>
            <div id="ddBaths" class="ddBaths dropdownContent">
                <div class="bedsCheckbox">
                    <input name="minfullBaths" onchange="doSearch();" value="" type="radio" <?php echo (!isset($_REQUEST['minfullBaths'])?" checked ":"") ?> />Any
                </div>
                <div class="bedsCheckbox">
                    <input name="minfullBaths" onchange="doSearch();" <?php echo (isset($_REQUEST['minfullBaths']) && $_REQUEST['minfullBaths']=="2" ?" checked ":"") ?> value="2" type="radio" />2+
                </div>
                <div class="bedsCheckbox">
                    <input name="minfullBaths" onchange="doSearch();" <?php echo (isset($_REQUEST['minfullBaths']) && $_REQUEST['minfullBaths']=="3" ?" checked ":"") ?> value="3" type="radio" />3+
                </div>
                <div class="bedsCheckbox">
                    <input name="minfullBaths" onchange="doSearch();" <?php echo (isset($_REQUEST['minfullBaths']) && $_REQUEST['minfullBaths']=="4" ?" checked ":"") ?> value="4" type="radio" />4+
                </div>
                <div class="bedsCheckbox">
                    <input name="minfullBaths" onchange="doSearch();" <?php echo (isset($_REQUEST['minfullBaths']) && $_REQUEST['minfullBaths']=="5" ?" checked ":"") ?> value="5" type="radio" />5+
                </div>
            </div>
            <button onclick="showMoreFilters()" class="dropbtn">
                More <i class="fa fa-plus"></i>
            </button>
            <div id="ddMore" class="ddMore dropdownContent">
                <div class="moreFilters">
                    <div class="col1-3">
                        <label>Square Footage</label>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="minsize" id="minsize">
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="" ?" selected ":"") ?> value="">Any</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="500" ?" selected ":"") ?> value="500">500</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="550" ?" selected ":"") ?> value="550">550</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="600" ?" selected ":"") ?> value="600">600</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="650" ?" selected ":"") ?> value="650">650</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="700" ?" selected ":"") ?> value="700">700</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="750" ?" selected ":"") ?> value="750">750</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="800" ?" selected ":"") ?> value="800">800</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="850" ?" selected ":"") ?> value="850">850</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="900" ?" selected ":"") ?> value="900">900</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="950" ?" selected ":"") ?> value="950">950</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="1000" ?" selected ":"") ?> value="1000">1,000</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="1050" ?" selected ":"") ?> value="1050">1,050</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="1100" ?" selected ":"") ?> value="1100">1,100</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="1150" ?" selected ":"") ?> value="1150">1,150</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="1200" ?" selected ":"") ?> value="1200">1,200</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="1250" ?" selected ":"") ?> value="1250">1,250</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="1500" ?" selected ":"") ?> value="1500">1,500</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="1750" ?" selected ":"") ?> value="1750">1,750</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="2000" ?" selected ":"") ?> value="2000">2,000</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="2250" ?" selected ":"") ?> value="2250">2,250</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="2500" ?" selected ":"") ?> value="2500">2,500</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="2750" ?" selected ":"") ?> value="2750">2,750</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="3000" ?" selected ":"") ?> value="3000">3,000</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="3500" ?" selected ":"") ?> value="3500">3,500</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="4000" ?" selected ":"") ?> value="4000">4,000</option>
                                <option <?php echo (isset($_REQUEST['minsize']) && $_REQUEST['minsize']=="5000" ?" selected ":"") ?> value="5000">5,000</option>
                            </select>
                        </div>
                        <div class="searchTo">To</div>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="maxsize" id="maxsize">
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="" ?" selected ":"") ?> value="">Any</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="500" ?" selected ":"") ?> value="500">500</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="550" ?" selected ":"") ?> value="550">550</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="600" ?" selected ":"") ?> value="600">600</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="650" ?" selected ":"") ?> value="650">650</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="700" ?" selected ":"") ?> value="700">700</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="750" ?" selected ":"") ?> value="750">750</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="800" ?" selected ":"") ?> value="800">800</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="850" ?" selected ":"") ?> value="850">850</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="900" ?" selected ":"") ?> value="900">900</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="950" ?" selected ":"") ?> value="950">950</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="1000" ?" selected ":"") ?> value="1000">1,000</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="1050" ?" selected ":"") ?> value="1050">1,050</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="1100" ?" selected ":"") ?> value="1100">1,100</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="1150" ?" selected ":"") ?> value="1150">1,150</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="1200" ?" selected ":"") ?> value="1200">1,200</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="1250" ?" selected ":"") ?> value="1250">1,250</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="1500" ?" selected ":"") ?> value="1500">1,500</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="1750" ?" selected ":"") ?> value="1750">1,750</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="2000" ?" selected ":"") ?> value="2000">2,000</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="2250" ?" selected ":"") ?> value="2250">2,250</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="2500" ?" selected ":"") ?> value="2500">2,500</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="2750" ?" selected ":"") ?> value="2750">2,750</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="3000" ?" selected ":"") ?> value="3000">3,000</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="3500" ?" selected ":"") ?> value="3500">3,500</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="4000" ?" selected ":"") ?> value="4000">4,000</option>
                                <option <?php echo (isset($_REQUEST['maxsize']) && $_REQUEST['maxsize']=="5000" ?" selected ":"") ?> value="5000">5,000</option>
                            </select>
                        </div>
                        <label>Acres</label>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="minlotsize" id="minlotsize">
                                <option <?php echo (isset($_REQUEST['minlotsize']) && $_REQUEST['minlotsize']=="" ?" selected ":"") ?> value="">Any</option>
                                <option <?php echo (isset($_REQUEST['minlotsize']) && $_REQUEST['minlotsize']=="0.01" ?" selected ":"") ?> value="0.01">1/100</option>
                                <option <?php echo (isset($_REQUEST['minlotsize']) && $_REQUEST['minlotsize']=="0.15" ?" selected ":"") ?> value="0.15">1/8</option>
                                <option <?php echo (isset($_REQUEST['minlotsize']) && $_REQUEST['minlotsize']=="0.25" ?" selected ":"") ?> value="0.25">1/4</option>
                                <option <?php echo (isset($_REQUEST['minlotsize']) && $_REQUEST['minlotsize']=="0.5" ?" selected ":"") ?> value="0.5">1/2</option>
                                <option <?php echo (isset($_REQUEST['minlotsize']) && $_REQUEST['minlotsize']=="0.75" ?" selected ":"") ?> value="0.75">3/4</option>
                                <option <?php echo (isset($_REQUEST['minlotsize']) && $_REQUEST['minlotsize']=="1" ?" selected ":"") ?> value="1">1</option>
                                <option <?php echo (isset($_REQUEST['minlotsize']) && $_REQUEST['minlotsize']=="2" ?" selected ":"") ?> value="2">2</option>
                            </select>
                        </div>
                        <div class="searchTo">To</div>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="maxlotsize" id="maxlotsize">
                                <option <?php echo (isset($_REQUEST['maxlotsize']) && $_REQUEST['maxlotsize']=="" ?" selected ":"") ?> value="">Any</option>
                                <option <?php echo (isset($_REQUEST['maxlotsize']) && $_REQUEST['maxlotsize']=="0.01" ?" selected ":"") ?> value="0.01">1/100</option>
                                <option <?php echo (isset($_REQUEST['maxlotsize']) && $_REQUEST['maxlotsize']=="0.15" ?" selected ":"") ?> value="0.15">1/8</option>
                                <option <?php echo (isset($_REQUEST['maxlotsize']) && $_REQUEST['maxlotsize']=="0.25" ?" selected ":"") ?> value="0.25">1/4</option>
                                <option <?php echo (isset($_REQUEST['maxlotsize']) && $_REQUEST['maxlotsize']=="0.5" ?" selected ":"") ?> value="0.5">1/2</option>
                                <option <?php echo (isset($_REQUEST['maxlotsize']) && $_REQUEST['maxlotsize']=="0.75" ?" selected ":"") ?> value="0.75">3/4</option>
                                <option <?php echo (isset($_REQUEST['maxlotsize']) && $_REQUEST['maxlotsize']=="1" ?" selected ":"") ?> value="1">1</option>
                                <option <?php echo (isset($_REQUEST['maxlotsize']) && $_REQUEST['maxlotsize']=="2" ?" selected ":"") ?> value="2">2</option>
                            </select>
                        </div>
                        <label>Stories</label>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="minstories" id="minstories">
                                <option <?php echo (isset($_REQUEST['minstories']) && $_REQUEST['minstories']=="" ?" selected ":"") ?> value="">Any</option>
                                <option <?php echo (isset($_REQUEST['minstories']) && $_REQUEST['minstories']=="1" ?" selected ":"") ?> value="1">1</option>
                                <option <?php echo (isset($_REQUEST['minstories']) && $_REQUEST['minstories']=="2" ?" selected ":"") ?> value="2">2</option>
                                <option <?php echo (isset($_REQUEST['minstories']) && $_REQUEST['minstories']=="3" ?" selected ":"") ?> value="3">3</option>
                                <option <?php echo (isset($_REQUEST['minstories']) && $_REQUEST['minstories']=="4" ?" selected ":"") ?> value="4">4</option>
                                <option <?php echo (isset($_REQUEST['minstories']) && $_REQUEST['minstories']=="5" ?" selected ":"") ?> value="5">5</option>
                            </select>
                        </div>
                        <div class="searchTo">To</div>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="maxstories" id="maxstories">
                                <option <?php echo (isset($_REQUEST['maxstories']) && $_REQUEST['maxstories']=="" ?" selected ":"") ?> value="">Any</option>
                                <option <?php echo (isset($_REQUEST['maxstories']) && $_REQUEST['maxstories']=="1" ?" selected ":"") ?> value="1">1</option>
                                <option <?php echo (isset($_REQUEST['maxstories']) && $_REQUEST['maxstories']=="2" ?" selected ":"") ?> value="2">2</option>
                                <option <?php echo (isset($_REQUEST['maxstories']) && $_REQUEST['maxstories']=="3" ?" selected ":"") ?> value="3">3</option>
                                <option <?php echo (isset($_REQUEST['maxstories']) && $_REQUEST['maxstories']=="4" ?" selected ":"") ?> value="4">4</option>
                                <option <?php echo (isset($_REQUEST['maxstories']) && $_REQUEST['maxstories']=="5" ?" selected ":"") ?> value="5">5</option>
                            </select>
                        </div>
                    </div><!-- end 1/3 -->
                    <div class="col1-3">
                        <label>Year Built</label>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="minyearbuilt" id="minyearbuilt">
                                <option value="">Any</option>
                                <?php for($i=date("Y");$i>=2005;$i--){ 
                                          echo "<option" . (isset($_REQUEST['minyearbuilt']) && $_REQUEST['minyearbuilt']==$i ?" selected ":"") . " value='$i'>$i</option>";
                                      }
                                      for($i=2000;$i>1899;$i-=10){
                                          echo "<option" . (isset($_REQUEST['minyearbuilt']) && $_REQUEST['minyearbuilt']==$i ?" selected ":"") . " value='$i'>$i</option>";
                                      }
                                ?>
                            </select>
                        </div>
                        <div class="searchTo">To</div>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="maxyearbuilt" id="maxyearbuilt">
                                <option value="">Any</option>
                                <?php for($i=date("Y");$i>=2005;$i--){ 
                                          echo "<option" . (isset($_REQUEST['maxyearbuilt']) && $_REQUEST['maxyearbuilt']==$i ?" selected ":"") . " value='$i'>$i</option>";
                                      }
                                      for($i=2000;$i>1899;$i-=10){
                                          echo "<option" . (isset($_REQUEST['maxyearbuilt']) && $_REQUEST['maxyearbuilt']==$i ?" selected ":"") . " value='$i'>$i</option>";
                                      }
                                ?>
                            </select>
                        </div>
                        <label>Parking Spaces</label>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="minparkingSpaces" id="minparkingSpaces">
                                <option <?php echo (isset($_REQUEST['minparkingSpaces']) && $_REQUEST['minparkingSpaces']=="" ?" selected ":"") ?> value="">Any</option>
                                <option <?php echo (isset($_REQUEST['minparkingSpaces']) && $_REQUEST['minparkingSpaces']=="1" ?" selected ":"") ?> value="1">1</option>
                                <option <?php echo (isset($_REQUEST['minparkingSpaces']) && $_REQUEST['minparkingSpaces']=="2" ?" selected ":"") ?> value="2">2</option>
                                <option <?php echo (isset($_REQUEST['minparkingSpaces']) && $_REQUEST['minparkingSpaces']=="3" ?" selected ":"") ?> value="3">3</option>
                                <option <?php echo (isset($_REQUEST['minparkingSpaces']) && $_REQUEST['minparkingSpaces']=="4" ?" selected ":"") ?> value="4">4</option>
                                <option <?php echo (isset($_REQUEST['minparkingSpaces']) && $_REQUEST['minparkingSpaces']=="5" ?" selected ":"") ?> value="5">5</option>
                            </select>
                        </div>
                        <div class="searchTo">To</div>
                        <div class="selectHolder">
                            <select class="dropBtn" onchange="menuStick('ddMore'); doSearch();" name="maxparkingSpaces" id="maxparkingSpaces">
                                <option <?php echo (isset($_REQUEST['maxparkingSpaces']) && $_REQUEST['maxparkingSpaces']=="" ?" selected ":"") ?> value="">Any</option>
                                <option <?php echo (isset($_REQUEST['maxparkingSpaces']) && $_REQUEST['maxparkingSpaces']=="1" ?" selected ":"") ?> value="1">1</option>
                                <option <?php echo (isset($_REQUEST['maxparkingSpaces']) && $_REQUEST['maxparkingSpaces']=="2" ?" selected ":"") ?> value="2">2</option>
                                <option <?php echo (isset($_REQUEST['maxparkingSpaces']) && $_REQUEST['maxparkingSpaces']=="3" ?" selected ":"") ?> value="3">3</option>
                                <option <?php echo (isset($_REQUEST['maxparkingSpaces']) && $_REQUEST['maxparkingSpaces']=="4" ?" selected ":"") ?> value="4">4</option>
                                <option <?php echo (isset($_REQUEST['maxparkingSpaces']) && $_REQUEST['maxparkingSpaces']=="5" ?" selected ":"") ?> value="5">5</option>
                            </select>
                        </div>
                    </div><!-- end 1/3 column -->
                </div>
                <div class="ddMoreFooter">
                    <button class="button right">Show Results</button>
                    <button class="button">Show More Filters</button>
                </div>
            </div>
        </div>
        <div class="resultsFilters">
            <div class="resultsSort">
                <div class="btnSort dropbtn" onclick="showSort()">
                    Sort <i class="fa fa-angle-down"></i>
                </div>
              	<div id="ddSort" class="ddSort dropdownContent">
                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="descprice" ?" checked ":"") ?> type="radio" value="descprice" />Price (Highest)</div>
                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="ascprice" ?" checked ":"") ?> type="radio" value="ascprice" />Price (Lowest)</div>
                     <!--<div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="5" ?" checked ":"") ?> type="radio" />Most Popular</div>-->
                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="descbedrooms" ?" checked ":"") ?> type="radio" value="descbedrooms" />Bedrooms (Most)</div>
                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="descfullBaths" ?" checked ":"") ?> type="radio" value="descfullBaths" />Bathrooms (Most)</div>
                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="desclotsize" ?" checked ":"") ?> type="radio" value=desclotsize />Acreage (Highest)</div>
                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="asclotsize" ?" checked ":"") ?> type="radio" value="asclotsize" />Acreage (Lowest)</div>
                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="descyearbuilt" ?" checked ":"") ?> type="radio" value="descyearbuilt" />Year Built (Newest)</div>
                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="ascyearbuilt" ?" checked ":"") ?> type="radio" value="ascyearbuilt" />Year Built (Oldest)</div>
                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (!isset($_REQUEST['sort']) || (isset($_REQUEST['sort']) && $_REQUEST['sort']=="desccreated") ?" checked ":"") ?> type="radio" value=desccreated />Days on Site (Newest)</div>
                     <div class="sortCheckbox"><input name="sort" onchange="doSearch();" <?php echo (isset($_REQUEST['sort']) && $_REQUEST['sort']=="asccreated" ?" checked ":"") ?> type="radio" value="asccreated"/>Days on Site (Oldest)</div>
                </div>
                <div onclick="changeDisplay('map');" class="btnMap<?php echo (isset($_REQUEST['display']) && $_REQUEST['display']=='map'?' selectedDisplay':'') ?>">
                    Map <i class="fa fa-map-marker"></i>
                </div>
                <div onclick="changeDisplay('photo');" class="btnPhoto<?php echo (isset($_REQUEST['display']) && $_REQUEST['display']=='photo'?' selectedDisplay':'') ?>">
                    Photo <i class="fa fa-photo"></i>
                </div>
                <div onclick="changeDisplay('gallery');" class="btnGallery<?php echo (!isset($_REQUEST['display']) || (isset($_REQUEST['display']) && $_REQUEST['display']=='gallery')?' selectedDisplay':'') ?>">
                    Gallery <i class="fa fa-th"></i>
                </div>
            </div>
            <?php if((!isset($showSave) || (isset($showSave) && $showSave==true)) && is_user_logged_in()):?>
                <button onClick="showSaveSearch();" class="button">Save This Search</button>
            <?php endif;?>
            <div id="divFilters">
                <?php if(isset($_REQUEST["minprice"])) :?>
                    <div class="filter">Price Min: <?php echo $_REQUEST["minprice"]?><div class="remove" onclick="removeFilter('minprice');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["maxprice"])) :?>
                    <div class="filter">Price Max: <?php echo $_REQUEST["maxprice"]?><div class="remove" onclick="removeFilter('maxprice');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["minbedrooms"])) :?>
                    <div class="filter">Bed(s) Min: <?php echo $_REQUEST["minbedrooms"]?><div class="remove" onclick="removeFilter('minbedrooms');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["maxbedrooms"])) :?>
                    <div class="filter">Bed(s) Max: <?php echo $_REQUEST["maxbedrooms"]?><div class="remove" onclick="removeFilter('maxbedrooms');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["minfullBaths"])) :?>
                    <div class="filter">Bath(s) Min: <?php echo $_REQUEST["minfullBaths"]?><div class="remove" onclick="removeFilter('minfullBaths');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["maxfullBaths"])) :?>
                    <div class="filter">Bath(s) Max: <?php echo $_REQUEST["maxfullBaths"]?><div class="remove" onclick="removeFilter('maxfullBaths');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["minsize"])) :?>
                <div class="filter">Square Footage Min: <?php echo $_REQUEST["minsize"]?><div class="remove" onclick="removeFilter('minsize');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["maxsize"])) :?>
                    <div class="filter">Square Footage Max: <?php echo $_REQUEST["maxsize"]?><div class="remove" onclick="removeFilter('maxsize');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["minlotsize"])) :?>
                    <div class="filter">Acres Min: <?php echo $_REQUEST["minlotsize"]?><div class="remove" onclick="removeFilter('minlotsize');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["maxlotsize"])) :?>
                    <div class="filter">Acres Max: <?php echo $_REQUEST["maxlotsize"]?><div class="remove" onclick="removeFilter('maxlotsize');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["minyearbuilt"])) :?>
                <div class="filter">Year Built Min: <?php echo $_REQUEST["minyearbuilt"]?><div class="remove" onclick="removeFilter('minyearbuilt');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["maxyearbuilt"])) :?>
                    <div class="filter">Year Built Max: <?php echo $_REQUEST["maxyearbuilt"]?><div class="remove" onclick="removeFilter('maxyearbuilt');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["minparkingSpaces"])) :?>
                    <div class="filter">Parking Spaces Min: <?php echo $_REQUEST["minparkingSpaces"]?><div class="remove" onclick="removeFilter('minparkingSpaces');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["maxparkingSpaces"])) :?>
                    <div class="filter">Parking Spaces Max: <?php echo $_REQUEST["maxparkingSpaces"]?><div class="remove" onclick="removeFilter('maxparkingSpaces');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["minstories"])) :?>
                    <div class="filter">Stories Min: <?php echo $_REQUEST["minstories"]?><div class="remove" onclick="removeFilter('minstories');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["maxstories"])) :?>
                    <div class="filter">Stories Max: <?php echo $_REQUEST["maxstories"]?><div class="remove" onclick="removeFilter('maxstories');">X</div></div>
                <?php endif;?>
                <?php if(isset($_REQUEST["searchtype"]) && isset($_REQUEST["search"])) :?>
                <div class="filter"><?php echo $_REQUEST["searchtype"]?>: <?php echo $_REQUEST["search"]?><div class="remove" onclick="removeFilter('<?php echo $_REQUEST["searchtype"]?>');">X</div></div>
                <?php endif;?>
                <?php foreach($types as $type){
                          if(isset($type) && $type!="")
                              echo "<div class='filter'>Property Type: " . $type . "<div class='remove' onclick='removeFilter(\"" . $type . "\");'>X</div></div>";
                      }
                ?>
            </div>
        </div>
    </div><!-- end container -->
</div><!-- end search header -->
<script type="text/javascript">
    jQuery(function () {
        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();

        jQuery("#inputSearch").keyup(function () {
            delay(function () {
                doAutoComplete();
            }, 200);
        });

        var mnu = getCookie("openMenu");

        if (mnu != "") {
            jQuery("." + mnu).addClass("show");

            if (mnu == 'ddPrice') {
                jQuery("#maxprice").focus();
                jQuery("#selMinPrice").hide();
                jQuery("#selMaxPrice").show();
            }

            document.cookie = "openMenu=";
        }
    });
    function doAutoComplete() {
        if (jQuery("#searchtype").val() == "Address" || jQuery("#searchtype").val() == "MLSID")
            queryListings("url as redirect,'" + jQuery("#searchtype").val() + "' as section," + jQuery("#searchtype").val() + " as result", jQuery("#searchtype").val().replace(" ", "") + " like '%" + jQuery("#inputSearch").val() + "%'", populateSearch, null, jQuery("#searchtype").val().replace(" ", ""), "10");
        else if (jQuery("#searchtype").val() == "School")
            queryListings("count(*),'" + jQuery("#searchtype").val() + "' as section," + jQuery("#searchtype").val().replace(" ", "") + " as result", "elementaryschool like '%" + jQuery("#inputSearch").val() + "%' or middleschool like '%" + jQuery("#inputSearch").val() + "%' or highschool like '%" + jQuery("#inputSearch").val() + "%'", populateSearch, jQuery("#searchtype").val().replace(" ", "") + ",section", jQuery("#searchtype").val().replace(" ", ""), "10");
        else if(jQuery("#searchtype").val()=="" || jQuery("#searchtype").val()=="all")
            autoComplete(jQuery("#inputSearch").val(), 4, populateSearch);
        else
            queryListings("count(*),'" + jQuery("#searchtype").val() + "' as section," + jQuery("#searchtype").val().replace(" ", "") + " as result", jQuery("#searchtype").val().replace(" ", "") + " like '%" + jQuery("#inputSearch").val() + "%'", populateSearch, jQuery("#searchtype").val().replace(" ", "") + ",section", jQuery("#searchtype").val().replace(" ", ""), "10");
    }
    var lastSection = "";
    function populateSearch(list) {
        jQuery(".ulSearchResults").empty();
        var html = "";

        if(list!=null && list.length>0)
        {
            for(var i=0;i<list.length;i++)
            {
                if (lastSection != list[i].section || i==0) {
                    html += "<li class='resultsHeader'>" + list[i].section + "</span></li>";
                    lastSection = list[i].section;
                }

                if (list[i].redirect == null || list[i].redirect=="")
                    html += "<li class='resultsItem'><a href='javascript:doSearch(null,null,null,\"" + lastSection + "\",\"" + list[i].result + "\");'>" + list[i].result + "</a></li>";
                else
                    html += "<li class='resultsItem'><a href='javascript:redirect(\"" + list[i].redirect + "\");'>" + list[i].result + "</a></li>";
            }
        }
        else
        {
            html += "<li class='resultsHeader'>" + lastSection + "<br/>No Results</span></li>";
        }

        jQuery(".ulSearchResults").append(html);
    }
    function redirect(url)
    {
        window.location = "/property-details/" + url;
    }
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
    function setPrice(cnt,vl)
    {
        if(vl!=0)
            jQuery("#" + cnt).val(vl);
        else
            jQuery("#" + cnt).val("");

        if(cnt=="minprice")
        {
            jQuery("#maxprice").focus();
            jQuery("#selMinPrice").hide();
            jQuery("#selMaxPrice").show();
            menuStick("ddPrice");
            doSearch();
        }
        else
        {
            jQuery("#minprice").focus();
            jQuery("#selMinPrice").show();
            jQuery("#selMaxPrice").hide();
            doSearch();
        }
    }
    function setSearchType(t,label) {
        jQuery("#searchtype").val(t);
        jQuery("#inputSearch").prop("placeholder", label);
    }
    function menuStick(vl) {
        document.cookie = "openMenu=" + vl;
    }
    function showSearchBy() {
        jQuery(".dropdownContent").removeClass("show");
        document.getElementById("dropdown").classList.toggle("show");
  }
    function showPriceFilters() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddPrice").classList.toggle("show");
  }
    function showTypeFilters() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddType").classList.toggle("show");
  }
    function showBedFilters() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddBeds").classList.toggle("show");
  }
    function showBathFilters() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddBaths").classList.toggle("show");
  }
    function showMoreFilters() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddMore").classList.toggle("show");
  }
  	function showSort() {
        jQuery(".dropdownContent").removeClass("show");
    document.getElementById("ddSort").classList.toggle("show");
  }
    function showSearchResults() {
        jQuery(".dropdownContent").removeClass("show");
        document.getElementById("ddSearchResults").classList.toggle("show");
  }
  // Close the dropdown if the user clicks outside of it
  window.onclick = function(event) {
  if (!event.target.matches('.dropBtn')) {

  var dropdowns = document.getElementsByClassName("dropdownContent");
  var i;
  for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
  function showSaveSearch() {
      var filters="";
      var display = "";
      var propType="";
        <?php if(isset($_REQUEST["minprice"]))
        {
            echo "filters+='pba__ListingPrice_pb_min__c:" . $_REQUEST["minprice"] . ";';";
            echo "display+='<div class=\"filter\">Price Min: " . $_REQUEST["minprice"] . "</div>';";
         }
        if(isset($_REQUEST["maxprice"]))
        {
            echo "filters+='pba__ListingPrice_pb_max__c:" . $_REQUEST["maxprice"] . ";';";
            echo "display+='<div class=\"filter\">Price Max: " . $_REQUEST["maxprice"] . "</div>';";
         }
        if(isset($_REQUEST["maxbedrooms"]))
        {
            echo "filters+='pba__Bedrooms_pb_max__c:" . $_REQUEST["maxbedrooms"] . ";';";
            echo "display+='<div class=\"filter\">Bed(s) Max: " . $_REQUEST["maxbedrooms"] . "</div>';";
        }
        if(isset($_REQUEST["minbedrooms"]))
        {
            echo "filters+='pba__Bedrooms_pb_min__c:" . $_REQUEST["minbedrooms"] . ";';";
            echo "display+='<div class=\"filter\">Bed(s) Min: " . $_REQUEST["minbedrooms"] . "</div>';";
        }
        if(isset($_REQUEST["minfullBaths"]))
        {
            echo "filters+='pba__FullBathrooms_pb_min__c:" . $_REQUEST["minfullBaths"] . ";';";
            echo "display+='<div class=\"filter\">Bath(s) Min: " . $_REQUEST["minfullBaths"] . "</div>';";
        }
        if(isset($_REQUEST["maxfullBaths"]))
        {
            echo "filters+='pba__FullBathrooms_pb_max__c:" . $_REQUEST["maxfullBaths"] . ";';";
            echo "display+='<div class=\"filter\">Bath(s) Max: " . $_REQUEST["maxfullBaths"] . "</div>';";
        }
        if(isset($_REQUEST["minsize"]))
        {
            echo "filters+='pba__TotalArea_pb_min__c:" . $_REQUEST["minsize"] . ";';";
            echo "display+='<div class=\"filter\">Size Min: " . $_REQUEST["minsize"] . "</div>';";
        }
        if(isset($_REQUEST["maxsize"]))
        {
            echo "filters+='pba__TotalArea_pb_max__c:" . $_REQUEST["maxsize"] . ";';";
            echo "display+='<div class=\"filter\">Size Max: " . $_REQUEST["maxsize"] . "</div>';";
        }
        if(isset($_REQUEST["minlotsize"]))
        {
            echo "filters+='pba__LotSize_pb_min__c:" . $_REQUEST["minlotsize"] . ";';";
            echo "display+='<div class=\"filter\">Acres Min: " . $_REQUEST["minlotsize"] . "</div>';";
        }
        if(isset($_REQUEST["maxlotsize"]))
        {
            echo "filters+='pba__LotSize_pb_max__c:" . $_REQUEST["maxlotsize"] . ";';";
            echo "display+='<div class=\"filter\">Acres Max: " . $_REQUEST["maxlotsize"] . "</div>';";
        }
        if(isset($_REQUEST["minyearbuilt"]))
        {
            echo "filters+='pba__YearBuilt_pb_min__c:" . $_REQUEST["minyearbuilt"] . ";';";
            echo "display+='<div class=\"filter\">Year Built Min: " . $_REQUEST["minyearbuilt"] . "</div>';";
        }
        if(isset($_REQUEST["maxyearbuilt"]))
        {
            echo "filters+='pba__YearBuilt_pb_min__c:" . $_REQUEST["maxyearbuilt"] . ";';";
            echo "display+='<div class=\"filter\">Year Built Max: " . $_REQUEST["maxyearbuilt"] . "</div>';";
        }
        if(isset($_REQUEST["minparkingSpaces"]))
        {
            echo "filters+='ParkingSpaces__c:" . $_REQUEST["minparkingSpaces"] . ";';";
            echo "display+='<div class=\"filter\">Parking Spaces Min: " . $_REQUEST["minparkingSpaces"] . "</div>';";
        }
        if(isset($_REQUEST["maxparkingSpaces"]))
        {
            echo "filters+='Parking_Spaces_Max__c:" . $_REQUEST["maxparkingSpaces"] . ";';";
            echo "display+='<div class=\"filter\">Parking Spaces Max: " . $_REQUEST["maxparkingSpaces"] . "</div>';";
        }
        if(isset($_REQUEST["minstories"]))
        {
            echo "filters+='Stories_Min__c:" . $_REQUEST["minstories"] . ";';";
            echo "display+='<div class=\"filter\">Stories Min: " . $_REQUEST["minstories"] . "</div>';";
        }
        if(isset($_REQUEST["maxstories"]))
        {
            echo "filters+='Stories_Max__c:" . $_REQUEST["maxstories"] . ";';";
            echo "display+='<div class=\"filter\">Stories Max: " . $_REQUEST["maxstories"] . "</div>';";
        }
        if(isset($_REQUEST["searchtype"]) && isset($_REQUEST["search"]))
        {
            if($_REQUEST["searchtype"]=="City")
                echo "filters+='pba__City_pb__c:" . $_REQUEST["search"] . ";';";
            else if($_REQUEST["searchtype"]=="Zip Code")
                echo "filters+='pba__PostalCode_pb__c:" . $_REQUEST["search"] . ";';";
            else if($_REQUEST["searchtype"]=="area")
                echo "filters+='pba__Area_pb__c:" . $_REQUEST["search"] . ";';";
            else if($_REQUEST["searchtype"]=="Subdivision")
                echo "filters+='Subdivision__c:" . $_REQUEST["search"] . ";';";

            echo "display+='<div class=\"filter\">" . $_REQUEST["searchtype"]. ": " . $_REQUEST["search"] . "</div>';";
        }

        foreach($types as $type){
            if(isset($type) && $type!="")
            echo "propType+='" . $type . ",';";
            echo "display+='<div class=\"filter\">Property Type: " . $type . "</div>';";
        }
        ?>

      if (filters != "" || propType != "")
      {
          if(filters!="")
            filters=filters.substring(0,filters.length-1);

          if (filters != "" && propType != "")
              filters += ";";

          if (propType != "")
          {
              propType = propType.substring(0, propType.length - 1);
              filters += "pba__PropertyType__c:" + propType;
              }

          jQuery("#filters").val(filters);
          jQuery('#saveSearch').modal('show');
      }
      else
          alert("Please specify your search parameters above.");
  }
</script>