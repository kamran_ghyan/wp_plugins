<?php
/*
Plugin Name:i5ListingMap
Plugin URI: http://www.i5fusion.com
Description: Google Map Propertybase Integration
Author: Jarrett Fisher
Version: 1.0
Author URI:http://www.i5fusion.com
 */

define( 'WEBMAP__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
require_once( WEBMAP__PLUGIN_DIR . 'class.i5ListingMap-ShortCode.php' );

wp_register_script( "Prospect_Script", WP_PLUGIN_URL.'/i5ListingsMap/Resources/jquery.number.min.js', array('jquery') );

add_shortcode("i5ListingMap",array( 'i5ListingMap_ShortCode', 'map_shortCode' ));

