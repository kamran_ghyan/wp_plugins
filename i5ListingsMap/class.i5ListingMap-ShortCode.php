<?php
class i5ListingMap_ShortCode
{
  public static function map_shortCode($atts){
    	$vars=shortcode_atts(array(
          'style' => '',
          'apikey' => '',
          'lat' => 38.980731,
          'long' => -107.7936206,
          'zoom' => 10,
          'maxzoom'=>12,
          'minzoom'=>8,
          'showdetails'=>true,
          'showpolygon'=>true
          ),$atts);

        ?>
				<script src="https://maps.google.com/maps/api/js?v=3&key=<?php echo $vars["apikey"]?>&libraries=drawing"></script>
        <script src="<?php echo WP_PLUGIN_URL ?>/i5ListingsMap/Resources/i5ListingMap.js"></script>
				<div style="display:none;" id="divLoading">Loading....</div>
				<div id="divListings" style="<?php echo $vars["style"] ?>">
				</div>
				<script>
          	
            InitializeMap("divListings", <?php echo $vars["zoom"]?>, <?php echo $vars["maxzoom"]?>, <?php echo $vars["minzoom"]?>, <?php echo $vars["lat"]?>,<?php echo $vars["long"]?>,<?php echo $vars["showdetails"]?>,<?php echo $vars["showpolygon"]?>);
        </script>
        <?php
  }
}
